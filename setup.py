#!/usr/bin/env python

"""
setup.py file for the scipack package
    
On Unix like machines, you should type
    
    python setup.py build_ext --inplace
    
On Windows machine, you should type
    
    python setup.py build_ext --inplace --compiler=msvc
"""

import sys
from distutils.core import setup, Extension
import os


#
# If on window, you should put here the directory the wxWidget development archive is
#
if (os.sys.platform =='win32'):
    wxDevDir = 'C:/wxWidgets/2.9.4.0-win32/'


#
# Set the flagSP flag
#   - if True then all scipack us built
#   - if False then just scipack.lab is built
#
if (sys.argv[1] == 'lab'):
    flagSP = False
    sys.argv[1:] = sys.argv[2:]
else: flagSP = True


#
# Getting the flags wor the wx library
#
# For unix based machine we use the command wx-config in order to get information about the install
#

if (flagSP):
    if (os.sys.platform =='win32'):
        #??????????? Y-a-t-il vraiment des includes dans  lib/vc90_dll/mswu ????
        wx_include_dirs = [wxDevDir+'include',wxDevDir+'lib/vc90_dll/mswu']
        wx_extra_compile_args = ['/EHsc','/DWIN32','/MD','/GR','/D_WINDOWS','/D__WXMSW__','/D_UNICODE','/D__WXDEBUG__','/DWXUSINGDLL','/D__NO_VC_CRTDBG']
        library_dirs = [wxDevDir+'lib/vc90_dll']
        libraries = ['wxmsw29u_adv','wxmsw29u_core','wxbase29u','wxzlib','wxtiff','wxpng','wxregexu','wxjpeg','wxexpat','wxmsw29u_aui','wxscintilla','wxmsw29u_propgrid','wxmsw29u_richtext']
    else:
        stream = os.popen("wx-config --cxxflags")
        stream.close
        line = stream.readline()
        line = line.split()
        wx_include_dirs = []
        wx_extra_compile_args = []
        for l in line:
            if (l[0:2] == '-I'): wx_include_dirs += [l[2:]]
            else: wx_extra_compile_args += [l]
        library_dirs = []
        libraries = []



#################################
#
#    The scipack.lab.sig module
#
#################################

#
# Some definitions
#

src_cpp_dir = 'src/lab/sig/'
src_swig_dir = 'src/lab/sig/'

output_py_dir = 'scipack/lab/'
extension_name = 'scipack.lab._sig'

sig_fredholm_files = ['fredholm/fred2.c','fredholm/fredin.c','fredholm/gauleg.c','fredholm/lubksb.c','fredholm/ludcmp.c','fredholm/nrutil.c','fredholm/sp_fredholm.cpp']

sig_cpp_files = ['sig.cpp','rand.cpp','error.cpp','matrix.cpp','nr_linear_algebra.cpp']+sig_fredholm_files
sig_swig_files = ['sig.i']

######

## Pour extra_link_args ci-dessous => C'est pour mettre le .lib "au bon endroit"
## Je ne sais pas si j'ai mis le bon chemin
## Il faudrait peut etre creer un seul repertoire avec toutes les librairies
## Pour mac je ne sais pas trop comment faire pour que cette option ne soit pas prise en compte

if (os.sys.platform =='win32'):
    outlibname = output_py_dir+'_sig.lib'
    extra_link_args = ['/IMPLIB:'+outlibname]
    extra_compile_args = ['/EHsc','/DWIN32','/GR','-D_FILE_OFFSET_BITS=64','-DSP_SCRIPT','-DSWIG_TYPE_TABLE=_wxPython_table']
else:
    extra_link_args = ['']
    extra_compile_args = ['-D_FILE_OFFSET_BITS=64','-DSP_SCRIPT','-DSWIG_TYPE_TABLE=_wxPython_table','-fpermissive']

swig_opts=['-outdir', output_py_dir, '-c++', '-modern', '-new_repr','-Isrc/']


##??????????????????
## Y-a-t-il un moyen pour que le sig.lib soit aussi construit dans le meme directory que le sig.pyd ?
## Le directory de sig.pyd est donne par l'option swig (juste au-dessus) swig_opts=['-outdir', output_py_dir]


#
# Finding the numpy include directory
#
import numpy as np
import os
[dir,file] = os.path.split(np.__file__)
numpy_include =  dir+'/core/include'


#
# The source files for the sig package
#

for i in range(0,len(sig_cpp_files)):
    sig_cpp_files[i] = src_cpp_dir+sig_cpp_files[i]
for i in range(0,len(sig_swig_files)):
    sig_swig_files[i] = src_swig_dir+sig_swig_files[i]


#
# Defining the module
#
sig_module = Extension(extension_name,
                       sources=sig_swig_files+sig_cpp_files,
                       include_dirs = [numpy_include]+ ['src'],
                       extra_compile_args = extra_compile_args,
                       swig_opts = swig_opts,
                       extra_link_args = extra_link_args
                       # depends = sig_cpp_files+sig_h_files
                       )


#################################
#
#    The scipack.sciplot.sp C extension
#
#################################

if (flagSP):
    
    src_cpp_dir = 'src/sciplot/core/'
    src_swig_dir = 'src/sciplot/core/'
    
    output_py_dir = 'scipack/sciplot/'
    extension_name = 'scipack.sciplot._sp'
    
    
    sp_cpp_files = ['axis.cpp', 'box.cpp', 'canvas.cpp', 'comment.cpp', 'container.cpp', 'debug.cpp', 'error.cpp', 'event.cpp', 'formattedText.cpp', 'framedview.cpp', 'grid.cpp', 'group.cpp', 'interactor_cursor.cpp', 'interactor.cpp', 'interactor_msge.cpp', 'object_set.cpp', 'object_smart_ptr.cpp', 'object.cpp', 'print.cpp', 'ref.cpp', 'shape.cpp','text.cpp', 'textbox.cpp', 'topcontainer.cpp', 'utils.cpp','view.cpp']
    sp_swig_files = ['sp.i']
    
    include_dirs =  wx_include_dirs+['src']
    
    if (os.sys.platform =='win32'):
        outlibname = output_py_dir+'_sp.lib'
        extra_link_args1 = ['/IMPLIB:'+outlibname]
        extra_compile_args = wx_extra_compile_args + ['-DWXP_USE_THREAD=1','-DSWIG_TYPE_TABLE=_wxPython_table', '-DSP_SCRIPT']
    else:
        extra_link_args1 = ['']
        extra_compile_args = wx_extra_compile_args + ['-DWXP_USE_THREAD=1','-DSWIG_TYPE_TABLE=_wxPython_table', '-DSP_SCRIPT','-Wno-deprecated','-fpermissive']
    
    swig_opts=['-outdir', output_py_dir, '-c++', '-modern', '-new_repr','-Isrc/']
    
    ##??????????????????
    ## Y-a-t-il un moyen pour que le core.lib soit aussi construit dans le meme directory que le core.pyd ?
    ## Le directory de core.pyd est donne par l'option swig (juste au-dessus) swig_opts=['-outdir', output_py_dir]
    
    ## => Je l'ai fait avec l'option /IMPLIB dans extra_link_args ci-dessous
    ## Par contre je ne sais pas comment l'initialiser sous mac pour qu'elle ne soit pas prise en compte
    
    
    for i in range(0,len(sp_cpp_files)):
        sp_cpp_files[i] = src_cpp_dir+sp_cpp_files[i]
    for i in range(0,len(sp_swig_files)):
        sp_swig_files[i] = src_swig_dir+sp_swig_files[i]
    
    
    sp_module = Extension(extension_name,
                          sources=sp_swig_files+sp_cpp_files,
                          include_dirs = include_dirs,
                          extra_compile_args = extra_compile_args,
                          swig_opts = swig_opts,
                          library_dirs = library_dirs,
                          libraries = libraries,
                          extra_link_args = extra_link_args1
                          )


#################################
#
#    The scipack.sciplot.objects.spsig C extension
#
#################################


if (flagSP):
    
    src_cpp_dir = 'src/sciplot/spsig/'
    src_swig_dir = 'src/sciplot/spsig/'
    
    output_py_dir = 'scipack/sciplot/objects/'
    extension_name = 'scipack.sciplot.objects._spsig'
    
    
    spsig_cpp_files = ['signalobject.cpp']
    spsig_swig_files = ['signalobject.i']
    
    include_dirs =  wx_include_dirs + ['src']
    
    if (os.sys.platform =='win32'):
        extra_compile_args = wx_extra_compile_args + ['-DWXP_USE_THREAD=1','-DSWIG_TYPE_TABLE=_wxPython_table', '-DSP_SCRIPT']
    else:
        extra_compile_args = wx_extra_compile_args + ['-DWXP_USE_THREAD=1','-DSWIG_TYPE_TABLE=_wxPython_table', '-DSP_SCRIPT','-Wno-deprecated','-fpermissive']
    
    swig_opts=['-outdir', output_py_dir, '-c++', '-modern', '-new_repr','-Isrc/']
    
    for i in range(0,len(spsig_cpp_files)):
        spsig_cpp_files[i] = src_cpp_dir+spsig_cpp_files[i]
    for i in range(0,len(spsig_swig_files)):
        spsig_swig_files[i] = src_swig_dir+spsig_swig_files[i]
    
    # On win32 platform we have to add the dynamic libraries
    if (os.sys.platform =='win32'):
        libraries1 = libraries+['_sp','_sig']
        library1_dirs = library_dirs+["scipack/lab","scipack/sciplot"]
    else:
        libraries1 = libraries
        library1_dirs = library_dirs
    
    spsig_module = Extension(extension_name,
                             sources=spsig_swig_files+spsig_cpp_files,
                             include_dirs = include_dirs,
                             extra_compile_args = extra_compile_args,
                             swig_opts = swig_opts,
                             library_dirs = library1_dirs,
                             libraries = libraries1
                             )


#################################
#
#    Building scipack
#
#################################



if (flagSP):
    setup(name = 'scipack',
          version = '0.12',
          author      = "Emmanuel Bacry",
          description = """SciPack Python Library""",
          ext_modules = [sig_module,sp_module,spsig_module]
          #      py_modules = ["scipack.sciplot"],
          #      packages = ["scipack"]
          )

else:
    setup(name = 'scipack',
          version = '0.12',
          author      = "Emmanuel Bacry",
          description = """SciPack Python Library""",
          ext_modules = [sig_module],
          #      py_modules = ["scipack.sciplot"],
          #      packages = ["scipack"]
          )








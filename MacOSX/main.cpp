/*..........................................................................*/
/*                                                                          */
/*      SciPack   P a c k a g e  0.12                                       */
/*                                                                          */
/*      Copyright (C) 2014 Emmanuel Bacry.                                  */
/*      email : emmanuel.bacry@polytechnique.fr                             */
/*                                                                          */
/*..........................................................................*/
/*                                                                          */
/*      This program is a free software, you can redistribute it and/or     */
/*      modify it under the terms of the GNU General Public License as      */
/*      published by the Free Software Foundation; either version 2 of the  */
/*      License, or (at your option) any later version                      */
/*                                                                          */
/*      This program is distributed in the hope that it will be useful,     */
/*      but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*      GNU General Public License for more details.                        */
/*                                                                          */
/*      You should have received a copy of the GNU General Public License   */
/*      along with this program (in a file named COPYRIGHT);                */
/*      if not, write to the Free Software Foundation, Inc.,                */
/*      59 Temple Place, Suite 330, Boston, MA  02111-1307  USA             */
/*                                                                          */
/*..........................................................................*/


#include "sp.h"
#include "sig.h"
#include "signalobject.h"
#include "main.h"


#include "hawkes.h"

// ID for the menu commands
enum
{
	TYPES_QUIT = wxID_EXIT,
	TYPES_ABOUT = wxID_ABOUT,
	
	// clipboard menu
	TEXT_CLIPBOARD_COPY = 200,
	TEXT_CLIPBOARD_PASTE,
};


BEGIN_EVENT_TABLE(MyFrame, wxFrame)
EVT_ACTIVATE(MyFrame::OnActivate)
EVT_MENU(TYPES_QUIT, MyFrame::OnQuit)
EVT_MENU(TYPES_ABOUT, MyFrame::OnAbout)
END_EVENT_TABLE()


//
// Dealing with the main window
//

// Menu : OnQuit
void MyFrame::OnQuit(wxCommandEvent& WXUNUSED(event))
{
  exit(0);
}

// Menu : OnAbout
void MyFrame::OnAbout(wxCommandEvent& WXUNUSED(event) )
{
  wxBeginBusyCursor();
	
  wxMessageDialog dialog(this, _T("LastWave 3.0b\n\n  Copyright (c) 2000-2006, Emmanuel Bacry\n\n"),
                         _T("About LastWave"),
                         wxOK | wxICON_INFORMATION);
	
	dialog.ShowModal();
	wxEndBusyCursor();
}

// Constructor
MyFrame::MyFrame(wxString title,int x, int y, int w,int h) : wxFrame(NULL, -1, title,wxPoint(x,y),wxSize(w,h),wxDEFAULT_FRAME_STYLE | wxWANTS_CHARS)
{
	// Make a menubar
	wxMenu *file_menu = new wxMenu;
	
	file_menu->Append(TYPES_ABOUT, _T("&About"));
	file_menu->AppendSeparator();
	file_menu->Append(TYPES_QUIT, _T("E&xit\tAlt-X"));
	
	wxMenuBar *menu_bar = new wxMenuBar;
	
/*	menu_bar->Append(file_menu, _T("&File"));
	
	wxMenu *menuClipboard = new wxMenu;
	menuClipboard->Append(TEXT_CLIPBOARD_COPY, _T("&Copy\tCtrl-C"),
												_T("Copy the first line to the clipboard"));
	menuClipboard->Append(TEXT_CLIPBOARD_PASTE, _T("&Paste\tCtrl-V"),
												_T("Paste from clipboard to the text control"));
	menu_bar->Append(menuClipboard, _T("&Edit"));
*/
	
	SetMenuBar(menu_bar); 
	CreateStatusBar();
	SetStatusText( _("Welcome to wxWidgets!") );
	
}

// Destructor
MyFrame::~MyFrame()
{
	
}

// On Activate
void MyFrame::OnActivate(wxActivateEvent& event)
{
  if (event.GetActive()) {
		SetFocus();
	}
}


//
//
// Create the new application object
//
//

IMPLEMENT_APP    (MyApp)

IMPLEMENT_DYNAMIC_CLASS    (MyApp, wxApp)

BEGIN_EVENT_TABLE(MyApp, wxApp)
END_EVENT_TABLE()

MyApp *myApp;



#include <wx/image.h>
#include <wx/listctrl.h>
#include <wx/sizer.h>
#include <wx/log.h>
#include <wx/intl.h>
#include <wx/print.h>
#include <wx/filename.h>

//
// Initialization of the App
//

spView *MYVIEW;



bool MyApp::OnInit()
{
	
    
/*	
	spDebugEventsMouse = 1<<0,
  spDebugEventsMotion = 1<<1,
  spDebugGeometryUpdate = 1<<2,
  spDebugCreatingView = 1<<3,
  spDebugDraw = 1<<4,	
  spDebugUpdatePaint = 1<<5,	
	spDebugResize = 1<<6
	spDebugOverlay = (1<<7)
 */
	

//    spSetDebug(spDebugRef);

	myApp = this;
	
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// We first define some useful interactors that can be shared
	//
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// A zoom with the wheel (shift for x alt for y and both for xy)	
	spInteractorZoomWheelView *iZoomWheelView	= new spInteractorZoomWheelView();
	
	// A 2 button zoom with the space bar to unzoom
	spInteractorZoom3View *iZoom3View= new spInteractorZoom3View();
	iZoom3View->SetUnzoom(spOnKeyDown,'a'); //  --> here we change the event for unzooming
	
	// A Zoom using selection with left button
	// Here y is constrained
	spInteractorZoomSelectView *iZoomYView  = new spInteractorZoomSelectView('y',true);	
	
	// A selector zoom (which includes previous zoom)
	spInteractorSelector *iSelZoom = new spInteractorSelector('z');
	iSelZoom->Add(new spInteractorZoom3View());
	iSelZoom->Add(new spInteractorZoomSelectView('y',true));
	iSelZoom->Add(new spInteractorZoomSelectView(' ',true));
	
	// A ScrollX interactor with the wheel (same with 'y' exists)
	spInteractorScrollView *iScrollX = new spInteractorScrollView('x');
	
	// A move object interactor (to add in a view)
	spInteractorMoveView *iMove = new spInteractorMoveView();
	
	// A cross hair cursor interactor selector
	spInteractorSelector *iSelCursor = new spInteractorSelector('c');
	iSelCursor->Add(new spInteractorCursorView(NULL,spPoint(),false));  // A cursor just printing info on the mouse coordinates
	iSelCursor->interactorList.Last()->SetName("Info Cursor");
	iSelCursor->Add(new spInteractorCursorView(new spCursorCrossHair(),spPoint(),false));  // same thing as above + cross hair
	iSelCursor->interactorList.Last()->SetName("Sticky CrossHair Cursor");
	iSelCursor->Add(new spInteractorCursorView(new spCursorCrossHair(),spPoint(),true));   // same thing as above + cross hair following the closest object
	iSelCursor->interactorList.Last()->SetName("CrossHair Cursor");

	
	// A "circle" cursor interactor selector
	spObject *circle = new spShape(NULL,spShapeEllipse,spAnchor(0,0),spMarginPixel(6,6,6,6),spFLAG_OBJECT_OVERLAY_BUFFER);
	circle->SetBrush(*wxTRANSPARENT_BRUSH);
	spInteractorSelector *iSelCCursor = new spInteractorSelector('c');
	iSelCCursor->Add(new spInteractorCursorView(NULL,spPoint(),false));  // A cursor just printing info on the mouse coordinates
	iSelCCursor->interactorList.Last()->SetName("Info Cursor");
	iSelCCursor->Add(new spInteractorCursorView(circle,spPoint(),false));  // same thing as above + circle
	iSelCCursor->interactorList.Last()->SetName("Sticky Circle Cursor");
	iSelCCursor->Add(new spInteractorCursorView(circle,spPoint(),true));   // same thing as above + circle following the closest object
	iSelCCursor->interactorList.Last()->SetName("Circle Cursor");
	
	
	spObject *o;
	int size;
	spRealRect r;
	
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// The first window including a log and below it a sciplot canvas
	//
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////

	MyFrame *myFrame = new MyFrame(_T("Let's Try Sci Plot"),100,100,800,500);
	
	// Adding a log to the window
	wxTextCtrl      *m_log = new wxTextCtrl( myFrame, -1, wxT("This is the log window.\n"), wxPoint(0,0), wxSize(100,100), wxTE_MULTILINE );
	
	// Adding a spCanvas to the window
	MyWindow *myWindow = new MyWindow(myFrame,wxID_ANY,wxPoint(0,0),wxDefaultSize,wxBORDER_NONE);

  // Place one of top of the other	
	wxBoxSizer *topsizer = new wxBoxSizer( wxVERTICAL );	
	topsizer->Add( m_log, 0, wxEXPAND );
	topsizer->Add( myWindow, 1, wxEXPAND );	
	spCanvas *myCanvas = myWindow->canvas;
	
	myFrame->SetAutoLayout( TRUE );
	myFrame->SetSizer( topsizer );
	
	// Show the window and make it the top window
	myFrame->Show(true);
	SetTopWindow(myFrame);
	
	
	/////////////////////////////////
	//
	// Get the topview associated to the canvas
	//
	/////////////////////////////////
	
	spTopContainer *tv = myCanvas->container;
		
    
    // We will have 2 frameViews in the canvas (and use the grid of the topview for that purpose)
	tv->nGrid = 2;
	
	spInteractorLogScaleMenu *iPopupScale = new spInteractorLogScaleMenu(spOnKeyDown,'l');

	//
	// The first framed view
	//
	
	spFramedView *fv1 = new spFramedView(tv,spAnchorGrid(0,0),spMarginGrid(1,1));
    fv1->AddInteractor(spInteractorMoveView::Get());
    fv1->SetBrush(*wxGREY_BRUSH);

    spView *v1 = fv1->GetViewObject();
	v1->SetBrush(*wxGREY_BRUSH);
	v1->AddInteractor(iPopupScale);
	v1->AddInteractor(iMove);
    
	o = new spShape(v1,spShapeInfiniteLine,spAnchor(12,30),spMargin(5,105));
	
    spComment *cm = new spComment(v1,spRealPoint(5,280),_CCHAR("Bonjour ca va ?"));
    spText *mt =  new spText(v1,spAnchor(20,10),spJustifyHLeft,spJustifyVBase);
    mt->SetText("coucou");
    
	r = v1->ComputeMinXYBound();
	v1->SetXYBound(r);
    
	// Adding a signal in v1
	size = 1000;
	SIGNAL sig1 = new Signal(size,sigXYType,sigTypeReal);
	sig1->SetSize(size);
	sig1->Y(0) = 0;
	for (int i = 0;i<size;i++) {
    sig1->SetX(i,(i+1)/100.0);
		sig1->Y(i) = 10*(pow(sig1->X(i),2));
	}	
	spSignalObject *so = new spSignalObject(v1,sig1);	
	so->SetPen(wxColour(0,200,0));
	so->SetBrush(wxColour(0,0,200,10));
//	spSignalAttributeDot att;
//  so->SetAttribute(att);
	
	// Adding interactors
	v1->AddInteractor(iSelZoom);
  v1->AddInteractor(iScrollX);
	fv1->CreateInfoObject();
	v1->AddInteractor(iSelCursor);
	v1->SetName("v1");
	fv1->SetName("fv1");

	
	// Set the minimum bound in the views
	SPUpdate();
    cm->SetAnchor(spRealPoint(0,0));
	SPUpdate();

	r = v1->ComputeMinXYBound();
	v1->SetXYBound(r);
	SPUpdate();
	
	// The following is to set log scales
	// v1->SetLogScaleX();
	// v1->SetLogScaleY();
  // SPUpdate();
	
	//
	// The second framed view
	//	
	spFramedView *fv2 = new spFramedView(tv,spAnchorGrid(0,1),spMarginGrid(1,1));
    fv2->AddInteractor(spInteractorMoveView::Get());
  spView *v2 = fv2->GetViewObject();
	v2->AddInteractor(iPopupScale);
	v2->AddInteractor(iMove);

	// Adding a signal in v2
	size = 1000000;
	SIGNAL sig2 = new Signal(size,sigXYType,sigTypeReal);
	sig2->SetSize(size);
	sig2->SetX(0,0.4);
	sig2->Y(0) = 0.4;
	for (int i = 1;i<size;i++) {
    sig2->SetX(i,3*i/30.0+.4);
		sig2->Y(i) = sig2->Y(i-1)+(rand()/((double)RAND_MAX)-0.5);
	}	
	o = new spSignalObject(v2,sig2);
	o->SetName("signal2");	
	o->SetPen(wxColour(0,200,0));
	o->SetBrush(wxColour(0,0,200,10));

	// Adding some objects in v2	
/*	o=new spShape(v2,spShapeEllipse,spAnchor(100,100),spMargin(10,10,0,0));
	o->SetBrush(wxColour(0,0,200,30));
	o->SetName("fixed");
*/
	v2->AddInteractor(iSelZoom);
    v2->AddInteractor(iScrollX);
	fv2->CreateInfoObject();
	v2->AddInteractor(iSelCCursor);

	
	// Set the minimum bound in the views
	r = v2->ComputeMinXYBound();
	v2->SetXYBound(r);	 
	
 	
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// The second window includes a single sciplot canvas
	//
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	MyFrame *myFrame2 = new MyFrame(_T("Let's Try Sci Plot 2 "),1000,100,800,500);

	// Adding a spCanvas to the window
    MyWindow *myWindow2 = new MyWindow(myFrame2,wxID_ANY,wxPoint(0,0),wxDefaultSize,wxBORDER_NONE);
	spCanvas *myCanvas2 = myWindow2->canvas;
	spTopContainer *tv2 = myCanvas2->container;

	// Show the window and make it the top window
	myFrame2->Show(true);
	
	// Get the framed view and the view
	spFramedView *fv3 = new spFramedView(tv2,spAnchorGrid(0,0),spMarginGrid(1,1));
    fv3->AddInteractor(spInteractorMoveView::Get());
    spView *v3 = fv3->GetViewObject();
	
	// Adding a signal in v3
  size = 35;
	SIGNAL sig3 = new Signal(size,sigXYType,sigTypeReal);
	sig3->SetSize(size);
	sig3->SetX(0,0.4);
	sig3->Y(0) = 0.4;
	for (int i = 1;i<size;i++) {
    sig3->SetX(i,3*i/35.0+.4);
		sig3->Y(i) = sig3->Y(i-1)+(rand()/((double)RAND_MAX)-0.5);
	}
	o = new spSignalObject(NULL,sig3);
	o->SetName("signal3");	
	o->SetPen(wxColour(200,0,0));
	o->SetBrush(wxColour(200,0,0,20));

	o=	new spShape(NULL,spShapeEllipse,spAnchor(1,1),spRealPoint(.2,.2));

	// o=	new spShape(v3,spShapeEllipse,spAnchor(0.3,0.1),spMarginPixel(20,20,20,20));
	o->SetBrush(wxColour(0,200,200,30));
	o->SetBrush(*wxBLACK_BRUSH);
	
	v3->AddInteractor(iPopupScale);

	v3->AddInteractor(iSelZoom);
  v3->AddInteractor(iScrollX);
	v3->AddInteractor(iMove);
	
	// A customized cursor : a text box with a circle !
  spContainer *ct1 = new spContainer(NULL,spAnchor(),spMarginPixel(3000,3000,3000,3000),spFLAG_OBJECT_OVERLAY_BUFFER);
	ct1->SetBrush(*wxTRANSPARENT_BRUSH);
	spText *tb = new spText(ct1,spAnchor(20,10),spJustifyHLeft,spJustifyVBase);
	tb->SetFrameMargin();
	tb->SetBrush(wxColour(200,200,200));
	tb->SetFont(10,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD); 
 
	spObject *circle1 = new spShape(ct1,spShapeEllipse,spAnchor(0,0),spMarginPixel(6,6,6,6));
	circle1->SetBrush(*wxTRANSPARENT_BRUSH);
	circle1->SetPen(*wxBLACK_PEN);
	spInteractorCursorView *iCursor = new spInteractorCursorView(ct1);
	iCursor->SetOutput(tb);
	iCursor->SetFullInfo();
	v3->AddInteractor(iCursor);
	
	
	
	// Set the minimum bound in the views
	r = v3->ComputeMinXYBound();
	v3->SetXYBound(r);

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Synchronizing all the framed views !
	//
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	 spInteractorSynchroZoom *synchro = new spInteractorSynchroZoom(spSynchroZoomX);
	 synchro->AddView(v1);
	 synchro->AddView(v2);
	 synchro->AddView(v3);
	 r = v3->ComputeMinXYBound();
	 v3->SetXYBound(r);
    myCanvas->ToPS("AAA.ps",1.,1.);
  
//    extern SIGNAL HEstim1 (SIGNAL g, SIGNAL lambda, int N, int n, double xMax);

    /*
    Hawkes *h = new Hawkes(1);
    HawkesKernel *k = new HawkesKernelExp(0.1,0.2);
    h->SetKernel(0, 0,k);
    h->SetMu(0,0.05);
    h->Run(1000000);
     */
 
    /*
    SIGNAL p = (h->GetProcess())[0];
    for (int i=1;i<p->Size();i++) p->Y(i) +=  p->Y(i-1);
    SIGNAL law = PointProcessCondLaw(p,p,0.1,40);
    SIGNAL l = new Signal();
    l->SetSize(1);
    l->Y(0) = 0.1;
    SIGNAL phi = HEstim1(law, l, 1, 50, 40);
    o = new spSignalObject(v3,k->GetSignal());
    o = new spSignalObject(v3,phi);
    
	r = v3->ComputeMinXYBound();
	v3->SetXYBound(r);
    
    extern bool TESTPRINT();

//    TESTPRINT();
//	return true;

//    myCanvas->Print("AAA.pdf");
    */
	return true;	
}






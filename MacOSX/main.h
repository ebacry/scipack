/*..........................................................................*/
/*                                                                          */
/*      SciPack   P a c k a g e  0.12                                       */
/*                                                                          */
/*      Copyright (C) 2014 Emmanuel Bacry.                                  */
/*      email : emmanuel.bacry@polytechnique.fr                             */
/*                                                                          */
/*..........................................................................*/
/*                                                                          */
/*      This program is a free software, you can redistribute it and/or     */
/*      modify it under the terms of the GNU General Public License as      */
/*      published by the Free Software Foundation; either version 2 of the  */
/*      License, or (at your option) any later version                      */
/*                                                                          */
/*      This program is distributed in the hope that it will be useful,     */
/*      but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*      GNU General Public License for more details.                        */
/*                                                                          */
/*      You should have received a copy of the GNU General Public License   */
/*      along with this program (in a file named COPYRIGHT);                */
/*      if not, write to the Free Software Foundation, Inc.,                */
/*      59 Temple Place, Suite 330, Boston, MA  02111-1307  USA             */
/*                                                                          */
/*..........................................................................*/

#ifndef _WX_MAIN_H_
#define _WX_MAIN_H_


//
//
// WX headers
//
//


#include "wx/wxprec.h"

// for all others, include the necessary headers (this file is usually all you
// need because it includes almost all "standard" wxWidgets headers)
#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif


//
// Some useful macros for managing different systems
//

#define __CASEMAC case wxOS_MAC_OS : case wxOS_MAC_OSX_DARWIN
#define __CASEWINDOWS case wxOS_WINDOWS_9X : case wxOS_WINDOWS_NT 
#define __CASEUNIX case wxOS_UNIX_LINUX 


//
// Some useful macros for managing Unicode
//

#ifdef UNICODE
#include <wx/string.h>
#include <wx/strconv.h>
//#define WXSTRING2ANSI(str) ((char*) str.mb_str(wxConvUTF8))
#define WXSTRING2ANSI(str) ((char *) ((const char*) str.mb_str()))
#define ANSI2WXSTRING(input) (wxString(input, wxConvUTF8))
#else
#define WXSTRING2ANSI(str) ((char*) str.c_str())
#define ANSI2WXSTRING(input) (wxString(input))
#endif


//
// Define a new application type
//
class MyApp : public wxApp
{
public:
    
    MyApp() {};
    
    bool OnInit();
    int OnExit() {return wxApp::OnExit();}

private:

    DECLARE_DYNAMIC_CLASS(MyApp)
    DECLARE_EVENT_TABLE()
};

DECLARE_APP(MyApp)

extern MyApp *myApp;



//
// The Application will be made of a window class : MyFrame
//

class MyFrame : public wxFrame 
{
  public :
  
	MyFrame(wxString title,int x, int y, int w,int h);
	~MyFrame();
	void OnActivate(wxActivateEvent &);
	void OnQuit(wxCommandEvent& event);
	void OnAbout(wxCommandEvent& event);
	
	DECLARE_EVENT_TABLE()
};

class MyWindow : public wxWindow
{
	public : 
	spCanvas *canvas;
	MyWindow(wxWindow *parent, wxWindowID id, const wxPoint &pos=wxDefaultPosition, const wxSize &size=wxDefaultSize, long style=0, const wxString &name=wxPanelNameStr) : wxWindow(parent,id,pos,size,style,name){
		canvas = new spCanvas(GetId());
	};
	
	~MyWindow() {
		delete canvas;
	}	
};

#endif

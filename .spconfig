
###################################################################
#
#  spInteractors Defaults
# 
###################################################################

from scipack.sciplot.interactors import IHelpAttributesPopupMenu, IHelpInteractorsPopupMenu, IScrollViewWheel, IScrollViewKey, IZoomSelector, ICursorSelector, IZoomWheel,IMove,IXYScale

from scipack.sciplot.objects.interactor_fit import IFitKey

from scipack.sciplot.sp import EventType

#
# Help interactors
#

&IHelpAttributesPopupMenu.eventType=EventType(OnKeyDown,wx.WXK_ESCAPE)
&IHelpInteractorsPopupMenu.eventType=EventType(OnKeyDown,wx.WXK_TAB)

#
# Interactor  for moving objects around
# It can be attached to a single object to be able to move it
# or to a container in order to move all the objects (except unmoveable objects) 
# it contains
#
&IMove.button = (OnDownLeft,ModShift)
&IMove.scrollingSpeed = 50    # Only Used when object is in a view (set it to 0 in order to forbid scrolling)


#
# Interactors for scrolling a view
#

# Using the mouse wheel
&IScrollWheel.direction = 'x'
&IScrollWheel.eventType = OnMouseWheel

# Scrolling using a key
&IScrollViewKey.speed = 5


#
# Select a (rectangle) region in a view (this is used by the zoom)
#
&ISelectRegion.button = (OnDownLeft,ModShift)
&ISelectRegion.pen = 'blue'
&ISelectRegion.brush = wx.Colour(0,0,200,150)



#
# IInteractorSelector : allows to switch between interactors
#
&IInteractorSelector.eventType = 'z'  # The event that triggers the switch
&IInteractorSelector.delay = 600                  # The duration (in ms) the name of the interactor is displayed
&IInteractorSelector.position = "center"          # The position of the name ('center' or 'mouse')


#
# Interactors for Zooming in a view
#

# This is a "3-button" type zoom interactor
&IZoom3View.left = OnDownLeft       # drop what is left of the mouse
&IZoom3View.right = OnDownRight     # drop what is right of the mouse
&IZoom3View.unzoom = ' '            # dezoom

# This is a zoom that uses a selected rectangle
&IZoomSelect.constrained = 'y'      # This can be either 'y', 'x' or ''
&IZoomSelect.button = OnDownLeft
&IZoomSelect.unzoom = ' '


# 
# This is an interactor (inherits from IInteractorSelector) that combines 
#    the "3-button" zoom interactor
#    a selected rectangle zoom interactor with constrained = 'y'
#    a selected rectangle zoom interactor with no constrain (i.e., = '')
# Use the event 'eventType' to select between these 3 interactors
#
&IZoomSelector.eventType = 'z'
&IZoomSelector.position = 'center'              # Position of the selection info ('mouse' or 'center')



&IZoomWheel.eventType = (OnMouseWheel,ModShift)


#
# Interactors for cursor in a view
#

# The basic cursor interactor
&ICursorView.fullInfo = True
&ICursorView.followMouse = True

# A selector cursor that lets you choose between sticky/None sticky and no cursor
&ICursorSelector.eventType = 'c'
&ICursorSelector.position = 'center'

#
# Interactor for changing the x or y scale using a popup menu
#
&IXYScale.eventType = 'l'

#
# Interactor for signal linear fitting
#
&IFitKey.eventType = 'f'

####################################################################
#
#  spObjects Defaults
#
####################################################################

#
# spObject defaults
#

@Object.pen = 'black'
@Object.brush = 'white'
@Object.font = 12

iHelpAttributesPopupMenu = IHelpAttributesPopupMenu().config()
@Object.AddInteractor(iHelpAttributesPopupMenu)

iHelpInteractorsPopupMenu = IHelpInteractorsPopupMenu().config()
@Object.AddInteractor(iHelpInteractorsPopupMenu)


#
# DispWindow defaults
#

@DispWindow.pos = (50,800)
@DispWindow.size = (600,300)

#
# spTopContainer defaults
#
@TopContainer.brush = wx.Colour(150,150,150)

#
# spFramedView defaults
#

# Background of the framedview
@FramedView.brush = wx.Colour(150,150,150)

# An interactor for moving all he objects of a framedview around (labels and titles)
iMoveContainer = IMove().config()
@FramedView.AddInteractor(iMoveContainer)

# Labels attributes
@FramedView:Text(labelx).font = 12
@FramedView:Text(labelx).text = ""
@FramedView:Text(labely).font = 12
@FramedView:Text(labely).text = ""

# Title attributes
@FramedView:Text(title).text = ""
@FramedView:Text(title).fontSize = 15
@FramedView:Text(title).fontWeight = wx.FONTWEIGHT_BOLD

# Axis attributes
@FramedView:AxisView(axis).fontSize = 10
@FramedView:AxisView(axis).pixelMargin = 2
@FramedView:AxisView(axis).flagTicksIn = True
@FramedView:AxisView(axis).flagFrame = True

# Info attributes
@FramedView:TextBox(info).brush = wx.TRANSPARENT_BRUSH

# Create an info text box at the bottom of the framedView for displaying text
# Starting from right margin then down,left and up
@FramedView.margins = [30,60,80,40,0]

# Create an info text box at the bottom of the framedView for displaying text
@FramedView.CreateInfoObject()


#
# Views in FramedViews
#

# background color
@FramedView:View.brush = wx.Colour(200,200,200)

# Adding some interactors
iScrollWheel=IScrollViewWheel().config()
iScrollRightKey = IScrollViewKey(direction="x+",key=wx.WXK_RIGHT,name="scroll_right").config()
iScrollLeftKey = IScrollViewKey(direction="x-",key=wx.WXK_LEFT,name="scroll_left").config()
iScrollUpKey = IScrollViewKey(direction="y+",key=wx.WXK_UP,name="scroll_up").config()
iScrollDownKey = IScrollViewKey(direction="y-",key=wx.WXK_DOWN,name="scroll_down").config()
iZoomSelector = IZoomSelector().config()
iCursorSelector = ICursorSelector().config()
iZoomWheel = IZoomWheel().config()
iFitKey = IFitKey().config()
iXYScale = IXYScale().config()


@FramedView:View.AddInteractor(iScrollWheel)
@FramedView:View.AddInteractor(iScrollRightKey)
@FramedView:View.AddInteractor(iScrollLeftKey)
@FramedView:View.AddInteractor(iScrollUpKey)
@FramedView:View.AddInteractor(iScrollDownKey)
@FramedView:View.AddInteractor(iZoomSelector)
@FramedView:View.AddInteractor(iCursorSelector)
@FramedView:View.AddInteractor(iZoomWheel)
@FramedView:View.AddInteractor(iFitKey)
@FramedView:View.AddInteractor(iXYScale)


#
# Instead of the IZoomSelector, we could have add
#     iZoom3=IZoom3View().config()
#     @FramedView:View.AddInteractor(iZoom3)
# or
#     iZoomSelect = IZoomSelect().config()
#     @FramedView:View.AddInteractor(iZoomSelect)
#

# For being able to move object around in the view, we could add
#     @FramedView:View.AddInteractor(iMoveContainer)


#
# spComment defaults
#

@Comment:Text.brush = wx.Colour(100,100,100)
@Comment:Text.hMode = JUSTIFY_HMIDDLE
@Comment:Text.vMode = JUSTIFY_VMIDDLE
@Comment:Text.frameMargin = RealPoint(6,3)
@Comment:Shape.brush = wx.Colour(100,100,100)


# An interactor for moving the comment
iMove = IMove(flagForParticularObject=True).config()
@Comment:Text.AddInteractor(iMove)


#
# An init to print labels in cursor strings if s aigns
#

#@SignalObject.AddCursorLabel()






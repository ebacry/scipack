

#ifndef _ERROR_H
#define _ERROR_H

#include <string.h>
#include "scipackdefs.h"

#define _CCHAR(x) const_cast<char *>(x)


extern SPEXPORT void Warningf(const char *funcname, const char *format,...);

#define THROW(str) \
throw const_cast<char *>(str)

#define DECLARETHROW throw(const char *)

#endif

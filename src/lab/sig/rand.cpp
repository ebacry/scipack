//
//  rand.cpp
//  sp
//
//  Created by bacry on 01/03/13.
//
//


#define _USE_MATH_DEFINES
#include <math.h>
#include <time.h>

#include "sig.h"
#include "rand.h"



//
//
// UNIFORM NON THREAD SAFE
//
//

# define MBIG 1000000000
# define MSEED 161803398
# define MZ 0
# define FAC (1.0/MBIG)

int Rand::iff=0;
long int Rand::idum = -1;


void Rand::Init(long int idum1)
{
    iff = 0;
    idum = idum1;    
}

double Rand::Uniform(void)
{
    static int inext,inextp;
    static long ma[56];
    register long mj,mk;
    register int i,ii,k;
    
    if (idum <0 || iff == 0){
        iff = 1;
        if (idum < 0) idum = -time(NULL);
        mj = MSEED - ( idum < 0 ? -idum : idum);
        mj %= MBIG;
        ma[55] = mj;
        mk = 1;
        for ( i=1; i<=54; i++){
            ii = ( 21*i) % 55;
            ma[ii] = mk;
            mk = mj - mk;
            if ( mk < MZ ) mk += MBIG;
            mj = ma[ii];
        }
        for ( k=1; k<=4; k++)
            for (i=1; i<=55; i++){
                ma[i] -= ma[1 + (i+30) % 55];
                if ( ma[i] < MZ ) ma[i] += MBIG;
            }
        inext = 0;
        inextp = 31;
        idum = 1;
    }
    if( ++inext == 56 )inext=1;
    if( ++inextp == 56 )inextp=1;
    mj = ma[inext] - ma[inextp];
    if( mj < MZ) mj += MBIG;
    ma[inext] = mj;
    return mj*FAC;
}


//
//
// UNIFORM THREAD SAFE
//
//


void TSRand::Init(long int idum1)
{
    iff = 0;
    idum = idum1;
}

long TSRand::nTSRand = 0;
TSRand::TSRand(long int idum1)
{
    if (idum1 == -1) idum1 = nTSRand;
    
    Init(idum1);
    iset = 0;
    oldm = -1;

    nTSRand++;
}

double TSRand::Uniform()
{
    register long mj,mk;
    register int i,ii,k;
    
    if (idum <0 || iff == 0){
        iff = 1;
        if (idum < 0) idum = -time(NULL);
        mj = MSEED - ( idum < 0 ? -idum : idum);
        mj %= MBIG;
        ma[55] = mj;
        mk = 1;
        for ( i=1; i<=54; i++){
            ii = ( 21*i) % 55;
            ma[ii] = mk;
            mk = mj - mk;
            if ( mk < MZ ) mk += MBIG;
            mj = ma[ii];
        }
        for ( k=1; k<=4; k++)
            for (i=1; i<=55; i++){
                ma[i] -= ma[1 + (i+30) % 55];
                if ( ma[i] < MZ ) ma[i] += MBIG;
            }
        inext = 0;
        inextp = 31;
        idum = 1;
    }
    if( ++inext == 56 )inext=1;
    if( ++inextp == 56 )inextp=1;
    mj = ma[inext] - ma[inextp];
    if( mj < MZ) mj += MBIG;
    ma[inext] = mj;
    return mj*FAC;
}


//
//
// UNIFORM INT NON THREAD SAFE
//
//
int Rand::UniformInt(int a, int b)
{
    return (int) floor(Uniform()*(b-a+1)+a);
}

//
//
// UNIFORM INT THREAD SAFE
//
//
int TSRand::UniformInt(int a, int b)
{
    return (int) floor(Uniform()*(b-a+1)+a);
}


//
//
// GAUSSIAN NON THREAD SAFE
//
//


double Rand::Gaussian(double sigma)
{
    static int iset=0;
    static double gset;
    double fac,rsq,v1,v2;
    
    if (iset == 0) {
        do {
            v1 = 2.0*Uniform()-1.0;
            v2 = 2.0*Uniform()-1.0;
            rsq=v1*v1+v2*v2;
        }
        while (rsq >= 1.0 || rsq == 0.0);
        fac=sqrt(-2.0*log(rsq)/rsq);
        gset = v1*fac;
        iset=1;
        return(v2*fac*sigma);
    }
    else {
        iset = 0;
        return(sigma*gset);
    }
}

//
//
// GAUSSIAN THREAD SAFE
//
//


double TSRand::Gaussian(double sigma)
{
    double fac,rsq,v1,v2;
    
    if (iset == 0) {
        do {
            v1 = 2.0*Uniform()-1.0;
            v2 = 2.0*Uniform()-1.0;
            rsq=v1*v1+v2*v2;
        }
        while (rsq >= 1.0 || rsq == 0.0);
        fac=sqrt(-2.0*log(rsq)/rsq);
        gset = v1*fac;
        iset=1;
        return(v2*fac*sigma);
    }
    else {
        iset = 0;
        return(sigma*gset);
    }
}



//
//
// Poisson : NON THREAD SAFE
//
//


// Useful functions
static double gammln(double xx)
{
    double x,y,tmp,ser;
    static double cof[6]={76.18009172947146,-86.50532032941677,
        24.01409824083091,-1.231739572450155,
        0.1208650973866179e-2,-0.5395239384953e-5};
    int j;
    y=x=xx;
    tmp=x+5.5;
    tmp -= (x+0.5)*log(tmp);
    ser=1.000000000190015;
    for (j=0;j<=5;j++) ser += cof[j]/++y;
    return -tmp+log(2.5066282746310005*ser/x);
}


double Rand::Poisson(double xm)
{
    static double sq,alxm,g,oldm=(-1.0);
    double em,t,y;
    long int intem;
    
    if (xm< 12.0) {
        if (xm != oldm){
            oldm = xm;
            g = exp(-xm);
        }
        em = -1;
        t=1.0;
        do {
            ++em;
            t *= Uniform();
        } while(t>g);
    }
    else {
        if (xm != oldm){
            oldm = xm;
            sq = sqrt(2.0*xm);
            alxm=log(xm);
            g=xm*alxm-gammln(xm+1.0);
        }
        do {
            do {
                y = tan(3.141592654*Uniform());
                em = sq*y+xm;
            } while(em <0.0);
            intem = em;
            em = intem;
            t = 0.9*(1.0+y*y)*exp(em*alxm-gammln(em+1.0)-g);
        } while(Uniform() > t);
    }
    return(em);
}



//
//
// Poisson : THREAD SAFE
//
//


double TSRand::Poisson(double xm)
{
    double em,t,y;
    long int intem;
    
    if (xm< 12.0) {
        if (xm != oldm){
            oldm = xm;
            g = exp(-xm);
        }
        em = -1;
        t=1.0;
        do {
            ++em;
            t *= Uniform();
        } while(t>g);
    }
    else {
        if (xm != oldm){
            oldm = xm;
            sq = sqrt(2.0*xm);
            alxm=log(xm);
            g=xm*alxm-gammln(xm+1.0);
        }
        do {
            do {
                y = tan(3.141592654*Uniform());
                em = sq*y+xm;
            } while(em <0.0);
            intem = em;
            em = intem;
            t = 0.9*(1.0+y*y)*exp(em*alxm-gammln(em+1.0)-g);
        } while(Uniform() > t);
    }
    return(em);
}


//
//
// Stable : NON THREAD SAFE
//
//

static double StableNormalized(double alpha, double beta)
{
	double u = (Rand::Uniform()-.5)*M_PI;
	double w = Rand::Exponential(1);
	double zeta = -beta*tan(M_PI*alpha/2);
	double xi;
	
	if (alpha == 1) {
		xi = M_PI/2;
		return ((M_PI/2+beta*u)*tan(u)-beta*log((M_PI/2)*w*cos(u)/(M_PI/2+beta*u)))/xi;
	}
	else {
		xi = atan(-zeta)/alpha;
		return pow(1+zeta*zeta,1/2*alpha)*(sin(alpha*(u+xi))/pow(cos(u),1/alpha))*pow(cos(u-alpha*(u+xi))/w,(1-alpha)/alpha);
	}
}

double Rand::Stable(double alpha, double sigma,double beta, double mu)
{
	double x = StableNormalized(alpha,beta);
	if (alpha != 1) return sigma*x+mu;
	else return sigma*x + beta*sigma*log(sigma)*2/M_PI + mu;
}


//
//
// Stable : THREAD SAFE
//
//

double TSRand::Stable(double alpha, double sigma,double beta, double mu)
{
	double u = (Uniform()-.5)*M_PI;
	double w = Exponential(1);
	double zeta = -beta*tan(M_PI*alpha/2);
	double xi;
	double x;
    
	if (alpha == 1) {
		xi = M_PI/2;
		x= ((M_PI/2+beta*u)*tan(u)-beta*log((M_PI/2)*w*cos(u)/(M_PI/2+beta*u)))/xi;
	}
	else {
		xi = atan(-zeta)/alpha;
		x= pow(1+zeta*zeta,1/2*alpha)*(sin(alpha*(u+xi))/pow(cos(u),1/alpha))*pow(cos(u-alpha*(u+xi))/w,(1-alpha)/alpha);
	}

	if (alpha != 1) return sigma*x+mu;
	else return sigma*x + beta*sigma*log(sigma)*2/M_PI + mu;
}

//
//
// Exponential : NON THREAD SAFE
//
//

double Rand::Exponential(double intensity)
{
    double r = Uniform();
    return -log(r)/intensity;
}


//
//
// Exponential : THREAD SAFE
//
//

double TSRand::Exponential(double intensity)
{
    double r = Uniform();
    return -log(r)/intensity;
}





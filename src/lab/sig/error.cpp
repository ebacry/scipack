//
//  error.cpp
//  sp
//
//  Created by bacry on 04/03/13.
//
//


#include <stdio.h>
#include <stdarg.h>
#include <iostream>
using namespace std;

#include "error.h"
#include "sig.h"

static char tempStr[10000];
static char header[] = "*** Warning : ";
void SPEXPORT Warningf(const char *funcname, const char *format,...)
{
    va_list ap;
    
    va_start(ap,format);
    
#ifndef SP_SCRIPT
    strcpy(tempStr,header);
    if (funcname) {
        strcat(tempStr,funcname);
        strcat(tempStr," : ");
    }
    vsprintf(tempStr+strlen(tempStr),format,ap);
    va_end(ap);
    cerr << tempStr << endl;
#else
    extern void SCRIPT_Warning(const char *msge);
    vsprintf(tempStr,format,ap);
    va_end(ap);
    SCRIPT_Warning(tempStr);
#endif
}

//
//  matrix.h
//  sp
//
//  Created by bacry on 04/03/13.
//
//

#ifndef __MATRIX__
#define __MATRIX__

#include <stdio.h>
#include <cstring>
#include "error.h"
#include "nr_linear_algebra.h"
#include "sig.h"

class MATRIX;

// Floating matrix structure
class SPEXPORT Matrix
{
    long count;

public:
	// Reference count
	inline long ref() {return count++;};
	inline long getcount() {return count;};
	inline long unref() {
		if (count == 0 || --count == 0 ) {
			delete this;
			return 0;
		}
		return count;
	}

    Matrix(unsigned long rows, unsigned long cols);
    ~Matrix() {
        delete [] data;
    }
    double& operator()(unsigned long i, unsigned long j);
    double operator()(unsigned long i, unsigned long j) const;
    double *GetData() {return data;};
    
    void SetSize(unsigned long nRows, unsigned long nCols);
    
    MATRIX Copy();
    
    MATRIX Inverse(Matrix *out=NULL) DECLARETHROW;
    SIGNAL SymDiagonalize(Matrix *eigenVectors=NULL) DECLARETHROW;
    
    FMATRIX FMatrix() DECLARETHROW;
    void SetFMatrix(FMATRIX m);
    
private:
    unsigned long nRows;
    unsigned long nCols;
    double *data;
};



class SPEXPORT MATRIX 
{
public:
    static double _temp_double;
    
	// The signal it points to
	Matrix *matrix;
	
	public :
	
	// Constructor from a Signal
	MATRIX(Matrix *matrix1=NULL) {
		matrix = matrix1;
		if (matrix) matrix->ref();
	}
    
	// Constructor with copy
	MATRIX(const MATRIX &ptr) {
		matrix = ptr.matrix;
		if (matrix) matrix->ref();
	}
	
    // Destructor
	~MATRIX() {
		if (matrix) matrix->unref();
	}
	
	// assignation
	MATRIX operator=(MATRIX rhs) {
		if (rhs.matrix) rhs.matrix->ref();
            if (matrix) matrix->unref();
                matrix = rhs.matrix;
		return rhs;
	}
    
	// Pointer operator
	inline Matrix * operator ->() {
		return matrix;
	}
	
	// The * unary operator
	Matrix& operator*() {
		return *matrix;
	}
	
	// cast operator to boolean
	operator bool() const {
		return matrix != NULL;
	}
    
    double& operator()(unsigned long i, unsigned long j);
    double operator()(unsigned long i, unsigned long j) const;
	
};


#endif 

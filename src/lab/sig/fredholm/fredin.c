/*
 *  fredin.c
 *  LastWave_XCode
 *
 *  Created by bacry on 23/10/12.
 *  Copyright 2012 __MyCompanyName__. All rights reserved.
 *
 */

#include "nrutil.h"


//
// Original numerical recipee algo for computing f(x) for any x where f is the solution of the 1d fredholm solved in fred2.c
//
// Returns f(x)
//
double fredin(double x, int n, double a, double b, double t[], double f[],
						 double w[], double (*g)(double), double (*ak)(double, double))
{
	int i;
	double sum=0.0;
	
	for (i=1;i<=n;i++) sum += (*ak)(x,t[i])*w[i]*f[i];
	return (*g)(x)+sum;
}


//
// Adaptation to N dimensional
//
// Returns f_i(x)
//
double system_fredin(double x, int i, int N, int n, double a, double b, double t[], double f[],
						 double w[], double (*g)(int,double), double (*ak)(int,int,double, double))
{
	int k,j;
	double sum=0.0;

	for (j=1;j<=N;j++) {
	  for (k=1;k<=n;k++) 
			sum += (*ak)(i,j,x,t[k])*w[k]*f[k+(j-1)*n];
	}
	
	return (*g)(i,x)+sum;
}

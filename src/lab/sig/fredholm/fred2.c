/*
 *  fredholm.c
 *  LastWave_XCode
 *
 *  Created by bacry on 23/10/12.
 *  Copyright 2012 __MyCompanyName__. All rights reserved.
 *
 */


#include "nrutil.h"

//
// 1d original Numerical Recipee function
//
// Solve 
//
//              f(t) = \int_a^b K(t,s)f(s)ds + g(t)
//
// n : number of samples
// a,b : bounds of the integral
// g   : the function
// ak  : the kernel
//
// returns : 
//      t : the sampling points
//      f : the solution
//      w : the quadrature weights
//  
void fred2(int n, double a, double b, double t[], double f[], double w[],
					 double (*g)(double), double (*ak)(double, double))
{
	void gauleg(double x1, double x2, double x[], double w[], int n);
	void lubksb(double **a, int n, int *indx, double b[]);
	void ludcmp(double **a, int n, int *indx, double *d);
	int i,j,*indx;
	double d,**omk;
	
	indx=ivector(1,n);
	omk=matrix(1,n,1,n);
	gauleg(a,b,t,w,n);
	for (i=1;i<=n;i++) {
		for (j=1;j<=n;j++)
			omk[i][j]=(double)(i == j)-(*ak)(t[i],t[j])*w[j];
		f[i]=(*g)(t[i]);
	}
	ludcmp(omk,n,indx,&d);
	lubksb(omk,n,indx,f);
	free_matrix(omk,1,n,1,n);
	free_ivector(indx,1,n);
}


//
// Adaptation of fred2 for N dimensional
//
//
// Solve 
//
//              f_k(t) = \int_a^b K_kl(t,s)f_l(s)ds + g_k(t)
//
// where k varies between 1 and N
//
// Quadrature is used for computing the integral : The t[] will be the same for all the i and the weights w[] too
//
// the solutions f_k will be concatenated in f[]
//

void system_fred2(int N, int n, double a, double b, double t[], double gg[], double w[],
					 double (*g)(int,double), double (*ak)(int,int,double, double))
{
	void gauleg(double x1, double x2, double x[], double w[], int n);
	void lubksb(double **a, int n, int *indx, double b[]);
	void ludcmp(double **a, int n, int *indx, double *d);
	int i,j,*indx,k,l;
	double d,**omk;
	
  // Here we have to build the matrix and the vector
	indx=ivector(1,n*N);
	omk=matrix(1,n*N,1,n*N);
	
	
	// We prepare the quadrature
	gauleg(a,b,t,w,n);

	// We fill up the equation corresponding to K_kl
	for (k = 1;k<=N;k++) {
		for (i=1;i<=n;i++) {
		  for (l = 1;l<=N;l++) {
		    for (j=1;j<=n;j++)
			    omk[i+(k-1)*n][j+(l-1)*n]=(double)(i == j && k==l)-(*ak)(k,l,t[i],t[j])*w[j];
			}
			gg[i+(k-1)*n]=(*g)(k,t[i]);
	  }
	}
	ludcmp(omk,n*N,indx,&d);
	lubksb(omk,n*N,indx,gg);
  free_matrix(omk,1,n*N,1,n*N);
	free_ivector(indx,1,n*N);
}















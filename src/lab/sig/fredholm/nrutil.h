


extern void nrerror(char error_text[]);
extern double *vector(int nl,int nh);
extern int *ivector(int nl,int nh);
extern double *dvector(int nl,int nh);
extern double **matrix(int nrl,int nrh,int ncl,int nch);
extern double **dmatrix(int nrl,int nrh,int ncl,int nch);
extern int **imatrix(int nrl,int nrh,int ncl,int nch);
extern double **submatrix(double **a,int oldrl,int oldrh,int oldcl,int oldch,int newrl,int newcl);
extern void free_vector(double *v,int nl,int nh);
extern void free_ivector(int *v,int nl,int nh);
extern void free_dvector(double *v,int nl,int nh);
extern void free_matrix(double **m,int nrl,int nrh,int ncl,int nch);
extern void free_dmatrix(double **m,int nrl,int nrh,int ncl,int nch);
extern void free_imatrix(int **m,int nrl,int nrh,int ncl,int nch);
extern void free_submatrix(double **b,int nrl,int nrh,int ncl,int nch);
extern double **convert_matrix(double *a,int nrl,int nrh,int ncl,int nch);
extern void free_convert_matrix(double **b,int nrl,int nrh,int ncl,int nch);

//
//  fredholm.h
//  SciPack
//
//  Created by bacry on 09/04/13.
//  Copyright (c) 2013 bacry. All rights reserved.
//

#ifndef SciPack_fredholm_h
#define SciPack_fredholm_h


extern double *FredholmSolver(int N, int n, double a, double b, double (*G)(int,double), double (*K)(int,int,double, double),SIGNAL quad_x, SIGNAL quad_w);
extern double *FredholmInterpolation(double dx,SIGNAL x, int N, double a, double b, double (*G)(int,double), double (*K)(int,int,double, double), SIGNAL quad_x, SIGNAL quad_w,double *solution);


extern "C" void gauleg(double x1,double x2,double x[],double w[], int n);


#endif

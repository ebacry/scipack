//  SciPack
//
//  Created by bacry on 24/04/13.
//  Copyright (c) 2013 bacry. All rights reserved.
//


#include "sig.h"
#include "nr_linear_algebra.h"

#include <iostream>
using namespace std;

#ifdef SP_SCRIPT
extern void SCRIPT_Warning(const char *msge);
extern void SCRIPT_DECREF(void * ref);
extern void SCRIPT_INCREF(void * ref);
extern void *SCRIPT_MALLOC(size_t);
extern void SCRIPT_FREE(void * ref);
#endif


double Signal::_tempDouble = 0;
Complex Signal::_tempComplex = 0;

double Signal::FactorIncrAlloc = SIG_Factor_IncrAlloc;

// Clear
void Signal::ClearY()
{
	if (Yarray) {
#ifdef SP_SCRIPT
		if (Yowner) {
		  SCRIPT_DECREF(Yowner);
			Yowner = NULL;
		}
		else {
			SCRIPT_FREE(Yarray);
		}
#else
		if (flagComplex) delete [] ((Complex *) Yarray);
		else delete [] ((double *) Yarray);
#endif
        Yarray=NULL;
        sizeMallocY = 0;
    }
#ifdef SP_SCRIPT
	Yowner = NULL;
#endif
	
	size = 0;
}


// Clear
void Signal::ClearX()
{
	if (Xarray) {
#ifdef SP_SCRIPT
		if (Xowner) {
			SCRIPT_DECREF(Xowner);
			Xowner = NULL;
		}
		else SCRIPT_FREE(Xarray);
#else
		delete [] Xarray;
#endif
        Xarray=NULL;
		sizeMallocX = 0;
    }
#ifdef SP_SCRIPT
	Xowner = NULL;
#endif
}

#ifdef SP_SCRIPT
void Signal::SetYowner(void *array, void *ptr, unsigned long size1)
{
	ClearY();
	Yowner = array;
	SCRIPT_INCREF(array);
	Yarray = ptr;
	size = size1;
}

void Signal::GiveYownership(void *array)
{
            Yowner = array;
	SCRIPT_INCREF(array);

}
void Signal::GiveXownership(void *array)
{
	Xowner = array;
	SCRIPT_INCREF(array);
}
#endif

// Destructor
Signal::~Signal()
{
	ClearX();
	ClearY();
//    cout << "Delete signal" << name << endl;
}

#ifdef SP_SCRIPT
void *AllocValues(unsigned long size,int sigValueType)
{
	switch (sigValueType) {
		case sigTypeComplex:
			return SCRIPT_MALLOC(sizeof(Complex)*size);
			break;
		case sigTypeReal:
			return SCRIPT_MALLOC(sizeof(double)*size);
			break;
		default:
			return NULL;
	}
}
#else
void *AllocValues(unsigned long size,int sigValueType)
{
	switch (sigValueType) {
		case sigTypeComplex:
            return new Complex[size];
			break;
		case sigTypeReal:
			return new double[size];
			break;
		default:
			return NULL;
	}
}
#endif

void Signal::SetSize(unsigned long newSize, bool flagRemember, sigXY_Type sigXYType, sigTypeValue type)
{
	if (type == sigTypeUnknown) type = GetValueType();
	if (sigXYType == sigXYTypeUnknown) sigXYType = GetXYType();
	
    unsigned long mysize = size;
    
	//
	// Managing the Y array
	//
	
	// Will we need to clear the Yarray ?
	bool flagYClear = false;
	if (type != GetValueType()) flagYClear = true;
	if (type == GetValueType() && newSize > sizeMallocY) flagYClear = true;
	
	// Case we need to clear the Yarray
	if (flagYClear) {
		long unsigned allocSize;
		if (newSize == 0) allocSize = 10;
		else if (flagRemember && newSize > mysize) allocSize = newSize*FactorIncrAlloc;
		else allocSize = newSize;
		void * ptr = AllocValues(allocSize,type);
		if (newSize != 0 && flagRemember) {
			switch(type) {
                case sigTypeReal:
					if (type == GetValueType()) memcpy(ptr,Yarray,sizeof(double)*MIN(mysize,newSize));
					else  for (unsigned long i=0;i<MIN(mysize,newSize);i++) ((double *) ptr)[i] = real(((Complex *) Yarray)[i]);
					break;
                case sigTypeComplex:
					if (type == GetValueType()) memcpy(ptr,Yarray,sizeof(Complex)*MIN(mysize,newSize));
					else for (unsigned long i=0;i<MIN(mysize,newSize);i++) {
                        ((Complex *) ptr)[i] = Complex(((double *) Yarray)[i],0);
					}
					break;
                default: break;
			}
		}
		ClearY();
		Yarray = ptr;
		sizeMallocY = allocSize;
	}
	
	// Case we do not need to clear the Y array
	else {
		// Do nothing !
	}
	
	//
	// Managing the X array
	//
	
	// Case a Y signal is asked
	if (sigXYType == sigYType) {
		ClearX();
	}
	
	// Case a XY signal is asked
	else {
		// If no Xarray --> allocation
		if (!IsXY()) {
			long unsigned allocSize;
			if (newSize == 0) allocSize = 10;
			else allocSize = newSize;
			Xarray = (double *) AllocValues(allocSize,sigTypeReal);
			sizeMallocX = allocSize;
		}
		
		// Will we need to clear the Xarray ?
		bool flagXClear = false;
		if (newSize > sizeMallocX) flagXClear = true;
		
		// Case we need to clear the Xarray
		if (flagXClear) {
			long unsigned allocSize;
			if (newSize == 0) allocSize = 10;
			else if (flagRemember && newSize > mysize) allocSize = newSize*SIG_Factor_IncrAlloc;
			else allocSize = newSize;
			double * ptr = (double *) AllocValues(allocSize,sigTypeReal);
			if (flagRemember) memcpy(ptr,Xarray,sizeof(double)*MIN(mysize,newSize));
			ClearX();
			Xarray = ptr;
			sizeMallocX = allocSize;
		}
	}
	
	// We are done
	flagComplex = (type == sigTypeComplex);
	size = newSize;
}


Signal::Signal(unsigned long allocSize,sigXY_Type sigXY_Type,sigTypeValue type)
{
    name = "";
    interMode = sigInterLinear;
    Xarray = NULL;
    Yarray = NULL;
    sizeMallocX = sizeMallocY = 0;
    size = 0;
    x0 = 0;
    dx = 1;
	count = 0;
	
#ifdef SP_SCRIPT
	Xowner = NULL;
	Yowner = NULL;
#endif
	
    size = 0;
	flagComplex = (type == sigTypeUnknown ? false : (type == sigTypeReal ? false : true));
	
	size = 0;
	if (allocSize != 0) SetSize(allocSize,false,sigXY_Type,type);
}

// Append two signals
void Signal::Append(Signal *sig)
{
	long oldSize = size;
	
    SetSize(size+sig->Size(),true,NewXYType(sig),NewValueType(sig));
	
	if (IsXY()) {
        if (sig->IsXY()) memcpy(Xarray+oldSize,sig->_Xarray(),sig->Size()*sizeof(double));
        else {
            for (unsigned long i=0;i<sig->Size();i++) {
                SetX(i+oldSize,sig->X(i));
            }
        }
	}
	
	if (IsComplex())
        if (sig->IsComplex()) memcpy(((Complex*) Yarray)+oldSize,sig->_Ycarray(),sig->Size()*sizeof(Complex));
        else {
            for (unsigned long i=0;i<sig->Size();i++) {
                SetY(i+oldSize,sig->Yc(i));
            }
        }
	else
		memcpy(((double*) Yarray)+oldSize,sig->_Yarray(),sig->Size()*sizeof(double));
}

// Append a with a double
void Signal::Append(double dbl)
{
	long oldSize = size;
    SetSize(size+1,true);
    SetY(oldSize,dbl);
}

// Append a point
void Signal::Append(double x, double y)
{
	unsigned long oldSize = size;
    if (size == 0) SetSize(1,true,sigXYType);
    else SetSize(size+1,true);
	
	SetY(oldSize,y);    
    SetX(oldSize,x);
}

// Append a with a complex
void Signal::Append(Complex cpl)
{
	long oldSize = size;
    SetSize(size+1,true,sigXYTypeUnknown,sigTypeComplex);
	
	SetY(oldSize,cpl);
}

// Append a point
void Signal::Append(double x, Complex cpl)
{
	long oldSize = size;
    if (size == 0) SetSize(1,true,sigXYType,sigTypeComplex);
    else SetSize(size+1,true,sigXYTypeUnknown,sigTypeComplex);
	
	SetY(oldSize,cpl);
	SetX(oldSize,x);
}


// To_string
#define SIG_TOSTR_MAXLENGTH 2000
#define YSigPrintLength 6
#define XYSigPrintLength 3
const char *LWDoubleFormat = "%.16g";
char *Signal::ToStr(bool flagShort)
{
    static char strShort[50],strShort1[50];
	static char str[SIG_TOSTR_MAXLENGTH];
    unsigned long n;
    
	//
	// Empty signal case
	//
    if (size == 0) {
        sprintf(strShort,"<size=%ld>",size);
        return(strShort);
    }
	
	//
	// Short case
	//
    if (flagShort) {
        if (!IsXY()) sprintf(strShort,"<size=%ld>",size);
        else sprintf(strShort,"<XY,size=%ld>",size);
        return(strShort);
    }
	
	//
	// Long case
	//
	
	// Y case
    if (!IsXY()) {
        if (size < YSigPrintLength) sprintf(str,"<");
        else {
            sprintf(str,"<size=%ld",size);
            if (dx == 1 && x0 == 0) strcat(str,";");
            else strcat(str,",");
        }
        if (dx != 1 || x0 != 0) {
            sprintf(strShort,"x0=%g,dx=%g;",x0,dx);
            strcat(str,strShort);
        }
        for (n=0;n<MIN(YSigPrintLength,size);n++) {
			if (!IsComplex() || imag(Yc(n)) == 0) sprintf(strShort,LWDoubleFormat,Y(n));
			else if (real(Yc(n)) == 0) {
				sprintf(strShort,LWDoubleFormat,imag(Yc(n)));
				strcat(str,strShort);
                sprintf(strShort,"j");
			}
			else {
                sprintf(strShort,LWDoubleFormat,real(Yc(n)));
				strcat(str,strShort);
                sprintf(strShort,"%+g",imag(Yc(n)));
				strcat(str,strShort);
                sprintf(strShort,"j");
			}
            strcat(str,strShort);
            if (n!=MIN(YSigPrintLength,size)-1) strcat(str,",");
        }
        if (size > YSigPrintLength) strcat(str,",...>");
        else strcat(str,">");
        return(str);
    }
	
	// XY case
    else {
        if (size < XYSigPrintLength) sprintf(str,"<");
        else sprintf(str,"<size=%ld;",size);
        for (n=0;n<MIN(XYSigPrintLength,size);n++) {
            sprintf(strShort,"(");
            sprintf(strShort1,LWDoubleFormat,X(n));
            strcat(strShort,strShort1);
            strcat(strShort,"/");
			if (!IsComplex() || imag(Yc(n)) == 0) sprintf(strShort1,LWDoubleFormat,Y(n));
			else if (real(Yc(n)) == 0) {
				sprintf(strShort1,LWDoubleFormat,imag(Yc(n)));
				strcat(strShort,strShort1);
                sprintf(strShort1,"j");
			}
			else {
                sprintf(strShort1,LWDoubleFormat,real(Yc(n)));
				strcat(strShort,strShort1);
                sprintf(strShort1,"%+g",imag(Yc(n)));
				strcat(strShort,strShort1);
                sprintf(strShort1,"j");
			}
            strcat(strShort,strShort1);
            strcat(str,strShort);
            strcat(str,")");
            if (n!=MIN(XYSigPrintLength,size)-1) strcat(str,",");
        }
        if (size > XYSigPrintLength) strcat(str,",...>");
        else strcat(str,">");
        return(str);
    }
}

//
// Various operations
// Throw errors
//
/*
 Signal *Signal::Sqrt(Signal *res) DECLARETHROW
 {
 if (res == NULL) res = this;
 res->SetSize(size,res==this,GetXYType(),GetValueType());
 
 switch(res->GetValueType()) {
 case sigTypeReal:
 for (unsigned long i=0;i<size;i++) {
 if (Y(i)<0) NPY_INFINITY;
 res->Y(i) = sqrt(Y(i));
 }
 break;
 case sigTypeComplex:
 break;
 }
 return res;
 }
 
 Signal *Signal::Log(Signal *res) DECLARETHROW
 {
 if (res == NULL) res = this;
 res->SetSize(size,res==this,GetXYType(),GetValueType());
 
 switch(res->GetValueType()) {
 case sigTypeReal:
 for (unsigned long i=0;i<size;i++) {
 if (Y(i)<=0) THROW("Signal::Log : trying log a negative number");
 res->Y(i) = log(Y(i));
 }
 break;
 case sigTypeComplex:
 break;
 }
 return res;
 }
 */


// Setting X using the X of another signal
void Signal::SetXArray(Signal *sig)
{
    if (this == sig) return;
	if (!sig->IsXY()) {
        if (IsXY()) ClearX();
		dx = sig->Dx();
		x0 = sig->X0();
		return;
	}
	
	if (sizeMallocX < sig->Size()) SetSize(sig->Size(), true, sigXYType);
	
	memcpy(Xarray,sig->_Xarray(),sizeof(double)*sig->Size());
}


Signal *Signal::Conj()
{
    if (!IsComplex()) return this;
	Signal *sig = new Signal();
	sig->SetSize(Size(), false, GetXYType(), sigTypeComplex);
	for (unsigned long i = 0;i<Size();i++) sig->SetY(i,conj(Yc(i)));
	sig->SetXArray(this);
	return sig;
}

Signal *Signal::Real()
{
    if (!IsComplex()) return this;
	Signal *sig = new Signal();
	sig->SetSize(Size(), false, GetXYType(), sigTypeReal);
	for (unsigned long i = 0;i<Size();i++) sig->SetY(i,real(Yc(i)));
	sig->SetXArray(this);
	return sig;
}
Signal *Signal::Imag()
{
	Signal *sig = new Signal();
	sig->SetSize(Size(), false, GetXYType(), sigTypeReal);
	for (unsigned long i = 0;i<Size();i++) sig->SetY(i,imag(Yc(i)));
	sig->SetXArray(this);
	return sig;
}
Signal *Signal::Phase(bool flagRad)
{
	Signal *sig = new Signal();
	sig->SetSize(Size(), false, GetXYType(), sigTypeReal);
	for (unsigned long i = 0;i<Size();i++) sig->SetY(i,arg(Yc(i)));
	sig->SetXArray(this);
	return sig;
}
Signal *Signal::Modulus()
{
	Signal *sig = new Signal();
	sig->SetSize(Size(), false, GetXYType(), sigTypeReal);
    if (IsComplex())
        for (unsigned long i = 0;i<Size();i++) sig->SetY(i , sqrt(real(Yc(i))*real(Yc(i))+imag(Yc(i))*imag(Yc(i))));
    else
        for (unsigned long i = 0;i<Size();i++) sig->SetY(i , sqrt(real(Yc(i))*real(Yc(i))+imag(Yc(i))*imag(Yc(i))));
	sig->SetXArray(this);
	return sig;
}



////////////////////////////////////////////////////////
//
// ADDITION
//
////////////////////////////////////////////////////////

Signal *Signal::Add(Signal *sig,Signal *res) DECLARETHROW
{
	if (sig->size != size) {
		THROW("Signal::Add : trying to add 2 signals of different size");
		return NULL;
	}
	
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),NewValueType(sig));
	if (res != this) res->SetXArray(this);
	
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)+sig->Y(i);
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, Yc(i)+sig->Yc(i));
			break;
		case sigTypeUnknown: break;
	}
    return res;
}

Signal *Signal::Add(Complex c,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),sigTypeComplex);
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->SetY(i, Yc(i)+c);
    return res;
}

Signal *Signal::Add(double dbl,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)+dbl;
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, Yc(i)+dbl);
			break;
		case sigTypeUnknown: break;
	}
    return res;
}

////////////////////////////////////////////////////////
//
// SUBSTRACTION
//
////////////////////////////////////////////////////////

Signal *Signal::Sub(Signal *sig,Signal *res) DECLARETHROW
{
	if (sig->size != size) {
		THROW("Signal::Sub : trying to add 2 signals of different size");
		return NULL;
	}
	
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),NewValueType(sig));
	if (res != this) res->SetXArray(this);
	
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)-sig->Y(i);
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, Yc(i)-sig->Yc(i));
			break;
		case sigTypeUnknown: break;
	}
    return res;
}

Signal *Signal::Sub(Complex c,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),sigTypeComplex);
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->SetY(i , Yc(i)-c);
    return res;
}

Signal *Signal::Sub(double dbl,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)-dbl;
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, Yc(i)-dbl);
			break;
		case sigTypeUnknown: break;
	}
    return res;
}

Signal *Signal::RSub(Complex c,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),sigTypeComplex);
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->SetY(i , -1.0*Yc(i)+c);
    return res;
}

Signal *Signal::RSub(double dbl,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = -Y(i)+dbl;
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, -1.0*Yc(i)+dbl);
			break;
		case sigTypeUnknown: break;
	}
    return res;
}

////////////////////////////////////////////////////////
//
// MULTIPLICATION
//
////////////////////////////////////////////////////////

Signal *Signal::Mul(Signal *sig,Signal *res) DECLARETHROW
{
	if (sig->size != size) {
		THROW("Signal::Mul : trying to multiply 2 signals of different size");
		return NULL;
	}
	
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),NewValueType(sig));
	if (res != this) res->SetXArray(this);
	
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)*sig->Y(i);
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, Yc(i)*sig->Yc(i));
			break;
		case sigTypeUnknown: break;
	}
    return res;
}

Signal *Signal::Mul(Complex c,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),sigTypeComplex);
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->SetY(i,Yc(i)*c);
    return res;
}

Signal *Signal::Mul(double dbl,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)*dbl;
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, Yc(i)*dbl);
			break;
		case sigTypeUnknown: break;
	}
    return res;
}

////////////////////////////////////////////////////////
//
// DIVISION
//
////////////////////////////////////////////////////////

Signal *Signal::Div(Signal *sig,Signal *res) DECLARETHROW
{
	if (sig->size != size) {
		THROW("Signal::Div : trying to divide 2 signals of different size");
		return NULL;
	}
	
	bool flagDivByZero = false;
	
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),NewValueType(sig));
	if (res != this) res->SetXArray(this);
	
	double d;
	Complex c;
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = ((d = sig->Y(i)) == 0 ? flagDivByZero=true,Y(i)/d : Y(i)/d);
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, ((c=sig->Yc(i)) == 0. ? flagDivByZero=true,Yc(i)/c : Yc(i)/c));
			break;
		case sigTypeUnknown: break;
	}
	
	if (flagDivByZero) Warningf("","Division by zero");
	
    return res;
}

Signal *Signal::Div(Complex c,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),sigTypeComplex);
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->SetY(i, Yc(i)/c);
	if (c==0.) Warningf("","Division by zero");
    return res;
}

Signal *Signal::Div(double dbl,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)/dbl;
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, Yc(i)/dbl);
			break;
		case sigTypeUnknown: break;
	}
	if (dbl==0) Warningf("","Division by zero");
    return res;
}

Signal *Signal::RDiv(Complex c,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),sigTypeComplex);
	if (res != this) res->SetXArray(this);
	
	bool flagDivByZero = false;
	Complex c1;
	for (unsigned long i=0;i<size;i++) res->SetY(i, ((c1=Yc(i))==0. ? flagDivByZero=true,c/c1 : c/c1));
	if (flagDivByZero) Warningf("","Division by zero");
    return res;
}

Signal *Signal::RDiv(double dbl,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	bool flagDivByZero = false;
	
	double d;
	Complex c;
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = ((d = Y(i)) == 0 ? flagDivByZero=true,dbl/d : dbl/d);
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, ((c=Yc(i)) == 0. ? flagDivByZero=true,dbl/c : dbl/c));
			break;
		case sigTypeUnknown: break;
	}
    return res;
}


////////////////////////////////////////////////////////
//
// FLOOR DIVISION
//
////////////////////////////////////////////////////////

Signal *Signal::FDiv(Signal *sig,Signal *res) DECLARETHROW
{
	if (sig->size != size) {
		THROW("Signal::FDiv : trying to divide 2 signals of different size");
		if(IsComplex() || sig->IsComplex()) THROW("Signal::FDiv : operates on real signals only");
		return NULL;
	}
	
	bool flagDivByZero = false;
	
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),NewValueType(sig));
	if (res != this) res->SetXArray(this);
	
	double d;
	for (unsigned long i=0;i<size;i++) res->Y(i) = ((d = sig->Y(i)) == 0 ? flagDivByZero=true,floor(Y(i)/d) : floor(Y(i)/d));
	
	if (flagDivByZero) Warningf("","Division by zero");
	
	return res;
}

Signal *Signal::FDiv(double dbl,Signal *res) DECLARETHROW
{
	if (res == NULL) res = new Signal();
	if(IsComplex()) THROW("Signal::FDiv : operates on real signals only");
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = floor(Y(i)/dbl);
	
	if (dbl==0) Warningf("","Division by zero");
	return res;
}

Signal *Signal::RFDiv(double dbl,Signal *res) DECLARETHROW
{
	if (res == NULL) res = new Signal();
	if(IsComplex()) THROW("Signal::FDiv : operates on real signals only");
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	bool flagDivByZero = false;
	
	double d;
	for (unsigned long i=0;i<size;i++) res->Y(i) = ((d = Y(i)) == 0 ? flagDivByZero=true,floor(dbl/d) : floor(dbl/d));
	
	if (flagDivByZero) Warningf("","Division by zero");
	return res;
}


////////////////////////////////////////////////////////
//
// MOD
//
////////////////////////////////////////////////////////

Signal *Signal::Mod(Signal *sig,Signal *res) DECLARETHROW
{
	if (sig->size != size) {
		THROW("Signal::Mod : trying to take mod on 2 signals of different size");
		if(IsComplex() || sig->IsComplex()) THROW("Signal::Mod : operates on real signals only");
		return NULL;
	}
	
	bool flagDivByZero = false;
	
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),NewValueType(sig));
	if (res != this) res->SetXArray(this);
	
	double d;
	for (unsigned long i=0;i<size;i++) res->Y(i) = ((d = sig->Y(i)) == 0 ? flagDivByZero=true,Y(i)-(floor(Y(i)/d))*d : Y(i)-(floor(Y(i)/d))*d);
	
	if (flagDivByZero) Warningf("","Division by zero");
	
	return res;
}

Signal *Signal::Mod(double dbl,Signal *res) DECLARETHROW
{
	if (res == NULL) res = new Signal();
	if(IsComplex()) THROW("Signal::Mod : operates on real signals only");
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)-floor(Y(i)/dbl)*dbl;
	
	if (dbl==0) Warningf("","Division by zero");
	return res;
}

Signal *Signal::RMod(double dbl,Signal *res) DECLARETHROW
{
	if (res == NULL) res = new Signal();
	if(IsComplex()) THROW("Signal::Mod : operates on real signals only");
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	bool flagDivByZero = false;
	
	double d;
	for (unsigned long i=0;i<size;i++) res->Y(i) = ((d = Y(i)) == 0 ? flagDivByZero=true,dbl-floor(dbl/d)*d : dbl-floor(dbl/d)*d);
	
	if (flagDivByZero) Warningf("","Division by zero");
	return res;
}



////////////////////////////////////////////////////////
//
// Power
//
////////////////////////////////////////////////////////

Signal *Signal::Pow(Signal *sig,Signal *res) DECLARETHROW
{
	if (sig->size != size) {
		THROW("Signal::Pow : trying to divide 2 signals of different size");
		return NULL;
	}
	
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),NewValueType(sig));
	if (res != this) res->SetXArray(this);
	
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = pow(Y(i),sig->Y(i));
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, pow(Yc(i),sig->Yc(i)));
			break;
		case sigTypeUnknown: break;
	}
	
    return res;
}

Signal *Signal::Pow(Complex c,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),sigTypeComplex);
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->SetY(i,pow(Yc(i),c));
    return res;
}

Signal *Signal::Pow(double dbl,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = pow(Y(i),dbl);
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, pow(Yc(i),dbl));
			break;
		case sigTypeUnknown: break;
	}
    return res;
}

Signal *Signal::RPow(Complex c,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),sigTypeComplex);
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->SetY(i, pow(c,Yc(i)));
    return res;
}

Signal *Signal::RPow(double dbl,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = pow(dbl,Y(i));
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, pow(dbl,Yc(i)));
			break;
		case sigTypeUnknown: break;
	}
    return res;
}


////////////////////////////////////////////////////////
//
// Tests
//
////////////////////////////////////////////////////////

Signal *Signal::Less(Signal *sig,Signal *res) DECLARETHROW
{
	if (sig->size != size) {
		THROW("Signal::Less : trying to take mod on 2 signals of different size");
		if(IsComplex() || sig->IsComplex()) THROW("Signal::Less : operates on real signals only");
		return NULL;
	}
	
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),NewValueType(sig));
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)<sig->Y(i);
	
	return res;
}

Signal *Signal::Less(double dbl,Signal *res) DECLARETHROW
{
	if (res == NULL) res = new Signal();
	if(IsComplex()) THROW("Signal::Less : operates on real signals only");
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)<dbl;
	
	return res;
}

Signal *Signal::RLess(double dbl,Signal *res) DECLARETHROW
{
	if (res == NULL) res = new Signal();
	if(IsComplex()) THROW("Signal::Less : operates on real signals only");
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = dbl<Y(i);
	
	return res;
}


Signal *Signal::LessEq(Signal *sig,Signal *res) DECLARETHROW
{
	if (sig->size != size) {
		THROW("Signal::LessEq : trying to take mod on 2 signals of different size");
		if(IsComplex() || sig->IsComplex()) THROW("Signal::LessEq : operates on real signals only");
		return NULL;
	}
	
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),NewValueType(sig));
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)<=sig->Y(i);
	
	return res;
}

Signal *Signal::LessEq(double dbl,Signal *res) DECLARETHROW
{
	if (res == NULL) res = new Signal();
	if(IsComplex()) THROW("Signal::LessEq : operates on real signals only");
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)<=dbl;
	
	return res;
}

Signal *Signal::RLessEq(double dbl,Signal *res) DECLARETHROW
{
	if (res == NULL) res = new Signal();
	if(IsComplex()) THROW("Signal::LessEq : operates on real signals only");
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = dbl<=Y(i);
	
	return res;
}


Signal *Signal::Greater(Signal *sig,Signal *res) DECLARETHROW
{
	if (sig->size != size) {
		THROW("Signal::Greater : trying to take mod on 2 signals of different size");
		if(IsComplex() || sig->IsComplex()) THROW("Signal::Greater : operates on real signals only");
		return NULL;
	}
	
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),NewValueType(sig));
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)>sig->Y(i);
	
	return res;
}

Signal *Signal::Greater(double dbl,Signal *res) DECLARETHROW
{
	if (res == NULL) res = new Signal();
	if(IsComplex()) THROW("Signal::Greater : operates on real signals only");
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)>dbl;
	
	return res;
}

Signal *Signal::RGreater(double dbl,Signal *res) DECLARETHROW
{
	if (res == NULL) res = new Signal();
	if(IsComplex()) THROW("Signal::Greater : operates on real signals only");
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = dbl>Y(i);
	
	return res;
}


Signal *Signal::GreaterEq(Signal *sig,Signal *res) DECLARETHROW
{
	if (sig->size != size) {
		THROW("Signal::GreaterEq : trying to take mod on 2 signals of different size");
		if(IsComplex() || sig->IsComplex()) THROW("Signal::Greater : operates on real signals only");
		return NULL;
	}
	
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),NewValueType(sig));
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)>=sig->Y(i);
	
	return res;
}

Signal *Signal::GreaterEq(double dbl,Signal *res) DECLARETHROW
{
	if (res == NULL) res = new Signal();
	if(IsComplex()) THROW("Signal::GreaterEq : operates on real signals only");
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)>=dbl;
	
	return res;
}

Signal *Signal::RGreaterEq(double dbl,Signal *res) DECLARETHROW
{
	if (res == NULL) res = new Signal();
	if(IsComplex()) THROW("Signal::GreaterEq : operates on real signals only");
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = dbl>=Y(i);
	
	return res;
}



Signal *Signal::Eq(Signal *sig,Signal *res) DECLARETHROW
{
	if (sig->size != size) {
		THROW("Signal::Eq : trying to compare 2 signals of different size");
		return NULL;
	}
	
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),NewValueType(sig));
	if (res != this) res->SetXArray(this);
	
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)==sig->Y(i);
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, (Yc(i)==sig->Yc(i)));
			break;
		case sigTypeUnknown: break;
	}
    return res;
}

Signal *Signal::Eq(Complex c,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),sigTypeComplex);
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->SetY(i,(Yc(i)==c));
    return res;
}

Signal *Signal::Eq(double dbl,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)==dbl;
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, (Yc(i)==dbl));
			break;
		case sigTypeUnknown: break;
	}
    return res;
}


Signal *Signal::NEq(Signal *sig,Signal *res) DECLARETHROW
{
	if (sig->size != size) {
		THROW("Signal::Eq : trying to compare 2 signals of different size");
		return NULL;
	}
	
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),NewValueType(sig));
	if (res != this) res->SetXArray(this);
	
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)!=sig->Y(i);
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i,(Yc(i)!=sig->Yc(i)));
			break;
		case sigTypeUnknown: break;
	}
    return res;
}

Signal *Signal::NEq(Complex c,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),sigTypeComplex);
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->SetY(i, (Yc(i)!=c));
    return res;
}

Signal *Signal::NEq(double dbl,Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i)!=dbl;
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, (Yc(i)!=dbl));
			break;
		case sigTypeUnknown: break;
	}
    return res;
}

Signal *Signal::Neg(Signal *res)
{
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType());
	if (res != this) res->SetXArray(this);
	
	switch(res->GetValueType()) {
		case sigTypeReal:
			for (unsigned long i=0;i<size;i++) res->Y(i) = -Y(i);
			break;
		case sigTypeComplex:
			for (unsigned long i=0;i<size;i++) res->SetY(i, -1.*Yc(i));
			break;
		case sigTypeUnknown: break;
	}
    return res;
}

Signal *Signal::And(Signal *sig,Signal *res) DECLARETHROW
{
	if (sig->size != size) {
		THROW("Signal::And : trying to and 2 signals of different size");
		return NULL;
	}
	if(IsComplex() || sig->IsComplex()) THROW("Signal::And : operates on real signals only");
    
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),NewValueType(sig));
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i) && sig->Y(i);
    return res;
}

Signal *Signal::And(double dbl,Signal *res)
{
	if(IsComplex()) THROW("Signal::And : operates on real signals only");
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i) && dbl;
    return res;
}

Signal *Signal::Or(Signal *sig,Signal *res) DECLARETHROW
{
	if (sig->size != size) {
		THROW("Signal::And : trying to and 2 signals of different size");
		return NULL;
	}
	if(IsComplex() || sig->IsComplex()) THROW("Signal::And : operates on real signals only");
	
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),NewValueType(sig));
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i) || sig->Y(i);
    return res;
}

Signal *Signal::Or(double dbl,Signal *res)
{
	if(IsComplex()) THROW("Signal::Or : operates on real signals only");
	if (res == NULL) res = new Signal();
	res->SetSize(size,res==this,GetXYType(),GetValueType());
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = Y(i) || dbl;
    return res;
}


Signal *Signal::Abs(Signal *res) DECLARETHROW
{
	if (res == NULL) res = new Signal();
	if(IsComplex()) THROW("Signal::Abs : operates on real signals only");
	res->SetSize(size,res==this,GetXYType());
	if (res != this) res->SetXArray(this);
	
	for (unsigned long i=0;i<size;i++) res->Y(i) = fabs(Y(i));
    return res;
}

/****************************************/
/*    Sort a signal according to X           */
/****************************************/

static void SortArrays(double *x, Complex *y, int n)
{
	int l,j,ir,i;
	double xx;
	Complex yy;
	
	l = ( n>>1)+1;
	ir = n;
	for (;;) {
		if(l>1) {
			xx = x[--l-1];
			if (y != NULL) yy = y[l-1];
		}
		else {
			xx = x[ir-1];
			x[ir-1] = x[0];
			if (y != NULL) {
				yy = y[ir-1];
				y[ir-1] = y[0];
			}
			if(--ir ==1) {
				x[0] = xx;
				if (y != NULL) y[0] = yy;
				return;
			}
		}
		i=l;
		j=l<<1;
		while( j<= ir) {
			if( j<ir && x[j-1]< x[j]) ++j;
			if(xx < x[j-1]) {
				x[i-1] = x[j-1];
				if (y != NULL) y[i-1] = y[j-1];
				j+=(i=j);
			}
			else j=ir+1;
		}
		x[i-1] = xx;
		if (y != NULL) y[i-1] = yy;
	}
}

static void SortArrays(double *x, double *y, int n)
{
	int l,j,ir,i;
	double xx,yy;
	
	l = ( n>>1)+1;
	ir = n;
	for (;;) {
		if(l>1) {
			xx = x[--l-1];
			if (y != NULL) yy = y[l-1];
		}
		else {
			xx = x[ir-1];
			x[ir-1] = x[0];
			if (y != NULL) {
				yy = y[ir-1];
				y[ir-1] = y[0];
			}
			if(--ir ==1) {
				x[0] = xx;
				if (y != NULL) y[0] = yy;
				return;
			}
		}
		i=l;
		j=l<<1;
		while( j<= ir) {
			if( j<ir && x[j-1]< x[j]) ++j;
			if(xx < x[j-1]) {
				x[i-1] = x[j-1];
				if (y != NULL) y[i-1] = y[j-1];
				j+=(i=j);
			}
			else j=ir+1;
		}
		x[i-1] = xx;
		if (y != NULL) y[i-1] = yy;
	}
}

// Sort a signal according to X
void Signal::XSort()
{
	if (size == 0 || size == 1) return;
	if (!IsXY()) return;
	
	unsigned long n;
	
	// Do we need to sort ? */
	for (n=0;n<size-1;n++) if (X(n) > X(n+1)) break;
	if (n == size-1) return;
	
	// Sort
	if (IsComplex()) SortArrays(Xarray,(Complex *) Yarray,size);
	else SortArrays(Xarray,(double *) Yarray,size);
}


/* Only called by the DichX routine */
long int Signal::DichX_(double x,int iMin,int iMax)
{
	int i;
	
	if (iMin == iMax) return(iMin);
	
	if (iMax-iMin == 1) {
		if (X(iMax) == x) return(iMax);
		return(iMin);
	}
	
	i = (iMax+iMin)/2;
	if (X(i) > x)  return(DichX_(x,iMin,i));
	return(DichX_(x,i,iMax));
}

/*
 * Get the index associated to a x-value within a signal
 * (Only for XYSIG signal)
 * returns -1 if x is too low
 */
long int Signal::DichX(double x)
{
	if (Size() == 0) return -1;
	
	if (X(0) > x) return(-1);
	
	if (Size() == 1) return(0);
	
	if (X(1) > x) return(0);
	
	if (Size() == 2) return(1);
	
	if (X(Size()-1) <= x) return(Size()-1);
	
	
	return(DichX_(x,0,Size()-1));
}

/*
 * The main routine that can be called from outside to get an index
 * associated to an x value ('xValue') whatever the type of the signal is.
 * This routines behaves differently depending on the type of signal :
 *   - YSIG  : the index is a double number that can be out of range and that
 *             is computed only using the dx and x0 fields of the signal
 *   - XYSIG : the index is an integer that can be out of range (-1)
 *             it is the  index i such that x[i] <= xvalue <  x[i+1]
 */
double Signal::X2FIndex(double xValue)
{
	if (IsXY()) return(DichX(xValue));
	return((xValue-x0)/dx);
}


/*
 * The main routine that returns a Y value given an x value
 * given an interpolation mode and a border type.
 * If flagindex is YES then x corresponds to a double index
 * otherwise, it is an x-value
 */
Complex Signal::X2Yc(double x, sigInterMode im, BorderType bt, bool flagIndex) DECLARETHROW
{
	unsigned long i;
	double index;
	
	
	if (flagIndex) index = x;
	else index = X2FIndex(x);
	
    
	/*
	 * We first deal with the border type
	 */
	
	switch (GetXYType()) {
			
			/* case of a YSignal */
		case sigYType :
			
			/* Manage border effects */
			switch (bt) {
				case sigBorderNone :
					break;
				case sigBorderPer :
					index = fmod(index,Size());
					if (index < 0) index+=Size();
					break;
				case sigBorderMir1 :
					index = fmod(index,2*Size());
					if (index < 0) index+=2*Size();
					if (index>=Size()) index = 2*Size()-1-index;
					break;
				case sigBorderMir :
					index = fmod(index,2*Size()-2);
					if (index < 0) index+=2*Size()-2;
					if (index>=Size()) index = 2*Size()-2-index;
					break;
				case sigBorderCon :
					if (index < 0) return(Yc(0));
					if (index>=Size()) return(Yc(Size()-1));
					break;
				case sigBorder0 :
					if (index < 0 || index>=Size()) return(0);
					break;
					
				default : ;
			}
			break;
			
			/* case of a XYSignal */
		case sigXYType :
			
			/* Manage border effects */
			switch (bt) {
				case sigBorderNone :
					break;
				case sigBorderCon :
					if (index < 0) return(Yc(0));
					if (index>=Size()) return(Yc(Size()-1));
					break;
				case sigBorder0 :
					if (index < 0 || index>=Size()) return(0);
					break;
					
				default : ;
			}
			break;
            
        case sigXYTypeUnknown: break;
			
	}
	
	/*
	 * Then the Y value
	 */
	
    double ii = floor(index);
	i = (unsigned long) ii;
	if (ii <0 || ii > Size()-1) THROW("X2YSig() : Weird 1");

	if (flagIndex) return Yc(i);
    
    if (im == sigInterNone) im = interMode;
    

	switch(im) {
		case sigInterDirac : if ((!flagIndex && X(i) == x) || flagIndex) return(Yc(i)); else return 0;
		case sigInterConstRight : return(Yc(i));
		case sigInterConstLeft : if (i > 0) return(Yc(i-1)); else return (Yc(0));
		case sigInterNone: case sigInterLinear :
			if (i == Size()-1) return(Yc(i));
			if (i+1> Size()-1) THROW("X2YSig() : Weird 2");
			switch (GetXYType()) {
				case sigYType : return((Yc(i+1)-Yc(i))*(index-i)+Yc(i));
				case sigXYType :
					if (X(i+1)==X(i)) return(Yc(i));
					return((Yc(i+1)-Yc(i))*(x-X(i))/(X(i+1)-X(i))+Yc(i));
                default:;
			}
		default : return(0);
	}
}

/* Returns the index corresponding to an xValue (always in range [0,signal->size-1]) */
unsigned long Signal::X2Index(double xValue)
{
	long i;
	
	if (IsXY()) i = DichX(xValue);
	else i = (int) ((xValue-x0)/dx);
	
	if (i < 0) i = 0;
	else if ((unsigned long) i >= Size()) i = Size()-1;
	
//    if (X(i)>=xValue && X(i+1)<=value)
	return((unsigned long) i);
}

/* Return the xValue to an index */
double Signal::Index2X(long int index)
{
	if (IsXY())
		return(X(index));
	else
		return(x0+dx*index);
}

/* Return either the x or the y value corresponding to an index */
/*
 double XYSig(SIGNAL signal,int index,char which)
 {
 switch(which) {
 case 'Y': 
 return(signal->Y[index]);
 break;
 case 'X':
 return(XSig(signal,index));
 break;
 default:
 Errorf("Bad value of 'which' in 'XYSig' function");
 }
 } 
 
 */

void Signal::SetFVector(FVECTOR v)
{
    SetSize(v->size);
    memcpy(Yarray,v->data+1,v->size*sizeof(double));
}



//
// We want to compute E((Y[t+delta]-Y[t])*(Z[t+delta+lag]-Z[t+lag]))
//
// on t in [m,M[ using N points
//
SIGNAL CovPointProcess(SIGNAL y, SIGNAL z, double delta, double m, double M, long N, int oversampling, bool flagM)
{
    double mY = 0;
    double mZ = 0;
    double vY = 0;
    double vZ = 0;
    
	// The result
    SIGNAL res = new Signal();
    res->SetSize(N);
    for (unsigned long i=0;i<res->Size();i++) res->Y(i) = 0;
	res->SetDx((M-m)/N);
	res->SetX0(m);
        
	// The loop on the lags
	for(unsigned long i=0;i<(unsigned long)N;i++) {
		double lag = i*res->Dx()+res->X0();
		
		// The absissa on each signal --> first point
		double y_abs =  y->Index2X(0);
		double z_abs = y_abs+lag;
		if (z_abs< z->Index2X(0)) {
			z_abs = z->Index2X(0);
			y_abs = z_abs-lag;
		}
		
		// The corresponding indices
		int y_index,z_index;
		y_index = y->X2Index(y_abs);
		z_index = z->X2Index(z_abs);
		
		// Loop
		unsigned long n = 0;
		while (1) {
			
			// End of loop ?
			if (y_index == y->Size()-1 || z_index == z->Size()-1) break;
			
			int y_index_last = y_index;
			while (y_index_last+1<=y->Size()-1 && y->Index2X(y_index_last+1)<=y_abs+delta) y_index_last++;
			
			int z_index_last = z_index;
			while (z_index_last+1<=z->Size()-1 && z->Index2X(z_index_last+1)<=z_abs+delta) z_index_last++;
			
			res->Y(i) += (y->Y(y_index_last)-y->Y(y_index))*(z->Y(z_index_last)-z->Y(z_index));
			if (flagM == 1) {
                double rY = y->Y(y_index_last)-y->Y(y_index);
				mY += rY;
                vY += rY*rY;
                double rZ = (z->Y(z_index_last)-z->Y(z_index));
				mZ += rZ;
                vZ += rZ*rZ;
			}
			n+= 1;
			
			y_abs += delta/oversampling;
			z_abs += delta/oversampling;
			
			while (y_index+1<=y->Size()-1 && y->Index2X(y_index+1)<=y_abs) y_index++;
			while (z_index+1<=z->Size()-1 && z->Index2X(z_index+1)<=z_abs) z_index++;
		}
		
		res->Y(i)/=n;
		mY /= n;
		mZ /= n;
		res->Y(i) -= mY*mZ;
	}
	
    return res;
}



/* A correlation function */
SIGNAL Corr(SIGNAL in1, SIGNAL in2, double dxmin, double dxmax, bool flagNormalized) DECLARETHROW
{
    long imin,imax,i,di;
    long jmin,jmax,j;
    long first1,last1;
    long first2,last2;
    long N;
    double mean1,mean2,var1,var2;
    
    SIGNAL out = new Signal();
    
    if (in1->IsComplex() || in2->IsComplex()) THROW("Corr() : Sorry the signals must be real valued signals");
    if (in1->IsXY() || in2->IsXY()) THROW("Corr() : Sorry the signals must be Y signals");
    if (in1->Dx() != in2->Dx()) THROW("Corr() : Sorry the signals must have the same dx");
    
    imin = (long) (dxmin/in1->Dx());
    imax = (long) (dxmax/in1->Dx());
    di = (long) ((in2->X0()-in1->X0())/in1->Dx());
    
    out->SetSize(imax-imin+1);
    out->SetDx(in1->Dx());
    out->SetX0(imin*out->Dx());
    for (long k =0;k<out->Size();k++) out->Y(k) = 0;
    
    if (imin > imax) THROW("Corr() : Sorry, dxmin should be smaller than dxmax");
    
    first1 = 0;
    last1 = in1->Size()-1;
    first2 = 0;
    last2 = in2->Size()-1;
    
    N = MIN(last1-first1+1,last2-first2+1);
    
    mean1 = 0;
    for (i = first1; i<=last1;i++) {
        mean1 += in1->Y(i);
    }
    mean1 /= last1-first1+1;
    //  mean1 = 0;
    var1 = 0;
    for (i = first1; i<=last1;i++) {
        var1 += (in1->Y(i)-mean1)*(in1->Y(i)-mean1);
    }
    var1 /= last1-first1;
    
    mean2 = 0;
    for (i = first2; i<=last2;i++) {
        mean2 += in2->Y(i);
    }
    mean2 /= last2-first2+1;
    //  mean2 = 0;
    var2 = 0;
    for (i = first2; i<=last2;i++) {
        var2 += (in2->Y(i)-mean2)*(in2->Y(i)-mean2);
    }
    var2 /= last2-first2;
    
    if (!flagNormalized) var1 = var2 = 1;
    
    for (i = imin; i<= imax;i++) {
        
        jmin = MAX(first1,first2+i+di);
        jmax = MIN(last1,last2+i+di);
        
        for (j=jmin;j<=jmax;j++) out->Y(i-imin) += (in1->Y(j)-mean1)*(in2->Y(j-i-di)-mean2);
        
        /* NOT CONSISTENT!    out->Y[i-imin] /= (jmax-jmin+1); */
        
        out->Y(i-imin) /= N;
        if (flagNormalized) out->Y(i-imin) /= sqrt(var1*var2);
        
    }
    
    return(out);
}





//
//  nr_linear_algebra.cpp
//  sp
//
//  Created by bacry on 04/03/13.
//
//

#define _USE_MATH_DEFINES
#include <math.h>
#include "error.h"
#include "nr_linear_algebra.h"


IVECTOR NewIVector(unsigned long size)
{
    IVECTOR ov;
    
    ov = new struct ivector;
    
    ov->size = size;
    ov->data = new int[size]-1;
    
    return(ov);
}

/* Delete a IVECTOR Structure */
void DeleteIVector(IVECTOR ov)
{
    if (ov)
    {
        delete [] (ov->data+1);
        delete ov;
    }
}

FVECTOR NewFVector(unsigned long size)
{
    FVECTOR ov;
    
    ov = new struct fvector;
    
    ov->size = size;
    ov->data = new double[size]-1;
    
    return(ov);
}

void DeleteFVector(FVECTOR ov)
{
    if (ov)
    {
        delete [] (ov->data+1);
        delete ov;
    }
}


void DeleteFMatrix(FMATRIX m)
{
    unsigned long int i;
    
    if (m) {
        for(i=1;i<=m->size;i++) delete [] (m->data[i]+1);
        delete [] (m->data+1);
    }
    
    delete m;
}


FMATRIX NewFMatrix(unsigned long size)
{
    FMATRIX cm = new struct fmatrix;
    cm->size = size;

    cm->data = (new double*[size])-1;
    for(unsigned long i=1;i<=size;i++) cm->data[i] = new double[size]-1;

    return cm;
}


/*
 * Math routines
 */

/* Matrix Inversion using LU decomposition */
#define TINY 1.0e-20;
static void ludcmp(FMATRIX a,IVECTOR indx,double *d) DECLARETHROW
{
    
    int n = a->size;
    int i,imax,j,k;
    double big,dum,sum,temp;
    FVECTOR vv;
    
    vv=NewFVector(n);
    *d=1.0;
    for (i=1;i<=n;i++) {
        big=0.0;
        for (j=1;j<=n;j++)
            if ((temp=fabs(a->data[i][j])) > big) big=temp;
        if (big == 0.0) {
            DeleteFVector(vv);
            THROW("Sorry ludcmp of matrix failed");
        }
        vv->data[i]=1.0/big;
    }
    for (j=1;j<=n;j++) {
        for (i=1;i<j;i++) {
            sum=a->data[i][j];
            for (k=1;k<i;k++) sum -= a->data[i][k]*a->data[k][j];
            a->data[i][j]=sum;
        }
        big=0.0;
        for (i=j;i<=n;i++) {
            sum=a->data[i][j];
            for (k=1;k<j;k++)
                sum -= a->data[i][k]*a->data[k][j];
            a->data[i][j]=sum;
            if ( (dum=vv->data[i]*fabs(sum)) >= big) {
                big=dum;
                imax=i;
            }
        }
        if (j != imax) {
            for (k=1;k<=n;k++) {
                dum=a->data[imax][k];
                a->data[imax][k]=a->data[j][k];
                a->data[j][k]=dum;
            }
            *d = -(*d);
            vv->data[imax]=vv->data[j];
        }
        indx->data[j]=imax;
        if (a->data[j][j] == 0.0) a->data[j][j]=TINY;
        if (j != n) {
            dum=1.0/(a->data[j][j]);
            for (i=j+1;i<=n;i++) a->data[i][j] *= dum;
        }
    }
    DeleteFVector(vv);	
}
#undef TINY

static void lubksb(FMATRIX a,IVECTOR indx,FVECTOR b)
{
    int n = a->size;
    int i,ii=0,ip,j;
    double sum;
    
    for (i=1;i<=n;i++) {
        ip=indx->data[i];
        sum=b->data[ip];
        b->data[ip]=b->data[i];
        if (ii)
            for (j=ii;j<=i-1;j++) sum -= a->data[i][j]*b->data[j];
        else if (sum) ii=i;
        b->data[i]=sum;
    }
    for (i=n;i>=1;i--) {
        sum=b->data[i];
        for (j=i+1;j<=n;j++) sum -= a->data[i][j]*b->data[j];
        b->data[i]=sum/a->data[i][i];
    }
}

void InverseFMatrix(FMATRIX m) DECLARETHROW
{
    int n = m->size;
    double d;
    IVECTOR indx;
    FVECTOR col;
    FMATRIX res;
    int i,j;
	
    indx = NewIVector(n);
    col = NewFVector(n);
    res = NewFMatrix(n);
    
    try {ludcmp(m,indx,&d);}
    catch (const char *str) {
        DeleteIVector(indx);
        DeleteFVector(col);
        DeleteFMatrix(res);
        THROW(str);
    }
    
    for (j=1;j<=n;j++) {
        for (i=1;i<=n;i++) col->data[i] = 0.0;
        col->data[j] = 1.0;
        lubksb(m,indx,col);
        for(i=1;i<=n;i++) res->data[i][j] = col->data[i];
    }
    
    for (j=1;j<=n;j++) 
        for (i=1;i<=n;i++) m->data[i][j] = res->data[i][j];
    
    DeleteIVector(indx);
    DeleteFVector(col);
    DeleteFMatrix(res);
}






static void tred2(FMATRIX a, FVECTOR d, FVECTOR e)
{
    int l,k,j,i;
    double scale,hh,h,g,f;
    int n = a->size;
    
    for (i=n;i>=2;i--) {
        l=i-1;
        h=scale=0.0;
        if (l > 1) {
            for (k=1;k<=l;k++) scale += fabs(a->data[i][k]);
            if (scale == 0.0) e->data[i]=a->data[i][l];
            else {
                for (k=1;k<=l;k++) {
                    a->data[i][k] /= scale;
                    h += a->data[i][k]*a->data[i][k];
                }
                f=a->data[i][l];
                g=(f >= 0.0 ? -sqrt(h) : sqrt(h));
                e->data[i]=scale*g;
                h -= f*g;
                a->data[i][l]=f-g;
                f=0.0;
                for (j=1;j<=l;j++) {
                    /* Next statement can be omitted if eigenvectors not wanted */
                    a->data[j][i]=a->data[i][j]/h;
                    g=0.0;
                    for (k=1;k<=j;k++) g += a->data[j][k]*a->data[i][k];
                    for (k=j+1;k<=l;k++) g += a->data[k][j]*a->data[i][k];
                    e->data[j]=g/h;
                    f += e->data[j]*a->data[i][j];
                }
                hh=f/(h+h);
                for (j=1;j<=l;j++) {
                    f=a->data[i][j];
                    e->data[j]=g=e->data[j]-hh*f;
                    for (k=1;k<=j;k++) a->data[j][k] -= (f*e->data[k]+g*a->data[i][k]);
                }
            }
        }
        else e->data[i]=a->data[i][l];
        d->data[i]=h;
    }
    
    /* Next statement can be omitted if eigenvectors not wanted */
    d->data[1]=0.0;
    e->data[1]=0.0;
    
    /* Contents of this loop can be omitted if eigenvectors not
     wanted except for statement d[i]=a[i][i]; */
    for (i=1;i<=n;i++) {
        l=i-1;
        if (d->data[i]) {
            for (j=1;j<=l;j++) {
                g=0.0;
                for (k=1;k<=l;k++) g += a->data[i][k]*a->data[k][j];
                for (k=1;k<=l;k++) a->data[k][j] -= g*a->data[k][i];
            }
        }
        d->data[i]=a->data[i][i];
        a->data[i][i]=1.0;
        for (j=1;j<=l;j++) a->data[j][i]=a->data[i][j]=0.0;
    }
}


#define SQR(x) ((x) * (x))
static double pythag(double a, double b)
{
    double absa,absb;
    
    absa=fabs(a);
    absb=fabs(b);
    
    if (absa > absb) return absa*sqrt(1.0+SQR(absb/absa));
    else return (absb == 0.0 ? 0.0 : absb*sqrt(1.0+SQR(absa/absb)));
}
#undef SQR

#define SIGN1(a,b) ((b) > 0.0 ? fabs(a) : -fabs(a))
static void tqli(FVECTOR d, FVECTOR e, FMATRIX z) DECLARETHROW
{
    double pythag(double a, double b);
    int m,l,iter,i,k;
    double s,r,p,g,f,dd,c,b;
    int n = d->size;
    
    for (i=2;i<=n;i++) e->data[i-1]=e->data[i];
    e->data[n]=0.0;
    
    for (l=1;l<=n;l++) {
        iter=0;
        do {
            for (m=l;m<=n-1;m++) {
                dd=fabs(d->data[m])+fabs(d->data[m+1]);
                if ((double)(fabs(e->data[m])+dd) == dd) break;
            }
            if (m != l) {
                if (iter++ == 30) THROW("Too many iterations in tqli");
                g=(d->data[l+1]-d->data[l])/(2.0*e->data[l]);
                r=pythag(g,1.0);
                g=d->data[m]-d->data[l]+e->data[l]/(g+SIGN1(r,g));
                s=c=1.0;
                p=0.0;
                for (i=m-1;i>=l;i--) {
                    f=s*e->data[i];
                    b=c*e->data[i];
                    e->data[i+1]=(r=pythag(f,g));
                    if (r == 0.0) {
                        d->data[i+1] -= p;
                        e->data[m]=0.0;
                        break;
                    }
                    s=f/r;
                    c=g/r;
                    g=d->data[i+1]-p;
                    r=(d->data[i]-g)*s+2.0*c*b;
                    d->data[i+1]=g+(p=s*r);
                    g=c*r-b;
                    /* Next loop can be omitted if eigenvectors not wanted*/
                    for (k=1;k<=n;k++) {
                        f=z->data[k][i+1];
                        z->data[k][i+1]=s*z->data[k][i]+c*f;
                        z->data[k][i]=c*z->data[k][i]-s*f;
                    }
                }
                if (r == 0.0 && i >= l) continue;
                d->data[l] -= p;
                e->data[l]=g;
                e->data[m]=0.0;
            }
        } while (m != l);
    }
}
#undef SIGN1


FVECTOR SymDiagonalizeFMatrix(FMATRIX a) DECLARETHROW
{
    FVECTOR v,v1;
    
    v = NewFVector(a->size);
    v1 = NewFVector(a->size);
    
    tred2(a,v,v1);
    
    try {tqli(v,v1,a);}
    catch(const char *str) {
        DeleteFVector(v1);
        THROW(str);
    }
    
    DeleteFVector(v1);
    
    return v;
}






//
//  scipackdefs.h
//  SciPack
//
//  Created by bacry on 24/04/13.
//  Copyright (c) 2013 bacry. All rights reserved.
//

#ifndef SciPack_scipackdefs_h
#define SciPack_scipackdefs_h


#ifndef SPEXPORT
# if defined(_WIN32) || defined(__WIN32__) || defined(__CYGWIN__)
#   if defined(STATIC_LINKED)
#     define SPEXPORT
#   else
#     define SPEXPORT __declspec(dllexport)
#   endif
# else
#   if defined(__GNUC__) && defined(GCC_HASCLASSVISIBILITY)
#     define SPEXPORT __attribute__ ((visibility("default")))
#   else
#     define SPEXPORT
#   endif
# endif
#endif



#endif

//  SciPack
//
//  Created by bacry on 24/04/13.
//  Copyright (c) 2013 bacry. All rights reserved.
//

// Not a module


////////////////////////////////////////////
//
// Help/Doc functions
//
////////////////////////////////////////////

%define %newgroup
%pythoncode {
	%#---------------------------------------------------------------------------
}
%enddef


%define %DocMeth(meth,def,details)
%feature("autodoc") meth def;
%feature("docstring") meth details;
%enddef

%define %DocClass(klass,doc)
%feature("docstring") klass doc;
%enddef




%{
	void SCRIPT_DECREF(void * ref) 
	{
		if (ref) Py_DECREF((PyObject *) ref);
	}
	%}

%{
	void SCRIPT_INCREF(void * ref) 
	{
		if (ref) Py_INCREF((PyObject *) ref);
	}
%}

%{
	void SCRIPT_FREE(void * ref)
	{
		PyMem_Free(ref);
	}
%}

%{
	void *SCRIPT_MALLOC(size_t n)
	{
		return PyMem_Malloc(n);
	}
%}


// Called by C++ whenever there is a warning 
%{
	void SCRIPT_Warning(const char *msge)
	{
        PyErr_WarnEx(PyExc_RuntimeWarning, msge,2);
        return;
    }
%}

// Print a msge on the terminal
%{
	void SCRIPT_PrintErrorMsge(const char *msge)
	{
        PyErr_WarnEx(PyExc_RuntimeWarning, msge,2);
        return;

		static PyObject *sysmodule = NULL;
		
		PyObject *sysstderr = NULL;
		
		// Init
		if (sysmodule == NULL) {
			sysmodule= PyImport_AddModule("sys");
		}
		
		// Print in python
		sysstderr = PyObject_GetAttrString(sysmodule,_CCHAR("stderr"));
		PyObject *res = PyObject_CallMethod(sysstderr,_CCHAR("write"),_CCHAR("s"),msge);
		Py_XDECREF(res);
		res = PyObject_CallMethod(sysstderr,_CCHAR("flush"),_CCHAR(""));
		Py_XDECREF(sysstderr);
		Py_XDECREF(res);
	}
%}


// Print a python error using PyErr_Print
// There is a weired bug .... the error is printed in stdout, so we need to redirect it !
%{
	void PrintPyError()
	{
		static char *filename = NULL;
		static PyObject *sysmodule = NULL;
		
		if (!PyErr_Occurred()) return;
		
		PyObject *sysstderr = NULL;
		fpos_t pos;
		int    fd;
		char result[10000];
		
		// Init
		if (sysmodule == NULL) {
			sysmodule= PyImport_AddModule("sys");
			filename = tmpnam(NULL);
		}
		
		// Redirection
		fflush(stdout);
		fgetpos(stdout, &pos);
		fd = dup(fileno(stdout));
		freopen(filename, "w", stdout);	
		
		// Print Error
		PyErr_PrintEx(0); // We do not want to hold some traceback information because it will keep some references on some spObjects
		PyErr_Clear();
		
		// Redirection back
		fflush(stdout);
		dup2(fd, fileno(stdout));
		close(fd);
		clearerr(stdout);
		fsetpos(stdout, &pos);
		
		// Read file
		FILE *stream = fopen(filename,"r");
		int n = fread(result, 1, 9999,stream);
		result[n] = '\0';
		fclose(stream);
		
		// Print in python
		sysstderr = PyObject_GetAttrString(sysmodule,"stderr");
		PyObject *res = PyObject_CallMethod(sysstderr,_CCHAR("write"),_CCHAR("s"),result);
		Py_XDECREF(res);
		res = PyObject_CallMethod(sysstderr,_CCHAR("flush"),_CCHAR(""));
		Py_XDECREF(sysstderr);
		Py_XDECREF(res);
	}
%}



%{
    PyObject *WrapIntArray(int array[],int size)
    {
        int i;
        PyObject *result = PyList_New(size);
        for (i = 0; i < size; i++) {
            PyObject *o = PyInt_FromLong((long) array[i]);
            PyList_SetItem(result,i,o);
        }
        return result;
    }
%}

%{
    PyObject *WrapDoubleArray(double array[],int size)
    {
        int i;
        PyObject *result = PyList_New(size);
        for (i = 0; i < size; i++) {
            PyObject *o = PyFloat_FromDouble(array[i]);
            PyList_SetItem(result,i,o);
        }
        return result;
    }
%}

// Whenever a method in C++ can generate some error
// one must use DECLARETHROW in the declaration in the .i file
%define DECLARETHROW
throw(const char *)
%enddef

// For dealing with spError
%define EXCEPTION_ON
%exception {
	try {
		$action
	}
	catch (char *str) {
		PyErr_SetString(PyExc_IndexError,str);
		SWIG_fail;
	}
}
%enddef

%define EXCEPTION_OFF
%exception;
%enddef


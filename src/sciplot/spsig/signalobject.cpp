/*
 *  signalopbject.cpp
 *  SciPlot
 *
 *  Created by bacry on 10/07/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include <vector>

#include "sciplot/core/sp.h"
#include "lab/sig/sig.h"
#include "signalobject.h"


#define GETOBJY(obj,index) \
(obj->flagReal ? (obj->signal->IsComplex() ? real(obj->signal->Yc(index)) : obj->signal->Y(index)) : imag(obj->signal->Yc(index)))

#define GETY(index) \
(flagReal ? (signal->IsComplex() ? real(signal->Yc(index)) : signal->Y(index)) : imag(signal->Yc(index)))


const char *CursorCBSignal(spObject *object, bool flagFullStr, spRealPoint &pt)
{
	static char str[10000];
	spSignalObject *sigobject = (spSignalObject *) object;
	if (!sigobject->IsSignal()) return "";
	spRealPoint pt1 = sigobject->container->l2top.IApply(pt);
	unsigned long index = sigobject->signal->X2Index(pt1.x);
	pt1.y = GETOBJY(sigobject,index);
	pt1.x = sigobject->signal->X(index);
	pt = sigobject->container->l2top.ApplyInt(pt1);
	if (flagFullStr) sprintf(str,"x=%g [index = %ld] y=%g",pt1.x,index,pt1.y);
	else {
		sprintf(str,"x=%g [index = %ld]\ny=%g",pt1.x,index,pt1.y);
	}
	return str;
}
 

double spSignalObject::GetDistance(spRealPoint pt)
{
	if (!IsSignal()) return -1;
     
    spRealPoint pt1 = container->l2top.IApply(pt);
	if (signal->X(0)>pt1.x) return -1;
	if (signal->X(signal->Size()-1)<pt1.x) return -1;
	
    unsigned long index = signal->X2Index(pt1.x);
    
    spRealPoint pt2 = container->l2top.Apply(spRealPoint(signal->X(index),GETY(index)));
	double dist = sqrt((pt.y-pt2.y)*(pt.y-pt2.y)+(pt.x-pt2.x)*(pt.x-pt2.x));
	return dist;
}

void spSignalObject::ComputeMinMax(double xBoundMin,double xBoundMax,bool flagForce)
{
    //	printf("%g %g\n",xBoundMin,xBoundMax);
	// Case entire signal
	if (xBoundMin > xBoundMax || (xBoundMin <= signal->X(0) && xBoundMax >= signal->X(signal->Size()-1))) {
        //		printf("BEN\n");
		if (flagForce) {
			
            double yMin = GETY(0);
            double yMax = yMin;
			double xMin = signal->X(0);
			double xMax = xMin;
            if (attribute->HasBase()) {
                double base = attribute->GetBase();
                yMin = MIN(base,yMin);
                yMax = MAX(base,yMax);
            }
            for (unsigned long i = 0;i<signal->Size();i++) {
                xMin = MIN(xMin,signal->X(i));
                xMax = MAX(xMax,signal->X(i));
                yMin = MIN(yMin,GETY(i));
                yMax = MAX(yMax,GETY(i));
                //				printf("%ld %g %g\n",signal->X(i),GETY(i));
            }
			maxRect = spRealRect(spRealPoint(xMin,yMin),spRealPoint(xMax,yMax));
		}
		maxLocRect = maxRect;
		xBoundMinLoc = 1;
		xBoundMaxLoc = 0;
		return;
	}
	
	// Case part of the signal
	if (flagForce || xBoundMin != xBoundMinLoc || xBoundMax != xBoundMaxLoc) {
		
        double xMin = maxRect.GetPosition().x;
        double xMax = maxRect.GetPosition1().x;
		double yMin,yMax;
		bool flagFirst = true;
		for (unsigned long i = 0;i<signal->Size();i++) {
			if (signal->X(i) < xBoundMin || signal->X(i) > xBoundMax) continue;
			if (flagFirst) {
				xMax = xMin = signal->X(i);
				yMax = yMin = GETY(i);
				flagFirst = false;
				continue;
			}
			xMin = MIN(xMin,signal->X(i));
			xMax = MAX(xMax,signal->X(i));
			yMin = MIN(yMin,GETY(i));
			yMax = MAX(yMax,GETY(i));
		}
        if (attribute->HasBase()) {
            double base = attribute->GetBase();
            yMin = MIN(base,yMin);
            yMax = MAX(base,yMax);
        }
		maxLocRect = spRealRect(spRealPoint(xMin,yMin),spRealPoint(xMax,yMax));
		xBoundMinLoc = xBoundMin;
		xBoundMaxLoc = xBoundMax;
	}
	return;
}


static spAnchor getSignalAnchorFunc(spObject *object,double xmin,double xmax)
{
	spSignalObject *o = (spSignalObject *) object;
	
	if (!o->IsSignal()) return spAnchor(0,0);
	o->ComputeMinMax(xmin,xmax);
	return spAnchor(o->maxLocRect.GetPosition());
}


static spMargin getSignalMarginFunc(spObject *object,double xmin,double xmax)
{
	spSignalObject *o = (spSignalObject *) object;
	
	if (!o->IsSignal()) return spMargin();
    
	spMargin margin;
    
	o->ComputeMinMax(xmin,xmax);
	margin.Set(o->maxLocRect.width,o->maxLocRect.height);
    margin.SetPixelMargin(o->attribute->GetPixelMargin());
	
	return margin;
    //	cout << "margin " << xMargin << " " << yMargin << endl;
}




spSignalObject::spSignalObject(spContainer *ctn,SIGNAL signal1, unsigned long flags,const char *name1) : spObject(ctn,spAnchorFunc(getSignalAnchorFunc),spMarginFunc(getSignalMarginFunc),flags,name1)
{
	flagReal = true;
	signal = signal1;
    brush = *wxBLACK_BRUSH;
    pen = *wxBLACK_PEN;
    switch (signal->interMode) {
        case sigInterNone: case sigInterLinear:
            attribute = new spSignalAttributeLine;
            break;
        case sigInterConstRight:
            attribute = new spSignalAttributeCircleConst(8);
            break;
        case sigInterDirac:
            attribute = new spSignalAttributeDiracCircle(10);
            break;
        case sigInterConstLeft:
        default :
            attribute = new spSignalAttributeLine;
    }
	ComputeMinMax(1,0,true);
	SetCursorCallback(CursorCBSignal);
}

spSignalObject::~spSignalObject()
{
	delete attribute;
}

void spSignalObject::Draw(spRect &rect)
{
  	if (!IsSignal()) return;
    
    /* Getting the minimum and maximum indexes the drawing should be perform on */
	spRealRect r = container->l2top.IApply(rect);
	spRealPoint pt = r.GetPosition();
	spRealPoint pt1 = r.GetPosition1();
    long iMin = signal->X2Index(pt.x)-1;
    long iMax = signal->X2Index(pt1.x)+1;
    iMin = MAX(0,iMin);
    iMax = MIN(((long) signal->Size())-1,iMax);
	// Call Attribute Draw
	attribute->Draw(this, iMin, iMax);
}



//
// Drawing circles at each point
//
void spSignalAttributeCircle::Draw(spSignalObject *signalObject, long int iMin, long int iMax)
{
	SIGNAL signal = signalObject->signal;
    spContainer *container = signalObject->container;
    double resolution = signalObject->GetCanvas()->GetResolution();

    // This is to keep the points being frawn at the same abscissa
    // We will keep track of them so that we don't draw twice the same point
    std::vector<spPoint> points;
    
    
    for (int i = iMin ; i <= iMax; i++) {
        
		spRealPoint cur = container->l2top.Apply(spRealPoint(signal->X(i),GETOBJY(signalObject,i)));
        spPoint curint = (cur/resolution).GetRoundPoint();
        
        if (i==iMin || (points.size()>0 && points[0].x != curint.x)) {
            signalObject->DrawCEllipse(cur.x,cur.y,size/2,size/2);
            points.resize(0);
            points.push_back(curint);
        }
        else {
            bool flagDraw = true;
            for (int j = 0; j< points.size(); j++)
                if (points[j] == curint) {flagDraw=false;break;};
            if (flagDraw) {
                signalObject->DrawCEllipse(cur.x,cur.y,size/2,size/2);
                points.push_back(curint);
            }
        }
	}
}


//
// Drawing crosses at each point
//
void spSignalAttributeCross::Draw(spSignalObject *signalObject, long int iMin, long int iMax)
{
	SIGNAL signal = signalObject->signal;
    spContainer *container = signalObject->container;
    double resolution = signalObject->GetCanvas()->GetResolution();
    
    // This is to keep the points being frawn at the same abscissa
    // We will keep track of them so that we don't draw twice the same point
    std::vector<spPoint> points;
    
    
    for (int i = iMin ; i <= iMax; i++) {
        
		spRealPoint cur = container->l2top.Apply(spRealPoint(signal->X(i),GETOBJY(signalObject,i)));
        spPoint curint = (cur/resolution).GetRoundPoint();
        
        if (i==iMin || (points.size()>0 && points[0].x != curint.x)) {
            signalObject->DrawLine(cur.x-size/2,cur.y,cur.x+size/2,cur.y);
            signalObject->DrawLine(cur.x,cur.y-size/2,cur.x,cur.y+size/2);
            points.resize(0);
            points.push_back(curint);
        }
        else {
            bool flagDraw = true;
            for (int j = 0; j< points.size(); j++)
                if (points[j] == curint) {flagDraw=false;break;};
            if (flagDraw) {
                signalObject->DrawLine(cur.x-size/2,cur.y,cur.x+size/2,cur.y);
                signalObject->DrawLine(cur.x,cur.y-size/2,cur.x,cur.y+size/2);
                points.push_back(curint);
            }
        }
	}
}

//
// Drawing points at each point
//
void spSignalAttributeDot::Draw(spSignalObject *signalObject, long int iMin, long int iMax)
{
	SIGNAL signal = signalObject->signal;
    spContainer *container = signalObject->container;
    double resolution = signalObject->GetCanvas()->GetResolution();
    
    // This is to keep the points being frawn at the same abscissa
    // We will keep track of them so that we don't draw twice the same point
    std::vector<spPoint> points;
    
    
    for (int i = iMin ; i <= iMax; i++) {
        
		spRealPoint cur = container->l2top.Apply(spRealPoint(signal->X(i),GETOBJY(signalObject,i)));
        spPoint curint = (cur/resolution).GetRoundPoint();
        
        if (i==iMin || (points.size()>0 && points[0].x != curint.x)) {
            signalObject->DrawPoint(cur.x,cur.y);
            points.resize(0);
            points.push_back(curint);
        }
        else {
            bool flagDraw = true;
            for (int j = 0; j< points.size(); j++)
                if (points[j] == curint) {flagDraw=false;break;};
            if (flagDraw) {
                signalObject->DrawPoint(cur.x,cur.y);
                points.push_back(curint);
            }
        }
	}
}

//
// Draw a piece-wise linear signal
//
void spSignalAttributeLine::Draw(spSignalObject *signalObject, long int iMin, long int iMax)
{
	SIGNAL signal = signalObject->signal;
    spContainer *container = signalObject->container;
	
    /*
     * Let's start the loop !
     */
	
    int yInf = INT_MAX;
    int ySup = INT_MIN;
	spRealPoint last;
	spPoint lastint;
    double resolution = signalObject->GetCanvas()->GetResolution();
    
    for (int i = iMin ; i <= iMax; i++) {
		
        /* Computing the point coordinates */
		spRealPoint cur = container->l2top.Apply(spRealPoint(signal->X(i),GETOBJY(signalObject,i)));
		spPoint curint = (cur/resolution).GetRoundPoint();
        
		// If the same abscissa as last point or first point
		if (lastint.x == curint.x || i == iMin) {
			yInf = MIN(yInf,cur.y);
			ySup = MAX(ySup,cur.y);
			// If last point we should draw
			if (i==iMax && yInf <= ySup) signalObject->DrawLine(cur.x,yInf,cur.x,ySup);
		}
		
		// If not
		else {
			// We might need to draw the last point
			if (yInf < ySup) signalObject->DrawLine(last.x,yInf,last.x,ySup);
			signalObject->DrawLine(last.x,last.y,cur.x,cur.y);
			yInf = cur.y;
			ySup = cur.y;
		}
		
		last = cur;
        lastint = curint;
	}
}



void spSignalAttributeLineCircle::Draw(spSignalObject *signalObject, long int iMin, long int iMax)
{
    spSignalAttributeCircle att2(size);
    att2.Draw(signalObject,iMin,iMax);

    spSignalAttributeLine att1;
    att1.Draw(signalObject,iMin,iMax);    
}


void spSignalAttributeConst::Draw(spSignalObject *signalObject, long int iMin, long int iMax)
{
	SIGNAL signal = signalObject->signal;
    spContainer *container = signalObject->container;
	
    /*
     * Let's start the loop !
     */
	
    int yInf = INT_MAX;
    int ySup = INT_MIN;
    double resolution = signalObject->GetCanvas()->GetResolution();
	spPoint lastint;
	spRealPoint last,cur;
    
    for (int i = iMin ; i <= iMax; i++) {
		
        /* Computing the point coordinates */
		cur = container->l2top.ApplyInt(spRealPoint(signal->X(i),GETOBJY(signalObject,i)));
        spPoint curint = (cur/resolution).GetRoundPoint();

		// If the same abscissa as last point or first point
		if (lastint.x == curint.x || i == iMin) {
			yInf = MIN(yInf,cur.y);
			ySup = MAX(ySup,cur.y);
            
            // If not last point, we continue
            if (i<iMax) {
                last = cur;
                lastint = curint;
                continue;
            }
		}
		
        // We need to draw the last point
        if (yInf < ySup) {
            signalObject->DrawLine(last.x,yInf,last.x,ySup);
        }
        signalObject->DrawLine(last.x,last.y,cur.x,last.y);
        
		yInf = MIN(cur.y,last.y);
		ySup = MAX(cur.y,last.y);
		
		last = cur;
        lastint = curint;
	}
    
    // Now we draw the last point
    signalObject->DrawPoint(cur.x,yInf);
    signalObject->DrawPoint(cur.x,ySup);
}


void spSignalAttributeCircleConst::Draw(spSignalObject *signalObject, long int iMin, long int iMax)
{
    spSignalAttributeCircle att2(size);
    att2.Draw(signalObject,iMin,iMax);

    spSignalAttributeConst att1;
    att1.Draw(signalObject,iMin,iMax);    
}



//
// Drawing dirac
//
void spSignalAttributeDirac::Draw(spSignalObject *signalObject, long int iMin, long int iMax)
{
	SIGNAL signal = signalObject->signal;
    spContainer *container = signalObject->container;
    double resolution = signalObject->GetCanvas()->GetResolution();
    spRealPoint last;
    spRealPoint mybase = container->l2top.Apply(spRealPoint(0,0));
    
    // This is to keep the points being drawn at the same abscissa
    // We will keep track of them so that we don't draw twice the same point
    std::vector<spPoint> points;
    
    iMin = MAX(iMin-1,0);
    iMax = MIN(iMax+1,signal->Size()-1);
    
    for (int i = iMin ; i <= iMax; i++) {
        
		spRealPoint cur = container->l2top.Apply(spRealPoint(signal->X(i),GETOBJY(signalObject,i)));
        spPoint curint = (cur/resolution).GetRoundPoint();
        
        if (i==iMin || (points.size()>0 && points[0].x != curint.x)) {
            signalObject->DrawPoint(cur.x,cur.y);
            points.resize(0);
            points.push_back(curint);
            if (i!=iMin) {
                signalObject->DrawLine(last.x,mybase.y,cur.x,mybase.y);
            }
            signalObject->DrawLine(cur.x,mybase.y,cur.x,cur.y);
        }
        else {
            bool flagDraw = true;
            for (int j = 0; j< points.size(); j++)
                if (points[j] == curint) {flagDraw=false;break;};
            if (flagDraw) {
                signalObject->DrawPoint(cur.x,cur.y);
                signalObject->DrawLine(cur.x,mybase.y,cur.x,cur.y);
                points.push_back(curint);
            }
        }
        
        last = cur;
	}
}




void spSignalAttributeDiracCircle::Draw(spSignalObject *signalObject, long int iMin, long int iMax)
{
    spSignalAttributeDirac att1;
    att1.Draw(signalObject,iMin,iMax);

    spSignalAttributeCircle att2(size);
    att2.Draw(signalObject,iMin,iMax);    
}



void spSignalAttributeHisto::Draw(spSignalObject *signalObject, long int iMin, long int iMax)
{
	SIGNAL signal = signalObject->signal;
    spContainer *container = signalObject->container;
	
    /*
     * Let's start the loop !
     */
	
	spPoint last;
	spPoint cur;
    spPoint mybase = container->l2top.ApplyInt(spRealPoint(0,base));
    double x;
    
    
    for (int i = iMin ; i <= iMax; i++) {
		
        /* Computing the point coordinates */
		cur = container->l2top.ApplyInt(spRealPoint(signal->X(i),GETOBJY(signalObject,i)));
		
        if (i > 0) {
            double x1 = (cur.x+last.x)/2;
            if (i == 1) {
                signalObject->DrawRectangle(spRealRect(last.x,mybase.y,x1-last.x,last.y-mybase.y));
            }
            else {
                signalObject->DrawRectangle(spRealRect(x,mybase.y,x1-x,last.y-mybase.y));
            }
            x = x1;
            
        }
        
		last = cur;
	}
}







/*
 *  signalopbject.h
 *  SciPlot
 *
 *  Created by bacry on 10/07/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _SP_SIGNALOBJECT_
#define _SP_SIGNALOBJECT_

#define _CCHAR(x) const_cast<char *>(x)

class spSignalObject;
class spSignalAttribute {
	public : 
	
    virtual bool HasBase() {return false;}
    virtual double GetBase() {return 0;}
	spSignalAttribute() {};
    virtual ~spSignalAttribute() {};
	virtual int GetPixelMargin() {
		return 0;
	};
	virtual void Draw(spSignalObject *signalObject, long int iMin, long int iMax) = 0;
    virtual const char *GetName() {return _CCHAR("Unkown");}
};

class spSignalAttributeLine : public spSignalAttribute {
	
	public : 
	
	spSignalAttributeLine() : spSignalAttribute() {};
    virtual ~spSignalAttributeLine() {};

	virtual void Draw(spSignalObject *signalObject, long int iMin, long int iMax);
    virtual const char *GetName() {return _CCHAR("Line");}
};


class spSignalAttributeLineCircle : public spSignalAttribute {
	
	public :
	
    int size;
	spSignalAttributeLineCircle(int size1) : spSignalAttribute() {size = size1;};
    virtual ~spSignalAttributeLineCircle() {};
    
	virtual int GetPixelMargin() {return size;};
	virtual void Draw(spSignalObject *signalObject, long int iMin, long int iMax);
    virtual const char *GetName() {return _CCHAR("LineCircle");}
};

class spSignalAttributeCircleConst : public spSignalAttribute {
	
	public :
	
    int size;
	spSignalAttributeCircleConst(int size1) : spSignalAttribute() {size = size1;};
    virtual ~spSignalAttributeCircleConst() {};
    
	virtual int GetPixelMargin() {return size;};
	virtual void Draw(spSignalObject *signalObject, long int iMin, long int iMax);
    virtual const char *GetName() {return _CCHAR("CircleConst");}
};

class spSignalAttributeConst : public spSignalAttribute {
	
	public :
	
	spSignalAttributeConst() : spSignalAttribute() {};
    virtual ~spSignalAttributeConst() {};
	virtual int GetPixelMargin() {return 3;};
    
	virtual void Draw(spSignalObject *signalObject, long int iMin, long int iMax);
    virtual const char *GetName() {return _CCHAR("Const");}
};

class spSignalAttributeDot : public spSignalAttribute {
	
	public : 
	
	spSignalAttributeDot() : spSignalAttribute() {};
    virtual ~spSignalAttributeDot() {};

	virtual void Draw(spSignalObject *signalObject, long int iMin, long int iMax);
    virtual const char *GetName() {return _CCHAR("Dot");}
};

class spSignalAttributeCross : public spSignalAttribute {
	
    
	public :
	
    int size;
	spSignalAttributeCross(int size1) : spSignalAttribute() {size = size1;};
    virtual ~spSignalAttributeCross() {};
	virtual int GetPixelMargin() {return size;};
    
	virtual void Draw(spSignalObject *signalObject, long int iMin, long int iMax);
    virtual const char *GetName() {return _CCHAR("Cross");}
};

class spSignalAttributeCircle : public spSignalAttribute {
	
    
	public :
	
    int size;
	spSignalAttributeCircle(int size1) : spSignalAttribute() {size = size1;};
    virtual ~spSignalAttributeCircle() {};
	virtual int GetPixelMargin() {return size;};
    
	virtual void Draw(spSignalObject *signalObject, long int iMin, long int iMax);
    virtual const char *GetName() {return _CCHAR("Circle");}
};


class spSignalAttributeDirac : public spSignalAttribute {
	
	public :
	
    virtual bool HasBase() {return true;}
    virtual double GetBase() {return 0;}

	spSignalAttributeDirac() : spSignalAttribute() {};
    virtual ~spSignalAttributeDirac() {};
    
	virtual int GetPixelMargin() {return 15;};
	virtual void Draw(spSignalObject *signalObject, long int iMin, long int iMax);
    virtual const char *GetName() {return _CCHAR("Dirac");}
};

class spSignalAttributeDiracCircle : public spSignalAttribute {
	
	public :
	
    virtual bool HasBase() {return true;}
    virtual double GetBase() {return 0;}

    int size;

	spSignalAttributeDiracCircle(int size1) : spSignalAttribute() {size=size1;};
    virtual ~spSignalAttributeDiracCircle() {};
    
	virtual int GetPixelMargin() {return MAX(size,15);};
	virtual void Draw(spSignalObject *signalObject, long int iMin, long int iMax);
    virtual const char *GetName() {return _CCHAR("CircleDirac");}
};

class spSignalAttributeHisto : public spSignalAttribute {
	
    
	public :
	
    virtual bool HasBase() {return true;}
    virtual double GetBase() {return base;}

    double base;
	spSignalAttributeHisto(double base1=0) : spSignalAttribute() {base=base1;};
    virtual ~spSignalAttributeHisto() {};
    
	virtual void Draw(spSignalObject *signalObject, long int iMin, long int iMax);
    virtual const char *GetName() {return _CCHAR("Circle");}
};


class spSignalObject : public spObject
{
	public : 

	spSignalAttribute *attribute;
	void SetAttribute(spSignalAttribute *attribute1) {
		delete attribute;
		attribute = attribute1;
	};
	
	~spSignalObject();
	spSignalObject(spContainer *container=NULL,SIGNAL signal=NULL, unsigned long flags=0,const char *name=NULL);

	SIGNAL signal;
	bool flagReal;

	// Here is the mechanism to compute quickly the boundaries of the signal
	void ComputeMinMax(double xBoundMin=1,double xBoundMax=0,bool flagForce = false); // the main method to compute the bound
	spRealRect maxRect; // The boundary of the entire signal
	spRealRect maxLocRect; // The last computed boundary
	double xBoundMinLoc,xBoundMaxLoc; // The corresponding min/max
		
	
	virtual void NotifyCanvasDeleted() {
		signal = NULL;
	}
	bool IsSignal(SIGNAL sig=NULL) {
        if (sig) {return (sig == signal);}
		return ((signal) && signal->Size() != 0);
	}
	SIGNAL GetSignal() {return signal;}
    
    
	virtual void Draw(spRect &rect);
	
	virtual bool IsMoveable() {return false;};
	
	virtual double GetDistance(spRealPoint pt); // Returns a (positive) number that measures the distance of a (global) point to the object (0 if the point is within the object)


  virtual const char *GetClassName() {
		return _CCHAR("spSignalObject");
	}	
};

#endif
// Change log --> Update des axes
// Pb du dcBuffer




#include "sp.h"

void spGrid::InitGrid()
{
	nGrid=1;
	mGrid=1;
	rightMarginGrid=0;
	downMarginGrid=0;
	leftMarginGrid=0;
	upMarginGrid=0;
	interMarginGrid=0;	
}

spGrid::spGrid(int i,const char *name1) : spContainer(i,name1)
{
	InitGrid();
}

spGrid::spGrid(spContainer *ctn,const spAnchor &anchor,const spMargin &margin,unsigned long flags1,const char *name1) : spContainer(ctn,anchor,margin,flags1,name1)
{
	InitGrid();
}

spGrid::~spGrid()
{
}


spRealPoint spGrid::GetGridSize(spRealPoint pt)
{
	spRealRect r = l2top.IApply(gRect());
	
	int cellx = (int) (.5+(r.width-leftMarginGrid-rightMarginGrid+interMarginGrid)/((double) mGrid)-interMarginGrid);
	int celly = (int) (.5+(r.height-upMarginGrid-downMarginGrid+interMarginGrid)/((double) nGrid)-interMarginGrid);

	return spRealPoint(pt.x*cellx,pt.y*celly);
}

spRealPoint spGrid::GetGridPoint(spRealPoint pt)
{
	spRealRect r = l2top.IApply(gRect());
	
	int cellx = (int) (.5+(r.width-leftMarginGrid-rightMarginGrid+interMarginGrid)/((double) mGrid)-interMarginGrid);
	int celly = (int) (.5+(r.height-upMarginGrid-downMarginGrid+interMarginGrid)/((double) nGrid)-interMarginGrid);
	
  return spRealPoint(leftMarginGrid+(cellx+interMarginGrid)*pt.x,upMarginGrid+(celly+interMarginGrid)*pt.y);
}

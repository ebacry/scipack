
%module(directors="1") sp

%module(package="wx") _windows

%feature("autodoc", "1");  // 0 == no param types, 1 == show param types


%{
#include "sp.h"
%}

%pythoncode %{
import os
execfile(os.path.split(os.path.abspath(__file__))[0]+"/_sp.py")    
execfile(os.path.split(os.path.abspath(__file__))[0]+"/_config.py")
%}


// Include both scipack defs and scipack.sciplot.core defs
%include scipackdefs.i
%include sciplotdefs.i

%include "std_string.i"


//%pragma(python) code = "import wx"

%include wxutils.i
%include debug.i
%include error.i
%include utils.i
%include box.i

%include object.i
%include object_smart_ptr.i
%include canvas.i
%include shape.i
%include textbox.i
%include event.i
%include interactor.i
// %include interactor_msge.i
%include interactor_cursor.i
// %include interactor_popup.i

%include text.i
%include axis.i
%include container.i
%include grid.i
%include view.i
%include framedview.i
%include topcontainer.i
%include group.i
%include comment.i

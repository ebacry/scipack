/*
 *  object_set.hpp
 *  SciPlot
 *
 *  Created by bacry on 12/07/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


//
// 
// This file defines an object which is a set of spObjects that are automatically deleted from the set when the objects are deleted
//


#ifndef _SP_OBJECT_SET_
#define _SP_OBJECT_SET_


class spInteractorObjectSet;


//
// The class
//
class spObjectSet {
	
	public :
	
	spList<spObject> objectList;
	void AddObject(spObject *receiver);
	void RemoveObject(spObject *receiver);
	inline int Size() {
		return objectList.Size();
	}
	inline spObject * operator [] (int i) {
		return objectList[i];
	}
	spInteractorObjectSet *inter;
	
	spObjectSet();
	~spObjectSet();
	
};


#endif
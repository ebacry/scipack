/*
 *  spbox.cpp
 *  SciPlot
 *
 *  Created by bacry on 12/07/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "sp.h"



/////////////////////////////
//
//
// Anchor class
//
// This class is used to code the anchor of the bounding box of an object
//
//
/////////////////////////////

// Constructor for a pointer to anchor function
spAnchorFuncPtr::spAnchorFuncPtr()
{
    flagIsPixelTranslation = false;
    getFunc = NULL;
    translation = spRealPoint(0,0);
#ifdef SP_SCRIPT
	scriptFunc = NULL;
#endif
}

// Constructor for a pointer to anchor function
spAnchorFuncPtr::spAnchorFuncPtr(spAnchor (*getAnchorFunc1)(spObject *,double xmin,double xmax))
{
    flagIsPixelTranslation = false;
    flagGetFuncXMinXMax = true;
	getFunc = (void *) getAnchorFunc1;
    translation = spRealPoint(0,0);
#ifdef SP_SCRIPT
	scriptFunc = NULL;
#endif
}

// Constructor for a pointer to anchor function
spAnchorFuncPtr::spAnchorFuncPtr(spAnchor (*getAnchorFunc1)(spObject *))
{
    flagGetFuncXMinXMax = false;
	getFunc = (void *) getAnchorFunc1;
    translation = spRealPoint(0,0);
    flagIsPixelTranslation = false;
#ifdef SP_SCRIPT
	scriptFunc = NULL;
#endif
}

// Call an anchor function
spAnchor spAnchorFuncPtr::Call(spObject *object, double xmin, double xmax)
{
	if (flagGetFuncXMinXMax) return ((spAnchor (*)(spObject*,double,double)) getFunc)(object,xmin,xmax);
	else return ((spAnchor (*)(spObject*)) getFunc)(object);
}

// Update an anchor if an anchor function is used
void spAnchor::Update(spObject *object,double xmin,double xmax)
{
	if (!anchorFuncPtr.IsSet()) return;
	spAnchor a = anchorFuncPtr.Call(object,xmin,xmax);
	anchor = a.anchor;
	flagGrid = a.flagGrid;
    flagNone = a.flagNone;
    flagMoveable = a.flagMoveable;
    if (!flagGrid) {
        if (anchorFuncPtr.flagIsPixelTranslation && object->container != NULL)
            anchor = object->container->l2top.IApply(object->container->l2top.Apply(anchor)+anchorFuncPtr.translation);
        else anchor = anchor+anchorFuncPtr.translation;
        
    }
}

// Get the anchor (x,y) in father's container coordinate
spRealPoint spAnchor::Get(spObject *object,bool flagUpdate,double xMin,double xMax)
{
	if (flagUpdate) Update(object,xMin,xMax);
	if (!flagGrid) return anchor;
	else {
		spGrid *grid = (spGrid *) (object->container);
		return grid->GetGridPoint(anchor);
	}
}

// Move an anchor
void spAnchor::Move(spObject *object, double dx,double dy)
{
	if (!IsMoveable()) return;
    
    if (!anchorFuncPtr.IsSet()) anchor += spRealPoint(dx,dy);
    else {
        if (anchorFuncPtr.flagIsPixelTranslation && object->container != NULL)
            anchorFuncPtr.translation += object->container->l2top.Apply(anchor+spRealPoint(dx,dy))-object->container->l2top.Apply(anchor);
        else anchorFuncPtr.translation += spRealPoint(dx,dy);
        anchor += spRealPoint(dx,dy);
    }
}
void spAnchor::Set(spObject *object, spRealPoint anchor1)
{
	if (!IsMoveable()) return;
	if (anchorFuncPtr.IsSet()) {
        if (anchorFuncPtr.flagIsPixelTranslation && object->container != NULL)
            anchorFuncPtr.translation = object->container->l2top.Apply(anchor1)-object->container->l2top.Apply(anchor);
        else anchorFuncPtr.translation = anchor1-anchor;
        anchor=anchor1;
    }
	else anchor=anchor1;
}


/////////////////////////////
//
//
// margin class
//
// This class is used to code the margins of the bounding box of an object
//
//
/////////////////////////////


spMarginFuncPtr::spMarginFuncPtr()
{
	getFunc = NULL;
#ifdef SP_SCRIPT
	scriptFunc = NULL;
#endif
}

spMarginFuncPtr::spMarginFuncPtr(spMargin (*getMarginFunc1)(spObject *))
{
    flagGetFuncXMinXMax = false;
	getFunc = (void *) getMarginFunc1;
#ifdef SP_SCRIPT
	scriptFunc = NULL;
#endif
}


spMarginFuncPtr::spMarginFuncPtr(spMargin (*getMarginFunc1)(spObject *,double xmin,double xmax))
{
    flagGetFuncXMinXMax = true;
	getFunc = (void *) getMarginFunc1;
#ifdef SP_SCRIPT
	scriptFunc = NULL;
#endif
}

spMargin spMarginFuncPtr::Call(spObject *object, double xmin, double xmax)
{
	if (flagGetFuncXMinXMax) return ((spMargin (*)(spObject*,double,double)) getFunc)(object,xmin,xmax);
	else return ((spMargin (*)(spObject*)) getFunc)(object);
}


void spMargin::Update(spObject *object,double xmin,double xmax)
{
	if (IsInfinite()) return;
	if (!marginFuncPtr.IsSet()) return;
	spMargin margin = marginFuncPtr.Call(object,xmin,xmax);
	xMargin = margin.xMargin;
	mxMargin = margin.mxMargin;
	yMargin = margin.yMargin;
	myMargin = margin.myMargin;
	pixelMargin = margin.pixelMargin;
	flagGrid = margin.flagGrid;
	flagInfinite = margin.flagInfinite;
    flagPixel = margin.flagPixel;
    flagPixelStraight = margin.flagPixelStraight;
}

spContainer * spMargin::GetContainer(spObject *object) {
    if (object->container == NULL) return(NULL);
    return (IsPixel() || IsPixelStraight() ? object->container->topContainer : object->container);
}


spRealRect spMargin::Get(spObject *object, bool flagUpdate,double xmin,double xmax)
{
	if (flagUpdate) Update(object,xmin,xmax);
    
	spRealPoint pt,pt1;
	
	if (!flagGrid) {
		pt = spRealPoint(-mxMargin,-myMargin);
		pt1 = spRealPoint(xMargin,yMargin);
	}
	else {
		spGrid *grid = (spGrid *) (object->container);
		pt = grid->GetGridSize(spRealPoint(-mxMargin,-myMargin));
		pt1 = grid->GetGridSize(spRealPoint(xMargin,yMargin));
	}
	
	return spRealRect(pt,pt1);
}


spRealPoint spMargin::GetOriginOposite(spObject *object,spRealPoint origin)
{
    spRealPoint dv;
    
    if (flagCentered) {dv = spRealPoint(xMargin,yMargin);}
    else if (mxMargin == 0 && myMargin == 0) dv=spRealPoint(xMargin,yMargin);
    else if (mxMargin == 0 && yMargin == 0) dv=spRealPoint(xMargin,-myMargin);
    else if (xMargin == 0 && myMargin == 0) dv=spRealPoint(-mxMargin,yMargin);
    else if (xMargin == 0 && yMargin == 0) dv=spRealPoint(-mxMargin,-myMargin);
    else dv = spRealPoint(0,0);
    
    spContainer *marginContainer = GetContainer(object);
    spContainer *originContainer = object->container;
    
    if (marginContainer == NULL || originContainer == NULL) return dv+origin;

    if (flagGrid) return dv+origin;
    if (marginContainer == originContainer) return dv+origin;

    origin = originContainer->l2top.Apply(origin);
//    origin *= originContainer->l2top.GetSign();
    
    spRealPoint pt = originContainer->l2top.IApply(origin+dv);
    
    return pt;
}

void spMargin::SetOriginOposite(spObject *object,spRealPoint pt)
{
    if (!IsResizeable()) return;
    
    spContainer *ct = GetContainer(object);
    
    // Go back to ct coordinates
    if (!flagGrid && ct != NULL && ct != object->container && object->container != NULL) {
        pt = object->container->l2top.Apply(pt);
        pt = ct->l2top.IApply(pt);
    }
    
    // Then set it
    if (pt.x>0) {
        if (flagCentered) mxMargin = pt.x;
        else mxMargin = 0;
        xMargin = pt.x;
    }
    else {
        if (flagCentered) xMargin = -pt.x;
        else xMargin = 0;
        mxMargin = -pt.x;
    }
    
    if (pt.y>0) {
        if (flagCentered) myMargin = pt.y;
        else myMargin = 0;
        yMargin = pt.y;
    }
    else {
        if (flagCentered) yMargin = -pt.y;
        else yMargin = 0;
        myMargin = -pt.y;
    }
}

void spMargin::SetSize(double dx, double dy)
{
    if (!IsResizeable()) return;
    
    if (dx > 0) {
        xMargin = dx;
        if (flagCentered) mxMargin = dx; else mxMargin = 0;
    }
    else {
        mxMargin = -dx;
        if (flagCentered) xMargin = -dx; else mxMargin = 0;
    }

    if (dy > 0) {
        yMargin = dy;
        if (flagCentered) mxMargin = dy; else myMargin = 0;
    }
    else {
        mxMargin = -dy;
        if (flagCentered) yMargin = -dy; else myMargin = 0;
    }
}

spRealPoint spMargin::GetSize()
{
    if (flagCentered) return spRealPoint(xMargin,yMargin);
    
    spRealPoint dv;
    
    if (mxMargin == 0 && myMargin == 0) dv=spRealPoint(xMargin,yMargin);
    else if (mxMargin == 0 && yMargin == 0) dv=spRealPoint(xMargin,-myMargin);
    else if (xMargin == 0 && myMargin == 0) dv=spRealPoint(-mxMargin,yMargin);
    else if (xMargin == 0 && yMargin == 0) dv=spRealPoint(-mxMargin,-myMargin);
    else dv = spRealPoint(0,0);
    
    return dv;
}

/////////////////////////////
//
//
// Box class
//
// This class is used to code the bounding box of an object in local coordinate of the container it belongs to
//
//
/////////////////////////////


std::string spBox::ToStr(bool flagShort) {
	char str[200];
    /*	if (flagGrid) sprintf(str,"spBoxGrid(%g,%g,%g,%g)",anchor.x,anchor.y,xMargin,yMargin);
     else if (IsInfinite()) sprintf(str,"spBoxInfinite");
     else {
     str1 = str;
     sprintf(str1,"spBox(");
     str1 = str1+strlen(str1);
     if (getAnchorFunc != NULL) sprintf(str1,"anchorFunc,");
     else if (ct && ct->l2top.trX.GetFactor()==1 && ct->l2top.trY.GetFactor()==1)
     sprintf(str1,"anchor(%g,%g,pixel),",anchor.x,anchor.y);
     else sprintf(str1,"anchor(%g,%g),",anchor.x,anchor.y);
     str1 = str1+strlen(str1);
     if (getMarginFunc != NULL) sprintf(str1,"marginFunc)");
     else if (flagPixelSize || marginContainer && marginContainer->l2top.trX.GetFactor()==1 && marginContainer->l2top.trY.GetFactor()==1)
     sprintf(str1,"margin(%g,%g,-%g,-%g,pixel),",xMargin,yMargin,mxMargin,myMargin);
     else
     sprintf(str1,"margin(%g,%g,-%g,-%g),",xMargin,yMargin,mxMargin,myMargin);
     str1 = str1+strlen(str1);
     }
     */
	return (std::string) str;
}


spBox::spBox(spAnchor anchor1,spMargin margin1) :  anchor(anchor1), margin(margin1)
{
}

// Get Global Rect associated to a box (object is the object associated to the box)
spRect spBox::gRect(spObject *object)
{
	if (IsInfinite()) return object->container->gRect();
	
	spContainer *anchorContainer = object->container;
	spContainer *marginContainer = GetContainerMargin(object);
	
	// First we compute the anchor in the marginContainer coordinate
	spRealPoint pt = marginContainer->l2top.IApply(anchorContainer->l2top.Apply(GetAnchor(object)));
	
	// Then we apply the margins and compute the two bounding points defining the bounding rect
	spRealRect r = GetMargin(object);
	spRealPoint mmargin = r.GetPosition();
	spRealPoint margin = r.GetPosition1();
/*    if (IsPixelStraightMargin()) {
        spRealPoint sg = anchorContainer->l2top.GetSign();
        if (sg.x < 0) {
            mmargin.x = -mmargin.x;
            margin.x = -margin.x;
        }
        if (sg.y < 0) {
            mmargin.y = -mmargin.y;
            margin.y = -margin.y;
        }
    }
 */
 	spPoint pt1 = marginContainer->l2top.ApplyInt(pt + mmargin);
	spPoint pt2 = marginContainer->l2top.ApplyInt(pt + margin);
	spRect res = spRect(pt1,pt2);
    
	return res;
}


// Get Global Rect associated to a box (object is the object associated to the box)
spRect spBox::gBoundingRect(spObject *object)
{
	if (IsInfinite()) return object->container->gRect();
	
    spRect res = gRect(object);
	int inflate = margin.GetPixelMargin()+object->GetPenWidth()-1;
	
    if (inflate > 0) res.Inflate(inflate,inflate);
	
	return res;
}


// Get Local Rect in local (i.e., object->container) coordinates
spRealRect spBox::lRect(spObject *object)
{
	if (IsInfinite()) return object->container->l2top.IApply(object->container->gRect());
    
	spContainer *anchorContainer = object->container;
	spContainer *marginContainer = GetContainerMargin(object);
    
	// First we compute the anchor in the marginContainer coordinate
	spRealPoint pt = anchorContainer->l2top.Apply(GetAnchor(object));
	pt = marginContainer->l2top.IApply(pt);
    
	// Then we apply the margins and compute the two bounding points defining the bounding rect
	spRealRect r = GetMargin(object);
	spRealPoint mmargin = r.GetPosition();
	spRealPoint margin = r.GetPosition1();
/*    if (IsPixelStraightMargin()) {
        spRealPoint sg = anchorContainer->l2top.GetSign();
        if (sg.x < 0) {
            mmargin.x = -mmargin.x;
            margin.x = -margin.x;
        }
        if (sg.y < 0) {
            mmargin.y = -mmargin.y;
            margin.y = -margin.y;
        }
    }
 */
    spPoint pt1 = marginContainer->l2top.ApplyInt(pt + mmargin);
    spPoint pt2 = marginContainer->l2top.ApplyInt(pt + margin);
	spRect res = spRect(pt1,pt2);
    
	return object->container->l2top.IApply(res);
}

// Compute the union of rect with local bounding rect
// rect is empty if flagRectEmpty is true
// If an update of the bounding rect must be performed it is performed first
void spBox::UnionFloatRect(spRealRect &rect,spObject *object, bool &flagRectEmpty)
{
	if (object->flagGeometryMustBeUpdated) SPUpdate(false);
    
	
    //	spContainer *anchorContainer = object->container;
	spContainer *marginContainer = margin.GetContainer(object);
    //	int pixelMargin = margin.GetPixelMargin();
    
	
	if (IsEmpty()) return;
	if (IsGrid()) return;
	
	if (IsInfinite()) {
		if (!object->IsContainer()) return;
		spContainer *ct = (spContainer *) object;
		for (int i = 0;i<ct->objectList.Length();i++) {
			ct->objectList[i]->box.UnionFloatRect(rect,ct->objectList[i],flagRectEmpty);
		}
		return;
	}
	
    //	if (anchorContainer != object->container) return;
	
	spRealPoint anch = GetAnchor(object,true);
	spRealRect r;
	if (marginContainer  == object->container) {
		r = GetMargin(object,true);
		spRealPoint pt1 = r.GetPosition();
		spRealPoint pt2 = r.GetPosition1();
		r = spRealRect(anch+pt1,anch+pt2);
	}
	else {
		r = spRealRect(anch.x,anch.y,0,0);
	}
	
	if (flagRectEmpty) rect = r;
	else rect.Union(r);
	flagRectEmpty = false;
	return;
}


void spBox::UnionFloatRect(spRealRect &rect,spObject *object, bool &flagRectEmpty,double xmin, double xmax)
{
	if (object->flagGeometryMustBeUpdated) SPUpdate(false);
    
    	UpdateAnchor(object,xmin,xmax);
    	UpdateMargin(object,xmin,xmax);
	
    //	spContainer *anchorContainer = object->container;
	spContainer *marginContainer = margin.GetContainer(object);
    //	int pixelMargin = margin.GetPixelMargin();
    
	if (IsEmpty()) return;
	if (IsGrid()) return;
	
	if (IsInfinite()) {
		if (!object->IsContainer()) return;
		spContainer *ct = (spContainer *) object;
		for (int i = 0;i<ct->objectList.Length();i++) {
			ct->objectList[i]->box.UnionFloatRect(rect,ct->objectList[i],flagRectEmpty,xmin,xmax);
		}
		return;
	}
	
    //	if (anchorContainer != object->container) return;
	
	spRealPoint anch = GetAnchor(object,false);
	spRealRect r;
	if (marginContainer  == object->container) {
		r = GetMargin(object,false);
		spRealPoint pt1 = r.GetPosition();
		spRealPoint pt2 = r.GetPosition1();
		r = spRealRect(anch+pt1,anch+pt2);
		if ((r.GetPosition().x<xmin && r.GetPosition1().x<xmin) || (r.GetPosition().x>xmax && r.GetPosition1().x>xmax)) return;
		r.x = xmin;
		r.width = xmax-xmin;
	}
	else {
		r = spRealRect(anch.x,anch.y,0,0);
		if (r.x<xmin || r.x>xmax) return;
	}
	
	if (flagRectEmpty) rect = r;
	else rect.Union(r);
	flagRectEmpty = false;
	return;
}

void spBox::Expand_(double &m, double &M, bool flagReverse, double marginView2FatherView, double length, double x, double r, double l)
{
	double R = marginView2FatherView/length;
    //	cout << "--> " << m << " " << M << " " << x << " " << r << " " << l << " " << marginView2FatherView << endl;
    
	//
	// Particular case for a single object
	//
	if (M == m && x == m) {
		if (l > r) {
			if (flagReverse) {
				M = x+fabs(R*l);
				m = M-1;
			}
			else {
                m = x-fabs(R*l);
                M = m+1;
            }
		}
		else {
			if (flagReverse) {
				m = x-fabs(R*r);
				M = m+1;
			}
			else {
				M = x+fabs(R*r);
				m = M-1;
			}
		}
		return;
	}
	
	double alpham = 1;
	double alphaM = 1;
	double alpha = alpham+alphaM;
	
	//
	// General case
	//
	
    R = (flagReverse ? -R : R);
    
	double Q,P;
	double min=-DBL_MAX;
	double max=DBL_MAX;
	bool flagPossible = true;
	double epsilon;
	
	Q = 1+alpha*R*r;
	P = m-x-(M-m)*R*r;
	if (Q > 0) min = MAX(min,P/Q);
	else if (Q < 0) max = MIN(max,P/Q);
	else if (P>0) flagPossible = false;
	
	Q = 1-alpha*R*r;
	P = x-M+(M-m)*R*r;
	if (Q > 0) min = MAX(min,P/Q);
	else if (Q < 0) max = MIN(max,P/Q);
	else if (P>0) flagPossible = false;
    
	Q = 1-alpha*R*l;
	P = m-x+(M-m)*R*l;
	if (Q > 0) min = MAX(min,P/Q);
	else if (Q < 0) max = MIN(max,P/Q);
	else if (P>0) flagPossible = false;
    
	Q = 1+alpha*R*l;
	P = x-M-(M-m)*R*l;
	if (Q > 0) min = MAX(min,P/Q);
	else if (Q < 0) max = MIN(max,P/Q);
	else if (P>0) flagPossible = false;
	
	if (min>max) flagPossible = false;
	if (!flagPossible) return;
	
	if (min <= 0 && 0<=max) return;
    if (max <= 0) return;
	if (min >=0) epsilon = min;
	
	m = m-alpham*epsilon;
	M = M+alphaM*epsilon;
}

void spBox::ExpandX(spObject *object, double &m, double &M)
{
	if (IsEmpty()) return;
    
	if (IsGrid()) return;
	
	spContainer *anchorContainer = object->container;
	spContainer *marginContainer = margin.GetContainer(object);
	int pixelMargin = margin.GetPixelMargin();
    
    spRealRect r = margin.Get(object,false);
    spRealPoint pt = r.GetPosition();
	double mxMargin = -pt.x;
    //	double myMargin = -pt.y;
    pt = r.GetPosition1();
	double xMargin = pt.x;
    //	double yMargin = pt.y;
    
	if (IsInfinite()) {
		if (!object->IsContainer()) return;
		spContainer *ct = (spContainer *) object;
		for (int i = 0;i<ct->objectList.Length();i++) {
			ct->objectList[i]->box.ExpandX(ct->objectList[i],m,M);
		}
		return;
	}
	
	if (anchorContainer != object->container) return;
	if (marginContainer == anchorContainer && pixelMargin == 0) return;
    
	spRealPoint anch = GetAnchor(object,false);
    
    //	cout << "ExpandX " << object->container->lBoundingRect().width << endl;
	if (pixelMargin == 0 || 1 ) {
		if (!marginContainer->l2top.trX.GetFlagLn())
            Expand_(m,M,anchorContainer->flags & spFLAG_VIEW_REVERSE_X,marginContainer->l2top.trX.GetFactor()/object->container->container->l2top.trX.GetFactor(),fabs(object->container->lRect().width),anch.x,xMargin,mxMargin);
	}
	else {
        //		cout << "??? PIXEL " << pixelMargin << endl;
        //		cout << m << " " << " " << M << " " << anchor.x << endl;
		if (!marginContainer->l2top.trX.GetFlagLn()) {
            Expand_(m,M,anchorContainer->flags & spFLAG_VIEW_REVERSE_X,1/object->container->container->l2top.trX.GetFactor(),fabs(object->container->lRect().width),anch.x,pixelMargin,pixelMargin);
            Expand_(m,M,anchorContainer->flags & spFLAG_VIEW_REVERSE_X,1/object->container->container->l2top.trX.GetFactor(),fabs(object->container->lRect().width),anch.x+xMargin,pixelMargin,pixelMargin);
		}
	}
}


void spBox::ExpandY(spObject *object, double &m, double &M,double xmin,double xmax)
{
	if (IsEmpty()) return;
    
    /*	ComputeAnchor(object,xmin,xmax);
     ComputeMargin(object,xmin,xmax); */
    
	if (IsGrid()) return;
    
	spContainer *anchorContainer = object->container;
	spContainer *marginContainer = margin.GetContainer(object);
	int pixelMargin = margin.GetPixelMargin();
    spRealRect r = margin.Get(object,false);
    spRealPoint pt = r.GetPosition();
    //	double mxMargin = -pt.x;
	double myMargin = -pt.y;
    pt = r.GetPosition1();
    //	double xMargin = pt.x;
	double yMargin = pt.y;
	
	
	if (IsInfinite()) {
		if (!object->IsContainer()) return;
		spContainer *ct = (spContainer *) object;
		for (int i = 0;i<ct->objectList.Length();i++) {
			ct->objectList[i]->box.ExpandY(ct->objectList[i],m,M,xmin,xmax);
		}
		return;
	}
    
	if (anchorContainer != object->container) return;
	if (marginContainer == anchorContainer && pixelMargin == 0) return;
    
	spRealPoint anch = GetAnchor(object,false);
	if (xmin<xmax && (anch.x<xmin || anch.x>xmax)) return;
	
	
    //	cout << "ExpandY " << object->container->lBoundingRect().height << endl;
	if (pixelMargin == 0)  {
		if (!marginContainer->l2top.trY.GetFlagLn())
            Expand_(m,M,anchorContainer->flags & spFLAG_VIEW_REVERSE_Y,marginContainer->l2top.trY.GetFactor()/object->container->container->l2top.trY.GetFactor(),fabs(object->container->lRect().height),anch.y,yMargin,myMargin);
	}
	else {
		if (!marginContainer->l2top.trX.GetFlagLn()) {
            Expand_(m,M,anchorContainer->flags & spFLAG_VIEW_REVERSE_Y,1/object->container->container->l2top.trY.GetFactor(),fabs(object->container->lRect().height),anch.y,pixelMargin,pixelMargin);
            Expand_(m,M,anchorContainer->flags & spFLAG_VIEW_REVERSE_Y,1/object->container->container->l2top.trY.GetFactor(),fabs(object->container->lRect().height),anch.y+yMargin,pixelMargin,pixelMargin);
		}
	}
}


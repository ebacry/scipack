
// Not a module

%rename(ICursorView) spInteractorCursorView;

%rename(_SetOffset) SetOffset;
%rename(_GetOffset) GetOffset;

%rename(_GetFollowMouse) GetFollowMouse;
%rename(_SetFollowMouse) SetFollowMouse;

%rename(_GetFullInfo) GetFullInfo;
%rename(_SetFullInfo) SetFullInfo;

// We set the kwargs for some methods
%feature("kwargs") spInteractorCursorView::spInteractorCursorView;

class spInteractorCursorView : public spInteractorN {
	
	public : 
	
    // The string representation in python
    ObjRepr;

    // followMouse flag
    bool GetFollowMouse();
    void SetFollowMouse(bool flag);
    %attobj_def(followMouse,FollowMouse,"""""");

    // Offset
    void SetOffset(spPoint offset1);
	spPoint GetOffset();
    %attobj_def(offset,Offset,"""""");

    // Offset
    void SetFullInfo(bool fullInfo);
	bool GetFullInfo();
    %attobj_def(fullInfo,FullInfo,"""""");


	void SetOutput(spObject *o);

	spInteractorCursorView(spObject *cursor, spPoint offset = spPoint(0,0), bool flagFollowMouse = true, bool flagPropagate=true,const char *name = NULL);
	~spInteractorCursorView();	
};


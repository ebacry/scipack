/*
 *  object_smart_ptr.hpp
 *  SciPlot
 *
 *  Created by bacry on 12/07/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


//
// 
// This is a template definition that allows to define a pointer to an object of class KLASS that is automatically set to NULL when the object is deleted
//
//
// The syntax is either
//
//    spObjectSmartPtr<KLASS> ptr;
//
// One can use the macro instead 
//
//    SP_SMARTPTR(KLASS) ptr;
//
// The ptr can then be used as a pointer to an object of class KLASS (e.g., ptr->field)
//



class spInteractorObjectSmartPtr;

template <class T> class spObjectSmartPtr
{
	// The object it points to
	T *object;
    	
	// The interactor that manages setting the above object to NULL
	spInteractorObjectSmartPtr *interactor;
	

	public :
	
	// Constructor
	spObjectSmartPtr(T *object=NULL);
		
  // Destructor
	virtual ~spObjectSmartPtr();

	// assignation : smartPtr1 = object
	void operator=(T* rhs);
	
	// cast operator to T*
	inline operator T *() {
		return object;
	};
	
	// Pointer operator	
	inline T * operator ->() {
		return object;
	}

	// The * unary operator
/*	spObject& operator*() {
		return *object;
	}*/
	
	// cast operator to boolean 
	operator bool() const {
		return object != NULL;
	}	
};

//
// The corresponding interactor
//
class spInteractorObjectSmartPtr : public spInteractor1 {

	public :	
	void **field;
	spInteractorObjectSmartPtr(void **field);

	virtual const char *GetClassName() {
		return _CCHAR("spInteractor1");
	}
};


//
// Template definitions
//

template <class T> spObjectSmartPtr<T>::spObjectSmartPtr(T *object1)
{
	object = object1;
	interactor = NULL;
	if (object) {
        void **field = (void **) &object;
        interactor = new spInteractorObjectSmartPtr(field);
        object->AddInteractor(interactor);
        interactor->Ref();
    }
}

template <class T>  void spObjectSmartPtr<T>::operator=(T* object1)
{
	if (interactor) {
		if (object) object->RemoveInteractor(interactor);
        interactor->Unref();
	}
	object = object1;
	interactor = NULL;
	if (object) {
        void **field = (void **) &object;
        interactor = new spInteractorObjectSmartPtr(field);
        object->AddInteractor(interactor);
        interactor->Ref();
    }
}

template <class T>  spObjectSmartPtr<T>::~spObjectSmartPtr()
{
	if (interactor) {
		if (object) object->RemoveInteractor(interactor);
        interactor->Unref();
    }
}

#define SP_SMARTPTR(objectClass) \
spObjectSmartPtr<objectClass>

typedef spObjectSmartPtr<spObject> spWeakObject;



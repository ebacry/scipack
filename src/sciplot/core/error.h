/*
 *  error.h
 *  sp
 *
 *  Created by bacry on 17/11/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */



#ifndef _SP_ERROR_H
#define _SP_ERROR_H

#include <string.h>


#define _CCHAR(x) const_cast<char *>(x)


extern void spWarningf(const char *funcname, const char *format,...);

#define THROW(str) \
throw const_cast<char *>(str)

#define DECLARETHROW throw(const char *)

#endif

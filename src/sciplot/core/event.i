
// Not a module

%rename(OnNone) spOnNone;
%rename(OnKeyDown) spOnKeyDown;
%rename(OnDownLeft) spOnDownLeft;
%rename(OnDownRight) spOnDownRight;
%rename(OnDownMiddle) spOnDownMiddle;
%rename(OnUpLeft) spOnUpLeft;
%rename(OnUpRight) spOnUpRight;
%rename(OnUpMiddle) spOnUpMiddle;
%rename(OnDLeft) spOnDLeft;
%rename(OnDRight) spOnDRight;
%rename(OnDMiddle) spOnDMiddle;
%rename(OnEnter) spOnEnter;
%rename(OnLeave) spOnLeave;
%rename(OnMsge) spOnMsge;
%rename(OnMotion) spOnMotion;
%rename(OnDragLeft) spOnDragLeft;
%rename(OnDragRight) spOnDragRight;
%rename(OnDragMiddle) spOnDragMiddle;
%rename(OnMotionLeft) spOnMotionLeft;
%rename(OnMotionRight) spOnMotionRight;
%rename(OnMotionMiddle) spOnMotionMiddle;
%rename(OnAnyMotion) spOnAnyMotion;
%rename(OnComputeBound) spOnComputeBound;
%rename(OnChangeBound) spOnChangeBound;
%rename(OnDelete) spOnDelete;
%rename(OnChangedGeometry) spOnChangedGeometry;
%rename(OnMouseWheel) spOnMouseWheel;
%rename(OnTime) spOnTime;
%rename(_LAST_EVENT_SIMPLETYPE) spLAST_EVENT_SIMPLETYPE;

%rename(Event) spEvent;
%rename(EventType) spEventType;

//
// The diffent types of event
//
typedef enum {

	spOnNone = 0,
    spOnKeyDown,

	spOnDownLeft,
	spOnUpLeft,
	spOnDragLeft,
	spOnDLeft,
	spOnMotionLeft,
    
	spOnDownRight,
    spOnUpRight,
	spOnDragRight,
	spOnDRight,
	spOnMotionRight,
	
	spOnDownMiddle,
	spOnUpMiddle,
	spOnDragMiddle,
	spOnDMiddle,
	spOnMotionMiddle,

    spOnMotion,
    spOnAnyMotion,
    
	spOnEnter,
	spOnLeave,
	spOnMsge,
	spOnBeforeDraw,
	spOnAfterDraw,
	spOnComputeBound,
	spOnChangeBound,
	spOnDelete,
	spOnChangedGeometry,	
	spOnMouseWheel,	
	spOnTime,
        
    spLAST_EVENT_SIMPLETYPE

} spEventSimpleType;



%rename(ModNone) SP_MOD_NONE;
%rename(ModShift) SP_MOD_SHIFT;
%rename(ModMeta) SP_MOD_META;
%rename(ModCtrl) SP_MOD_CTRL;
%rename(ModAlt) SP_MOD_ALT;

enum {
	SP_MOD_NONE = 0,
	SP_MOD_SHIFT = 1<<0,
	SP_MOD_META = 1<<1,
	SP_MOD_CTRL = 1<< 2,
	SP_MOD_ALT = 1 << 3
};


//
// EventType class (holds a type and either modifier or a key)
//

// We start by the typemaps
%typemap(in) spEventType {
    void *ptr;
    
    // Case of an eventType object
    if (SWIG_IsOK(SWIG_ConvertPtr($input, &ptr,SWIGTYPE_p_spEventType, 0 |  0 ))) {
        $1 = *((spEventType *) ptr);
    }
    
    // case spOnDownLeft
    else if (PyInt_Check($input)) {
        $1 = spEventType(PyInt_AsLong($input));
    }

    // case of 'z'
    else if (PyString_Check($input) && PyString_Size($input) == 1) {
        char *str = PyString_AsString($input);
        $1 = spEventType(spOnKeyDown,str[0]);
    }

    // case of (spOnDownLeft,spModShift)
    else if (PyTuple_Check($input) && PyTuple_Size($input)==2) {
        int t;
        long m;
        char c;
        if (!PyArg_ParseTuple($input,"il",&t,&m)) {
            if (!PyArg_ParseTuple($input,"ic",&t,&c)) {
                PyErr_SetString(PyExc_RuntimeError,"Argument should be an eventType");
                return NULL;
            }
            $1 = spEventType(t , (unsigned long) c);
        }
        else $1 = spEventType(t , (unsigned long) m);
    }
    else {
        PyErr_SetString(PyExc_RuntimeError,"Argument should be an eventType");
        return(NULL);
    }
}
%typemap(typecheck,precedence=SWIG_TYPECHECK_INTEGER) spEventType {
    if (SWIG_IsOK(SWIG_ConvertPtr($input, NULL,SWIGTYPE_p_spEventType, 0 |  0 ))) $1=1;
    else if (PyInt_Check($input)) $1=1;
    else if (PyString_Check($input)) $1=1;
    else if (PyTuple_Check($input) && PyTuple_Size($input)==2) $1=1;
    else $1=0;
}


//
// Then EventType the class
//
%DocClass(spEventType,"A class that holds an event type. They are two ways for init :
    - sp.EventType(type,modifier=spModNone)
    - sp.EventType(character or keycode) (for Key Down Event if the key does not correspond to a printable key, use the wx.keycode instead, e.g. wx.WXK_TAB or wx.WXK_F1)
where type is one of spOnNone, spOnKeyDown, spOnDownLeft, spOnDownMiddle, spOnDownRight, spOnUpLeft, spOnUpMiddle, spOnUpRight, spOnDLeft, spOnDMiddle, spOnDRight, spOnMotion, spOnDragLeft, spOnDragMiddle, spOnDragRight, spOnAnyMotion, spOnEnter, spOnLeave, spOnMsge, spOnComputeBound, spOnChangeBound, spOnMouseWheel,spOnTime
and modifier a combination (addition) of spModShift, spModCtrl, spModAlt, spModMeta.")


class spEventType {

    public :
    
    //
    // This the __repr__ and __str__
    %extend {
        %pythoncode %{
            _eventTypesDict = {OnNone:'spOnNone',OnKeyDown:'spOnKeyDown',OnDownLeft:'spOnDownLeft',OnDownMiddle:'spOnDownMiddle',OnDownRight:'spOnDownRight',OnUpLeft:'spOnUpLeft',OnUpMiddle:'spOnUpMiddle',OnUpRight:'spOnUpRight',OnDLeft:'spOnDLeft',OnDMiddle:'spOnDMiddle',OnDRight:'spOnDRight',OnMotion:'spOnMotion',OnDragLeft:'spOnDragLeft',OnDragMiddle:'spOnDragMiddle',OnDragRight:'spOnDragRight',OnMotionLeft:'spOnMotionLeft',OnMotionMiddle:'spOnMotionMiddle',OnMotionRight:'spOnMotionRight',OnAnyMotion:'spOnAnyMotion',OnEnter:'spOnEnter',OnLeave:'spOnLeave',OnMsge:'spOnMsge',OnComputeBound:'spOnComputeBound',OnChangeBound:'spOnChangeBound',OnDelete:'spOnDelete',OnMouseWheel:'spOnMouseWheel',OnTime:'spOnTime'}

            def __repr__(self):
                if (self.type==OnKeyDown):
                    if (self.key==wx.WXK_ESCAPE): return "spEventType(spOnKeyDown,wx.WXK_ESCAPE)"
                    if (self.key==wx.WXK_TAB): return "spEventType(spOnKeyDown,wx.WXK_TAB)"
                    if (self.key >= 32 and self.key <= 126):
                        return "spEventType('"+chr(self.key)+"')"
                    else :
                        if (self.key==wx.WXK_RIGHT): return "spEventType(spOnKeyDown,wx.WXK_RIGHT)"
                        if (self.key==wx.WXK_LEFT): return "spEventType(spOnKeyDown,wx.WXK_LEFT)"
                        if (self.key==wx.WXK_UP): return "spEventType(spOnKeyDown,wx.WXK_UP)"
                        if (self.key==wx.WXK_DOWN): return "spEventType(spOnKeyDown,wx.WXK_DOWN)"
                        if (self.key==wx.WXK_F1): return "spEventType(spOnKeyDown,wx.WXK_F1)"
                        if (self.key==wx.WXK_F2): return "spEventType(spOnKeyDown,wx.WXK_F2)"
                        if (self.key==wx.WXK_F3): return "spEventType(spOnKeyDown,wx.WXK_F3)"
                        if (self.key==wx.WXK_F4): return "spEventType(spOnKeyDown,wx.WXK_F4)"
                        if (self.key==wx.WXK_F5): return "spEventType(spOnKeyDown,wx.WXK_F5)"
                        if (self.key==wx.WXK_F6): return "spEventType(spOnKeyDown,wx.WXK_F6)"

                        return "spEventType(spOnKeyDown,"+str(self.key)+")"

                mod = ""
                if (self.modifiers & ModShift): mod = mod + " + spModShift"
                if (self.modifiers & ModCtrl): mod = mod + " + spModCtrl"
                if (self.modifiers & ModAlt): mod = mod + " + spModAlt"
                if (self.modifiers & ModMeta): mod = mod + " + spModMeta"
                if (self.modifiers == ModNone): mod = "spModNone"
                else: mod = mod[3:]
                
                if (self.type <OnNone or self.type >= _LAST_EVENT_SIMPLETYPE): type = "?"
                else: type = self._eventTypesDict[self.type]
                return "spEventType("+type+","+mod+")"

            def __str__(self):
                return self.__repr__()
        %}
    }
    
    // The fields
    int type;
    unsigned long modifiers;

    spEventType(spEventType eventType);
    spEventType(int eventSimpleType, unsigned long modifiers1=SP_MOD_NONE);

    %DocMeth(DownToUp,"DownToUp() -> spEventType","Transforms a down button event type to the corresponding up button event type (e.g., spOnDownMiddle to spOnUpMiddle)")
    spEventType DownToUp();
    %DocMeth(UpToDown,"UpToDown() -> spEventType","Transforms a up button event type to the corresponding down button event type (e.g., spOnUpMiddle to spOnDownMiddle)")
    spEventType UpToDown();
    %DocMeth(DownToDrag,"DownToDrag() -> spEventType","Transforms a down button event type to the corresponding drag button event type (e.g., spOnDownMiddle to spOnDragMiddle)")
    spEventType DownToDrag();
    %DocMeth(DragToDown,"DragToDown() -> spEventType","Transforms a drag button event type to the corresponding down button event type (e.g., spOnDragMiddle to spOnDownMiddle)")
    spEventType DragToDown();
    %DocMeth(UpToDrag,"UpToDrag() -> spEventType","Transforms a up button event type to the corresponding drag button event type (e.g., spOnUpMiddle to spOnDragMiddle)")
    spEventType UpToDrag();
    %DocMeth(DragToUp,"DragToUp() -> spEventType","Transforms a drag button event type to the corresponding up button event type (e.g., spOnDragMiddle to spOnUpMiddle)")
    spEventType DragToUp();

    %DocMeth(IsButtonDown,"IsButtonDown() -> bool","Returns True if the event type corresponds to a mouse button down event.")
    bool IsButtonDown();
    %DocMeth(IsButtonUp,"IsButtonUp() -> bool","Returns True if the event type corresponds to a mouse button up event.")
    bool IsButtonUp();
    %DocMeth(IsButtonDrag,"IsButtonDrag() -> bool","Returns True if the event type corresponds to a mouse button drag event.")
    bool IsButtonDrag();
};

//
// The key field (same as modifiers)
//
%extend spEventType {
    unsigned long key;
}
%{
    unsigned long spEventType_key_get(spEventType *eventType) {return eventType->modifiers;}
    void spEventType_key_set(spEventType *eventType, unsigned long key) {eventType->modifiers = key;}
%}


//
// Then Event class
//
%DocClass(spEvent,"This is the class that codes an event. An event object is passed to the callback of an interactor. The fields are
    - object : the object which received the event
    - mouse : a sp.Point that indicates the position of the mouse (topcontainer coordinate)
    - eventType : the eventType of this event
    - wheel : an (signed) integer used for OnWheel events
    - keycode : the (wx) keycode for OnKeyDown events
    - xMin,yMin,xMax,yMax : this is used to store bounds values for OnComputeBound events (is used for unzoom of views)
    - flagXFixed : is used to indicate if xMin and xMax should ot be modified for OnComputeBound events
    - senderObject and receiverObject : are used for OnMsge events")

class spEvent {
  
	public : 
		
	int wheel;                   // rotation value of the wheel in case of spOnMouseWheel
	int keycode;

	spEvent();
    virtual ~spEvent();

	double xMin,yMin,xMax,yMax;
	bool flagXFixed;
};

//
// Get object, sender and receiver
//
%extend spEvent {
    %immutable;
    PyObject *object;
    PyObject *senderObject;
    PyObject *receiverObject;
    spPoint mouse;
    spEventType eventType;
    %DocMeth(IsTimerEvent,"IsTimerEvent(timer) -> bool","Returns True if event is OnTime and if the spTimer which triggered this event is timer")
    %mutable;
    bool IsTimerEvent(spTimer *timer) {return $self->timer == timer;}
}
%{
    spEventType *spEvent_eventType_get(spEvent *event) {
        if (event->type == spOnKeyDown) return new spEventType(event->type,event->keycode);
        else return new spEventType(event->type,event->modifiers);
    }
    spPoint *spEvent_mouse_get(spEvent *event) {return new spPoint(event->mouse);}
    PyObject *spEvent_object_get(spEvent *event) {return WrapObject(event->object);}
    PyObject *spEvent_senderObject_get(spEvent *event) {return WrapObject(event->sender);}
    PyObject *spEvent_receiverObject_get(spEvent *event) {return WrapObject(event->receiver);}
%}


//
// Then Timer class
//
%rename(Timer) spTimer;

%DocClass(spTimer,"A class that implements a timer which goes of periodically or just one shot. It triggers an OnTime event and send it to the object which is passed in the Start method.")
class spTimer
{
	public :

	spTimer();
	virtual ~spTimer();
	
    %DocMeth(GetInterval,"GetInterval() -> in (milliseconds)","Get the period of the alarm")
	int GetInterval();
    %DocMeth(IsOneShot,"IsOneShot() -> bool","Returns True if timer is one shot.")
	bool IsOneShot() const;
    %DocMeth(IsRunning,"IsRunning() -> bool","Returns True if timer is running.")
	bool IsRunning() const;
    %DocMeth(Start,"Start(object,milliseconds=-1,oneShot=False) -> bool","Starts the timer which will trigger the alarm wit ha delay of milliseconds (periodically if oneShot==False). When the alarm goes off, it will send an OnTime event to object. It returns True if no error.")
	bool Start(spObject *object,int milliseconds = -1, bool oneShot = false);
    %DocMeth(Stop,"Stop()","Stops the timer")
	void Stop();
};


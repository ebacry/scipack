//
//  comment.h
//  sp
//
//  Created by bacry on 25/01/13.
//
//

#ifndef __sp__comment__
#define __sp__comment__


class spComment : public spGroup
{
    public :
    
    spComment(spContainer *ct=NULL,spRealPoint pt = spRealPoint(0,0), const char *str = "TEXT", unsigned long flags=0,const char *name = NULL);
    virtual ~spComment();
    
    spShape *line;
    spText *text;
    spRealPoint dist,anchor;
    inline spRealPoint GetDist() {return dist;}
    inline void  SetDist(spRealPoint pt) {dist=pt;}
    
    virtual void NotifyObjectGeometryUpdate(spObject *);
    
    virtual const char *GetClassName() {
		return _CCHAR("spComment");
	}
    
    spShape * GetShapeObject(){return line;}
    spText * GetTextObject(){return text;}

    virtual spRealPoint GetAnchor() {return anchor;}
    virtual void SetAnchor(spRealPoint anchor);
    virtual void Move(double dx,double dy);

};

#endif /* defined(__sp__comment__) */

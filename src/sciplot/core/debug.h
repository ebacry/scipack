/*
 *  debug.h
 *  sp
 *
 *  Created by bacry on 17/11/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */



#ifndef _SP_DEBUG_H
#define _SP_DEBUG_H

enum 
{
  spDebugNothing = 0,
  spDebugEventsMouse = (1<<0),
  spDebugEventsMotion = (1<<1),
  spDebugGeometryUpdate = (1<<2),
  spDebugCreatingContainer = (1<<3),
  spDebugDraw = (1<<4),	
  spDebugUpdatePaint = (1<<5),	
	spDebugResize = (1<<6),
	spDebugOverlay = (1<<7),
	spDebugAlloc = (1<<8),
	spDebugRef = (1<<9),
	spDebugAddingObject = (1<<10)
};

extern unsigned long spDebug;
extern void spSetDebug(unsigned long);

extern void spDebugPrint(char *str);

#define SPDEBUG(flag,str) \
if (spDebug & (flag)) { \
cerr << str << endl; \
}

#endif

// Not a module

%rename(Group) spGroup;

// The container class is a director
%feature("director") spGroup;

// Some methods with named parameters
%feature("kwargs") spGroup::spGroup;


%DocClass(spGroup,"This is a container class which should be used to group objects together and specify the geometry links between them (i.e., some are fixed some are moveable, some move with others, .... see sp.Comment for an example of a group with 2 elements). The particularity of the group is that the drawing of a group ONLY consists in drawing all its elements. One you added all the objects you wanted to a group you should lock it.")
class spGroup : public spContainer
{
    public:
    
    ObjRepr;

    spGroup(spContainer *ct=NULL,unsigned long flags=0,const char *name = NULL);
    virtual ~spGroup();
    
	virtual bool IsGroup();

    %DocMeth(NotifyObjectGeometryUpdate,"NotifyObjectGeometryUpdate(object)","Is called to notify that some attributes of the object (which is an object of the group) has changed. You need to overload this method in order to perform the geometry links you want.")
    virtual void NotifyObjectGeometryUpdate(spObject *);
    
    %DocMeth(FixObject,"FixObject(object)","Notifies that the object of the group cannot be moved directly (it can however be moved by the group itself, i.e., in the method NotifyObjectGeometryUpdate).")
	void FixObject(spObject *object);
    
    %DocMeth(Lock,"Lock()","Locking the group. No more objects can be added afterwards.")
    void Lock();

    %DocMeth(UnLock,"UnLock()","Unlocking the group")
    void UnLock();

    %DocMeth(IsLocked,"IsLocked() -> bool","Returns True if group is locked.")
    bool IsLocked();
};

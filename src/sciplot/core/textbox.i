
// Not a module

%rename(TextBox) spTextBox;

%rename(_SetText) SetText;
%rename(_GetText) GetText;
%rename(_SetFrame) SetFrame;
%rename(_GetFrame) GetFrame;
%rename(_GetHMode) GetHMode;
%rename(_SetHMode) SetHMode;

%rename(JUSTIFY_HLEFT) spJustifyHLeft;
%rename(JUSTIFY_HRIGHT) spJustifyHRight;
%rename(JUSTIFY_HBRIGHT) spJustifyHBRight;
%rename(JUSTIFY_HMIDDLE) spJustifyHMiddle;
%rename(JUSTIFY_HBMIDDLE) spJustifyHBMiddle;
%rename(JUSTIFY_VUP) spJustifyVUp;
%rename(JUSTIFY_VDOWN) spJustifyVDown;
%rename(JUSTIFY_VBASE) spJustifyVBase;
%rename(JUSTIFY_VMIDDLE) spJustifyVMiddle;
%rename(JUSTIFY_VMIDDLE_UP) spJustifyVMiddleUp;

enum {
    spJustifyHLeft = 0,
    spJustifyHRight,
    spJustifyHBRight,
    spJustifyHMiddle,
    spJustifyHBMiddle,
    spJustifyVUp,
    spJustifyVDown,
    spJustifyVBase,
    spJustifyVMiddle,
    spJustifyVMiddleUp
};

%rename(_SetText) SetText;
%rename(_GetText) GetText;


%feature("director") spTextBox;

// We set the kwargs for some methods
%feature("kwargs") spTextBox::spTextBox;

%DocClass(spTextBox,"Object that displays a text which is justified relative to a rectangle defined by the anchor and margin of the object. The text is always centered vertically. Horizontally the hMode lets you choose the justification you want.")
class spTextBox : public spShape
{
	public : 
	
    ObjRepr;

	spTextBox(spContainer *ctn=NULL,const spAnchor &anchor=spAnchor(),const spMargin &margin=spMargin(), int hmode1=spJustifyHLeft, unsigned long flags=0,const char *name=NULL);
	
	virtual void Draw(spRect &rect);

    // Attribute : text
	virtual void SetText(const char *str);
	const char *GetText();
    %attobj_def(text,Text,
    """
    **Attribute** self.text = string
    
    The string to be displayed
    """);

    // Attribute : frame
	void SetFrame(bool flag);
	bool GetFrame();
    %attobj_def(frame,Frame,
    """
    **Attribute** self.frame = True or False
    
    If True a frame is drawn
    """);
    

    // Attribute : hMode
	void SetHMode(int hmode);
	int GetHMode();
    %attobj_def(hMode,HMode,
    """
    **Attribute** self.hMode = hMode
    
    An integer that indicates how the text is justify horizontally.
    One of JUSTIFY_HLEFT, JUSTIFY_HRIGHT, JUSTIFY_HBRIGHT,
    JUSTIFY_HMIDDLE, JUSTIFY_HBMIDDLE (the letter B stands for BLOCK justification, i.e., multilines)
    """);
};








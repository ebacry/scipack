
// Not a module

%rename(Grid) spGrid;


%feature("director") spGrid;

// Some methods with named parameters
%feature("kwargs") spGrid::spGrid;

%DocClass(spGrid,"A container class which divides its space in a table of size n x m. To place the objects in the grid using this table you must use sp.AnchorGrid and sp.MarginGrid. There is a margin all around the table and a inter-space between all the row and the columns.")
class spGrid : public spContainer
{
	public :
	
    ObjRepr;

    %extend {
        PyObject *_GetGrid() {
            int res[2];
            res[0] = $self->nGrid;
            res[1] = $self->mGrid;
            return(WrapIntArray(res,2));
        }
        void _SetGrid(int nGrid, int mGrid) {
            $self->AttBeforeUpdate();
            $self->nGrid = nGrid;$self->mGrid = mGrid;
            $self->AttAfterUpdate();
        }
        %attobj_arglist_def(grid,Grid,
        """
        **Attribute** self.grid = [nGrid mGrid]
            
        Number of rows and columns in the grid
        """
        );

        void _SetMargins(int rightMarginGrid,int downMarginGrid,int leftMarginGrid,int upMarginGrid,int interMarginGrid) {
            $self->AttBeforeUpdate();
            $self->rightMarginGrid = rightMarginGrid;
            $self->downMarginGrid = downMarginGrid;
            $self->leftMarginGrid = leftMarginGrid;
            $self->upMarginGrid = upMarginGrid;
            $self->interMarginGrid = interMarginGrid;
            $self->AttAfterUpdate();
        }
        PyObject *_GetMargins() {
            int res[5];
            res[0] = $self->rightMarginGrid;
            res[1] = $self->downMarginGrid;
            res[2] = $self->leftMarginGrid;
            res[3] = $self->upMarginGrid;
            res[4] = $self->interMarginGrid;
            return(WrapIntArray(res,5));
        }
        %attobj_arglist_def(margins,Margins,
        """
        **Attribute** self.margins = [rightMargin downMargin leftMargin upMargin interMargin]
        
        The margins around the table and the inter margin in between each column and row.
        """)
    }
/*
	wxRealPoint GetGridSize(spRealPoint pt);
	wxRealPoint GetGridPoint(spRealPoint pt);
*/	
	
	spGrid(spContainer *container,const spAnchor &anchor=spAnchor(),const spMargin &margin=spMargin(),unsigned long flags=0,const char *name=NULL);
  
    virtual ~spGrid();
    
	virtual bool IsGrid();

};


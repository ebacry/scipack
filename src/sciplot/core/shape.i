
// Not a module

%rename(SHAPE_ELLIPSE) spShapeEllipse;
%rename(SHAPE_RECT) spShapeRect;
%rename(SHAPE_LINE) spShapeLine;
%rename(SHAPE_INFINITELINE) spShapeInfiniteLine;
%rename(SHAPE_ARROW) spShapeArrow;
%rename(SHAPE_DOUBLE_ARROW) spShapeDoubleArrow;
%rename(Shape) spShape;

typedef enum spshape {
	spShapeEllipse = 0,
	spShapeRect,
    spShapeLine,
    spShapeInfiniteLine,
    spShapeArrow,
    spShapeDoubleArrow
} spShapeType;


%feature("director") spShape;

%feature("kwargs") spShape::spShape;

%rename(_GetShape) GetShape;
%rename(_SetShape) SetShape;
%rename(_GetPoint) GetPoint;
%rename(_GetArrowLength) GetArrowLength;
%rename(_SetArrowLength) SetArrowLength;
%rename(_GetArrowAngle) GetArrowAngle;
%rename(_SetArrowAngle) SetArrowAngle;
%rename(_GetTolerance) GetTolerance;
%rename(_SetTolerance) SetTolerance;


class spShape : public spObject
{
	public : 
	
    ObjRepr;
    
    void SetShape(spShapeType shape);
    spShapeType GetShape();
    %attobj_def(shape,Shape,"""""");

    void SetTolerance(unsigned int tolerance);
    unsigned int GetTolerance();
    %attobj_def(tolerance,Tolerance,"""""");

    void SetArrowLength(int arrowLength);
    int GetArrowLength() {return arrowLength;}
    %attobj_def(arrowLength,ArrowLength,"""""");

    void SetArrowAngle(double arrowAngle);
    double GetArrowAngle() {return arrowAngle;}
    %attobj_def(arrowAngle,ArrowAngle,"""""");

    spShape(spContainer *ctn=NULL,spShapeType shape=spShapeRect,const spAnchor &anchor=spAnchor(),const spMargin &margin=spMargin(), unsigned long flags=0,const char *name=NULL);
//	spShape(spContainer *container=NULL,spShapeType shape1=spShapeRect,spRealPoint anchorPoint= spRealPoint() ,spRealPoint point= spRealPoint() ,unsigned long flags=0,const char *name=NULL);
	
	virtual void Draw(spRect &rect);
};


// Change log --> Update des axes
// Pb du dcBuffer




#include "sp.h"

const char *CursorCBView(spObject *object, bool flagFullStr, spRealPoint &pt)
{
	static char str[10000];
	spView *view = (spView *) object;
	spRealPoint pt1 = view->l2top.IApply(pt);
	if (flagFullStr) sprintf(str,"x=%g y=%g",pt1.x,pt1.y);
	else {
		sprintf(str,"x=%g\ny=%g",pt1.x,pt1.y);
	}
	return str;
}


void spView::InitView()
{
	SetCursorCallback(CursorCBView);
}

spView::spView(spContainer *ctn,const spAnchor &anchor,const spMargin &margin,unsigned long flags1,const char *name1) : spContainer(ctn,anchor,margin,flags1,name1)
{
	InitView();
    SetXYBound(0,1,0,1);
}

spView::~spView()
{
}

void spView::UpdateGeometry()
{
	//	if (!flagHidden) return;	
/*	if (this->name) {		
		SPDEBUG(spDebugGeometryUpdate, "--> UpdateGeometryMain " << this->name << " "<< gBoundingRect << " l2g=" << l2g << " l2top=" << l2top);
	}
  else {		
		SPDEBUG(spDebugGeometryUpdate, "--> UpdateGeometryMain " << this << " "<< gBoundingRect << " l2g=" << l2g << " l2top=" << l2top);
	}
*/
	
	flagGeometryMustBeUpdated = false;

	// If we are dealing with a resize event and the bounds need to be kept we need to recompute l2g accordingly
	if (flags & spFLAG_VIEW_KEEPBOUND_WHEN_RESIZED && topContainer->ResizeEvent()) Recomputel2g();
		
	// In any case, we need to recompute the l2top
	Recomputel2top();	
		
	// We update the gBoundingRect and the clip
	spObject::UpdateGeometry();
	clip = gBoundingRect();
	clip.Intersect(container->clip);	
		
	// If the bounds haven't been kept we need to recompute them
//    printf("HELP\n");
//	if (!(flags & spFLAG_VIEW_KEEPBOUND_WHEN_RESIZED && topContainer->ResizeEvent())) RecomputeXYBound();
//    printf("OUT HELP\n");

	// Then We propagate the update
	//	cout << "YES" << objects[0] << " " << objects[1] << endl;
	for (int i=0;i<objectList.Size();i++) {
		objectList[i]->UpdateGeometryMain();
	}
//	SPDEBUG(spDebugGeometryUpdate, "<-- UpdateGeometryMain ");
}

void spView::ScrollX(int change,bool flagUpdateYScale)
{
	if (change == 0) return;
    Setl2gX(l2g.trX+change*l2g.trX.GetFactor()/l2top.trX.GetFactor());
	if (flagUpdateYScale) {
	  spRealRect r = _GetXYBound();
	  SetXYBound(ComputeMinXYBound(r.x,r.x+r.width));
	}
}

void spView::ScrollY(int change)
{
	if (change == 0) return;
  Setl2gY(l2g.trY+change*l2g.trY.GetFactor()/l2top.trY.GetFactor());
}


void spView::SetLogScaleY(bool flag)
{
	if (l2g.trY.GetFlagLn() == flag) return;
	spRealRect r = GetXYBound();
	double yMin = r.y;
	double yMax = r.y+r.height;
	spMap1d trY(yMin,l2g.trY.Apply(yMin),yMax,l2g.trY.Apply(yMax),flag);
	
  Setl2gY(trY);	
}

void spView::SetLogScaleX(bool flag)
{
	if (l2g.trX.GetFlagLn() == flag) return;
	spRealRect r = GetXYBound();
	double xMin = r.x;
	double xMax = r.x+r.width;
	spMap1d trX(xMin,l2g.trX.Apply(xMin),xMax,l2g.trX.Apply(xMax),flag);
	
  Setl2gX(trX);	
}

bool spView::GetLogScaleY()
{
	return (l2g.trY.GetFlagLn());
}

bool spView::GetLogScaleX()
{
	return (l2g.trX.GetFlagLn());
}


void spView::ZoomX(spRealPoint pt, int change)
{
	double factor = 1.01;
	factor = pow(factor, change);
	spRealPoint pt1 = l2top.IApply(pt);
	
	spMap1d trX(factor,(1-factor)*l2g.trX.Apply(pt1.x));
	spMap1d trY;
	spMap2d tr(trX,trY);
	
	Setl2g(l2g.ComposeLeft(tr));	
}

void spView::ZoomY(spRealPoint pt, int change)
{
	double factor = 1.01;
	factor = pow(factor, change);
	spRealPoint pt1 = l2top.IApply(pt);
	
	spMap1d trX;
	spMap1d trY(factor,(1-factor)*l2g.trY.Apply(pt1.y));
	spMap2d tr(trX,trY);
	
	Setl2g(l2g.ComposeLeft(tr));
}

void spView::Zoom(spRealPoint pt, int change)
{
	double factor = 1.01;
	factor = pow(factor, change);
	spRealPoint pt1 = l2top.IApply(pt);
	
	spMap1d trX(factor,(1-factor)*l2g.trX.Apply(pt1.x));
	spMap1d trY(factor,(1-factor)*l2g.trY.Apply(pt1.y));
	spMap2d tr(trX,trY);
	
	Setl2g(l2g.ComposeLeft(tr));
	
	SPUpdate();
}

// This method recomputes l2g using actual lBoundingRect and actual xyBound.
void spView::Recomputel2g()
{
	spRealPoint pt,pt1;
	spRealPoint boundsMin = xyBound.GetPosition();
	spRealPoint boundsMax = xyBound.GetPosition1();

	spRealRect r = box.GetMargin(this);
	pt = r.GetPosition();
	pt1 = r.GetPosition1();
	
	l2g = spMap2d(spMap1d(boundsMin.x,pt.x,boundsMax.x,pt1.x,l2g.trX.GetFlagLn()),spMap1d(boundsMin.y,pt.y,boundsMax.y,pt1.y,l2g.trY.GetFlagLn()));
}

// This method is called either when the bounds have changed or when the lBoundingRect has changed
void spView::_SetXYBound(double xmin,double xmax, double ymin, double ymax)
{
    if (ymin == ymax) {
        if (flags & spFLAG_VIEW_REVERSE_Y) {ymin +=.5; ymax -=.5;}
        else {ymax +=.5; ymin -=.5;}
        if (xmin == xmax) {
            if (flags & spFLAG_VIEW_REVERSE_X) xmin = xmax+1;
            else xmax = xmin+1;
        }
    }

	// If bounds haven't changed we must return
	spRealRect newXYBound(xmin,ymin,xmax-xmin,ymax-ymin);
	if (xyBound == newXYBound) return;

	xyBound = newXYBound;

	Recomputel2g();
	Recomputel2top();
	
	NotifyGeometryChange();
	if (IsVisible()) NotifyRectMustBeDrawn(gBoundingRect());
		
	SendChangeBoundEvent();

	//	cout << "<-- SetXYBound " << this << " " << l2g << endl;													
}

// This is to send a change bound event
void spView::SendChangeBoundEvent()
{
	spEvent event;
	event.type = spOnChangeBound;
	event.object = this;
	event.mouse = topContainer->canvas->Screen2Canvas(wxGetMousePosition());
	ProcessEvent1(event);	
}


void spView::SetXYBound(spRealRect r)
{
  double xmin,xmax,ymin,ymax;
	xmin = r.GetPosition().x;
	ymin = r.GetPosition().y;
	xmax = r.GetPosition1().x;
	ymax = r.GetPosition1().y;
//	cout << r << " " << ymax << endl;
	SetXYBound(xmin, xmax, ymin, ymax);
}

void spView::SetXYBound(double xmin,double xmax, double ymin, double ymax)
{
	spRealRect actualBound = spView::GetXYBound();
	
	if (actualBound.GetPosition().x == xmin &&  actualBound.GetPosition().y == ymin && actualBound.GetPosition1().x == xmax && actualBound.GetPosition1().y == ymax) return;
	
  if ((flags & spFLAG_VIEW_REVERSE_X && xmin<xmax) || (!(flags & spFLAG_VIEW_REVERSE_X) && xmin>xmax)) {
	  double temp=xmin;
	  xmin=xmax;
	  xmax=temp;
  }
	
  if ((flags & spFLAG_VIEW_REVERSE_Y && ymin<ymax) || (!(flags & spFLAG_VIEW_REVERSE_Y) && ymin>ymax)) {
	  double temp=ymin;
	  ymin=ymax;
	  ymax=temp;
  }	
	
	_SetXYBound(xmin,xmax,ymin,ymax);
}



void spView::Setl2g(spMap2d l2g1)
{
  l2g = l2g1;
	
	NotifyGeometryChange();
	if (IsVisible()) NotifyRectMustBeDrawn(gBoundingRect());

	Recomputel2top();
  RecomputeXYBound();
}

void spView::Setl2gX(spMap1d l2gx)
{
  l2g.trX = l2gx;
	NotifyGeometryChange();
	if (IsVisible()) NotifyRectMustBeDrawn(gBoundingRect());
	
	Recomputel2top();
  RecomputeXYBound();
}

void spView::Setl2gY(spMap1d l2gy)
{
  l2g.trY = l2gy;
	
	NotifyGeometryChange();
	if (IsHidden()) NotifyRectMustBeDrawn(gBoundingRect());
	
	Recomputel2top();
  RecomputeXYBound();

}


// Recomputes xyBound given lBoundingRect and l2g
// Sends a ChangeBoundEvent if the bounds has changed
void spView::RecomputeXYBound()
{
	spRealRect r = box.GetMargin(this);
	spRealPoint pt = r.GetPosition();
	spRealPoint pt1 = r.GetPosition1();
	
	pt = l2g.IApply(pt);
	pt1 = l2g.IApply(pt1);

    
	spRealRect newXYBound = spRealRect(pt.x,pt.y,pt1.x-pt.x,pt1.y-pt.y);	

	if (xyBound != newXYBound) {
        xyBound = newXYBound;
		SendChangeBoundEvent();
	}
}


spRealRect spView::GetXYBound()
{	
  spRealRect r=_GetXYBound();	
	r.Standardize();
	return r;
}



spRealRect spView::ComputeMinXYBound()
{
	// First pass to include all the FLOATING regions
  bool flagEmptyRect = true;
	spRealRect r;
	
	if (flagGeometryMustBeUpdated) SPUpdate(false);
    
    // Here we comute the rectangle that contains all the boxes in view coordinates
	for (int i=0;i<objectList.Size();i++) {
		if (objectList[i]->IsHidden()) continue;		
		objectList[i]->UnionFloatRect(r,objectList[i],flagEmptyRect);
	}

	double xmin,xmax,ymin,ymax;

	if (flagEmptyRect) {
        xmin = xmax=ymin,ymax=0;
    }
    else {
        xmin = r.GetPosition().x;
        xmax = r.GetPosition1().x;
        ymin = r.GetPosition().y;
        ymax = r.GetPosition1().y;
	
        if (xmin > xmax) {
            double temp = xmin;
            xmin = xmax;
            xmax = temp;
        }
        if (ymin > ymax) {
            double temp = ymin;
            ymin = ymax;
            ymax = temp;
        }

    // Then we need to expand due to MarginPixel
        for (int i=0;i<objectList.Size();i++) {
            if (objectList[i]->IsHidden()) continue;
            if (!l2top.trX.GetFlagLn()) objectList[i]->ExpandX(objectList[i], xmin, xmax);
            if (!l2top.trY.GetFlagLn()) objectList[i]->ExpandY(objectList[i], ymin, ymax);
        }
    }
	
//	cout << "TTTTTT " << xmin << " " << xmax << " " << ymin << " " << ymax << endl;
	
	spEvent event;
	event.object = this;
	event.type = spOnComputeBound;
	event.ComputeMinXYBound(false,xmin,xmax,ymin,ymax);
	ProcessEvent1(event);
	
	return spRealRect(spRealPoint(event.xMin,event.yMin),spRealPoint(event.xMax,event.yMax));	
}


spRealRect spView::ComputeMinXYBound(double xMin, double xMax)
{
	if (flagGeometryMustBeUpdated) SPUpdate(false);

	// First pass to include all the FLOATING regions
	bool flagEmptyRect = true;
	spRealRect r;
	for (int i=0;i<objectList.Size();i++) {
		if (objectList[i]->IsHidden()) continue;		
		objectList[i]->UnionFloatRect(r,objectList[i],flagEmptyRect,xMin,xMax);
	}

	double ymin,ymax;
    
	if (flagEmptyRect) {
        ymin = ymax = 0;
    }
    else {
//        if (r.width == 0 || r.height == 0) return spRealRect(xMin,r.y-.5,xMax-xMin,1);
	
        // First we set the xMin,xMax,yMin,yMax
        ymin = r.GetPosition().y;
        ymax = r.GetPosition1().y;
	
        if (ymin > ymax) {
            double temp = ymin;
            ymin = ymax;
            ymax = temp;
        }
	
        for (int i=0;i<objectList.Size();i++) {
            if (objectList[i]->IsHidden()) continue;
            if (!l2top.trY.GetFlagLn()) objectList[i]->ExpandY(objectList[i], ymin, ymax,xMin,xMax);
        }
    }
	
	spEvent event;
	event.object = this;
	event.type = spOnComputeBound;
	event.ComputeMinXYBound(true,xMin,xMax,ymin,ymax);
	ProcessEvent1(event);
	
	return spRealRect(spRealPoint(event.xMin,event.yMin),spRealPoint(event.xMax,event.yMax));	
}




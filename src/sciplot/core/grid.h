
#ifndef _SP_Grid_H_
#define _SP_Grid_H_


class spGrid : public spContainer
{
	public :
	
	//
  // Simple class tests	
	//
	virtual bool IsGrid() {return true;};
	virtual bool IsView() {return false;};
	virtual bool IsTopContainer() {return false;};
	
	//
	// Dealing with grid coordinates
	//
	// These are the parameters when the view is used as a grid
	//
	int nGrid,mGrid;                                                                    // Grid size
	int rightMarginGrid,downMarginGrid,leftMarginGrid,upMarginGrid,interMarginGrid;     // Grid margins
	
	spRealPoint GetGridSize(spRealPoint pt);
	spRealPoint GetGridPoint(spRealPoint pt);
	
	
	//
  // Destructor/Constructor and initialization
	//
	void InitGrid();
	spGrid(int i,const char *name = NULL); // Top container constuctor
	spGrid(spContainer *ctn=NULL,const spAnchor &anchor=spAnchor(),const spMargin &margin=spMargin(),unsigned long flags=0,const char *name=NULL);
  virtual ~spGrid();	

	virtual const char *GetClassName() {
		return _CCHAR("spGrid");
	}
	
};



#endif

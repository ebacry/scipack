
#ifndef _SP_H_
#define _SP_H_

#include <iostream>
using namespace std;
#include "wx/wxprec.h"

// for all others, include the necessary headers (this file is usually all you
// need because it includes almost all "standard" wxWidgets headers)
#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#ifndef spNOGC
#include <wx/graphics.h>
#include <wx/dcgraph.h>
#endif

#include "wx/image.h"

#include "wx/choicdlg.h"
#include "wx/fontdlg.h"
#include "wx/fontenum.h"
#include "wx/fontmap.h"
#include "wx/encconv.h"
#include "wx/splitter.h"
#include "wx/textfile.h"

#include "wx/gdicmn.h"
#include "wx/pen.h"


#include "scipackdefs.h"

#include "script.h"
#include "float.h"
#include "list.hpp"
#include "debug.h"
#include "error.h"
#include "utils.h"
#include "ref.h"

#include "formattedText.h"
#include "canvas.h"

#include "box.h"
#include "event.h"
#include "object_set.h"
#include "interactor.h"
#include "interactor_popup.h"
#include "interactor_select.h"
#include "interactor_move.h"
#include "interactor_zoom.h"
#include "interactor_scroll.h"
#include "object.h"
#include "object_smart_ptr.hpp"
#include "shape.h"
#include "textbox.h"
#include "interactor_cursor.h"
#include "interactor_msge.h"
#include "text.h"
#include "container.h"
#include "group.h"
#include "comment.h"
#include "grid.h"
#include "view.h"
#include "topcontainer.h"
#include "axis.h"
#include "framedview.h"
#ifndef SP_SCRIPT
#include "textedit.h"
#endif



#ifdef UNICODE
#include <wx/string.h>
#include <wx/strconv.h>
//#define WXSTRING2ANSI(str) ((char*) str.mb_str(wxConvUTF8))
#define WXSTRING2ANSI(str) ((char *) ((const char*) str.mb_str()))
#define ANSI2WXSTRING(input) (wxString(input, wxConvUTF8))
#else
#define WXSTRING2ANSI(str) ((char*) str.c_str())
#define ANSI2WXSTRING(input) (wxString(input))
#endif


/* Max of two numbers */
#ifndef MAX
#define MAX(x,y)  ((x) > (y) ? (x) : (y))
#endif

/* Min of two numbers */
#ifndef MIN
#define MIN(x,y)  ((x) < (y) ? (x) : (y))
#endif


#endif

//
//  group.cpp
//  sp
//
//  Created by bacry on 25/01/13.
//
//

#include "sp.h"


spGroup::spGroup(spContainer *ct,unsigned long flags,const char *name) : spContainer(ct,spAnchorNone(),spMarginInfinite(),flags,name)
{
    flagLocked = false;

    flagProcessGeometryUpdateNow = false;    
}

spGroup::~spGroup()
{
    UnLock();    
}

void spGroup::UnLock()
{
    flagLocked = false;
    objectAnchors.Init();
}

void spGroup::Lock()
{
    objectAnchors.Init();
    
    for (int i=0;i<objectList.Size();i++) {
        objectAnchors.Add(new spRealPoint(objectList[i]->GetAnchor()));
    }

    flagLocked = true;
}

void spGroup::AttBeforeUpdate()
{
    spContainer::AttBeforeUpdate();
    flagProcessGeometryUpdateNow = true;
}

void spGroup::AttAfterUpdate()
{
    spContainer::AttAfterUpdate();;
    flagProcessGeometryUpdateNow = false;
}


void spGroup::AddObject(spObject *object)
{
    if (IsLocked()) return;
    spContainer::AddObject(object);
}

void spGroup::FixObject(spObject *object)
{
    if (object->container != this) return;
    
    for (int i = 0;i<fixedObjects.Size();i++)
        if (fixedObjects[i] == object) return;
    
    object->Ref();
    fixedObjects.Add(object);
}

void spGroup::RemoveObject(spObject *object)
{
    if (IsLocked()) return;
    fixedObjects.Remove(object);
    object->Unref();
    spContainer::RemoveObject(object);
}


void spGroup::NotifyObjectGeometryUpdateMain(spObject *object)
{
    if (!IsLocked()) return;
    
    flagProcessGeometryUpdateNow = true;

    NotifyObjectGeometryUpdate(object);
    
    flagProcessGeometryUpdateNow = false;

}

void spGroup::NotifyObjectGeometryUpdate(spObject *object)
{
    // Find the object whose geometry has been changed
    int index;
    for (index=0;index<objectList.Size();index++) {
        if (object == objectList[index]) break;
    }
    
    
    // The "old" anchor
    spRealPoint anchor = *(objectAnchors[index]);
    
    // Case its anchor has changed
    if (anchor != object->GetAnchor()) {
        
        // If it is not allowed we put it back
        for (int i = 0;i<fixedObjects.Size();i++) {
            if (fixedObjects[i] == object) {
                object->SetAnchor(anchor);
            }
        }
    }
    
    // Compute the move
    spRealPoint move =  object->GetAnchor() - anchor;
    *(objectAnchors[index]) = objectList[index]->GetAnchor();
    
    if (move.x == 0 && move.y == 0) return;
    
    for (int j=0;j<objectList.Size();j++) {
        if (j==index) continue;
        int i;
        for (i = 0;i<fixedObjects.Size();i++) {
            if (fixedObjects[i] == objectList[j]) break;
        }
        if (i<fixedObjects.Size()) continue;
        objectList[j]->SetAnchor(objectList[j]->GetAnchor()+move);
        *(objectAnchors[j]) = objectList[j]->GetAnchor();
    }
}

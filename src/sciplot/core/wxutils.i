
// Not a module


//
// This feature allows the typemaps to work even when the declaration have default arguments
// This is needed if spALTERNATIVE_TYPEMAPS is needed
// Otherwise it is not needed
// Turning it on remove the named parameters in python ... which is a shame !
// We should try to keep it on
//
//%feature("compactdefaultargs");


//
// The next definitions allow to load some Python types : Pen, Brush, Colour and Font
// This is used for type checking
//

%{

static PyObject *_WX_Module = NULL;
static PyTypeObject *_WX_ColourType = NULL;
static PyTypeObject *_WX_PenType = NULL;
static PyTypeObject *_WX_BrushType = NULL;
static PyTypeObject *_WX_FontType = NULL;

void _WX_Init() 
{
  _WX_Module = PyImport_AddModule("wx");
	if (_WX_Module == NULL) {
		printf("MERDE\n");	
		return;
	}
    _WX_ColourType = (PyTypeObject *) PyObject_GetAttrString(_WX_Module,"Colour"); // New reference
	_WX_PenType = (PyTypeObject *) PyObject_GetAttrString(_WX_Module,"Pen"); // New reference
	_WX_BrushType = (PyTypeObject *) PyObject_GetAttrString(_WX_Module,"Brush"); // New reference
	_WX_FontType = (PyTypeObject *) PyObject_GetAttrString(_WX_Module,"Font"); // New reference
}

%}


//
// Typemap for wxColour
//
%{
	wxColour GetWxColour(PyObject* wrappedObject)
	{
		SwigPyObject* swigThis = (SwigPyObject*) PyObject_GetAttrString(wrappedObject,"this"); 
		if (swigThis) { 
			return *(reinterpret_cast<wxColour*>(swigThis->ptr)); 
		} 
		PyErr_SetString(PyExc_RuntimeError,"Something weired is happening during a wxColour swig conversion");
        throw 0;
	}
%}


%typemap(typecheck,precedence=SWIG_TYPECHECK_POINTER) wxColour {
   if (_WX_Module == NULL) _WX_Init();
   if ($input->ob_type != _WX_ColourType && !PyString_Check($input)) $1 = 0;
   else $1 = 1;
}

%typemap(out) wxColour {
    unsigned char blue = $1.Blue();
    unsigned char green = $1.Green();
    unsigned char red = $1.Red();
    unsigned char alpha = $1.Alpha();
    PyObject *arglist = Py_BuildValue("bbbb",red,green,blue,alpha);
    $result = PyObject_CallObject((PyObject *)_WX_ColourType,arglist);
    Py_XDECREF(arglist);
}

%typemap(in) wxColour {
    if (PyString_Check($input))
      $1 = wxColour(ANSI2WXSTRING(PyString_AS_STRING($input)));
    else {
      if (_WX_Module == NULL) _WX_Init();
        try {
            if ($input->ob_type == _WX_ColourType) $1 = GetWxColour($input);
            else {
                PyErr_SetString(PyExc_RuntimeError, "wx.Colour expected");
                SWIG_fail;
            }
        }
        catch(int e) {SWIG_fail;}
    }
}


//
// Typemap for wxPen
//

%{
wxPen GetWxPen(PyObject* wrappedObject) 
{
	SwigPyObject* swigThis = (SwigPyObject*) PyObject_GetAttrString(wrappedObject,"this"); 
	if (swigThis) {
	  return *(reinterpret_cast<wxPen*>(swigThis->ptr)); 
	} 
	PyErr_SetString(PyExc_RuntimeError,"Something weired is happening during a wxPen swig conversion");
    throw 0;
}
%}
%typemap(in) wxPen {
    try {
        if (_WX_Module == NULL) _WX_Init();
        if ($input->ob_type == _WX_PenType) $1 = GetWxPen($input);
        else {
            PyErr_SetString(PyExc_RuntimeError, "wx.Pen expected");
            SWIG_fail;
        }
    }
    catch(int e) {SWIG_fail;}


}

/*
%typemap(out) wxPen {
    wxPen *obj = new wxPen;
    *obj=$1;
    $result = SWIG_NewPointerObj(SWIG_as_voidptr(obj),SWIG_TypeQuery("_p_wxPen"),  SWIG_POINTER_OWN |  0 );
}
 */


%typemap(typecheck,precedence=SWIG_TYPECHECK_POINTER) wxPen {
   if (_WX_Module == NULL) _WX_Init();
   if ($input->ob_type != _WX_PenType) $1 = 0;
   else $1 = 1;
}


//
// Typemap for brush
//

%{
	wxBrush GetWxBrush(PyObject* wrappedObject) 
	{ 
		SwigPyObject* swigThis = (SwigPyObject*) PyObject_GetAttrString(wrappedObject,"this"); 
		if (swigThis) { 
			return *(reinterpret_cast<wxBrush*>(swigThis->ptr)); 
		} 
		PyErr_SetString(PyExc_RuntimeError,"Something weired is happening during a wxBrush swig conversion");
        throw 0;
	}
%}
%typemap(in) wxBrush {
   if (_WX_Module == NULL) _WX_Init();
    try {
        if ($input->ob_type == _WX_BrushType) $1 = GetWxBrush($input);
        else {
            PyErr_SetString(PyExc_RuntimeError, "wx.Brush expected");
            SWIG_fail;
        }
    }
    catch(int e) {SWIG_fail;}

}

%typemap(typecheck,precedence=SWIG_TYPECHECK_POINTER) wxBrush {
   if (_WX_Module == NULL) _WX_Init();
   if ($input->ob_type != _WX_BrushType) $1 = 0;
   else $1 = 1;
}

//
// Typemap for wxFont
//
%{
	wxFont GetWxFont(PyObject* wrappedObject)
	{
		SwigPyObject* swigThis = (SwigPyObject*) PyObject_GetAttrString(wrappedObject,"this"); 
		if (swigThis) { 
			return *(reinterpret_cast<wxFont*>(swigThis->ptr)); 
		} 
		PyErr_SetString(PyExc_RuntimeError,"Something weired is happening during a wxFont swig conversion");
        throw 0;
	}
	%}
%typemap(in) wxFont {
   if (_WX_Module == NULL) _WX_Init();
    try {
      if ($input->ob_type == _WX_FontType) $1 = GetWxFont($input);
      else {
        PyErr_SetString(PyExc_RuntimeError, "wx.Font expected");
        SWIG_fail;
      }
    }
    catch (int e) {
        SWIG_fail;
    }
}

%typemap(typecheck,precedence=SWIG_TYPECHECK_POINTER) wxFont {
   if (_WX_Module == NULL) _WX_Init();
   if ($input->ob_type != _WX_FontType) $1 = 0;
   else $1 = 1;
}



//
// Typemap for wxString
//

%typemap(in) wxString {
    if (PyString_Check($input)) $1 = ANSI2WXSTRING(PyString_AS_STRING($input));
    else {
        PyErr_SetString(PyExc_RuntimeError, "string expected");
        SWIG_fail;
    }
}
%typemap(typecheck,precedence=SWIG_TYPECHECK_STRING) wxString {
   $1 = PyString_Check($input->ob_type) ? 1 : 0;
}


//
// Typemap for font parmaters
//
%typemap(in) wxFontFamily {
    if (PyInt_Check($input)) $1 = (wxFontFamily) PyInt_AsLong($input);
        else {
            PyErr_SetString(PyExc_RuntimeError, "wx.FontFamily expected");
            SWIG_fail;
        }
}
%typemap(typecheck,precedence=SWIG_TYPECHECK_INTEGER) wxFontFamily {
    $1 = PyInt_Check($input) ? 1 : 0;
}
%typemap(in) wxFontStyle {
    if (PyInt_Check($input)) $1 = (wxFontStyle) PyInt_AsLong($input);
        else {
            PyErr_SetString(PyExc_RuntimeError, "wx.FontStyle expected");
            SWIG_fail;
        }
}
%typemap(out) wxFontStyle {
    $result = PyInt_FromLong ((long) $1);
}
%typemap(typecheck,precedence=SWIG_TYPECHECK_INTEGER) wxFontWeight {
    $1 = PyInt_Check($input) ? 1 : 0;
}
%typemap(in) wxFontWeight {
    if (PyInt_Check($input)) $1 = (wxFontWeight) PyInt_AsLong($input);
        else {
            PyErr_SetString(PyExc_RuntimeError, "wx.FontWeight expected");
            SWIG_fail;
        }
}
%typemap(typecheck,precedence=SWIG_TYPECHECK_INTEGER) wxFontWeight {
    $1 = PyInt_Check($input) ? 1 : 0;
}
%typemap(in) wxFontEncoding {
    if (PyInt_Check($input)) $1 = (wxFontEncoding) PyInt_AsLong($input);
        else {
            PyErr_SetString(PyExc_RuntimeError, "wx.FontEncoding expected");
            SWIG_fail;
        }
}
%typemap(typecheck,precedence=SWIG_TYPECHECK_INTEGER) wxFontEncoding {
    $1 = PyInt_Check($input) ? 1 : 0;
}


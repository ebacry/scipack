/*
 *  canvas.h
 *  SciPlot
 *
 *  Created by bacry on 24/06/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef _SP_CANVAS_H
#define _SP_CANVAS_H


//
// This is the Canvas class that is the netry point to the SciPLot library
//
// It uses spUpdate class in order to keep track of the updates to perform. There are 2 types of updates
//   - Drawing that must be performed
//   - Update some bounding rect
//



// The following class is the main sp class
class spTopContainer;
class spUpdate;
class spObject;
class spCanvas;
class spEvent;




//
// The main procedure to call to run all the necessary updates
// Wen call directly from main --> can generate spError
//
extern void SPUpdate(bool flagRedraw = true);    // --> DECLARETHROW



//
// The canvas class
//
class spCanvas
{
    
    public : long wxWindowId;
	wxWindow *window;
private:
    
	//
	// Dealing with the dc
	// dc       : The current dc all the drawing is performed in. It is created each time drawing is needed
	// dcBuffer : A copy of the dc without overlayObject (memoryDC is the correspondant memoryDC)
	//
    private :
	
    int _nRefs;
    
    public:
    void Ref();
    int Unref();
    int nRefs() {return _nRefs;}
    
    private:
	wxDC *dc;
	wxBitmap bitmap;
	void DesallocDCBuffer();
	void AllocDCBuffer(wxSize size);
#ifndef spNOGC
	wxGCDC *dcBuffer,*_dcBuffer; 
    wxMemoryDC *memoryDC;
#else
	wxMemoryDC *dcBuffer,*_dcBuffer;
#endif
	
	bool flagMouseIn;
	
	public :
	void DeactivateDCBuffer();
	void ActivateDCBuffer();
	void DCBufferToDC(spRect &r);
	
	
	private :
    
	// Used for drawing formatted string
	void _FormatText(double x,double y,spFormattedText *ft);
	void __FormatText(char *lines[], int nLines, int hPositionMode, double x, int vPositionMode,  double y, double angle, spFormattedText *ft, bool flagDraw);
	
	//
	// When a canvas is created, it is ready to Draw on ONLY after receiving a resize Event.
	// Then after this resize, we know we shall receive a Paint event.
	// As long as we have not received it we do not take update events !
	//
	bool flagFirstResizeEventReceived;
	bool flagIsReady;
	
	//
	// Dealing with Bounding Rect updates
	//
	// The objects the bounding rect must be updated
	//
	spList<spObject,10000,100> objectsWhoseGeometryMustBeUpdated;
	void UpdateObjectsGeometries();
	
	//
	// Dealing with Draw updates
	//
	// rectToBePainted : keeps track of the global rect that should be repainted
	//
private:
	spRect rectToBePainted;
    void InitRectToBePainted() {rectToBePainted = spRect(0,0,0,0);};
	void UpdatePaintWithDC(spRect &r);
	void UpdatePaintWithoutDC(spRect &r);
	void UpdatePaint();
	
    public :
	void Update(bool flagRedraw = true);
	void NotifyRectMustBeDrawn(spRect rect);
	void NotifyGeometryChange(spObject *obj);
	void NotifyObjectDeleted(spObject *obj);
	
	
    //
	// All the canvases and the current one
	//
	static spList<spCanvas,10,10> canvasList;
	static spCanvas *curCanvas;
	
	//
	// Constructors/Destructor
	//
	spCanvas(long id,spTopContainer *tc = NULL, const char *name = NULL);
	virtual ~spCanvas();
		
	
	//
	// Get Size of the canvas
	//
	void GetSize(int &width, int &height);
	
	//
	// Dealing with print
	//
	// When we print, we create a new wxDC so we need to apply a ratio on the coordinates of all drawings and (I don't know why!) we have to correct the clipping
	// Print : main method for printing the canvas (it will call Print_)
	//
private:
	int clipX,clipY;
	double ratioPixel;
    bool flagPrinting;
public:
    double GetResolution() {return 1/ratioPixel;}
    bool IsPrinting() {return flagPrinting;}
	void SendToDC(wxDC *dc,wxRect pageRect);
	void Print(bool flagPageSetupDialog=true, bool flagPrintDialog = true, int units_per_cm=200);
	void ToPS(char *filename,double x_margin_cm=0,double y_margin_cm=0);
	void ToSVG(char *filename);
	
	//
	// The topview associated to the canvas
	//
	spTopContainer *container;
    spTopContainer *GetTopContainer() {return container;}
    
	
	//
	// The wx interactorList
	//
	void OnPaint(wxPaintEvent& event);
	void OnMouseMotion(wxMouseEvent& event);
	void OnResize(wxSizeEvent& event);
	void OnMouseWheel(wxMouseEvent &event);
	void OnMouseDown(wxMouseEvent& event);
	void OnMouseUp(wxMouseEvent& event);
	void OnMouseEnter(wxMouseEvent& event);
	void OnMouseLeave(wxMouseEvent& event);
	void OnMouseDClick(wxMouseEvent& event);
	void OnKeyChar(wxKeyEvent& event);
    
#ifndef SP_SCRIPT
	void OnPopupClick(wxCommandEvent &evt);
#endif
    
	//	 void OnKeyDown(wxKeyEvent& event);
	
	
	
	
	//
	// Following is a list of method that are wxDC methods that are redirected by the canvas.
	// To draw in the canvas we should never use the wxDC methods but these methods
	//
    
	inline void DrawPolygon(int n, spRealPoint *pt) {
        if (n>=1000) return;
        static wxPoint points[1000];
        if (ratioPixel == 1) {
            for (int i = 0; i< n; i++) {
                points[i] = wxPoint(pt[i].x,pt[i].y);
            }
            if (dc) dc->DrawPolygon(n,points) ;
            if (dcBuffer) dcBuffer->DrawPolygon(n,points); return;
        }
        else {
            for (int i = 0; i< n; i++) {
                points[i] = wxPoint(pt[i].x*ratioPixel,pt[i].y*ratioPixel);
            }
            dc->DrawPolygon(n,points);
        }
	}

	inline void DrawPolygon(int n, spPoint *pt) {
        if (n>=1000) return;
        static wxPoint points[1000];
        if (ratioPixel == 1) {
            for (int i = 0; i< n; i++) {
                points[i] = wxPoint(ROUND(pt[i].x),ROUND(pt[i].y));
            }
            if (dc) dc->DrawPolygon(n,points) ;
            if (dcBuffer) dcBuffer->DrawPolygon(n,points); return;
        }
        else {
            for (int i = 0; i< n; i++) {
                points[i] = wxPoint(pt[i].x*ratioPixel,pt[i].y*ratioPixel);
            }
            dc->DrawPolygon(n,points);
        }
	}
    
	inline void DrawArrowTip(spRealPoint &tip, double angle, double length = 25, double alpha=45){
        alpha *= M_PI/180;
        spRealPoint list[3];
        list[0] = tip;
        list[1] =  spRealPoint(-length*cos(alpha),length*sin(alpha)).Rotate(angle)+tip;
        list[2] =  spRealPoint(-length*cos(alpha),-length*sin(alpha)).Rotate(angle)+tip;
        DrawPolygon(3,list);
	}

	inline void DrawRectangle(spRect &r) {
        if (ratioPixel == 1) {if (dc) dc->DrawRectangle(wxRect(r.x,r.y,r.width,r.height)) ; if (dcBuffer) dcBuffer->DrawRectangle(wxRect(r.x,r.y,r.width,r.height)); return;}
		dc->DrawRectangle(wxRect(r.x*ratioPixel,r.y*ratioPixel,r.width*ratioPixel,r.height*ratioPixel));
	}
	
	inline void DrawRectangle(double x, double y, double w, double h) {
        if (ratioPixel == 1) {if (dc) dc->DrawRectangle((int) x, (int) y, (int) w, (int) h) ; if (dcBuffer) dcBuffer->DrawRectangle((int) x, (int) y, (int) w, (int) h); return;}
		dc->DrawRectangle((int) (x*ratioPixel),(int) (y*ratioPixel), (int) (w*ratioPixel), (int) (h*ratioPixel));
	}
	
	inline void DrawEllipse(spRect &r) {
        if (ratioPixel == 1) {if (dc) dc->DrawEllipse(wxRect(r.x,r.y,r.width,r.height)) ; if (dcBuffer) dcBuffer->DrawEllipse(wxRect(r.x,r.y,r.width,r.height)) ; return;}
		dc->DrawEllipse(wxRect(r.x*ratioPixel,r.y*ratioPixel,r.width*ratioPixel,r.height*ratioPixel));
	}
	
	inline void DrawEllipse(double x, double y, double w, double h) {
        if (ratioPixel == 1) {if (dc) dc->DrawEllipse((int) x, (int) y, (int) w, (int) h) ; if (dcBuffer) dcBuffer->DrawEllipse((int) x, (int) y, (int) w, (int) h) ; return;}
		dc->DrawEllipse((int) (x*ratioPixel),(int) (y*ratioPixel), (int) (w*ratioPixel), (int) (h*ratioPixel));
	}
	
    inline void DrawLine(double x0, double y0, double x1, double y1)
	{
        if (ratioPixel == 1) {if (dc) dc->DrawLine((int) x0, (int) y0, (int) x1, (int) y1) ; if (dcBuffer) dcBuffer->DrawLine((int) x0, (int) y0, (int) x1, (int) y1); return;}
		dc->DrawLine((int) (x0*ratioPixel),(int) (y0*ratioPixel), (int) (x1*ratioPixel), (int) (y1*ratioPixel));
	}
	
    
    
    // Get string box --> not using pixelRatio since font has been changed if printing
	inline  void GetTextExtent(const wxString& string,int *x, int *y,int *descent = NULL,int *externalLeading = NULL,wxFont *theFont = NULL ) {
		if (dc) dc->GetTextExtent(string,x,y,descent,externalLeading,theFont);
        else dcBuffer->GetTextExtent(string,x,y,descent,externalLeading,theFont);
	}

    // Draw text --> no pixelRatio used
    inline void DrawTextNoRatio(const wxString& s,double x, double y, double angle=0) {
		if (angle != 0) {
			if (ratioPixel == 1) {if (dc) dc->DrawRotatedText(s,(int) x, (int) y, angle) ; if (dcBuffer) dcBuffer->DrawRotatedText(s,(int) x, (int) y, angle); return;}
			dc->DrawRotatedText(s,(int) x, y, angle);
		}
		else {
			if (ratioPixel == 1) {if (dc) dc->DrawText(s,(int) x, (int) y) ; if (dcBuffer) dcBuffer->DrawText(s,(int) x, (int) y); return;}
			dc->DrawText(s,x,y);
		}
	}

    // Draw text specifying coordinate of lower-left corner
	inline void DrawText(const wxString& s,double x, double y, double angle=0) {
		if (angle != 0) {
			if (ratioPixel == 1) {if (dc) dc->DrawRotatedText(s,(int) x, (int) y, angle) ; if (dcBuffer) dcBuffer->DrawRotatedText(s,(int) x, (int) y, angle); return;}
			dc->DrawRotatedText(s,(int) (x*ratioPixel), (int) (y*ratioPixel), angle);
		}
		else {
			if (ratioPixel == 1) {if (dc) dc->DrawText(s,(int) x, (int) y) ; if (dcBuffer) dcBuffer->DrawText(s,(int) x, (int) y); return;}
			dc->DrawText(s,(int) (x*ratioPixel), (int) (y*ratioPixel));
		}
	}

	// Draw text specifying coordinate of base
	inline void DrawTextBase(const wxString& s,double x, double y, double angle) {
		int w,h,d,i;
		GetTextExtent(s,&w,&h,&d,&i);
		if (angle != 0) {
			if (ratioPixel == 1) {if (dc) dc->DrawRotatedText(s,(int) x, (int) (y-(h-d)), angle) ; if (dcBuffer) dcBuffer->DrawRotatedText(s,(int) x, (int) y, angle); return;}
			dc->DrawRotatedText(s,(int) (x*ratioPixel), (int) ((y*ratioPixel)-(h-d)), angle);
		}
		else {
			if (ratioPixel == 1) {if (dc) dc->DrawText(s,(int) x, (int) (y-(h-d))) ; if (dcBuffer) dcBuffer->DrawText(s,(int) x, (int) (y-(h-d))); return;}
			dc->DrawText(s,(int) (x*ratioPixel), (int) ((y*ratioPixel)-(h-d)));
		}
	}

	// Draw a string given a format
	void DrawFormattedText(const char *str, char hPositionMode, double x, char vPositionMode, double y, double angle=0);
    
    //
    // Update the spFormattedText
    //
	inline void FormatText(spFormattedText *ft)
	{
        spRealPoint pt = ft->GetAnchorPoint();
        
        if (ratioPixel != 1) {
            ft->flagFormattedWhilePrinting = true;
            _FormatText(pt.x*ratioPixel,pt.y*ratioPixel,ft);
        }
        else {
            ft->flagFormattedWhilePrinting = false;
            _FormatText(pt.x,pt.y,ft);
        }        
	}
    
    // Draw a spFormmattedText object (FormatText must be called with ft before calling this method)
	inline void DrawFormattedText(spFormattedText &ft)
	{
        if (ratioPixel != 1 && !ft.flagFormattedWhilePrinting) FormatText(&ft);

		ft.Draw(this);

        if (ratioPixel != 1 && ft.flagFormattedWhilePrinting) {
            double ratioPixel1 = ratioPixel;
            ratioPixel = 1;
            spRealPoint pt = ft.GetAnchorPoint();
            FormatText(&ft);
            ratioPixel = ratioPixel1;
        }
	}
	

	inline void DrawPoint(double x, double y) {
        if (ratioPixel == 1) {if (dc) dc->DrawPoint((int) x, (int) y) ; if (dcBuffer) dcBuffer->DrawPoint((int) x, (int) y); return;}
		dc->DrawPoint((int) (x*ratioPixel), (int) (y*ratioPixel));
	}
	
    
    inline void SetClip(spRect &rect)
	{
		if (dc) dc->DestroyClippingRegion();
        if (dcBuffer) dcBuffer->DestroyClippingRegion();
        if (ratioPixel == 1) {
//            cout << "clip " << rect << endl;
            wxRect clip = wxRect(rect.x,rect.y,rect.width,rect.height);
			if (dc) dc->SetClippingRegion(clip);
            if (dcBuffer) dcBuffer->SetClippingRegion(clip);
			return;
		}
		wxRect clip = wxRect((int)(rect.x*ratioPixel),(int)(rect.y*ratioPixel),(int)(rect.width*ratioPixel),(int)(rect.height*ratioPixel));
		clip.x+=clipX;
		clip.y+=clipY;
		if (dc) dc->SetClippingRegion(clip);
        if (dcBuffer) dcBuffer->SetClippingRegion(clip);
	}
	
	inline void SetTextColour(const wxColour &textBackgroundColor,const wxColour &textForegroundColor) {
		if (dc) {
            dc->SetTextForeground(textForegroundColor);
            dc->SetTextBackground(textBackgroundColor);
        }
        if (dcBuffer) {
            dcBuffer->SetTextForeground(textForegroundColor);
            dcBuffer->SetTextBackground(textBackgroundColor);
        }
	}

	inline void SetPen(const wxPen &pen) {
		if (dc) dc->SetPen(pen);
        if (dcBuffer) dcBuffer->SetPen(pen);
	}
	
	inline void SetBrush(const wxBrush &brush) {
		if (dc) dc->SetBrush(brush);
        if (dcBuffer) dcBuffer->SetBrush(brush);
	}
	
	inline void SetFont1(const wxFont &font) {
        if (ratioPixel == 1) {
            if (dc) dc->SetFont(font);
            if (dcBuffer) dcBuffer->SetFont(font);
            return;
        }
        if (dc) {
            wxFont myFont = wxFont((int) (font.GetPointSize()*ratioPixel),font.GetFamily(),font.GetStyle(),font.GetWeight(),font.GetUnderlined(),font.GetFaceName(),font.GetEncoding());
            dc->SetFont(myFont);
        }        
	}
	
	inline spPoint Canvas2Screen(spPoint &pt) {return window->ClientToScreen(wxPoint(pt.x,pt.y));}
	inline spPoint Screen2Canvas(spPoint pt) {return window->ScreenToClient(wxPoint(pt.x,pt.y));}
    
	inline void SetPopup(wxMenu &menu) {
        window->PopupMenu(&menu);
	}
	
	public :
	void ProcessEvent(spObject *object, spEvent &event);
	void ProcessEvent1(spObject *object, spEvent &event);
	
};




#endif

#ifndef _SP_ObjectAuto_H_
#define _SP_ObjectAuto_H_


class spInteractorObjectAutoPtr;

template <class T> class spObjectAutoPtr 
{
	public :
	
	T *object;
	spInteractorObjectAutoPtr *inter;
	
	spObjectAutoPtr(T *object=NULL);
		
	void operator=(T* rhs);
	
	inline operator T *() {
		return object;
	};
	
	inline T * operator ->() {
		return object;
	}

	virtual ~spObjectAutoPtr();
};


class spInteractorObjectAutoPtr : public spInteractor1 {
	public :
	
	spObjectAutoPtr<spObject> *objectAutoPtr;
	spInteractorObjectAutoPtr(spObjectAutoPtr<spObject> *objectAutoPtr);
};


#endif

// Not a module

%rename(Comment) spComment;
%rename(_SetAnchor) SetAnchor;
%rename(_GetAnchor) GetAnchor;
%rename(_SetDist) SetDist;
%rename(_GetDist) GetDist;

// The container class is a director
%feature("director") spComment;

// Some methods with named parameters
%feature("kwargs") spComment::spComment;


%DocClass(spComment,
          "An object for displaying comments in box pointing to a point with an arrow.
          This class derives from sp.Group. It has 2 objects, a sp.Text object and a sp.Shape
          object (for the arrow). The anchor point corresponds to where the arrow points to. The
          text box is displayed at a distance of the anchor point which a fix distance in
          pixels.
          See also the command :func:`~scipack.sciplot.sp.CommentAdd`")
class spComment : public spGroup
{
    public:
    
    ObjRepr;
    
    spComment(spContainer *ct=NULL,spRealPoint pt = spRealPoint(0,0), const char *str = "Comment", unsigned long flags=0,const char *name = NULL);
    virtual ~spComment();
    
//    virtual void NotifyObjectGeometryUpdate(spObject *);
    
    %attobj_remove(pos)
    %attobj_remove(pos1)
    %attobj_remove(size)
    %attobj_remove(margin)
    %attobj_remove(penWidth)
    %attobj_remove(penStyle)
    %attobj_remove(pen)
    %attobj_remove(penColour)
    
    virtual spRealPoint GetAnchor();
    virtual void SetAnchor(spRealPoint point);
    %attobj_def(anchor,Anchor,
    """
    **Attribute** self.anchor = a RealPoint(x,y) representing the point the arrow points to.
    
    It is expressed in container's coordinate.
    """);
    
    virtual spRealPoint GetDist();
    virtual void SetDist(spRealPoint point);
    %attobj_def(dist,Dist,
    """
    **Attribute** self.dist = a RealPoint(x,y) representing the distance between the point the arrow points too and the text.
    
    It is expressed in pixels.
    """);

    virtual void Move(double dx,double dy);
};


%extend spComment {
    %immutable;
    PyObject *textObject;
    PyObject *shapeObject;
    %mutable;
}
%{
    PyObject *spComment_textObject_get(spComment *fv) {return WrapObject(fv->GetTextObject());}
    PyObject *spComment_shapeObject_get(spComment *fv) {return WrapObject(fv->GetShapeObject());}
%}

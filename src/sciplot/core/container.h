
#ifndef _SP_Container_H_
#define _SP_Container_H_


//
// The flags for views
//

#define spFLAG_CONTAINER_HIDE                0x01   // If set, then view is hidden when created
#define spFLAG_VIEW_KEEPBOUND_WHEN_RESIZED    0x02	 // If set then the (xmin,xmax,ymin,ymax) bounds are kept when view is resized
#define spFLAG_CONTAINER_DRAW_FRAME          0x20   // If set then a frame is drawn
#define spFLAG_VIEW_REVERSE_X	    0x10   // Should the X axis be reversed ?
#define spFLAG_VIEW_REVERSE_Y	    0x04   // Should the Y axis be reversed ?
#define spFLAG_OBJECT_OVERLAY_BUFFER      0x80

class spTopContainer;

class spContainer : public spObject
{
	public :
	
	//
    // Simple class tests
	//
	virtual bool IsContainer() {return true;};
	virtual bool IsGrid() {return false;};
	virtual bool IsView() {return false;};
	virtual bool IsTopContainer() {return false;};
	virtual bool IsGroup() {return false;};
	
	virtual double GetDistance(spRealPoint pt);
    
	//
	// The corresponding top view
	//
	spTopContainer *topContainer;
	spTopContainer *GetTopContainer() {return topContainer;}
    
    // Set recursively the topcontainer field;
    void SetTopContainer(spTopContainer *top);
    
    
	//
	// The objects list included in this view
	//
	spObject *myobj;
	spList<spObject,2,10> objectList;
	virtual void AddObject(spObject *object);
	virtual void RemoveObject(spObject *object);
	void RemoveAllObjects();
	spObject *GetObject(int i) {
		if (i >= objectList.Size()) return NULL;
		return objectList[i];
	}
	// Find the object closest to the mouse
	spObject *FindObjectMouse(spRealPoint mouse,bool flagCursor=false);
	
	//
    // Destructor/Constructor and initialization
	//
	void InitContainer();
	spContainer(int i,const char *name=NULL);
	spContainer(spContainer *view1=NULL,const spAnchor &anchor=spAnchor(),const spMargin &margin=spMargin(),unsigned long flags=0,const char *name = NULL);
    virtual ~spContainer();
    
	//
	// Dealing with flags
	//
	unsigned long flags;
	inline void AddFlag(unsigned long f) {flags |= f;};
	inline void RemoveFlag(unsigned long f) {flags &= ~f;};
    inline bool IsReversedX() {return flags&spFLAG_VIEW_REVERSE_X;};
    inline bool IsReversedY() {return flags&spFLAG_VIEW_REVERSE_X;};
	
	//
	// Dealing with maps
	//
	// l2g : Conversion from local coordinates inside the container to coordinates (point 0,0 is the corner point)
	// l2top : Conversion from local to canvas coordinate (the ones that are used to draw on the window)
	//
	spMap2d l2g;
	spMap2d l2top;
	void Recomputel2top();
    inline spRealPoint L2Top(const spRealPoint p) {
        return l2top.Apply(p);
    }
    inline spRealRect L2Top(const spRealRect r) {
        return l2top.Apply(r);
    }
    inline spRealPoint Top2L(const spRealPoint p) {
        return l2top.IApply(p);
    }
    inline spRealRect Top2L(const spRealRect r) {
        return l2top.IApply(r);
    }
    inline spRealPoint L2TopDer(const spRealPoint p = spRealPoint(1,1)) {
        return l2top.ApplyDer(p);
    }
    inline spRealPoint L2TopSign() {
        return l2top.GetSign();
    }
    
	
	//
	// Dealing with clipRect
	//
	// clip          --> The clipRect associated to the view (in global coordinates)
	//
	spRect clip;
	
	//
	// redefinition of the UpdateGeometryMain method (it calls the same methods for all the objects inside the view)
	//
	virtual void UpdateGeometry();
    
	//
	// Redefinition of the Draw method
	//
	virtual void Draw(spRect &rect);
	
	//
	// Clear the view using the brush
	//
	void Clear();
	
	virtual void NotifyCanvasDeleted();
    
	//
	// Process event
	//
	virtual bool ProcessEvent(spEvent &event);
	double toleranceEventDistance;
    
	virtual const char *GetClassName() {
		return _CCHAR("spContainer");
	}
	
};



#endif

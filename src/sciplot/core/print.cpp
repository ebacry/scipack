/*
 *  printing.h
 *  LastWaveTrade_XCode
 *
 *  Created by bacry on 19/06/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include <iostream>
#include "wx/wx.h"
#include "wx/print.h"
#include "wx/printdlg.h"
#define _USE_MATH_DEFINES
#include <cmath>
#include "wx/generic/printps.h"
#include "wx/generic/prntdlgg.h"
#include <wx/dcsvg.h>
#include <wx/dcps.h>

#include "sp.h"


/*
 * A Print Driver class
 */
class spPrintDriver : public wxPrintout
{
	wxPageSetupDialogData m_page_setup;
	
	/** the type of paper (letter, A4, etc...) */
	wxPaperSize m_paper_type;
	
	/** horizontal or vertical */
	int m_orient;
	
	// Or, if you use wxWidgets 2.9+ :
	// wxPrintOrientation m_orient;
	
	/** number of pages we want to print. here it's static, but in a real example you will often
	 * want to calculate dynamically once you know the size of the printable area from page setup
	 */
	int m_page_amount;
	
	/** margins, in millimeters */
	int m_margin_left, m_margin_right, m_margin_top, m_margin_bottom;
	
	/** we'll use this to determine the coordinate system; it describes the number of units per
	 * centimeter (i.e. how fine the coordinate system is)
	 */
	float m_units_per_cm;
	
	/** will contain the dimensions of the coordinate system, once it's calculated.
	 * in the printing callback, you can then draw from (0, 0) to (m_coord_system_width, m_coord_system_height),
	 * which will be the area covering the paper minus the margins
	 */
public:
	int m_coord_system_width, m_coord_system_height;
private:
	spCanvas *canvas;
	
public:
	
	/**
	 * @param page_amount    number of pages we want to print. Here it's static because it's just a test, but in
	 *                       real code you will often want to calculate dynamically once you know the size of the
	 *                       printable area from page setup
	 * @param title          name of the print job / of the printed document
	 * @param units_per_cem  we'll use this to determine the coordinate system; it describes the number of units
	 *                       per centimeter (i.e. how fine the coordinate system is)
	 */
	spPrintDriver(int page_amount, wxString title, spCanvas *canvas1,float m_units_per_cm1) : wxPrintout( title )
	{
		m_page_amount = page_amount;
		
		m_orient = wxPORTRAIT; // wxPORTRAIT, wxLANDSCAPE
		m_paper_type = wxPAPER_LETTER;
		m_margin_left   = 16;
		m_margin_right  = 16;
		m_margin_top    = 32;
		m_margin_bottom = 32;
		
		m_units_per_cm = m_units_per_cm1;
		canvas = canvas1;
	}
	
	
	//
    // Compute the m_page_setup using either default values or dialog
    //
    // returns false if user cancelled printing otherwise returns true
    //
	bool PerformPageSetup(const bool showPageSetupDialog)
	{
		// don't show page setup dialog, use default values
		wxPrintData printdata;
        
        printdata.SetPrintMode( wxPRINT_MODE_PRINTER );
        
		printdata.SetOrientation( m_orient );
		printdata.SetNoCopies(1);
		printdata.SetPaperId( m_paper_type );
		
		m_page_setup = wxPageSetupDialogData(printdata);
		m_page_setup.SetMarginTopLeft    (wxPoint(m_margin_left,  m_margin_top));
		m_page_setup.SetMarginBottomRight(wxPoint(m_margin_right, m_margin_bottom));
		
		if (showPageSetupDialog)
		{
			wxPageSetupDialog dialog( NULL, &m_page_setup );
			if (dialog.ShowModal() == wxID_OK)
			{
				
				m_page_setup = dialog.GetPageSetupData();
				m_orient = m_page_setup.GetPrintData().GetOrientation();
				m_paper_type = m_page_setup.GetPrintData().GetPaperId();
				
				wxPoint marginTopLeft = m_page_setup.GetMarginTopLeft();
				wxPoint marginBottomRight = m_page_setup.GetMarginBottomRight();
				m_margin_left   = marginTopLeft.x;
				m_margin_right  = marginBottomRight.x;
				m_margin_top    = marginTopLeft.y;
				m_margin_bottom = marginBottomRight.y;
			}
			else
			{
				return false;
			}
		}
		return true;
	}
	
    //
    // Get print data from the page setup
    //
	wxPrintData GetPrintData()
	{
		return m_page_setup.GetPrintData();
	}
	
    
    //
	// Called when printing starts
    //
	void OnBeginPrinting()
	{
                // Case we want to use the same resolution as the window
        if (m_units_per_cm <= 0) canvas->GetSize(m_coord_system_width,m_coord_system_height);
        
        // Case we want to use units_per_cm
        else {
            wxSize paperSize = m_page_setup.GetPaperSize();  // in millimeters
            
            // still in millimeters
            float large_side = std::max(paperSize.GetWidth(), paperSize.GetHeight());
            float small_side = std::min(paperSize.GetWidth(), paperSize.GetHeight());
            
            float large_side_cm = large_side / 10.0f;  // in centimeters
            float small_side_cm = small_side / 10.0f;  // in centimeters
            
            
            if (m_orient == wxPORTRAIT)
            {
                float ratio = float(large_side - m_margin_top  - m_margin_bottom) /
                float(small_side - m_margin_left - m_margin_right);
                
                m_coord_system_width  = (int)((small_side_cm - m_margin_left/10.f -
                                               m_margin_right/10.0f)*m_units_per_cm);
                m_coord_system_height = m_coord_system_width*ratio;
            }
            else
            {
                float ratio = float(large_side - m_margin_left - m_margin_right) /
                float(small_side - m_margin_top  - m_margin_bottom);
                
                m_coord_system_height = (int)((small_side_cm - m_margin_top/10.0f -
                                               m_margin_bottom/10.0f)* m_units_per_cm);
                m_coord_system_width  = m_coord_system_height*ratio;
                
            }
        }
         
	}
    
    
	//
    // Called when starting to print a document
    //
	bool OnBeginDocument(int startPage, int endPage)
	{
		return wxPrintout::OnBeginDocument(startPage, endPage);
	}
	    
	//
    // wx will call this to know how many pages can be printed
    //
	void GetPageInfo(int *minPage, int *maxPage, int *pageSelFrom, int *pageSelTo)
	{
		*minPage = 1;
		*maxPage = m_page_amount;
		
		*pageSelFrom = 1;
		*pageSelTo = m_page_amount;
	}
	
    //
	// called by wx to know what pages this document has
    //
	bool HasPage(int pageNum)
	{
		// wx will call this to know how many pages can be printed
		return pageNum >= 1 && pageNum <= m_page_amount;
	}
	
	
	//
    // called by wx everytime it's time to render a specific page onto the
    // printing device context
    //
	bool OnPrintPage(int pageNum)
	{
        FitThisSizeToPageMargins(wxSize(m_coord_system_width, m_coord_system_height), m_page_setup);
        
		wxDC* ptr = GetDC();
		if (ptr==NULL || !ptr->IsOk()) return false;
		wxDC& dc = *ptr;
		
        dc.Clear();
        canvas->SendToDC(ptr,wxRect(0,0,m_coord_system_width,m_coord_system_height));
        
		return true;
	}
	
	//
    // Called when printing is done. I have nothing to do in this case
    //
	void OnEndPrinting()
	{
	}
};

//
// Routine to print the canvas on a printer
//
void spPrintCanvas(spCanvas *canvas,int units_per_cm,bool flagPageSetupDialog,bool flagPrintDialog)
{
    int height,width;
    canvas->GetSize(width, height);
        
    // Create a spPrintDriver object
    int nPages = 1;
    spPrintDriver*  driver = new spPrintDriver(nPages,wxT("wxPrint test"),canvas,units_per_cm);
        
    // Compute page_setup (if user cancelled printing returns false)
    if (!driver->PerformPageSetup(flagPageSetupDialog)) return;
	
   // Get print data from the page_setup
	wxPrintDialogData data(driver->GetPrintData());
    
        // Create printer object from data
        wxPrinter printer(&data);
    
        bool success = printer.Print(NULL, driver, flagPrintDialog);
    
    // Cleaning
	delete driver;
	
	if (!success) spWarningf("spPrintCanvas()","Printing failed");
}

//#include <wx/app.h>

void spCanvasToPS(spCanvas *canvas,char *filename,double x_margin_cm,double y_margin_cm)
{
    wxPrintData data;
    
//    data.SetFontMetricPath(wxGetApp().GetFontPath());
    data.SetPrintMode( wxPRINT_MODE_FILE );
    data.SetFilename(filename);
    data.SetOrientation (wxPORTRAIT); // wxPORTRAIT, wxLANDSCAPE
    data.SetPaperId (wxPAPER_LETTER);
    double paper_width_cm = 8.5*2.54;
    double paper_height_cm = 11*2.54;
    
    // Set the resolution in points per inch (the default is 720)
    // dc.SetResolution(1440);
    
    wxPostScriptDC dc1(data);
        
    if (dc1.IsOk()) {
        int w,h;
        dc1.GetSize (&w, &h);
    
        int w1 = w*(paper_width_cm-2*x_margin_cm)/paper_width_cm;
        double x = w*x_margin_cm/paper_width_cm;
        double y = h*y_margin_cm/paper_height_cm;
    
        dc1.SetLogicalOrigin (-x,-y); // Pour une marge en haut à gauche !!
        dc1.StartDoc("Printing PS");
        
        canvas->SendToDC(&dc1,wxRect(0,0,w1,h));
        dc1.EndDoc();
    }
    
    spWarningf("spCanvasToPS()","Postscript file failed");
}

    
    

void spCanvasToSVG(spCanvas *canvas,char *filename)
{
    int height,width;
    canvas->GetSize(width, height);
    wxSVGFileDC dc (filename, width,height);
    
    canvas->SendToDC(&dc,wxRect(0,0,width,height));
}







#ifndef _SCRIPT_H_
#define _SCRIPT_H_

#ifdef SP_SCRIPT
class spRef;
extern void *SCRIPT_GET_DIRECTOR(spRef * ref);
extern void SCRIPT_StartEvent();
extern void SCRIPT_EndEvent();
extern void SCRIPT_DECREF(void * ref); 
extern void SCRIPT_INCREF(void * ref); 
extern void SCRIPT_PrintErrorMsge(const char *msge);
extern void SCRIPT_Warning(const char *msge);
extern void SCRIPT_Config(spRef *ref);
#endif


#endif

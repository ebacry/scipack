
#ifndef _SP_INTERACTOR_POPUP_H
#define _SP_INTERACTOR_POPUP_H

class spObject;

class spInteractorContextMenu;
class _Item
{
	public : 
	spInteractorContextMenu *i;
	int id;
	void (*cb)(spObject *);
	_Item(spInteractorContextMenu *i1, int id1, void (*cb1)(spObject *)) : i(i1),id(id1),cb(cb1) {};
};

class spInteractorContextMenu : public spInteractor1 {
	
	public : 
	
	
	static int _menu_id;
	static void ProcessPopup(int id);

	static spList<_Item,100> list;

	wxMenu menu;
	
	spInteractorContextMenu(spEventSimpleType e,int modifiers1,bool flagPropagate=false,const char *name = NULL);
	virtual ~spInteractorContextMenu();
	
	virtual bool CheckObjectIsValid(spObject *);
	void AddItem(const char *text,void (*cb)(spObject *));
    
    virtual const char *GetClassName() {
		return _CCHAR("spInteractorContextMenu");
	}
};

class spInteractorLogScaleMenu : public spInteractorContextMenu {
	public : 

	virtual bool OnlyOnViews() { return true;};
	spInteractorLogScaleMenu(spEventSimpleType e,int modifiers1,bool flagPropagate=false,const char *name = NULL);

    virtual const char *GetClassName() {
		return _CCHAR("spInteractorLogScaleMenu");
	}
	
};


#endif
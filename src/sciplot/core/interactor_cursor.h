
#ifndef _SP_INTERACTOR_CURSOR_H
#define _SP_INTERACTOR_CURSOR_H

class spObject;


//
// A cursor interactor to be attached to a view
//
class spInteractorCursorView : public spInteractorN {
	
	public : 

	virtual bool OnlyOnViews() { return true;};

	// A boolean to track whether the mouse is in the view or not
	bool flagIn;
	
	// The overlay object used for the cursor
	spObject *cursor;        

	// If flag is set to true then the cursor is drawn sticking to the mouse (and not sticking to the closes found object)
	bool flagFollowMouse;
	inline void SetFollowMouse(bool flag=true) {
		flagFollowMouse = flag;
	}
	inline bool GetFollowMouse() {
		return flagFollowMouse;
	}

	// An offset to draw the cursor
	spPoint offset;
	inline void SetOffset(spPoint offset1) {
		offset = offset1;
	}
	inline spPoint GetOffset() {
		return offset;
	}

	// If true a full info (versus short) is printed on the output
	bool flagFullInfo;
	inline void SetFullInfo(bool flag=true) {
		flagFullInfo = flag;
	}
	inline bool GetFullInfo() {
		return flagFullInfo;
	}
	
	// An object to display information
	// If output is NULL and flagPrintInfo is true, message is printed on the father object of the view
	SP_SMARTPTR(spObject) output;
	void SetOutput(spObject *o);
	bool flagPrintInfo;
	inline void SetFlagPrintInfo(bool flag=true) {
		flagPrintInfo = flag;
	}
	
	
	//
	// Constructor
	//
	//     cursor : the cursor object (i.e., the object that will be drawn to represent the cursor)
	//     delta  : an offset for drawing the cursor (relative to the mouse position)
	//     flagFollowMouse  : if true then the cursor follows the mouse (and do not find the closest object)
	//
	spInteractorCursorView(spObject *cursor, spPoint offset = spPoint(0,0), bool flagFollowMouse = true, bool flagPropagate=true,const char *name = NULL);
	~spInteractorCursorView();
	
	virtual const char *GetClassName() {
		return _CCHAR("spInteractorN");		
	}	
	
	virtual void Off();
	
};



#endif
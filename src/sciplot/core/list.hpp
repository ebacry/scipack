/*
 *  list.hpp
 *  LastPlot
 *
 *  Created by bacry on 02/10/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _SPLIST_
#define _SPLIST_

template <class T,int START=0, int INCR=10> class spList
{	
	T **list;
	int nList,nAllocList;
	
	public : 
	
	spList() {
	  nList = nAllocList = 0;
	  list = NULL;		
		if (START != 0) {
			list = new T *[START];
			nAllocList = START;			
		}
	}
	~spList() {
		if (list != NULL) delete [] list;
	}
	
	void Add(T *elem)
	{		
		if (nList+1>nAllocList) {
			if (nAllocList != 0) {
				T **c1 = list;
				list = new T *[nAllocList+INCR];
				memcpy(list,c1,sizeof(T*)*nList);
				delete [] c1;
			}
			else {
				list = new T *[nAllocList+INCR];
			}
			nAllocList+=INCR;
		}
		list[nList] = elem;
		nList+=1;
	}

	bool Remove(T *elem)
	{
		int i;
		for (i=0;i<nList;i++) {
			if (list[i] == elem) break;
		}
		
		if (i==nList) return false;
		memmove(list+i,list+i+1,sizeof(T*)*(nList-1-i));
		nList--;	
		return true;
	}

	inline T *operator [](int i) {
		return (i<0 || i>nList ? NULL : list[i]);
	}

	inline T *Last(int i=0) {
		i = nList-1+i;	
		return (i<0 || i>nList ? NULL : list[i]);
	}
	
	inline int Length() {
		return nList;
	}

	inline int Size() {
		return nList;
	}
	
	void Init() 
	{
		nList = 0;
	}
};

#endif
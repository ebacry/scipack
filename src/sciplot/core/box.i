
// Not a module

%rename(Anchor) spAnchor;
%rename(AnchorGrid) spAnchorGrid;
%rename(AnchorPixel) spAnchorPixel;
%rename(AnchorFunc) spAnchorFunc;
%rename(AnchorNone) spAnchorNone;
%rename(MarginGrid) spMarginGrid;
%rename(MarginPixel) spMarginPixel;
%rename(MarginCentered) spMarginCentered;
%rename(MarginCenteredPixel) spMarginCenteredPixel;
%rename(MarginInfinite) spMarginInfinite;
%rename(Margin) spMargin;
%rename(MarginFunc) spMarginFunc;


/////////////////////////////
//
//
// Anchor class 
//
// This class is used to code the anchor of the bounding box of an object
//
//
/////////////////////////////

%DocClass(spAnchor,"This class is used to code the anchor point of an object. It is coded using a sp.RealPoint (the anchor point).");
class spAnchor
{
  public : 
	
	%extend {
        std::string __repr__() {return self->ToStr(false);}
        std::string __str__() {return self->ToStr(true);}
	}

    // Constructor
	spAnchor();
	spAnchor(double x, double y);
	spAnchor(spRealPoint pt);
	
//	DocAStr(Set,"Set(spRealPoint)","Sets the anchor","");
//	void Set(spObject *object, spRealPoint anchor1);
//	void Move(spObject *object,double dx, double dy);
};


%newgroup


// The class to build an empty Anchor
%DocClass(spAnchorNone,"This class should be used for an object which does not have any anchor.");
class spAnchorNone : public spAnchor
{
	public :
	
	spAnchorNone();
};

%newgroup

%DocClass(spAnchorGrid,"This class is used to code the anchor point using grid coordinates. When used for an object, the container of this object should be a sp.Grid");
class spAnchorGrid : public spAnchor
{
  public : 
	
  spAnchorGrid(double x=0, double y=0);

  %extend {
        std::string __repr__() {return self->ToStr(false);}
        std::string __str__() {return self->ToStr(true);}
	}
    
};


%DocClass(spAnchorFunc,"This class is used when the anchor is obtained through the return value of a callable. The callable should have a single argument which corresponds to the object associated to this anchor. It should return an instance of any class deriving from sp.Anchor");
class spAnchorFunc : public spAnchor
{	
	public : 	
};


%{
	//
    // This function calls the anchor script function and returns the computed anchor
	//
	spAnchor getAnchorFuncScript(spObject *object)
	{
		// Get the python callback
		PyObject *callback = (PyObject *) object->box.GetAnchorFuncPtr().scriptFunc;
		if (callback == NULL) return spAnchor(); // should never happen

		// Get the proxy instance associated to object (it adds a reference)
		PyObject *self = WrapObject(object);
		if (self == NULL) return spAnchor(); // Should never happen

		// Make the python call, get the result, manage eventual errorin the function
		PyObject *arglist = Py_BuildValue("(O)", self);
		PyObject* result = PyObject_CallObject(callback, arglist);
		Py_XDECREF(arglist);

        // So now we have to delete the reference we created calling WrapObject
        Py_XDECREF(self); // If not a director, it will Unref automatically and the object is going to be destroyed !

		if (result == NULL) {
			PrintPyError();
			return spAnchor();
		}
		
		// Extract the spAnchor from the result 
		// Generate an error if it is not
		spAnchor *anchorPtr;
        spAnchor anchor;

		bool success = (SWIG_Python_ConvertPtr(result, (void**) &anchorPtr, SWIGTYPE_p_spAnchor, SWIG_POINTER_EXCEPTION) != -1);
        if (success) {
            anchor = *anchorPtr;
            Py_XDECREF(result);
        }

		if (success) return anchor;
        
        // ?? BOF : marche mal
		spWarningf("","Error in sp.Anchor.anchorFuncPtr : A python anchor call back function should return an sp.Anchor object\n");
        PrintPyError();
		return spAnchor();
	};
%}


// 
// The constructor from python
//
%extend spAnchorFunc {		
	spAnchorFunc(PyObject* callback,bool flagMoveable1=false) : spAnchor() {
        // We create a spAnchorFunc with a NULL function
		spAnchorFunc *self = new spAnchorFunc();
		self->SetMoveable(flagMoveable1);
        
        // Check that the the argument is a callable and set it in self->anchorFuncPtr.scriptFunc
		if (!PyCallable_Check(callback)) {
            PyErr_SetString(PyExc_RuntimeError,"The first argument should be a callable");
            return NULL;
        }
        Py_XINCREF(callback);
		self->anchorFuncPtr.SetScriptFunc(callback);

        // Then set the self->anchorFuncPtr.getAnchorFunc
		extern spAnchor getAnchorFuncScript(spObject*object);        
		self->anchorFuncPtr.SetGetFunc(&getAnchorFuncScript);

		return self;
	}

}



%newgroup


/////////////////////////////
//
//
// margin class 
//
// This class is used to code the margins of the bounding box of an object
//
//
/////////////////////////////

%DocClass(spMargin,"This class is used to code a margin around the anchor point of an object. The margin is specified by 4 numbers [x,y,mx,my]. If (ax,ay) is the anchor then the associated rect is defined by the four points (ax-mx,ay-my), (ax-mx,ay+y) (ax+x,ay+y) and (ax+x,ay-my).");
class spMargin
{
	public :
	
	%extend {
        std::string __repr__() {return self->ToStr(false);}
        std::string __str__() {return self->ToStr(true);}
	}

	spMargin();
	spMargin(double x,double y);
	spMargin(spRealPoint xy);
	spMargin(double x,double y, double mx, double my);
	
//    DocStr(SetMargin,"Set(xMargin,yMargin,mxMargin=0,myMargin=0) : Sets the margin", "");
//    void Set(double xMargin1, double yMargin1, double mxMargin1=0, double myMargin1=0);
	
//	DocStr(SetPixelMargin,"SetPixelMargin(pixelMargin) : Sets a frame margin (expressed in pixels) on top of the regular margin", "");
//	void SetPixelMargin(int pm);

//    void SetSize(spRealPoint dv);
//    void SetOriginOposite(spObject *object,spRealPoint pt);
};


%newgroup
%DocClass(spMarginGrid,"This class is used to code a margin around the anchor point of an object using grid coordinates. This type of margin should be used with an object inside an sp.Grid. The margin is specified by 2 numbers [x,y]. If (ax,ay) is the anchor point in sp.Grid coordrinates, the associated rect is defined by the four points (in sp.Grid coordinates) (ax,ay), (ax,ay+y) (ax+x,ay+y) and (ax+x,ay)")
class spMarginGrid : public spMargin
{
	public : 
	
    %extend {
        std::string __repr__() {return self->ToStr(false);}
        std::string __str__() {return self->ToStr(true);}
	}
    
    spMarginGrid(double x,double y);
    spMarginGrid(spRealPoint xy);
};

%newgroup

%DocClass(spMarginPixel,"This class is used to code a margin around the anchor point of an object using pixel units.")
class spMarginPixel : public spMargin
{
	public : 
	
    %extend {
        std::string __repr__() {return self->ToStr(false);}
        std::string __str__() {return self->ToStr(true);}
	}

    spMarginPixel(double x,double y);
    spMarginPixel(spRealPoint xy);
    spMarginPixel(double x,double y, double mx, double my);
};

%DocClass(spMarginCenteredPixel,"This class is used to code a margin around the anchor point of an object using pixel units. The anchor point is at the center of the associated rect. Thus margin is specified by 2 numbers [x,y]. If (ax,ay) is the anchor point in window's coordrinates, the associated rect is defined by the four points (in window's coordinates) (ax-x,ay-y), (ax-x,ay+y) (ax+x,ay+y) and (ax+x,ay-y)")
class spMarginCenteredPixel : public spMargin
{
	public :
    
    %extend {
        std::string __repr__() {return self->ToStr(false);}
        std::string __str__() {return self->ToStr(true);}
	}
    
    spMarginCenteredPixel(double x,double y);
    spMarginCenteredPixel(spRealPoint xy);
};

%DocClass(spMarginCentered,"This class is used to code a margin around the anchor point of an object. The anchor point is at the center of the associated rect. Thus margin is specified by 2 numbers [x,y]. If (ax,ay) is the anchor point, the associated rect is defined by the four points (ax-x,ay-y), (ax-x,ay+y) (ax+x,ay+y) and (ax+x,ay-y)")
class spMarginCentered : public spMargin
{
	public :
    
    %extend {
        std::string __repr__() {return self->ToStr(false);}
        std::string __str__() {return self->ToStr(true);}
	}
    
    spMarginCentered(double x,double y);
    spMarginCentered(spRealPoint xy);
};


%newgroup

%DocClass(spMarginInfinite,"This class is used to code an infinite margin")
class spMarginInfinite : public spMargin
{
	public :
	
	spMarginInfinite();
    
	%extend {
        std::string __repr__() {return self->ToStr(false);}
        std::string __str__() {return self->ToStr(true);}
	}
};


%DocClass(spMarginFunc,"This class is used when the margin is obtained through the return value of a callable. The callable should have a single argument which corresponds to the object associated to this margin. It should return an instance of any class deriving from sp.Margin");
class spMarginFunc : public spMargin
{	
	public : 	
};



%{
	//
    // This function calls the margin script function and returns the computed margin
	//
	spMargin getMarginFuncScript(spObject *object)
	{
		// Get the python callback
		PyObject *callback = (PyObject *) object->box.GetMarginFuncPtr().scriptFunc;
		if (callback == NULL) return spMargin(); // should never happen

		// Get the proxy instance associated to object (it adds a reference)
		PyObject *self = WrapObject(object);
		if (self == NULL) return spMargin(); // Should never happen

		// Make the python call, get the result, manage eventual errorin the function
		PyObject *arglist = Py_BuildValue("(O)", self);
		PyObject* result = PyObject_CallObject(callback, arglist);
		Py_XDECREF(arglist);

        // So now we have to delete the reference we created calling WrapObject
        Py_XDECREF(self); // If not a director, it will Unref automatically and the object is going to be destroyed !

		if (result == NULL) {
			PrintPyError();
			return spMargin();
		}
		
		// Extract the spMargin from the result 
		// Generate an error if it is not
		spMargin *marginPtr;
        spMargin margin;

		bool success = (SWIG_Python_ConvertPtr(result, (void**) &marginPtr, SWIGTYPE_p_spMargin, SWIG_POINTER_EXCEPTION) != -1);
        if (success) {
            margin = *marginPtr;
            Py_XDECREF(result);
        }

		if (success) return margin;
        
        // BOF : marche mal
		spWarningf("","Error in sp.Margin.marginFuncPtr : A python anchor call back function should return an sp.Margin object\n");
        PrintPyError();
		return spMargin();
	};
%}



// 
// The constructor from python suing a callable
//
%extend spMarginFunc {		
	spMarginFunc(PyObject* callback) : spMargin() {
        // We create a spMarginFunc with a NULL function
		spMarginFunc *self = new spMarginFunc();
        
        // Check that the the argument is a callable and set it in self->anchorFuncPtr.scriptFunc
		if (!PyCallable_Check(callback)) {
            PyErr_SetString(PyExc_RuntimeError,"The first argument should be a callable");
            return NULL;
        }
        Py_XINCREF(callback);
		self->marginFuncPtr.SetScriptFunc(callback);

        // Then set the self->marginFuncPtr.getMarginFunc
		extern spMargin getMarginFuncScript(spObject*object);
		self->marginFuncPtr.SetGetFunc(&getMarginFuncScript);

		return self;
	}

}

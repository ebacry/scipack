
#ifndef _SP_TextBox_H_
#define _SP_TextBox_H_

class spText : public spObject
{
	private :
 			

	public :

	spPoint frameMargin;
	
	spFormattedText ft;
	
	spText(spContainer *ctn=NULL,const spAnchor &anchor1=spAnchor(), int hmode1=spJustifyHLeft, int vmode1=spJustifyVBase, unsigned long flags=0,const char *name=NULL);
	~spText();

	// Set a frame
	void SetFrameMargin(spRealPoint pt=spRealPoint(5,2));
	spRealPoint GetFrameMargin() {return frameMargin;}
	bool IsFrame() {return (frameMargin.x >= 0 || frameMargin.y >= 0);}
	
	// The justification relative to the anchor
	void SetHVMode(int hmode1, int vmode1);
	void SetHMode(int hmode);
	void SetVMode(int hmode);
	int GetHMode() {return ft.hMode;}
	int GetVMode(){return ft.vMode;}
	
	// Get/Set the text
	virtual void SetText(const char *str);
	inline const char *GetText() {return ft.str;}
    
    // Set/Get the angle
	void SetAngle(double angle);
	double GetAngle(){return ft.angle;}
    
//  bool FitFrame2Text(bool flagFitFrameToText);
	
	virtual void Draw(spRect &rect);
	
	virtual const char *GetClassName() {
		return _CCHAR("spText");
	}
	
	
};



#endif

/*
 *  error.cpp
 *  sp
 *
 *  Created by bacry on 17/11/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


#include "sp.h"
#include <stdio.h>
#include <stdarg.h>

static char tempStr[10000];
static char header[] = "*** Warning : ";
void spWarningf(const char *funcname, const char *format,...)
{
    va_list ap;
    
    va_start(ap,format);
    
#ifndef SP_SCRIPT
    strcpy(tempStr,header);
    if (funcname) {
        strcat(tempStr,funcname);
        strcat(tempStr," : ");
    }
    vsprintf(tempStr+strlen(tempStr),format,ap);
    va_end(ap);
    cerr << tempStr << endl;
#else
    vsprintf(tempStr,format,ap);
    va_end(ap);
    SCRIPT_Warning(tempStr);
#endif
}





#ifndef _SP_EVENT_H
#define _SP_EVENT_H


//
//
// SciPlot is an event driven application. It uses a callback system (as wxWidget).
//
// An object in SciPlot can manage events using interactors (interactor is just a fancy name for callback!). 
// For most events the mechanism for processing the event is the following
//
//   1- Look for the first object that will receive the event (using the GetDistance(wxPoint point) method of the basic class spObject) using a downward recursive algorithm
//      a- Starts from the topview associated to the canvas and makes it the current object
//      b- If the current object is not a view then we are done
//      c- If the current object is a view, loop on all the objects of this view and call the GetDistance(wxPoint mouse) method (where mouse is the position of the mouse using global coordoinates)
//         If one of the objects GetDistance method returns 0 then it is made the current object and go back to b-
//         If all of the objects GetDistance method returns -1 then we are done (the view is the object looked for)
//         Otherwise the object with the smallest distance is made the current object and we go back to b-
//   At the end of this procedure, the current object is the object looked for and the event hierarchy is the object hierarchy of this object.
//
//   2- We send the event to the found object.
//      If this object has some interactors that can process the type of the event processed then these interactors are all called 
//        and the processing of the event is over unless all the interactors processed called the method spInteractor::Propagate()
//      If all the interactors that can process the of event processed called spInteractor::Propagate() or if there is no interactor that can process the event then 
//        the event is sent to the view containing the object and we start 2- again (i.e., we follow the event hierarchy)
//
//   There are some exceptions to this processing mechanism. The main ones concern "dragging" events (i.e., mouse button pressed and mouse dragged).
// 
// Special case of the "dragging" mouse processing : 
//    
//   - ButtonDown/ButtonUp (i.e., spOnDownLeft, spOnDownRight, spOnDownMiddle and spOnUpLeft, spOnUpRight, spOnUpMiddle) events cannot be Propagated, i.e., they cannot be processed by several objects
//   - When a mouse button has been pressed, the mouse is "grabbed" by the object which received the ButtonDown event.
//     It means that :
//        - the corresponding ButtonUp event will be sent to the same object (wherever the mouse is) forcing the smae modifier keys (shift, ctrl,...) that were used when button was pressed
//        - the hierarchy of this object is remembered and will be used as the event hierarchy following the mechanism above WHEREVER the mouse goes (even if it goes out of the window)
//        - All the motions of the mouse (as long as the button is down) will be sent to the same object as "Drag" events (i.e., spOnDragLeft, spOnDragRight or spOnDragMiddle) using the same modifier keys
//        - If these "Drag" events are propagated then they are sent upward (following the initial hierarchy) as "regular" motion events (i.e., spOnMotionLeft, spOnMotionRight, spOnMotionMiddle)
//          using the same modifier keys.
//          Thus a given "Drag" event can be processed only by a single object.      
//        - When the mouse is grabbed no other button down event can be processed.
//
// Other events specificities : 
//
//   - enter/leave events (spOnEnter, spOnLeave) are always sent to any object of the event hierachy the mouse goes in or out for the first time. 
//     For these events, the Interactor::Propagate method has no effect.
//     Let us note that if the mouse is grabbed, then no enter/leave events will be sent before it is ungrabbed.
//   - an interactor answering to spOnAnyMotion event will answer to any motion of the mouse (with any or no button pressed with any or no modifer keys)
//   - a spOnChangeBound event is sent to any view whose one or several of xMin,xMax,yMin,yMax change (the Interactor::Propagate method has no effect)
//   - a spOnChangedGeometry event is sent to any object whose bounding rect changes (the Interactor::Propagate method has no effect)
//   - a spOnBeforeDraw event is sent to any object before its Draw method is called (the Interactor::Propagate method has no effect)
//   - a spOnAfterDraw event is sent to any object right after its Draw method is called (the Interactor::Propagate method has no effect)
//   - a spOnDelete event is sent to any object right before it is deleted
//
// 	
// The type of an event is one of 
//
//    spOnDownLeft, spOnDownRight, spOnDownMiddle            --> Button down events (with eventual modifiers SP_MOD_SHIFT, SP_MOD_META, SP_MOD_CTRL, SP_MOD_ALT)
//    spOnUpLeft, spOnUpRight, spOnUpMiddle                  --> Button up events (with eventual modifiers SP_MOD_SHIFT, SP_MOD_META, SP_MOD_CTRL, SP_MOD_ALT)
//    spOnMotion                                             --> Motion with no button pressed (with eventual modifiers SP_MOD_SHIFT, SP_MOD_META, SP_MOD_CTRL, SP_MOD_ALT)
//    spOnMotionLeft, spOnMotionRight, spOnMotionMiddle,     --> Motion with a specific button pressed (with eventual modifiers SP_MOD_SHIFT, SP_MOD_META, SP_MOD_CTRL, SP_MOD_ALT)
//    spOnDragLeft, spOnDragRight, spOnDragMiddle,           --> Dragging with a specific button pressed (with eventual modifiers SP_MOD_SHIFT, SP_MOD_META, SP_MOD_CTRL, SP_MOD_ALT)
//    spOnEnter, spOnLeave                                   --> Enter/Leave an object
//    spOnMouseWheel                                         --> Mouse Wheel is turned (with eventual modifiers SP_MOD_SHIFT, SP_MOD_META, SP_MOD_CTRL, SP_MOD_ALT)
//    spOnBeforeDraw, spOnAfterDraw
//    spOnChangedGeometry
//    spOnChangedBound
//    spOnDelete                                             --> Sent before an object is deleted
//
// An interactor can answer any of these types. Morever if an interactor answers the spOnAnyMotion type, it means that it will process all the events whose type is one among :
//    spOnMotion,spOnMotionLeft, spOnMotionRight, spOnMotionMiddle, spOnDragLeft, spOnDragRight, spOnDragMiddle with any modifier keys
//


//
// The diffent simple types of event
// spEventSimpleType do not hold any information about modifiers or keys contrarily to spEventType
//
typedef enum {
	spOnNone = 0,      // Only used internally
    spOnKeyDown,

	spOnDownLeft,
	spOnUpLeft,
	spOnDragLeft,
	spOnDLeft,
	spOnMotionLeft,
    
	spOnDownRight,
    spOnUpRight,
	spOnDragRight,
	spOnDRight,
	spOnMotionRight,
	
	spOnDownMiddle,
	spOnUpMiddle,
	spOnDragMiddle,
	spOnDMiddle,
	spOnMotionMiddle,

    spOnMotion,
    spOnAnyMotion,
    
	spOnEnter,
	spOnLeave,
	spOnMsge,
	spOnBeforeDraw,
	spOnAfterDraw,
	spOnComputeBound,
	spOnChangeBound,
	spOnDelete,
	spOnChangedGeometry,	
	spOnMouseWheel,	
	spOnTime,
    
    spLAST_EVENT_SIMPLETYPE   // Should always be last

} spEventSimpleType;


typedef enum {
    spMsgeSimple = 0,
    spMsgeCursor
} spMsge;

//
// The modifier keys
//
enum {
	SP_MOD_NONE = 0,
	SP_MOD_SHIFT = 1<<0,
	SP_MOD_META = 1<<1,
	SP_MOD_CTRL = 1<< 2,
	SP_MOD_ALT = 1 << 3
};


//
// The EventType class is a EventSimpleType along with a modifier (or a key if keyEvent)
//
class spEventType {
    
    public :
    int type;
    unsigned long modifiers;
    
    spEventType(int eventSimpleType=spOnNone,unsigned long modifiers1=SP_MOD_NONE) {
        type = eventSimpleType;
        modifiers = modifiers1;
    }
    spEventType(char *key) {
        type = spOnKeyDown;
        modifiers = key[0];
    }
    spEventType(spEventType const &et) {
        type = et.type;
        modifiers = et.modifiers;
    }
   
    // This is useful in order to go from Down -> Up -> Drag -> DClick -> Motion
    spEventType operator + (short n) {
        return spEventType(type+n,modifiers);
    }
    spEventType operator - (short n) {
        return spEventType(type-n,modifiers);
    }
    
    spEventType DownToUp() {return *this + 1;}
    spEventType UpToDown() {return *this - 1;}
    spEventType DownToDrag() {return *this + 2;}
    spEventType DragToDown() {return *this - 2;}
    spEventType UpToDrag() {return *this + 1;}
    spEventType DragToUp() {return *this - 1;}
    spEventType DownToMotion() {return *this + 4;}
    spEventType MotionToDown() {return *this - 4;}
    spEventType UpToMotion() {return *this + 3;}
    spEventType MotionToUp() {return *this - 3;}
    spEventType DragToMotion() {return *this + 2;}
    spEventType MotionToDrag() {return *this - 2;}
    
    bool IsButtonDown() {
        return (type == spOnDownLeft || type == spOnDownRight || type == spOnDownMiddle);
    }
    bool IsButtonUp() {
        return (type == spOnUpLeft || type == spOnUpRight || type == spOnUpMiddle);
    }
    bool IsButtonDrag() {
        return (type == spOnDragLeft || type == spOnDragRight || type == spOnDragMiddle);
    }
};


//
// The class used to represent an event
//
// NEVER HOLD AN EVENT IN MEMORY (there is no reference count on objects)
//
class spTimer;
class spEvent {
  
	public : 
	
	// 
	// Static variables to handle the dragging mechanism of the mouse
	//
	static spObject *menuObject;
	static bool flagDrag;                   // true if the mouse is currently being dragged
	static spEventSimpleType buttonDrag;          // The button that is being dragged (one of spOnDownLeft, spOnDownRight, spOnDownMiddle)
	static unsigned int modifiersDrag;      // The original modifier keys when mouse button was pressed
	static spObject* objectDrag;            // The object which received the initial button down event
	static spObject *hierarchy[1000];       // The hierarchy of the objectDrag (last object of this hierarchy is objectDrag)
	static int nHierarchy;                  // The length of the hierarchy (thus hierarchy[nHierarchy-1] == objectDrag)
	static int nCurHierarchy;               // Used internally while processing events to keep track of the current depth in the hierarchy
	static bool flagPropagate;              // Used to remember wether an interactor asked for propagation
	
	//
	// Static method to perform Propagation
	//
	static void Propagate(bool f=true) {
		flagPropagate = f;
	};
    static bool IsPropagating() {
		return flagPropagate;
	}

	//
	// Static method to indicate that dragging starts/ends
	//
	static void DragStart(spEvent &event);
	static void DragEnd() {
		flagDrag = false;
	}
	static bool Dragging() {
		return flagDrag;
	}
	
	
	//
	// Last mouse motion event
	//
	static spPoint lastMouseMotion;
	static spCanvas *lastMouseCanvas;
	
	//
	// Useful variables for Bound events
    //
	double xMin,yMin,xMax,yMax;
	bool flagXFixed;
	void ComputeMinXYBound(bool flagXFixed1,double xMin1,double xMax1,double yMin1,double yMax1) {
        flagXFixed = flagXFixed1;
		xMin = xMin1;
		xMax = xMax1;
		yMin = yMin1;
		yMax = yMax1;
	}
	
 	//
	// Main fields
	//
	spEventSimpleType type;      // Type of the event
	spPoint mouse;               // Mouse global coordinates	
	unsigned int modifiers;      // Modifiers
	int wheel;                   // rotation value of the wheel in case of spOnMouseWheel
	spObject *object;            // First object that processed the event
	int delay;                   // Delay of a timer event
	spTimer *timer;              // The associated timer if a Timer event
	int keycode;                 // key code if a key is pressed
	spObject *sender;            // sender of a message  in a case of a MsgeEvent
	spObject *receiver;          // receiver of a message in a case of a MsgeEvent
	
	//
	// Constructor/Destructor
    //
    // NEVER HOLD AN EVENT IN MEMORY (there is no reference count on objects)
	//
	spEvent() {
		modifiers = 0;
		wheel = 0;
		object = NULL;
		keycode = 0;
        sender = NULL;
        receiver = NULL;
	}    
};


////////////////////////////////////
//
// This is the sp timer class
//
// It basically wraps a wxTimer class
//
////////////////////////////////////

class spTimer;

// It basically encapsulates a wxTimer (called myWxTimer)
class myWxTimer : public wxTimer 
{
	public : 
	spTimer *timer;
	myWxTimer(spTimer *timer1);
	virtual void Notify();
};



class spTimer
{
	spObject *object;

	public :
	
    static spList<spTimer,100,100> list;
	
	myWxTimer *wxtimer;
	
	spTimer();
	virtual ~spTimer();
	
	void Notify();
	
	
	int GetInterval() {
		return wxtimer->GetInterval();
    }
	bool IsOneShot() const {
		return wxtimer->IsOneShot();
	}
	bool IsRunning() const {
		return wxtimer->IsRunning();
	}
	bool Start(spObject *object, int milliseconds = -1, bool oneShot = false);
	void Stop();
};

#endif
/*
 *  spstring.cpp
 *  SciPlot
 *
 *  Created by bacry on 03/07/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "sp.h"


spFormattedText::spFormattedText()
{
	angle = 0;
	nLines = 0;
	str = NULL;
    flagFormattedWhilePrinting = false;
}

char *spFormattedText::GetText()
{
	return str;
}

void spFormattedText::SetHVMode(int hmode1,int vmode1)
{
	hMode = hmode1;
	vMode = vmode1;
}

void spFormattedText::SetText(const char *str2)
{
	if (str != NULL) {
		if (!strcmp(str,str2)) return;
		delete [] str;
	}
    
	str = new char[strlen(str2)+1];
	strcpy(str,str2);
    
	// We first empty everything
	for (int n = 0;n<nLines;n++) {
		delete [] lines[n];
	}
	
	// Then set the string
    nLines = 0;
	if (str[0] == '\0') return;
	
    char *str1 = str;
	int n;
    while (1) {
        char *str2 = strchr(str1,'\n');
        if (str2 == NULL) n = strlen(str1);
		else n = str2-str1;
		lines[nLines] = new char[n+1];
		strncpy(lines[nLines],str1,n);
		lines[nLines][n] = '\0';
        nLines++;
		if (str2 == NULL || nLines+1 == SP_MAX_NLINES_FORMATTEDTEXT) break;
        str1 = str2+1;
    }
}

void spFormattedText::SetAngle(double angle1)
{
	angle = angle1;
}

spFormattedText::~spFormattedText()
{
	for (int n = 0;n<nLines;n++) {
		delete [] lines[n];
	}
	if (str) delete [] str;
}

void spFormattedText::Draw(spCanvas *canvas)
{
	for (int n = 0;n<nLines;n++) {
		canvas->DrawTextNoRatio(lines[n],xPos[n],yPos[n],angle);
	}
}

void spFormattedText::Format(spCanvas *canvas)
{
    if (canvas == NULL) return;
    if (str == NULL) return;
    canvas->FormatText(this);
}

void spCanvas::DrawFormattedText(const char *str, char hPositionMode, double x, char vPositionMode,  double y, double angle)
{
	if (str == NULL || str[0] == '\0') return;
	
    if (ratioPixel != 1) {
        x*= ratioPixel;
        y*= ratioPixel;
    }
	
    int nLines;
	char *lines[SP_MAX_NLINES_FORMATTEDTEXT];
    
	// Then set the string
    nLines = 0;
    const char *str1 = str;
	int n;
    while (1) {
        const char *str2 = strchr(str1,'\n');
        if (str2 == NULL) n = strlen(str1);
		else n = str2-str1;
		lines[nLines] = new char[n+1];
		strncpy(lines[nLines],str1,n);
		lines[nLines][n] = '\0';
        nLines++;
		if (str2 == NULL || nLines+1 == SP_MAX_NLINES_FORMATTEDTEXT) break;
        str1 = str2+1;
    }
	
    __FormatText(lines,nLines,hPositionMode,x,vPositionMode,y,angle,NULL,true);
}


void spCanvas::_FormatText(double x, double y, spFormattedText *ft)
{
	if (ft->nLines == 0) return;
    
	// And call subroutine
    __FormatText(ft->lines,ft->nLines,ft->hMode,x,ft->vMode,y,ft->angle,ft,false);
}

void spCanvas::__FormatText(char *lines[], int nLines, int hPositionMode, double x, int vPositionMode,  double y, double angle, spFormattedText *ft,bool flagDraw)
{
	if (nLines == 0) return;
	
    // We compute the characteristics of the first line
    int strWidth,strHeight,lineHeightWithInterline;
    int ascent,descent,interline;
	wxString text = ANSI2WXSTRING(lines[0]);
	GetTextExtent(text,&strWidth,&strHeight,&descent,&interline);
    ascent = strHeight-descent;
	lineHeightWithInterline = ascent+descent+interline;
	
	// We need to compute all the strWidths
	int strWidths[SP_MAX_NLINES_FORMATTEDTEXT];
	strWidths[0] = strWidth;
	int maxStrWidth = strWidth;
	for (int n=1;n<nLines;n++) {
		wxString sw = ANSI2WXSTRING(lines[n]);
		int h,d,i;
		GetTextExtent(sw,strWidths+n,&h,&d,&i);
		maxStrWidth = MAX(maxStrWidth,strWidths[n]);
    }
    
	// The total height
	int textHeight = nLines*lineHeightWithInterline-interline;
	
	// The boudingRect
	spRealRect boundingRect;
	
	// Converting the angle into radians
	angle *= 2*M_PI/360;
	
	//
	// Now we loop on the lines
    //
	for (int n=0;n<nLines;n++) {
		
		// Let us fix the vertical position
		double j;
		switch (vPositionMode) {
			case spJustifyVBase:
				j = y-ascent+n*lineHeightWithInterline;
				break;
				
			case spJustifyVUp:
				j = y+1+n*lineHeightWithInterline;
				break;
				
			case spJustifyVDown:
				j = y-textHeight+n*lineHeightWithInterline;
				break;
				
			case spJustifyVMiddle:
				j = y-textHeight/2.+1+n*lineHeightWithInterline;
				break;
                
            case spJustifyVMiddleUp:
                j = y-textHeight/2.+n*lineHeightWithInterline;
		}
        
		// Let us then fix the horizontal position
		double i;
		switch (hPositionMode) {
			case spJustifyHLeft :
				i = x;
				break;
				
			case spJustifyHRight :
				i = x-strWidths[n];
				break;
				
			case spJustifyHBRight :
				i = x-maxStrWidth;
				break;
				
			case spJustifyHMiddle :
				i = x-strWidths[n]/2.;
				break;
				
			case spJustifyHBMiddle :
				i = x-maxStrWidth/2.;
				break;
		}
		
		// We compute the boundingRect for this line
		double ri1,rj1,ri,rj;
		ri = i;
		rj = j-1;
		rj1 = j+ascent+descent;
		ri1 = ri+strWidths[n];
        spRealRect myBoundingRect(ri,rj,ri1-ri,rj1-rj);
		
		// Managing the angle
		if (angle != 0) {
			spRealPoint translation((x-i)*(1-cos(angle))-sin(angle)*(y-j),sin(angle)*(x-i)+(1-cos(angle))*(y-j));
			i+=translation.x;
			j+=translation.y;
			ri+=translation.x;
			rj+=translation.y;
			ri1+=translation.x;
			rj1+=translation.y;
			
			myBoundingRect= spRealRect(ri,rj,1,1);
			double ri2 = (ri1-ri)*cos(angle)+(rj1-rj)*sin(angle)+ri;
			double rj2 = -(ri1-ri)*sin(angle)+(rj1-rj)*cos(angle)+rj;
			myBoundingRect.Union(spRealRect(ri2,rj2,1,1));
			ri2 = (rj1-rj)*sin(angle)+ri;
			rj2 = (rj1-rj)*cos(angle)+rj;
			myBoundingRect.Union(spRealRect(ri2,rj2,1,1));
			ri2 = (ri1-ri)*cos(angle)+ri;
			rj2 = -(ri1-ri)*sin(angle)+rj;
			myBoundingRect.Union(spRealRect(ri2,rj2,1,1));
		}
		
		// We add this boundingRect to the total boundingRect
		if (n == 0) boundingRect = myBoundingRect;
		else boundingRect.Union(myBoundingRect);
		
		// We store the so computed position if needed
		if (ft) {
            ft->xPos[n] = i;
            ft->yPos[n] = j;
		}
		
		// Draw the line otherwise
		if (flagDraw) DrawTextNoRatio(lines[n],i,j,angle*360/(2*M_PI));
		
	}
	
    // We save the boundingRect if needed
	if (ft) {
        ft->boundingRect = spRect(boundingRect.x,boundingRect.y,boundingRect.width,boundingRect.height);
    }
}


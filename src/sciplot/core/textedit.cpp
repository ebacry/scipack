/*
 *  textedit.cpp
 *  LastPlot
 *
 *  Created by bacry on 04/10/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


#include "sp.h"


BEGIN_EVENT_TABLE(spTextEdit, wxFrame)
EVT_CLOSE(spTextEdit::OnClose)
END_EVENT_TABLE()

#define TEXT_LENGTH_MAX 20000
void spTextEdit::OnClose(wxCloseEvent& event)
{
	int n = tc->GetNumberOfLines();
	
	char text[TEXT_LENGTH_MAX];
	char *text1;
	
	text[0] = '\0';
	text1 = text;
	
	for (int i = 0;i<n; i++) {
		const wxString str=tc->GetLineText(i);
		const char *str1 = str.fn_str();
		int l = strlen(str1);
		if (text1-text+l>=TEXT_LENGTH_MAX-2) break;
		if (i!=0) {
			strcpy(text1,"\n");
			text1++;
		}
		strcpy(text1,str1);
		text1+=l;
	}
	
	if (tb) {
	  tb->SetText(text);
		tb = NULL;
	  SPUpdate();
	}
	if (teInteractor) teInteractor->OnClose(text);
	
	event.Skip();
}

spTextEdit::spTextEdit(spText *tb1, const char *init ,int x, int y, int w,int h) : wxFrame(NULL, -1, "Text Edition",wxPoint(x,y),wxSize(w,h),wxDEFAULT_FRAME_STYLE | wxWANTS_CHARS)
{
	tb = tb1;
	
	tc = new wxTextCtrl(this, wxID_ANY, init, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE);

	Show(true);
}

spTextEdit::spTextEdit(spInteractorTextEdit *teInteractor1, const char *init ,int x, int y, int w,int h) : wxFrame(NULL, -1, "Text Edition",wxPoint(x,y),wxSize(w,h),wxDEFAULT_FRAME_STYLE | wxWANTS_CHARS)
{
	teInteractor = teInteractor1;
	tb = teInteractor->tb;
	tc = new wxTextCtrl(this, wxID_ANY, init, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE);
	
	Show(true);
}

spInteractorTextEdit *spInteractorTextEdit::theInteractor = NULL;

spInteractorTextEdit* spInteractorTextEdit::GetTheInteractor()
{
	if (theInteractor == NULL) {
		theInteractor = new spInteractorTextEdit();
		theInteractor->Ref();
	}
  return theInteractor;	
}

void spInteractorTextEdit::OnClose(const char *str)
{
	tb = NULL;
	te = NULL;
}


void CBTextEdit(spInteractor *i,spEvent &event)
{
	spInteractorTextEdit *inter = (spInteractorTextEdit *) i;

	// First, we close the text edit window if already open
	if (inter->te != NULL) inter->te->Close(true);

	// We save the textBox object
	inter->tb = (spText *) event.object;

  // Then we open a new textEdit window
	spPoint mouse = event.object->Canvas2Screen(event.mouse);
	inter->te = new spTextEdit(inter,inter->tb->GetText(),mouse.x,mouse.y,400,100);
}

spInteractorTextEdit::spInteractorTextEdit() : spInteractor1(CBTextEdit,spOnKeyDown,WXK_ESCAPE,false)
{
	te = NULL;
	tb = NULL;
}

spInteractorTextEdit::~spInteractorTextEdit()
{
	// When deleted we close the textEdit window
	if (te != NULL) te->Close(true);
}



#ifndef _SP_INTERACTOR_SCROLL_H
#define _SP_INTERACTOR_SCROLL_H

class spObject;


extern void CBScrollWheelX(spInteractor *cb, spEvent &event);
extern void CBScrollWheelY(spInteractor *cb, spEvent &event);


class spInteractorScrollView : public spInteractor1 {
	
	public : 
	
  virtual bool OnlyOnViews() { return true;};

	bool flagScrollX;
		
	spInteractorScrollView(char xy = 'x', bool flagPropagate=false,const char *name = NULL);
	
	inline void SetScroll(spEventSimpleType type, unsigned long modifiers) {SetEventType(type,modifiers);};
	
	virtual const char *GetClassName() {
		return _CCHAR("spInteractorScrollView");		
	}	
	
};




#endif
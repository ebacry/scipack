
// Not a module


%rename(SetDebug) spSetDebug;

%rename(DEBUG_NOTHING) spDebugNothing;
%rename(DEBUG_EVENT_MOUSE) spDebugEventsMouse;
%rename(DEBUG_EVENT_MOTION) spDebugEventsMotion;
%rename(DEBUG_GEOMETRY_UPDATE) spDebugGeometryUpdate;
%rename(DEBUG_CREATING_CONTAINER) spDebugCreatingContainer;
%rename(DEBUG_DRAW) spDebugDraw;
%rename(DEBUG_UPDATE_PAINT) spDebugUpdatePaint;
%rename(DEBUG_RESIZE) spDebugResize;
%rename(DEBUG_OVERLAY) spDebugOverlay;
%rename(DEBUG_ADDING_OBJECT) spDebugAddingObject;
%rename(DEBUG_ALLOC) spDebugAlloc;
%rename(DEBUG_REF) spDebugRef;


enum 
{
	spDebugNothing = 0,
  spDebugEventsMouse = (1<<0),
  spDebugEventsMotion = (1<<1),
  spDebugGeometryUpdate = (1<<2),
  spDebugCreatingContainer = (1<<3),
  spDebugDraw = (1<<4),	
  spDebugUpdatePaint = (1<<5),	
	spDebugResize = (1<<6),
	spDebugOverlay = (1<<7),
	spDebugAlloc = (1<<8),
	spDebugRef = (1<<9),
	spDebugAddingObject = (1<<10)	
};

%DocMeth(spSetDebug,"spSetDebug(flags)","Enables some debug printing. The flags is a combination (addition) of
DEBUG_NOTHING, DEBUG_EVENT_MOUSE, DEBUG_EVENT_MOTION, DEBUG_GEOMETRY_UPDATE, DEBUG_CREATING_CONTAINER,
DEBUG_DRAW, DEBUG_UPDATE_PAINT, DEBUG_RESIZE, DEBUG_OVERLAY, DEBUG_ADDING_OBJECT, DEBUG_ALLOC, DEBUG_REF
")
extern void spSetDebug(unsigned long);





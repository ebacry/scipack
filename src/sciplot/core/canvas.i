
// Not a module


%rename(Canvas) spCanvas;
%rename(Update) SPUpdate;


%feature("ref")   spCanvas "$this->Ref();"
%feature("unref") spCanvas "$this->Unref();"


%DocMeth(SPUpdate,"Update(flagRedraw=True)","Update whatever is needed to be updated : geometries of objects that have been changed and some part of the display. If flagRedraw is False then the dispaly is not updated.")
extern void SPUpdate(bool flagRedraw=true);


// Enable named paramaters for some methods
%feature("kwargs") spCanvas;

//
// spCanvas class definition
//
class spCanvas
{
  public :

    // 
    // repr and str methods
    //
	%extend {
	  std::string __repr__() {return self->GetTopContainer()->GetName();}
	  std::string __str__() {return self->GetTopContainer()->GetName();}
	}

    // COnstructor and destructor
	spCanvas(long id,spTopContainer *tc = NULL, const char *name = NULL);
	virtual ~spCanvas();

    long wxWindowId;
};


//
// The topcontainer field
//
%extend spCanvas {
%immutable;
    PyObject *topcontainer;
%mutable;
}
%{
    PyObject *spCanvas_topcontainer_get(spCanvas *s) {return WrapObject(s->GetTopContainer());}
%}


//
// The GetCurCanvas() method
//
%extend spCanvas {
    static PyObject *GetCurCanvas() {
        if (spCanvas::curCanvas) {
            PyObject *result = SWIG_NewPointerObj(SWIG_as_voidptr(spCanvas::curCanvas),SWIGTYPE_p_spCanvas,  SWIG_POINTER_OWN |  0 );
            spCanvas::curCanvas->Ref();
            return result;
        }
        Py_INCREF(Py_None);
        return Py_None;
    }
}

//
// The canvases() Method
//
%extend spCanvas {
    static PyObject *canvases() {
      PyObject* list = PyList_New(spCanvas::canvasList.Size());
      for (int i = 0; i < spCanvas::canvasList.Size(); i++) {
        PyObject* obj = SWIG_NewPointerObj(SWIG_as_voidptr(spCanvas::canvasList[i]),SWIGTYPE_p_spCanvas, SWIG_POINTER_OWN |  0);
        spCanvas::canvasList[i]->Ref();
        PyList_SET_ITEM(list, i, obj);
      }
      return list;
    }
}


//
// The ncanvases() method
//
%extend spCanvas {
    static int ncanvases() {return spCanvas::canvasList.Size();}
}


//
// nrefs field (read only)
//
%extend spCanvas {
%immutable;
    int nrefs;
%mutable;
}
%{
    int spCanvas_nrefs_get(spCanvas *s) {return s->nRefs();}
%}




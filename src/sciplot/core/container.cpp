
//
//
// A container is defined by a box, i.e., by an anchor (with and ct) and a margin (xMargin,yMargin,mxMargin,myMargin,with a marginContainer)
// Moreover a view has also XYBounds (xMin,xMax,yMin,yMax).
//
//
// The application l2g allows to map (xMin,yMin),(xMax,yMax) ---> (-mxMargin,-myMargin),(xMargin,yMargin)
//
// The application f that maps the rectangle (-mxMargin,-myMargin),(xMargin,yMargin) to the canvas rectangle is defined by
//
//      f' = marginContainer.l2top.derivative
//      f(0) = ct.l2top(anchor)
//
// The l2top application of the container is defined by
//
//      l2top = f composed with l2g
//
// 1- When l2g changes, in order to recompute l2top the formula above is used.
// 2- To compute the lBoundingRect of a box
//    a- we first need to express the anchor in the marginContainer coordinate
//       pt = marginContainer->l2canvasInverse(ct->l2top(anchor))
//    b- Then we compute the rectangle in this coordinate :
//       r = Rect(pt+(xMargin,yMargin),pt-(mxMargin,myMargin))
//    c- Then we translate this rectangle in local coordinates
//       lBoundingRect = container->l2canvasInverse(marginContainer->l2top(r))
// 3- To compute the gBoundingRect of a box (same as 2 but we stop before)
//








// Change log --> Update des axes
// Pb du dcBuffer

#include "sp.h"

void spContainer::AddObject(spObject *object)
{
	SPDEBUG(spDebugAddingObject,"Adding Object " << object << " to container " << this);
	objectList.Add(object);
	object->container = this;
	myobj = object;
	object->Ref();
}


void spContainer::RemoveObject(spObject *object)
{
	if (objectList.Remove(object)) {
//        printf("Object %s is removed from %s\n",object->GetName(),GetName());
        object->container = NULL;
        object->Unref();
	}
}

void spContainer::RemoveAllObjects()
{
    int n = objectList.Size()-1;
    
    while (n != -1) {
        RemoveObject(objectList[n]);
        n--;
    }
}

double spContainer::GetDistance(spRealPoint pt)
{
	// If box is not a group then just execute the default spObject::GetDistance
	if (!IsGroup()) return spObject::GetDistance(pt);
	
	// Otherwise we must loop on all the objects
	// and return the smallest distance
	double dist = -1;
	double d;
    for (int i=0;i<objectList.Size();i++) {
		if ((d=objectList[i]->GetDistance(pt))==-1) continue;
		if (dist == -1 || d<dist) dist=d;
	}
	
	return dist;
}

void spContainer::InitContainer()
{
	flags = 0;
    toleranceEventDistance = 20;
}

spContainer::spContainer(int i,const char *name1) : spObject(i,name1)
{
	InitContainer();
}

spContainer::spContainer(spContainer *ctn,const spAnchor &anchor,const spMargin &margin,unsigned long flags1,const char *name1) : spObject(ctn,anchor,margin,flags1,name1)
{
    //	SPDEBUG(spDebugCreatingGroup, "--> Creating container " << this << " in container " << ctn);
	
	InitContainer();
	if (container) topContainer = ctn->topContainer;
	else topContainer = NULL;
	
	AddFlag(flags1);
    
    //	SPDEBUG(spDebugCreatingGroup, "<-- Creating container " << this << " in container " << ctn);
}

// Deleting a container
spContainer::~spContainer()
{
    while(objectList.Size()!=0) {
        spObject *obj = objectList[0];
        RemoveObject(obj);
    }
    
	//????	topview->NotifyViewDeleted2OverlayObjects(this);	--> pas necessaire vu que tous les objets sont détruits
}

void spContainer::UpdateGeometry()
{
	flagGeometryMustBeUpdated = false;
    
	if (!IsTopContainer()) {
        // We need to recompute the l2top
        Recomputel2top();
		
        // We update the gBoundingRect and the clip
        spObject::UpdateGeometry();
        clip = gBoundingRect();
        clip.Intersect(container->clip);
	}
	
	// Then We propagate the update
	for (int i= 0;i<objectList.Size();i++) {
		objectList[i]->UpdateGeometryMain();
	}
	
    //	SPDEBUG(spDebugGeometryUpdate, "<-- UpdateGeometryMain ");
}


void spContainer::Clear()
{
    if (IsGroup()) return;
    
    UsePen(*wxTRANSPARENT_PEN);
    DrawRectangle(gRect().x,gRect().y,gRect().width,gRect().height);
    UsePen(pen);
}


void spContainer::NotifyCanvasDeleted()
{
	for (int i= 0;i<objectList.Size();i++) {
		objectList[i]->NotifyCanvasDeleted();
	}
}

void spContainer::Draw(spRect &r1)
{
	SPDEBUG(spDebugDraw, "--> Draw container" << this->GetName() << " " << r1);
	
    // clipCurrent stores the current clip of the view
	spRect clipCurrent = clip;
	clipCurrent.Intersect(r1);
    
    // We set the clip to clipCurrent
	SetClip(clipCurrent);
    
    // We clear the view
	Clear();
	
    // Should we draw a frame ?
    if (flags & spFLAG_CONTAINER_DRAW_FRAME)
		DrawRectangle(gRect().x,gRect().y,gRect().width,gRect().height);
	
    // We loop on the objects
	for (int i= 0;i<objectList.Size();i++) {
        
        // If object hidden --> nothing to do
		if (objectList[i]->IsHidden()) continue;
		
		// Should we update the bounding rect of the object ?
        if (objectList[i]->flagGeometryMustBeUpdated) objectList[i]->UpdateGeometryMain();
        
        // Compute the new clip : if empty nothing to do !
        // Otherwise set it
        spRect clipObject = clipCurrent;
		clipObject.Intersect(objectList[i]->gBoundingRect());
		if (clipObject.IsEmpty()) continue;
        
        // Then draw
        SPDEBUG(spDebugDraw, "--> Draw object" << objectList[i]->GetName());
        SetClip(clipObject);
		objectList[i]->UseAttributes();
		objectList[i]->Draw(clipObject);
		SPDEBUG(spDebugDraw, "<-- End Draw object" << objectList[i]->GetName());

        // Restore the clip
		SetClip(clipCurrent);
	}
	SPDEBUG(spDebugDraw, "<-- End Draw container" << this << " " << gBoundingRect());
}

// This method recomputes l2top given lBoundingRect, l2g and container->l2top
void spContainer::Recomputel2top()
{
	if (IsTopContainer()) return;
	
	if (box.IsNoAnchor() || box.IsInfinite()) {
        l2top = container->l2top;
		l2g = spMap2d();
		return;
	}
	spRealPoint  f0;
	spMap2d f;
    /*
     if (box.IsInfinite()) {
     f0 = box.GetAnchorContainer()->l2top.Apply(box.GetAnchor(this));
     f = spMap2d(spMap1d(1,f0.x),spMap1d(1,f0.y));
     l2top = container->l2top.ComposeRight(f);
     return;
     }
     */
    
	f0 = container->l2top.Apply(box.GetAnchor(this));
	f = spMap2d(spMap1d(GetContainerMargin()->l2top.trX.GetFactor(),f0.x),spMap1d(GetContainerMargin()->l2top.trY.GetFactor(),f0.y));
	l2top = f.ComposeRight(l2g);
}

bool spContainer::ProcessEvent(spEvent &event)
{
	//
	// Case mouse is currently dragged (i.e., we are either dealing with a button up or a button motion event)
	//
	if (spEvent::Dragging()) {
		
		//
		// Case we haven't reached the object which should receive the drag event
		// We must send the event down the hierarchy and get the result
		// When done, if res == false we must process the event at our level
		//
		
		if (spEvent::hierarchy[spEvent::nCurHierarchy]!=spEvent::objectDrag) {
            spEvent::nCurHierarchy++;
            bool res = spEvent::hierarchy[spEvent::nCurHierarchy]->ProcessEvent(event);
            if (res == false) return ProcessEvent1(event);
            return true;
		}
		
		//
        // Case we have just reached the object which should receive the drag event (we are at the bottom of the hierarchy)
		//
		
		// We must substitute onMotion events by onDrag events before processing the event
		// (we keep the original type in the variable type in order to restore it after processing)
		spEventSimpleType type = event.type;
		if (event.type == spOnMotionLeft) event.type = spOnDragLeft;
		else if (event.type == spOnMotionRight) event.type = spOnDragRight;
		else if (event.type == spOnMotionMiddle) event.type = spOnDragMiddle;
		
		// Process the event using ProcessEvent1 (i.e., no enter/leave event generated)
		bool res=ProcessEvent1(event);
		
		// Restore the type of the event
		event.type = type;
		
        // Done
		return res;
		
	}
	
	//
	// Case the mouse is not currently dragged
	//
	
	// Should we send some leave events ?
    ProcessPreLeaveEnterEvent();
	
	//
	// Process the event --> Looking for the object which should process the event
	// For that purpose, we call GetDistance on all the objects of the container and look for the smallest distance.
	// If one is 0 --> we are done
	// If all are -1, the current container should process the event
	// Otherwise the object with the smallest distance processes the event
	//
	
	double theDist=-1;
	spObject *theObject = NULL;
	
	// Loop on the objects (from last one to first one)
	bool res = false;
	for (int i= objectList.Size()-1;i>=0;i--) {
        
		if (objectList[i]->IsHidden()) continue;  // If object hidden then it is not taken into account
        spObject *o = objectList[i];
		double d = o->GetDistance(event.mouse);  // Get the distance
		
		// If in an object then we process the event and we are done
		if (d==0) {
			theDist=0;
            res = objectList[i]->ProcessEvent(event);
			break;
		}
		
		// If distance is -1 then continue the loop
		if (d==-1) continue;
		
		// If distance != -1 and != 0 then keep track of the smallest distance
		if (theDist==-1 || theDist>d) {
			theDist = d;
			theObject = objectList[i];
		}
	}
	
    if (theDist > toleranceEventDistance) theDist = -1;
    
	// If best distance is != 0 and !=-1 we send the event to the closest object
	if (theDist != -1 && theDist != 0) res = theObject->ProcessEvent(event);
	
	// if did not succeed we process the event of the container
	if (!res) res = ProcessEvent1(event);
	
	// Then we process eventual leave/enter events
	ProcessPostLeaveEvent();
	
	// That's it !
	return res;
}


// Find the closest object to the mouse
spObject *spContainer::FindObjectMouse(spRealPoint mouse,bool flagCursor)
{
	double theDist=-1;
	spObject *theObject = NULL;
	
	// Loop on the objects (from last one to first one)
	for (int i=objectList.Size()-1;i>=0;i--) {
		
		if (objectList[i]->IsHidden()) continue;  // If object hidden then it is not taken into account
		if (flagCursor && !objectList[i]->HasCursorCallback()) continue;
		double d = objectList[i]->GetDistance(mouse);  // Get the distance
		
		// If in an object then we are done
		if (d==0) {
			theObject = objectList[i];
			break;
		}
		
		// If distance is -1 then continue the loop
		if (d==-1) continue;
		
		// If distance != -1 and != 0 then keep track of the smallest distance
		if (theDist==-1 || theDist>d) {
			theDist = d;
			theObject = objectList[i];
		}
	}
	
    // If theObject is a group --> we call the FindObjectMouse method on this object
        if (theObject != NULL && theObject->IsContainer() && ((spContainer *) theObject)->IsGroup()) {
        theObject = ((spContainer *) theObject)->FindObjectMouse(mouse,flagCursor);
    }
	
	return theObject;
}



// Not a module

%rename(View) spView;

%rename(_GetLogx) GetLogScaleX;
%rename(_GetLogy) GetLogScaleY;
%rename(_SetLogx) SetLogScaleX;
%rename(_SetLogy) SetLogScaleY;

// The container class is a director
%feature("director") spView;

// Some methods with named parameters
%feature("kwargs") spView::spView;


%DocClass(spView,"A container class that allows arbitrary linear or logarithm coordinate system. It allows zooming and scrolling. It is used, most of the time, within  a FramedView.")
class spView : public spContainer
{
	public :
	
    ObjRepr;

	//
    // Simple class tests	
	//
	virtual bool IsView();
	
	
	spView(spContainer *container=NULL,const spAnchor &anchor=spAnchor(),const spMargin &margin=spMargin(),unsigned long flags=0,const char *name=NULL);
    virtual ~spView();	
	
    void SetLogScaleY(bool flag);
	bool GetLogScaleY();
    %attobj_def(logy,Logy,
                """
                **Attribute** self.logy = boolean
                
                ``True``if y-scale is log and ``False`` if linear
                """);

    void SetLogScaleX(bool flag);
    bool GetLogScaleX();
    %attobj_def(logx,Logx,
                """
                **Attribute** self.logx = boolean
                            
                ``True``if x-scale is log and ``False`` if linear
                """);
	
    %extend{
      %DocMeth(GetMinBound(),"GetMinBound() or GetMinBound(xmin,xmax) -> [xmin,xmax,ymin,ymax]","Returns the minimum bounds that contains all the objects. xmin and xmax can be specified. This method is used for unzooming")
      PyObject *GetMinBound() { // Computes the smallest xyBound that contains all the objects and returns it
        double bound[4];
        spRealRect r = $self->ComputeMinXYBound();
        bound[0] = r.x;
        bound[1] = r.x+r.width;
        bound[2] = r.y;
        bound[3] = r.y+r.height;
        return WrapDoubleArray(bound,4);
      }
      %DocMeth(GetMinBound(double xMin,double xMax),"","")
      PyObject *GetMinBound(double xMin,double xMax) {
        double bound[4];
        spRealRect r = $self->ComputeMinXYBound(xMin,xMax);
        bound[0] = r.x;
        bound[1] = r.x+r.width;
        bound[2] = r.y;
        bound[3] = r.y+r.height;
        return WrapDoubleArray(bound,4);
      }
        %DocMeth(SetMinBound(),"SetMinBound() or SetMinBound(xmin,xmax)","Is equivalent to self.bound = self.GetMinBound() or self.GetMinBound(xmin,xmax)")
      void SetMinBound() { // Sets the bound to the smallest xyBound that contains all the objects
        spRealRect r = $self->ComputeMinXYBound();
        $self->SetXYBound(r);
      }
        %DocMeth(SetMinBound(double xMin,double xMax),"","")
      void SetMinBound(double xMin,double xMax) { // Sets the bound to the smallest xyBound that contains all the objects
        spRealRect r = $self->ComputeMinXYBound(xMin,xMax);
        $self->SetXYBound(r);
      }
    }

	%extend {
        void _SetXMin(double xMin) {
            spRealRect r = $self->GetXYBound();
            double xMax = r.x+r.width;
            if (xMin > xMax) return;
            r = $self->ComputeMinXYBound(xMin,xMax);
            $self->SetXYBound(r);
        }
        double _GetXMin() {
            spRealRect r = $self->GetXYBound();
            return (r.x);
        }
        %attobj_def(xMin,XMin,
        """
        **Attribute** self.xMin = float
        
        The minimum abscissa displayed. Should be smaller than self.xMax.
        When changed, the y bounds yMin and yMax are automatically adapted to the min and max y values of what is displayed.
        """);
        void _SetXMin1(double xMin) {
            spRealRect r = $self->GetXYBound();
            double xMax = r.x+r.width;
            if (xMin > xMax) return;
            r = $self->GetXYBound();
            r.x = xMin;
            r.width = xMax-xMin;
            $self->SetXYBound(r);
        }
        double _GetXMin1() {
            spRealRect r = $self->GetXYBound();
            return (r.x);
        }
        %attobj_def(xMin1,XMin1,
        """
        **Attribute** self.xMin1 = float
        
        Same as self.xMin but the y bounds are not changed.
        """);
        
        void _SetXMax1(double xMax) {
            spRealRect r = $self->GetXYBound();
            double xMin = r.x;
            r = $self->GetXYBound();
            r.width = xMax-xMin;
            $self->SetXYBound(r);
        }
        double _GetXMax1() {
            spRealRect r = $self->GetXYBound();
            return (r.x+r.width);
        }
        %attobj_def(xMax1,XMax1,
        """
        **Attribute** self.xMax1 = float
        
        Same as self.xMax but the y bounds are not changed.
        """);
        
        void _SetXMax(double xMax) {
            spRealRect r = $self->GetXYBound();
            double xMin = r.x;
            if (xMin > xMax) return;
            r = $self->ComputeMinXYBound(xMin,xMax);
            $self->SetXYBound(r);
        }
        double _GetXMax() {
            spRealRect r = $self->GetXYBound();
            return (r.x+r.width);
        }
        %attobj_def(xMax,XMax,
        """
        **Attribute** self.xMax = float
        
        The maximum abscissa displayed. Should be greater than self.xMin.
        When changed, the y bounds yMin and yMax are automatically adapted to the min and max y values of what is displayed.
        """);
        void _SetYMin(double yMin) {
            spRealRect r = $self->GetXYBound();
            double yMax = r.y+r.height;
            r.y = yMin;
            if (yMin > yMax) return;
            $self->SetXYBound(r);
        }
        double _GetYMin() {
            spRealRect r = $self->GetXYBound();
            return (r.y);
        }
        %attobj_def(yMin,YMin,
        """
        **Attribute** self.yMin = float
        
        The maximum ordinate displayed. Should be smaller than self.yMax.
        """);
        
        void _SetYMax(double yMax) {
            spRealRect r = $self->GetXYBound();
            double yMin = r.y;
            r.height = yMax-yMin;
            if (yMin > yMax) return;
            $self->SetXYBound(r);
        }
        double _GetYMax() {
            spRealRect r = $self->GetXYBound();
            return (r.y+r.height);
        }
        %attobj_def(yMax,YMax,
        """
        **Attribute** self.yMax = float
        
        The maximum ordinate displayed. Should be greater than self.yMin.
        """);
        
        void _SetX(double xMin, double xMax) {
            if (xMin > xMax) return;
            spRealRect r = $self->ComputeMinXYBound(xMin,xMax);
            $self->SetXYBound(r);
        }
        PyObject * _GetX() {
            spRealRect r = $self->GetXYBound();
            double res[2];
            res[0] = r.x;
            res[1] = r.x + r.width;
            return(WrapDoubleArray(res,2));
        }
        PyObject * _GetY() {
            spRealRect r = $self->GetXYBound();
            double res[2];
            res[0] = r.y;
            res[1] = r.y + r.height;
            return(WrapDoubleArray(res,2));
        }
        void _SetY(double yMin, double yMax) {
            if (yMin > yMax) return;
            spRealRect r = $self->GetXYBound();
            r.y = yMin;
            r.height = yMax-yMin;
            $self->SetXYBound(r);
        }
        %attobj_def(x,X,
        """
        **Attribute** self.x = [xMin,xMax]
        
        The xmin,xmax bounds. The y scale automatically adapts when these bounds are changed.
        """);
        %attobj_def(y,Y,
        """
        **Attribute** self.y = [yMin,yMax]
        
        The ymin,ymax bounds.
        """);
        
        PyObject * _GetBound() {
            spRealRect r = $self->GetXYBound();
            double res[4];
            res[0] = r.x;
            res[1] = r.x + r.width;
            res[2] = r.y;
            res[3] = r.y + r.height;
            return(WrapDoubleArray(res,4));
        }
        void _SetBound(double xMin, double xMax, double yMin, double yMax) {
            if (xMin > xMax || yMin > yMax) return;
            spRealRect r = spRealRect(xMin,yMin,xMax-xMin,yMax-yMin);
            $self->SetXYBound(r);
        }
        %attobj_arglist_def(bound,Bound,
        """
        **Attribute** self.bound = [xMin,xMax,yMin,yMax]
        
        The x and y bounds of the view.
        """);
        spRealRect _GetBoundRect() {return $self->GetXYBound();}
        void _SetBoundRect(spRealRect boundRect) {$self->SetXYBound(boundRect);}
        %attobj_arglist_def(boundRect,BoundRect,
        """
        **Attribute** self.boundRect = sp.RealRect(xMin,yMin,xMax-xMin,yMax-yMin)
        
        The x and y bounds of the view expressed as a rectangle
        """);

    }

	//
	// Scrolling methods
	//
    %DocMeth(ScrollX,"ScrollX(npixels,flagUpdateYScale=True)","Scrolls the view  by npixels (could be negative) along the x-axis. If flagUpdateYScale is True then the y scale is adapted.")
	virtual void ScrollX(int change,bool flagUpdateYScale=true);
    %DocMeth(ScrollY,"ScrollY(npixels","Scrolls the view  by npixels (could be negative) along the y-axis.")
	virtual void ScrollY(int change);
	
	//
	// Zoom methods
	//
    %DocMeth(Zoom,"Zoom(point,w)","Zooms along the x and y-axis around the point. Use the parameter w to compute the zoom factor (w is typically the output of the mouse wheel field, it is an integer).")
	virtual void Zoom(spRealPoint &pt, int change);
    %DocMeth(ZoomX,"ZoomX(point,w)","Same as self.Zoom but zoom is performed along the x-axis only")
	virtual void ZoomX(spRealPoint &pt, int change);
    %DocMeth(ZoomY,"ZoomY(point,w)","Same as self.Zoom but zoom is performed along the y-axis only")
	virtual void ZoomY(spRealPoint &pt, int change);
		
};

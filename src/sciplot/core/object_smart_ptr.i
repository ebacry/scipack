

// Not a module

%rename(WeakRefObject) spWeakObject;

%DocClass(spWeakObject,"A class that implements weak reference to sciplot objects. This should be used, for instance, when you wanta  field of an interactor to be an object which is already attached to this interactor (thus the reference count of the interactor has been incremented and you don't want to loop). You can see an example of the use of this class in the IZoomSynchro interactor.");
class spWeakObject
{
    public :

	// Constructor
	spWeakObject(spObject *object);
		
    // Destructor
	virtual ~spWeakObject();

    // Get the object
    %extend {
        %DocMeth(__call__,"__call__() -> spObject","Returns the corresponding object")
        PyObject * __call__() {
            return WrapObject((spObject *) (*$self));
        }
    }
};



#include "sp.h"





spCursorCallback& spCursorCallback::operator =(const spCursorCallback& other)
{
	getFunc = other.getFunc;
#ifdef SP_SCRIPT
	if (other.scriptFunc) SCRIPT_INCREF(other.scriptFunc);
	if (scriptFunc) SCRIPT_DECREF(scriptFunc);
	scriptFunc = other.scriptFunc;
#endif
    return *this;
}

spCursorCallback::spCursorCallback(const char *(*callback1)(spObject *,bool, spRealPoint &))
{
	getFunc = callback1;
#ifdef SP_SCRIPT
	scriptFunc = NULL;
#endif
}

const char *spCursorCallback::Call(spObject *obj,bool flagFullInfo, spRealPoint &point)
{
	if (!IsSet()) return "";
	
#ifdef SP_SCRIPT
	if (scriptFunc != NULL) {
        void *ptr = (void *) getFunc;
		const char * (*func)(spCursorCallback *, spObject *,bool, spRealPoint &) = (const char *(*)(spCursorCallback *, spObject *,bool, spRealPoint &)) ptr;
        return func(this,obj,flagFullInfo,point);
	}
#endif
	
	return getFunc(obj,flagFullInfo,point);
}

spCursorCallback::~spCursorCallback()
{
#ifdef SP_SCRIPT
	if (scriptFunc != NULL) {
		SCRIPT_DECREF(scriptFunc);
		scriptFunc = NULL;
	}
#endif
}

spCursorCallback::spCursorCallback(const spCursorCallback & ccb)
{
	getFunc = ccb.getFunc;
#ifdef SP_SCRIPT
	scriptFunc = ccb.scriptFunc;
	if (scriptFunc != NULL) {
		SCRIPT_INCREF(scriptFunc);
	}
#endif
}






//
// Constructors
//

// This constructor is only used for the topContainer
// NEVER put any fancy call in it (such as SetFont(12) !!
spObject::spObject(int i,const char *name1) : spRef(true,name1)
{
	flagGeometryMustBeUpdated = true;
	flag_white_brush_printing = false;

	pen = *wxWHITE_PEN;
	brush = *wxWHITE_BRUSH;
    container = NULL;
	overlayType = spOverlayNone;
	overlayActivated = false;
    
    wxFont *fontPtr = wxTheFontList->FindOrCreateFont(12,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL,false,"",wxFONTENCODING_DEFAULT);
    font = *fontPtr;
}


// Main Constructor
spObject::spObject(spContainer *ctn, const spAnchor &anchor,const spMargin &margin,unsigned long flags1,const char *name1) : spRef(true,name1)
{
	container = NULL;
	overlayActivated = false;
	flagGeometryMustBeUpdated = false;
	flag_white_brush_printing = false;
    
	
	// flagHidden
	if (flags1 & spFLAG_CONTAINER_HIDE) flagHidden = true;
    else flagHidden  = false;
     
	SetFont(12);
	
	// case of overlay
	if (flags1 & spFLAG_OBJECT_OVERLAY_BUFFER && !IsView()) {
		flagHidden = true;
        overlayType = spOverlayBuffer;
		pen = *wxBLACK_PEN;        
		brush = *wxWHITE_BRUSH;
	}
    else {
		overlayType = spOverlayNone;
		pen = *wxBLACK_PEN;
		brush = *wxWHITE_BRUSH;
	}
    textForegroundColor = *wxBLACK;
    textBackgroundColor = wxTransparentColour;
    
	// Adding to the container
	if (overlayType == spOverlayNone && ctn != NULL) ctn->AddObject(this);
    else container = ctn;

	// Box initialization
	box = spBox(anchor,margin);
	if (box.IsGrid() && ctn && !ctn->IsGrid()) {
		box.SetGrid(false);
		spWarningf("spObject::spObject()","Trying to set the position of an object on a grid on a container which is not  a grid");
	}
	
	NotifyGeometryChange();
}


void spContainer::SetTopContainer(spTopContainer *top)
{
    topContainer = top;
    for (int i=0;i<objectList.Size();i++) {
        spObject *o = objectList[i];
        if (o->IsContainer()) ((spContainer *) o)->SetTopContainer(top);
    }
}

//
// WARNING : An overlay object IS NOT in the object list of its container
//
void SetContainer(spObject *object, spContainer *ctn)
{
    if (object == NULL) return;
	if (object->container == ctn) return;
	
    // To be sure the object is not deleted before finishing processing the function
    object->Ref();

    // Deactivate overlay // hide
	if (object->IsOverlay()) {
        //		EraseOverlay();
        object->DeactivateOverlay();
	}
	else if (object->container) {
		object->Hide();
        object->container->RemoveObject(object);
	}
	
    // If grid box and container is not a grid --> error
	if (object->box.IsGrid() && ctn && !ctn->IsGrid()) {
		object->box.SetGrid(false);
        object->Unref();
		spWarningf("spObject::SetContainer()","Trying to set the position of an object on a grid on a container which is not  a grid");
        return;
	}
	
    // Case new container is not NULL
	if (ctn != NULL) {
        if (object->overlayType == spOverlayNone) ctn->AddObject(object);
        else object->container = ctn;
        
        // Update the anchor/margin if necessary
		object->box.UpdateAnchor(object);
		object->box.UpdateMargin(object);
	}
    
    // Case object is a container
    if (object->IsContainer()) {
        spContainer *me = (spContainer *) object;
        
        // Update the topContainer field
        if (me->container) me->topContainer = me->container->topContainer;
        else me->topContainer = NULL;
	
        // If in a container
        // Update boxes of all objects in it
        // and set the topcontainer field
	    if (me->container) {
            for (int i=0;i<me->objectList.Size();i++) {
                spObject *o = me->objectList[i];
                o->box.UpdateAnchor(o);
                o->box.UpdateMargin(o);
                if (o->IsContainer()) ((spContainer *) o)->SetTopContainer(me->topContainer);
            }
        }
    }

    // Unref
    object->Unref();
}



// Destructor
spObject::~spObject()
{
	// We first send an event
	spEvent event;
	event.type = spOnDelete;
	event.object = this;
	ProcessEvent1(event);
    
	// Send messages to interactors
	for (int i=0;i<interactorList.Size();i++)
        interactorList[i]->NotifyObjectDeleted(this);
	
    
	// Then we proceed
	if (container && container->topContainer) {
        if (IsOverlay()) {
            container->topContainer->RemoveOverlayObject(this);
        }
        else {
            Hide();
        }
		container->topContainer->canvas->NotifyObjectDeleted(this);
	}
	
    if (container) container->RemoveObject(this);
	while (interactorList.Size()!=0) {
		spInteractor *i = interactorList[0];
		RemoveInteractor(i);
	}	
}

void spObject::UseAttributes() {
	if (!container) return;
	UsePen(pen);
    if (flag_white_brush_printing && container->topContainer && container->topContainer->canvas && container->topContainer->canvas->IsPrinting()) UseBrush(*wxWHITE_BRUSH);
	else UseBrush(brush);
	UseFont(font);
	UseTextColour(textBackgroundColor, textForegroundColor);
}


spContainer *spObject::GetNonGroupContainer()
{
    spContainer *ct  = container;
    
    while (ct != NULL && ct->IsGroup()) ct = ct->container;
    
    return ct;
}


//
// Pen Attributes
//
void spObject::SetPen(const wxColour &colour, int width, int style)
{
	wxPen *penPtr = wxThePenList->FindOrCreatePen(colour,width,style);
    AttBeforeUpdate();
	pen = *penPtr;
    UsePen(pen);
    AttAfterUpdate();;
}
void spObject::SetPen(const wxString& colourName, int width, int style)
{
	wxPen *penPtr = wxThePenList->FindOrCreatePen(colourName,width,style);
    AttBeforeUpdate();
	pen = *penPtr;
    UsePen(pen);
    AttAfterUpdate();;
}
void spObject::SetPen(const wxBitmap& stipple, int width)
{
    AttBeforeUpdate();
	pen = wxPen(stipple,width);
    UsePen(pen);
    AttAfterUpdate();;
}
void spObject::SetPen(const wxPen &pen1)
{
    AttBeforeUpdate();
	pen = pen1;
    UsePen(pen);
    AttAfterUpdate();;
}
void spObject::SetPenStyle(int style)
{
    AttBeforeUpdate();
    pen.SetStyle(style);
    UsePen(pen);
    AttAfterUpdate();;
}
void spObject::SetPenWidth(int width)
{
    AttBeforeUpdate();
    pen.SetWidth(width);
    UsePen(pen);
    AttAfterUpdate();;
}

void spObject::SetPenColour(const wxColour colour)
{
    AttBeforeUpdate();
    pen.SetColour(colour);
    UsePen(pen);
    AttAfterUpdate();;
}

//
// Brush Attributes
//
wxBrush &spObject::GetBrush()
{
    static wxBrush bb = *wxWHITE_BRUSH;
    if (flag_white_brush_printing && container->topContainer && container->topContainer->canvas && container->topContainer->canvas->IsPrinting())
        return bb;
    
    return brush;
}
wxColour spObject::GetBrushColour()
{
    if (flag_white_brush_printing && container->topContainer && container->topContainer->canvas && container->topContainer->canvas->IsPrinting())
        return *wxWHITE;
    
    return brush.GetColour();
}

void spObject::SetBrush(const wxColour &colour, int style)
{
    wxBrush *brushPtr = wxTheBrushList->FindOrCreateBrush(colour,style);
    AttBeforeUpdate();
    brush = *brushPtr;
    UseBrush(brush);
    AttAfterUpdate();;
}
void spObject::SetBrush(const wxString& colourName, int style)
{
    wxBrush *brushPtr = wxTheBrushList->FindOrCreateBrush(colourName,style);
    AttBeforeUpdate();
    brush = *brushPtr;
    UseBrush(brush);
    AttAfterUpdate();
}
void spObject::SetBrush(const wxBitmap& stipple)
{
    AttBeforeUpdate();
    brush = wxBrush(stipple);
    UseBrush(brush);
    AttAfterUpdate();;
}
void spObject::SetBrush(const wxBrush &brush1)
{
    AttBeforeUpdate();
    brush = brush1;
    UseBrush(brush);
    AttAfterUpdate();;
}
void spObject::SetBrushStyle(int style)
{
    AttBeforeUpdate();
    brush.SetStyle(style);
    UseBrush(brush);
    AttAfterUpdate();;
}

void spObject::SetBrushColour(const wxColour colour)
{
    SetBrush(colour,brush.GetStyle());
}

void spObject::SetFontSize(int size)
{
    AttBeforeUpdate();
    font.SetPointSize(size);
    UseFont(font);
    AttAfterUpdate();;
}
void spObject::SetFont(int pointSize, wxFontFamily family, int style, wxFontWeight weight, const bool underline, const wxString& faceName, wxFontEncoding encoding)
{
    wxFont *fontPtr = wxTheFontList->FindOrCreateFont(pointSize,family,style,weight,underline,faceName,encoding);
    AttBeforeUpdate();
    font = *fontPtr;
    UseFont(font);
    AttAfterUpdate();;
}
void spObject::SetFont(const wxFont &font1)
{
    AttBeforeUpdate();
    font = font1;
    UseFont(font);
    AttAfterUpdate();;
}
void spObject::SetFontStyle(int style)
{
    AttBeforeUpdate();
    font.SetStyle(style);
    UseFont(font);
    AttAfterUpdate();;
}

void spObject::SetFontWeight(int weight)
{
    AttBeforeUpdate();
    font.SetWeight(weight);
    UseFont(font);
    AttAfterUpdate();;
}

void spObject::SetFontFaceName(char *faceName)
{
    AttBeforeUpdate();
    font.SetFaceName(ANSI2WXSTRING(faceName));
    UseFont(font);
    AttAfterUpdate();;
}
char *spObject::GetFontFaceName()
{
    return WXSTRING2ANSI(font.GetFaceName());
}

void spObject::SetFontFamily(int family)
{
    AttBeforeUpdate();
    font.SetFamily(family);
    UseFont(font);
    AttAfterUpdate();;
}


void spObject::SetTextBackground (const wxColour colour)
{
    AttBeforeUpdate();
    textBackgroundColor=colour;
    UseTextColour(textBackgroundColor,textForegroundColor);
    AttAfterUpdate();;
}
void spObject::SetTextForeground (const wxColour colour)
{
    AttBeforeUpdate();
    textForegroundColor=colour;
    UseTextColour(textBackgroundColor,textForegroundColor);
    AttAfterUpdate();;
}


void spObject::UpdateGeometry()
{
	_gBoundingRect = box.gBoundingRect(this);
	_gRect = box.gRect(this);
}

void spObject::UpdateGeometryMain(bool flagSendGroupEvent)
{
	if (!container) return;
	
	flagGeometryMustBeUpdated = false;
    
	if (this->GetName()) {
        SPDEBUG(spDebugGeometryUpdate,"--> UpdateGeometryMain " << this->GetName());
	}
    else {
        SPDEBUG(spDebugGeometryUpdate,"--> UpdateGeometryMain " << this);
	}

    UpdateGeometry();
    
    if (flagSendGroupEvent && container && container->IsGroup()) {
        ((spGroup *) container)->NotifyObjectGeometryUpdateMain(this);
    }

	SPDEBUG(spDebugGeometryUpdate, "<-- UpdateGeometryMain ");
}


void spObject::NotifyRectMustBeDrawn(spRect &r)
{
    if (container == NULL || container->topContainer == NULL || container->topContainer->canvas == NULL) return;
	container->topContainer->canvas->NotifyRectMustBeDrawn(r);
}

void spObject::NotifyGeometryChange()
{
    if (container && container->IsGroup() && ((spGroup *) container)->ProcessGeometryUpdateNow())
    {
        UpdateGeometry();
        flagGeometryMustBeUpdated = false;
        return;
    }
	if (flagGeometryMustBeUpdated) return;
	if (container == NULL || container->topContainer == NULL || container->topContainer->canvas == NULL) return;
	container->topContainer->canvas->NotifyGeometryChange(this);
}

bool spObject::Hide()
{
	if (!IsVisible() || IsOverlay()) {
        flagHidden = true;
        return(false);
    }
	if(!flagGeometryMustBeUpdated) NotifyRectMustBeDrawn(gBoundingRect());
    flagHidden = true;
	return true;
}

void spObject::Show()
{
	if (IsVisible() || IsOverlay()) {
        flagHidden = false;
        return;
    }
	if(!flagGeometryMustBeUpdated) NotifyRectMustBeDrawn(gBoundingRect());
	flagHidden = false;
}

void spObject::AttBeforeUpdate()
{
    if (!IsHidden()) NotifyRectMustBeDrawn(gBoundingRect());
}

void spObject::AttAfterUpdate()
{
    NotifyGeometryChange();
}



bool spObject::IsVisible()
{
	if (IsHidden() || container == NULL || container->topContainer == NULL) return(false);
    else return(true);
/*	spContainer *v;
	for (v=container;v!=container->topContainer && v != NULL;v=v->container)
		if (v->IsHidden()) return false;
	if (v==NULL || v->IsHidden()) return false;
	return true;*/
}

spRealRect spObject::lRect()
{
	return box.lRect(this);
}

bool spObject::IsDefinedBy2Points()
{
    return box.IsDefinedBy2Points();
}

void spObject::Move(double dx,double dy)
{
	if (!IsMoveable()) return;
    
    AttBeforeUpdate();
	box.Move(this,dx,dy);
    AttAfterUpdate();;
}


spRealPoint spObject::GetAnchor(bool flagUpdate,double xmin,double xmax)
{
    return(box.GetAnchor(this,flagUpdate,xmin,xmax));
}

void spObject::SetAnchor(spRealPoint anchor)
{
    if (!IsMoveable()) return;
    
    AttBeforeUpdate();
    box.SetAnchor(this,anchor);
    AttAfterUpdate();;
}

void spObject::SetMargin(double xMargin, double yMargin, double mxMargin, double myMargin)
{
    if (!IsResizeable()) return;
    
    AttBeforeUpdate();
    box.SetMargin(xMargin,yMargin,mxMargin,myMargin);
    AttAfterUpdate();;
}

void spObject::SetSize(double dx, double dy)
{
    if (!IsResizeable()) return;
    
    AttBeforeUpdate();
    box.SetSize(dx,dy);
    AttAfterUpdate();;
}


spRealPoint spObject::GetAnchorOposite()
{
    return box.GetAnchorOposite(this);
}

void spObject::SetAnchorOposite(spRealPoint pt)
{
    if (!IsResizeable()) return;

    AttBeforeUpdate();
    box.SetAnchorOposite(this,pt);
    AttAfterUpdate();;
}

double spObject::GetDistance(spRealPoint pt)
{
	return (gBoundingRect().Contains(pt) ? 0 : -1);
}

void spObject::ActivateOverlay()
{
	if (!IsOverlay()) return;
	if (IsOverlayActivated()) return;
	if (container == NULL) return;
	overlayActivated = true;
	SPDEBUG(spDebugOverlay,"Activate Overlay ");
	flagHidden = true;
	UpdateGeometryMain();
    /*	  bm = new wxBitmap(gBoundingRect.width,gBoundingRect.height);
     dcBuffer->SelectObject(*bm);
     if (!dcBuffer->IsOk()) cout << "MMMMMMMM " << endl;
     */
	container->topContainer->AddOverlayObject(this);
}

void spObject::DeactivateOverlay()
{
	if (!IsOverlay()) return;
	if (!IsOverlayActivated()) return;
	if (container == NULL) return;
	overlayActivated = false;
	SPDEBUG(spDebugOverlay,"Deactivate Overlay ");
	container->topContainer->RemoveOverlayObject(this);
}

void spObject::DrawOverlay()
{
	if (!IsOverlay() || !flagHidden || container == NULL) return;
    SPDEBUG(spDebugOverlay,"Draw Overlay " << this << " " << this->GetClassName() << container->clip);
    
	SetClip(container->clip);
	UseAttributes();
	
	//		bm->SaveFile("screenshot.jpg",wxBITMAP_TYPE_JPEG);
	DrawAll();
	
	flagHidden = false;
}

void spObject::EraseOverlay()
{
	if (!IsOverlay() || flagHidden || container == NULL) return;
	
	//	SPDEBUG(spDebugOverlay,"Erase Overlay " << this << " " << r);
	spRect clip = container->clip;
	clip.Intersect(gBoundingRect());
	DCBufferToDC(clip);
	
	flagHidden = true;
}

void spObject::AddInteractor(spInteractor *cb)
{
	if (cb == NULL) return;
	if (!cb->CheckObjectIsValid(this)) return;
	interactorList.Add(cb);
	cb->Ref();
	cb->NotifyObjectAdded(this);
}

bool spObject::RemoveInteractor(spInteractor *cb)
{
	if (interactorList.Remove(cb)) {
        cb->NotifyObjectRemoved(this);
		cb->Unref();
        return true;
	}
	else return false;
}


 void spObject::SendMsge(spObject *receiver,spMsge msge)
 {
 spEvent event;
 event.type = spOnMsge;
 event.modifiers = msge;
 event.sender = this;
 event.receiver = receiver;
 event.object = receiver;
     
 receiver->ProcessMsgeEvent(event);
}


void spObject::ProcessMsgeEvent(spEvent &event)
{
    // If no interactor --> returns
    if (interactorList.Size()==0) return;
    
    // We loop on the interactors
    for (int i=0; i<interactorList.Size() ; i++) {
        
        // We process the event
        bool res1 = false;
        if (interactorList[i]->IsOn()) {
            res1 = interactorList[i]->Process(event);
        }
    }
}


void spObject::ProcessPreLeaveEnterEvent()
{
	// Should we send some leave events ?
    if (spEvent::nCurHierarchy<spEvent::nHierarchy && spEvent::hierarchy[spEvent::nCurHierarchy] != this) {
		spEvent leaveEvent;
		leaveEvent.type = spOnLeave;
		for (int i = spEvent::nHierarchy-1 ; i>=spEvent::nCurHierarchy ; i--) {
			leaveEvent.object = spEvent::hierarchy[i];
			SPDEBUG(spDebugEventsMouse, leaveEvent << this << "(" << this->GetClassName() << ")");
			spEvent::hierarchy[i]->ProcessEvent1(leaveEvent);
		}
		spEvent::nHierarchy = spEvent::nCurHierarchy;
	}
    
	if (spEvent::nCurHierarchy==spEvent::nHierarchy) {
		spEvent enterEvent;
		enterEvent.type = spOnEnter;
		enterEvent.object = this;
		SPDEBUG(spDebugEventsMouse, enterEvent << this << "(" << this->GetClassName() << ")");
		ProcessEvent1(enterEvent);
		spEvent::hierarchy[spEvent::nHierarchy] = this;
		spEvent::nHierarchy++;
	}
	
	// Update hierarchy
	spEvent::nCurHierarchy++;
}

void spObject::ProcessPostLeaveEvent()
{
	// Should we send some leave events ?
	spEvent leaveEvent;
	leaveEvent.type = spOnLeave;
	for (int i = spEvent::nHierarchy-1 ; i>=spEvent::nCurHierarchy ; i--) {
		leaveEvent.object = spEvent::hierarchy[i];
		SPDEBUG(spDebugEventsMouse, leaveEvent << leaveEvent.object << "(" << leaveEvent.object->GetClassName() << ")");
		spEvent::hierarchy[i]->ProcessEvent1(leaveEvent);
	}
	spEvent::nHierarchy = spEvent::nCurHierarchy;
    
}

//
// Processing the event (this routine is overloaded for spContainer)
//
bool spObject::ProcessEvent(spEvent &event)
{
	//
	// Case mouse is being dragged
	//
	if (spEvent::Dragging()) {
		
		// We store the event type in order to send a Drag event
        spEventSimpleType type = event.type;
		if (event.type == spOnMotionLeft) event.type = spOnDragLeft;
		else if (event.type == spOnMotionRight) event.type = spOnDragRight;
		else if (event.type == spOnMotionMiddle) event.type = spOnDragMiddle;
		
		// We process the event
		bool res=ProcessEvent1(event);
		
		// We restore the event type
		event.type = type;
		
		// done
		return res;
    }
	
	//
	// Case the mouse is not being dragged
	//
	
	// Send enter/leave events
	ProcessPreLeaveEnterEvent();
	
	// Process the event
	bool res = ProcessEvent1(event);
    
	// Send enter/leave events
    ProcessPostLeaveEvent();
	
	// Done
	return res;
}

//
// Processing event without leave/enter events and dragging
//
bool spObject::ProcessEvent1(spEvent &event)
{
	// If no interactor --> returns
	if (interactorList.Size()==0) return false;
    
	// variable to store the calue that will be returned by this procedure
	bool res = false;
	
	// A priori we consider we do propagate
    bool flagPropagation = true;
	
	// We loop on the interactors
	for (int i=0; i<interactorList.Size() ; i++) {
		
		// We set the propagate flag to false
		spEvent::Propagate(false);
        
		// We process the event
		bool res1 = false;
		if (interactorList[i]->IsOn()) {
			event.object = this;
            res1 = interactorList[i]->Process(event);
			event.object = NULL;
		}
		
		// If succeeded we store the object in the corresponding event field
		if (res1) event.object = this;
		
		// If a mouse down event was processed --> we start dragging (and no propagation)
		if (res1 && (event.type == spOnDownLeft || event.type == spOnDownRight || event.type == spOnDownMiddle)) {
			spEvent::DragStart(event);
			flagPropagation = false;
		}
		
		// If any other event processed and no propagation we set the flagPropagation to false
		else if (res1 && !spEvent::IsPropagating()) flagPropagation = false;
		
		// We go on the loop
		res |= res1;
	}
	
	// If no propagation we return res
	if (flagPropagation == false) return res;
	
	// Otherwise we allow processing of the event by another object ==> we return false
	return false;
}

spCanvas *spObject::GetCanvas()
{
	if (container == NULL || container->topContainer == NULL) return NULL;
	return container->topContainer->canvas;
}

void spObject::DrawAll()
{
    Draw(gBoundingRect());
};

void spObject::Draw(spRect &rect)
{
    printf("debile\n");
}


#define DC container->topContainer->canvas
#define IF if (container == NULL || container->topContainer == NULL || container->topContainer->canvas == NULL) return
#define IF1 if (container == NULL || container->topContainer == NULL || container->topContainer->canvas == NULL)
void spObject::DrawPolygon(int n, spRealPoint *points) {IF;DC->DrawPolygon(n,points);}
void spObject::DrawPolygon(int n, spPoint *points) {IF;DC->DrawPolygon(n,points);}
void spObject::DrawArrowTip(spRealPoint tip, double angle, double length, double alpha) {IF;DC->DrawArrowTip(tip,angle,length,alpha);}
void spObject::DrawRectangle(spRealRect r) {IF;DC->DrawRectangle(r.x,r.y,r.width,r.height);}
void spObject::DrawRectangle(double x, double y, double w, double h) {IF;DC->DrawRectangle(x,y,w,h);}
void spObject::DrawEllipse(spRealRect r) {IF;DC->DrawEllipse(r.x,r.y,r.width,r.height);}
void spObject::DrawEllipse(double x, double y, double w, double h) {IF;DC->DrawEllipse(x,y,w,h);}
void spObject::DrawCEllipse(double x, double y, double w, double h) {IF;DC->DrawEllipse(x-w,y-h,2*w,2*h);};
void spObject::DrawCEllipse(spRealPoint pt, double w, double h) { if (h<0) h=w; IF;DC->DrawEllipse(pt.x-w,pt.y-h,2*w,2*h);}; 
void spObject::DrawLine(double x0, double y0, double x1, double y1) {IF;DC->DrawLine(x0,y0,x1,y1);}
void spObject::DrawLine(spRealPoint pt1, spRealPoint pt2) {IF; DC->DrawLine(pt1.x,pt1.y,pt2.x,pt2.y);}
void spObject::GetTextExtent(const wxString& string,int *x, int *y,int *descent,int *externalLeading,wxFont *theFont) {IF;DC->GetTextExtent(string,x,y,descent,externalLeading,theFont);}
void spObject::DrawText(const wxString& s,double x, double y, double angle) {IF;DC->DrawText(s,x,y,angle);}
void spObject::DrawTextBase(const wxString& s,double x, double y, double angle) {IF;DC->DrawTextBase(s,x,y,angle);}
void spObject::DrawPoint(double x, double y) {IF;DC->DrawPoint(x,y);}
void spObject::DrawFormattedText(const char *str, char hPositionMode, double x, char vPositionMode, double y, double angle) {IF;DC->DrawFormattedText(str,hPositionMode,x,vPositionMode,y,angle);}
void spObject::SetClip(spRect &rect) {IF;DC->SetClip(rect);}
void spObject::SetClip(spRealRect &rect) {
    spRect r;
    IF;
    r = rect.Round();
    DC->SetClip(r);
}
void spObject::UseTextColour(wxColour &textBackgroundColor,wxColour &textForegroundColor) {IF;DC->SetTextColour(textBackgroundColor,textForegroundColor);}
void spObject::UsePen(const wxPen &pen) {IF;DC->SetPen(pen);}
void spObject::UseBrush(const wxBrush &brush) {IF;DC->SetBrush(brush);}
void spObject::UseFont(const wxFont &font) {IF;DC->SetFont1(font);}
void spObject::DeactivateDCBuffer() {IF;DC->DeactivateDCBuffer();}
void spObject::ActivateDCBuffer() {IF;DC->ActivateDCBuffer();}
void spObject::DCBufferToDC(spRect &r) {IF;DC->DCBufferToDC(r);}
spPoint spObject::Canvas2Screen(spPoint &pt) {IF1 return spPoint(0,0);return DC->Canvas2Screen(pt);}
void spObject::DrawFormattedText(spFormattedText &ft) {IF;DC->DrawFormattedText(ft);}


spRealPoint spObject::GetPointProjectionOnLine(spRealPoint pt, spRealPoint line_pt,double line_angle)
{
    double a = -sin(line_angle);
    double b = cos(line_angle);
    double c = -a*line_pt.x-b*line_pt.y;
    double c1 = b*pt.x-a*pt.y;
    double x = (b*c1-a*c)/(a*a+b*b);
    double y = -(a*c1+b*c)/(a*a+b*b);    
    return spRealPoint(x,y);
}


//spCursorCrossHair::spCursorCrossHair() : spObject(NULL,spAnchorPixel(),spMarginPixel(1200,1200,1200,1200),spFLAG_OBJECT_OVERLAY_BUFFER)
spCursorCrossHair::spCursorCrossHair() : spObject(NULL,spAnchor(),spMarginInfinite(),spFLAG_OBJECT_OVERLAY_BUFFER)
{
};



void spCursorCrossHair::Draw(spRect &rect)
{
	spRealPoint pt = container->l2top.ApplyInt(GetAnchor());
	DrawLine(container->gRect().x,pt.y,container->gRect().x+container->gRect().width,pt.y);
	DrawLine(pt.x,container->gRect().y,pt.x,container->gRect().y+container->gRect().height);
}



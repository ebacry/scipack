

#include "sp.h"

/*
 
 En python, on doit pouvoir créer un menu.
 Chaque entrée doit avoir un id donné par _menu_id

--> On doit pouvoir construire un menu en python et ensuite le créer en sp :
 spInteractorContextMenu->SetMenu()
 
--> on fait une fonction GetNextMenuId()
 menu.Append(_menu_id,text);
--> Puis on doit enregistrer cet item :
 spInteractorContextMenu::list.Add(new _Item(this,_menu_id,cb));

*/


//
// Debug CallBack
//
int spInteractorContextMenu::_menu_id = 2000;


void spInteractorContextMenu::ProcessPopup(int id)
{
	for (int i=0;i<list.Size();i++) {
		if (list[i]->id == id) {
		  list[i]->cb(spEvent::menuObject);
			break;
		}
	}
	SPUpdate();
}

void spInteractorContextMenu::AddItem(const char *text,void (*cb)(spObject *))
{
	menu.Append(_menu_id,text);
	list.Add(new _Item(this,_menu_id,cb));
	_menu_id++;
}

static void CBContextMenu(spInteractor *cb,spEvent &event) 
{
	// !! ??
	spInteractorContextMenu *i = (spInteractorContextMenu *) cb;
	spCanvas *canvas = event.object->container->topContainer->canvas;
	
	spPoint point = event.mouse;
	
	spEvent::menuObject = event.object;
	
	i->menu.Bind(wxEVT_COMMAND_MENU_SELECTED, &spCanvas::OnPopupClick,canvas);
	canvas->SetPopup(i->menu);
}

spInteractorContextMenu::~spInteractorContextMenu()
{
	/*	for (int i=0;i<list.Size();i++) {
	 
	 }
	 */
}

spList<_Item,100> spInteractorContextMenu::list;

spInteractorContextMenu::spInteractorContextMenu(spEventSimpleType e, int modifiers1, bool flagPropagate1,const char *name) : spInteractor1(CBContextMenu,e, modifiers1,flagPropagate1,name)
{
}

bool spInteractorContextMenu::CheckObjectIsValid(spObject *o)
{
	if (o->container == NULL || o->container->topContainer == NULL || o->container->topContainer->canvas == NULL) {
        spWarningf("spInteractorContextMenu::CheckObjectIsValid()","spInteractorContextMenu interactor cannot be added to an object with no container");
        return false;
    }
	return spInteractor1::CheckObjectIsValid(o);
}

static void _CBXLinYLin(spObject *o)
{
	spView *view = (spView *) o;
	view->SetLogScaleX(false);
	view->SetLogScaleY(false);
	SPUpdate();
}

static void _CBXLogYLin(spObject *o)
{
	spView *view = (spView *) o;
	view->SetLogScaleX(true);
	view->SetLogScaleY(false);
//	SPUpdate();
}

static void _CBXLinYLog(spObject *o)
{
	spView *view = (spView *) o;
	view->SetLogScaleX(false);
	view->SetLogScaleY(true);
//	SPUpdate();
}

static void _CBXLogYLog(spObject *o)
{
	spView *view = (spView *) o;
	view->SetLogScaleX(true);
	view->SetLogScaleY(true);
//	SPUpdate();
}

spInteractorLogScaleMenu::spInteractorLogScaleMenu(spEventSimpleType e, int modifiers1, bool flagPropagate1,const char *name) : spInteractorContextMenu(e, modifiers1,flagPropagate1,name)
{
	AddItem("XLin-YLin Scale", _CBXLinYLin);	
	AddItem("XLog-YLin Scale", _CBXLogYLin);	
  AddItem("XLin-YLog Scale", _CBXLinYLog);	
  AddItem("XLog-YLog Scale", _CBXLogYLog);	

}

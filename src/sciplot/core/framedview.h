/*
 *  framedview.h
 *  SciPlot
 *
 *  Created by bacry on 29/06/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _SP_FRAMEDVIEW_H_
#define _SP_FRAMEDVIEW_H_



class spFramedView : public spGrid
{
	spView *view;
	spText *title;
	spText *labelx;
	spText *labely;
	spAxisView *axis;
    spTextBox *info;
    

	public :
	
	spFramedView(spContainer *ctn=NULL,const spAnchor &anchor=spAnchor(),const spMargin &margin=spMargin(), unsigned long flags=spFLAG_VIEW_REVERSE_Y | spFLAG_VIEW_KEEPBOUND_WHEN_RESIZED,const char *name=NULL);
    virtual ~spFramedView();

	spView *GetViewObject() {return view;}
	spText *GetTitleObject() {return title;}
	spText *GetLabelxObject() {return labelx;}
	spText *GetLabelyObject() {return labely;}
	spAxisView *GetAxisObject() {return axis;}
	spTextBox *GetInfoObject() {return info;}

	virtual void SetInfo(const char *str);
	virtual void SetText(const char *str) {SetInfo(str);}
	virtual const char *GetInfo();
	
	void CreateInfoObject();
	
	virtual const char *GetClassName() {
		return _CCHAR("spFramedView");
	}		
};

#endif


#include "sp.h"

	
void CBScrollWheelX(spInteractor *cb, spEvent &event)
{
	if (!event.object->IsView()) return;	
	spView *v = (spView *) event.object;
	
	v->ScrollX(event.wheel,true); 
//	SPUpdate();
}

void CBScrollWheelY(spInteractor *cb, spEvent &event)
{
	if (!event.object->IsView()) return;	
	spView *v = (spView *) event.object;
	
	v->ScrollY(event.wheel); 
//	SPUpdate();
}


spInteractorScrollView::spInteractorScrollView(char xy, bool flagPropagate,const char *name) : spInteractor1((xy=='x' ? CBScrollWheelX : CBScrollWheelY),spOnMouseWheel,0,flagPropagate,name)
{
    if (name == NULL)
        SetName(flagScrollX ? _CCHAR("Scroll X") : _CCHAR("Scroll Y"));
}

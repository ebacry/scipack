//
//  shape.cpp
//  sp
//
//  Created by bacry on 15/01/13.
//
//

#include "sp.h"


spShape::spShape(spContainer *container,spShapeType shape1,const spAnchor &anchor,spMargin margin,unsigned long flags,const char *name) : spObject(container,anchor,(shape1 == spShapeInfiniteLine ? spMarginInfinite() : margin),flags,name)
{
    arrowLength = 10;
    arrowAngle= 30;
    tolerance = 30;
    if (shape1 == spShapeInfiniteLine) {
        shape = spShapeInfiniteLine; 
        spRealPoint size = margin.GetSize();
        angle = atan2(size.y,size.x);
    }
    else  SetShape(shape1);

}

void spShape::SetShape(spShapeType sh) {
    if (sh == spShapeInfiniteLine) {
        spWarningf("spShape::spShape", "Cannot change shape to spShapeInifiniteLine");
        return;
    }
    if (shape == spShapeInfiniteLine) {
        spWarningf("spShape::spShape", "Cannot change shape of an spShapeInifiniteLine");
        return;
    }
    if (sh == spShapeArrow || sh == spShapeLine || sh == spShapeDoubleArrow) {
        if (!IsDefinedBy2Points()) {
            spWarningf("spShape::spShape", "spShape lines must be defined by two points, i.e., spAnchor(pt1), spMargin(pt2-pt1)");
            return;
        }
    }

    AttBeforeUpdate();
    shape = sh;
    if (shape == spShapeArrow || shape == spShapeDoubleArrow) box.SetPixelMargin(MAX(3,arrowLength/2));
    else box.SetPixelMargin(3);
    AttAfterUpdate();;
}

void spShape::SetArrowLength(int length) {
    AttBeforeUpdate();
    arrowLength = length;
    if (shape == spShapeArrow || shape == spShapeDoubleArrow) box.SetPixelMargin(MAX(3,arrowLength/2));
    else box.SetPixelMargin(3);
    AttAfterUpdate();;
}

void spShape::SetArrowAngle(double angle) {
    AttBeforeUpdate();
    arrowAngle = angle;
    if (shape == spShapeArrow || shape == spShapeDoubleArrow) box.SetPixelMargin(MAX(3,arrowLength/2));
    else box.SetPixelMargin(3);
    AttAfterUpdate();;
}

double spShape::GetDistance(spRealPoint pt)
{
    if (shape != spShapeArrow && shape != spShapeDoubleArrow && shape != spShapeLine && shape != spShapeInfiniteLine)
        return spObject::GetDistance(pt);

    if (shape != spShapeInfiniteLine && spObject::GetDistance(pt) == -1) return -1;

    spRealPoint pt1 = container->l2top.IApply(pt);
    spRealPoint anchor = GetAnchor();
    
    if (shape != spShapeInfiniteLine) {
        spRealPoint anchor1 = GetAnchorOposite();
        angle = atan2(anchor.y-anchor1.y,anchor.x-anchor1.x);
    }
    
    spRealPoint pt2 = GetPointProjectionOnLine(pt1,anchor,angle);
    
    pt1 = container->l2top.Apply(pt1);
    pt2 = container->l2top.Apply(pt2);
    spRealPoint d = pt1-pt2;
    double dd = sqrt(d.x*d.x+d.y*d.y);
    if (dd>tolerance) return -1;
    return dd;
}



void spShape::Draw(spRect &r)
{
    spRealPoint pt,pt1,pt2;
    
    //	cout << "**Draw Circle"  << endl;
	
	switch(shape) {
		case spShapeEllipse :
			DrawEllipse(gRect().x, gRect().y, gRect().width, gRect().height);
			break;
		case spShapeRect :
			DrawRectangle(gRect().x, gRect().y, gRect().width, gRect().height);
			break;
		case spShapeLine : case spShapeArrow : case spShapeDoubleArrow :
            pt = container->l2top.Apply(GetAnchor());
            pt1 = container->l2top.Apply(GetAnchorOposite());
			DrawLine(pt,pt1);
            if (shape != spShapeLine) {
                double angle = atan2(pt.y-pt1.y,pt.x-pt1.x)*180/M_PI;
                DrawArrowTip(pt,angle,arrowLength,arrowAngle);
                if (shape == spShapeDoubleArrow) {
                    DrawArrowTip(pt1,angle+180,arrowLength,arrowAngle);
                }
             
            }
            break;
		case spShapeInfiniteLine :
            spRealRect rect = container->l2top.IApply(container->gRect());
            double xmin = rect.x;
            double xmax = rect.x+rect.width;
            double ymin = rect.y;
            double ymax = rect.y+rect.height;
            
            spRealPoint anchor = GetAnchor();

            if (angle > M_PI/2+0.01 || angle < M_PI/2-0.01) {
                ymin = tan(angle)*(xmin-anchor.x)+anchor.y;
                ymax = tan(angle)*(xmax-anchor.x)+anchor.y;
            }
            else {
                xmin = (cos(angle)/sin(angle))*(ymin-anchor.y)+anchor.x;
                xmax = (cos(angle)/sin(angle))*(ymax-anchor.y)+anchor.x;
            }
            pt1 = container->l2top.Apply(spRealPoint(xmin,ymin));
            pt2 = container->l2top.Apply(spRealPoint(xmax,ymax));
			DrawLine(pt1,pt2);
            break;
	}
}


// Not a module


%rename(TopContainer) spTopContainer;


// The container class is a director
%feature("director") spTopContainer;

%feature("kwargs") spTopContainer::spTopContainer;
%feature("kwargs") spTopContainer::Print;


%DocClass(spTopContainer,"The container class that is the top of the object hierarchy. The spCanvas class is a class that will be merged with sp.TopContainer class in a future version. Don't use it !")
class spTopContainer : public spGrid
{
	public :

    ObjRepr;

    %DocMeth(Print,"Print(flagPageSetupDialog=True,flagPrintDialog=True,units_per_cm=200)","Send the content of the topcontainer to a printer")
    void Print(bool flagPageSetupDialog=true, bool flagPrintDialog = true, int units_per_cm=200);

    %DocMeth(ToPS,"ToPS(filename,x_margin_cm=0,y_margin_cm=0)",
    "Create a postscript file with the topcontainer content.

    .. warning:: 
        There is still a small bug, wxWidget cannot find the right path for the AFM fonts,
        so the texts are not justified correctly.
    ")
    void ToPS(char *filename, double x_margin_cm=0,double y_margin_cm=0);

    %DocMeth(ToSVG,"ToSVG(filename)",
    "Create an SVG file
    
    .. warning::
        It works only for wxWidget >= 3.0
        Moreover, even in this case, the svg file is missing a </g> tag right before the last line
        (i.e., the </svg> line).
        It seems to be a wxWidget error

    .. warning::
        There is the same bug as for postscript, wxWidget cannot find the right path for the AFM fonts,
        so the texts are not justified correctly.
    ")
    void ToSVG(char *filename);

	spTopContainer(spCanvas *canvas,const char *name=NULL);
    virtual ~spTopContainer();

	virtual bool IsTopContainer();
};


%extend spTopContainer {
    %immutable;
    spCanvas *canvas;
    %mutable;
}
%{
    spCanvas *spTopContainer_canvas_get(spTopContainer *s) {return s->canvas;}
%}


%extend spTopContainer {
    %DocMeth(GetCurTopContainer,"GetCurTopContainer","Get current topcontainer (the last one something was drawn in it or the last one which was visited by the mouse")
    static PyObject *GetCurTopContainer() {
        if (spCanvas::curCanvas) {
            return WrapObject(spCanvas::curCanvas->GetTopContainer());
        }
        Py_INCREF(Py_None);
        return Py_None;
    }
}


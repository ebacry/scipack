
// Not a module

%rename(FLAG_CONTAINER_HIDE) spFLAG_CONTAINER_HIDE;
%rename(FLAG_VIEW_KEEPBOUND_WHEN_RESIZED) spFLAG_VIEW_KEEPBOUND_WHEN_RESIZED;
%rename(FLAG_CONTAINER_DRAW_FRAME) spFLAG_CONTAINER_DRAW_FRAME;
%rename(FLAG_VIEW_REVERSE_X) spFLAG_VIEW_REVERSE_X;
%rename(FLAG_VIEW_REVERSE_Y) spFLAG_VIEW_REVERSE_Y;
%rename(FLAG_OBJECT_OVERLAY_BUFFER) spFLAG_OBJECT_OVERLAY_BUFFER;

%rename(Container) spContainer;

enum  {
	spFLAG_CONTAINER_HIDE = 0x01,
	spFLAG_VIEW_KEEPBOUND_WHEN_RESIZED= 0x02,
	spFLAG_CONTAINER_DRAW_FRAME= 0x20,
	spFLAG_VIEW_REVERSE_X= 0x10,
	spFLAG_VIEW_REVERSE_Y= 0x04,
	spFLAG_OBJECT_OVERLAY_BUFFER= 0x80
};



// The container class is a director
%feature("director") spContainer;

// Some methods with named parameters
%feature("kwargs") spContainer::spContainer;


//
// The spContainer class
//
%DocClass(spContainer,"A rectangular object that contains other objects. The new valid values for the flags are
- sp.FLAG_CONTAINER_HIDE (containers only) : container is hidden when created.
- sp.FLAG_CONTAINER_DRAW_FRAME (containers only) : draw frame around container
- sp.FLAG_VIEW_REVERSE_X (views only) : X axis is reversed
- sp.FLAG_VIEW_REVERSE_Y (views only) : Y axis is reversed
- sp.FLAG_VIEW_KEEPBOUND_WHEN_RESIZED (views only) : Keeps bounds of the views when resized")
class spContainer : public spObject
{
	public :
	
    ObjRepr;

    %DocMeth(IsGroup,"IsGroup() -> bool","Returns True if container is a sp.Group")
	virtual bool IsGroup();
    
	virtual void Draw(spRect &rect);

    %DocMeth(L2Top(const spRealPoint p),"","")
    spRealPoint L2Top(const spRealPoint p);
    %DocMeth(L2Top(const spRealRect r),"L2Top(rect or point) -> rect or point","Converts local rect or point to top container rect or point")
    spRealRect L2Top(const spRealRect r);

    %DocMeth(Top2L(const spRealPoint p),"","")
    spRealPoint Top2L(const spRealPoint p);
    %DocMeth(Top2L(const spRealRect r),"Top2L(rect or point) -> rect or point","Converts topcontainer rect or point to local rect or point")
    spRealRect Top2L(const spRealRect r);
    
    %DocMeth(L2TopDer,"L2TopDer(pt = spRealPoint(1,1)) -> point","Returns derivative of the L2Top map at point pt")
    spRealPoint L2TopDer(const spRealPoint p = spRealPoint(1,1));
    %DocMeth(L2TopSign,"L2TopSign() -> sp.RealPoint","Returns sign of derivative of the L2Top map")
    spRealPoint L2TopSign();
    
    
    %DocMeth(AddObject,"AddObject(object)","Adds object to the container")
	void AddObject(spObject *object);
    %DocMeth(RemoveObject,"RemoveObject(object)","Removes object of the container")
	void RemoveObject(spObject *object);
    %DocMeth(RemoveAllObjects,"RemoveAllObjects()","Removes all the objects of the container")
	void RemoveAllObjects();


	%extend {
      %DocMeth(FindObjectMouse,"FindObjectMouse(pt,flagCursor=False) -> object or None","Finds object which is the closest to the point (topcontainer's coordinate).")
      PyObject *FindObjectMouse(spRealPoint mouse,bool flagCursor=false)
      {
        spObject *object = $self->FindObjectMouse(mouse,flagCursor);
        return WrapObject(object);
      }
    }
	
	spContainer(spContainer *container=NULL,const spAnchor &anchor=spAnchor(),const spMargin &margin=spMargin(),unsigned long flags=0,const char *name=NULL);
    virtual ~spContainer();
	
    %DocMeth(IsReversedX,"IsReversedX() -> bool","True if x axis is reversed")
    bool IsReversedX();
    %DocMeth(IsReversedY,"IsReversedY() -> bool","True if y axis is reversed")
    bool IsReversedY();

    %DocMeth(Clear,"Clear()","(To be called within the Draw method only) Clears the rectangle of the container")
	void Clear();
    
    virtual bool IsContainer();
};


//
// Getting l2top if necessary
//
%extend spContainer {
    %DocMeth(GetL2TopMap,"GetL2TopMap() -> spMap2d","Get the Map2d that transforms local coordinates to topcontainer's coordinates.")
  spMap2d GetL2TopMap(spContainer *ct) {return ct->l2top;}
}

//
// objects Read-Only field
//
%extend spContainer {
    %DocMeth(objects_,"FAKE METHOD just for indicating doc of self.objects","Use field objects for getting a list of the objects inside the container.")
    void objects_() {};
}
%extend spContainer {
%immutable;
    PyObject *objects;
%mutable;
}
%{
    PyObject *spContainer_objects_get(spContainer *ct) {
      PyObject* list = PyList_New(ct->objectList.Size());
      for (int i = 0; i < ct->objectList.Size(); i++) {
        PyObject *obj = WrapObject(ct->objectList[i]);
        PyList_SET_ITEM(list, i, obj);
      }
      return list;
    }
%}

//
// The nobjects Read-only field
//
%extend spContainer {
    %DocMeth(nobjects_,"FAKE METHOD just for indicating doc of self.nobjects","Use field nobjects for getting the number of objects inside the container.")
    void objects_() {};
}
%extend spContainer {
    %immutable;
    int nobjects;
    %mutable;
}
%{
    int spContainer_nobjects_get(spContainer *s) {return s->objectList.Size();}
%}


//
// The object(i) method
//
%extend spContainer {
    %DocMeth(object,"object(index) -> spObject","Get the index-th object inside the container.")
  PyObject *object(int index) {return WrapObject($self->GetObject(index));}
}

//
// The topcontainer Read-Only field
//
%extend spContainer {
    %DocMeth(topcontainer_,"FAKE METHOD just for indicating doc of self.topcontainer","Use field topcontainer for getting the topcontainer of the container. If the container is a topcontainer, then it returns itself.")
    void topcontainer_() {};
}
%extend spContainer {
%immutable;
    PyObject *topcontainer;
%mutable;
}
%{
    PyObject *spContainer_topcontainer_get(spContainer *ct) {return WrapObject(ct->GetTopContainer());}
%}









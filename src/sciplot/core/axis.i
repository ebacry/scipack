
// Not a module

%rename(AxisView) spAxisView;

%rename(_GetFlagTicksIn) GetFlagTicksIn;
%rename(_SetFlagTicksIn) SetFlagTicksIn;
%rename(_GetPixelMargin) GetPixelMargin;
%rename(_SetPixelMargin) SetPixelMargin;
%rename(_GetFlagFrame) GetFlagFrame;
%rename(_SetFlagFrame) SetFlagFrame;

// We set the kwargs for some methods
%feature("kwargs") spAxisView::spAxisView;

class spAxisView : public spObject
{
	public :

    // The string representation in python
    ObjRepr;

    virtual void Draw(spRect &rect);
	virtual double GetDistance(spRealPoint pt);

	spAxisView(spView *connectedView=NULL, unsigned long flags = 0,const char *name = NULL);
    
    bool GetFlagTicksIn();
    void SetFlagTicksIn(bool flag);
    %attobj_def(flagTicksIn,FlagTicksIn,"""""");
    
    int GetPixelMargin();
    void SetPixelMargin(int pixelMargin);
    %attobj_def(pixelMargin,PixelMargin,"""""");
    
    bool GetFlagFrame();
    void SetFlagFrame(bool flag);
    %attobj_def(flagFrame,FlagFrame,"""""");

};


//
// The connectView Read-only field
//
%extend spAxisView {
%immutable;
PyObject *connectedView;
%mutable;
}
%{
    PyObject *spAxisView_connectedView_get(spAxisView *a) {return WrapObject(a->GetConnectedView());}
%}



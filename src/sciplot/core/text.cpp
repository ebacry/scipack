


#include "sp.h"

static spMargin getMarginFunc(spObject *o,double xmin,double xmax)
{
	spMargin margin;
	spText *t = (spText *) o;
	
	t->UseAttributes();
	
	spRealPoint anchor = t->container->l2top.ApplyInt(t->GetAnchor(true));
    
    t->ft.SetAnchorPoint(spRealPoint(anchor.x,anchor.y));
    t->ft.Format(t->GetCanvas());
    spRealRect r = t->ft.boundingRect;
	if (t->IsFrame()) {
		r.x-=t->frameMargin.x;
		r.y-=t->frameMargin.y;
		r.width+=t->frameMargin.x*2;
		r.height+=t->frameMargin.y*2;
	}
	
    margin.Set(r.GetPosition1().x-anchor.x,r.GetPosition1().y-anchor.y,anchor.x-r.GetPosition().x,anchor.y-r.GetPosition().y);
    margin.SetPixelStraight();
	margin.SetPixelMargin(0);
	return margin;
}

spText::spText(spContainer *ctn,const spAnchor &anchor1,int hmode1, int vmode1, unsigned long flags,const char *name1)
: spObject(ctn,spAnchor(anchor1),spMarginFunc(getMarginFunc),flags,name1)
{
	ft.SetHVMode(hmode1,vmode1);
    ft.SetAngle(0);
	SetPen(*wxBLACK_PEN);
	SetBrush(*wxTRANSPARENT_BRUSH);
	frameMargin = spPoint(-1,-1);
    //	SetBrush(*wxBLACK_BRUSH);
//	AddInteractor(spInteractorTextEdit::GetTheInteractor());
}


spText::~spText()
{
}


void spText::SetFrameMargin(spRealPoint frameMargin1)
{
    AttBeforeUpdate();
	frameMargin = frameMargin1.GetPointInt();
    AttAfterUpdate();;
}

void spText::SetText(const char * str1)
{
    AttBeforeUpdate();
	ft.SetText(str1);
    AttAfterUpdate();;
}

void spText::SetAngle(double angle1)
{
    AttBeforeUpdate();
	ft.SetAngle(angle1);
    AttAfterUpdate();;
}

void spText::SetHVMode(int hMode, int vMode)
{
    AttBeforeUpdate();
	ft.SetHVMode(hMode,vMode);
    AttAfterUpdate();;
}
void spText::SetHMode(int hMode)
{
    AttBeforeUpdate();
	ft.SetHVMode(hMode,ft.vMode);
    AttAfterUpdate();;
}
void spText::SetVMode(int vMode)
{
    AttBeforeUpdate();
	ft.SetHVMode(ft.hMode,vMode);
    AttAfterUpdate();;
}

void spText::Draw(spRect &rect)
{
	UseAttributes();
    
	if (IsFrame()) {
        spRealRect r = gRect();
        DrawRectangle(r.x,r.y,r.width,r.height);
	}
    
    DrawFormattedText(ft);
    
    //	printf("????%d %d %d %d %s\n",ft.boundingRect.x,ft.boundingRect.y,ft.boundingRect.width,ft.boundingRect.height,ft.str);
    //	DrawCircle(textPt.x,textPt.y,3);
    //	DrawCircle(box.anchor.x,box.anchor.y,5);
}


/*
 spAnnotation::spAnnotation(spContainer *view1,const spBox &box, char *str1, unsigned long flags) : spText(view1,box,spJustifyHLeft,spJustifyVBase,flags)
 {
 String(str);
 }
 */







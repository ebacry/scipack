//
//  textbox.cpp
//  sp
//
//  Created by bacry on 15/01/13.
//
//

#include "sp.h"


#define DC container->topContainer->canvas

spTextBox::spTextBox(spContainer *ctn,const spAnchor &anchor,const spMargin &margin, int hmode1, unsigned long flags,const char *name1) : spShape(ctn,spShapeRect,anchor,margin,flags,name1)
{
    flagFrame = true;
	hMode = hmode1;
	ft.SetHVMode(hMode,spJustifyVMiddle);
    ft.SetAngle(0);
	SetPen(*wxBLACK_PEN);
	SetBrush(*wxTRANSPARENT_BRUSH);
//	AddInteractor(spInteractorTextEdit::GetTheInteractor());
}

void spTextBox::SetHMode(int hMode)
{
    AttBeforeUpdate();
    ft.SetHVMode(hMode,spJustifyVMiddle);
    AttAfterUpdate();;
}

void spTextBox::SetText(const char * str1)
{
	if (str1 == NULL) return;
	
    AttBeforeUpdate();
    
	ft.SetText(str1);
    
    AttAfterUpdate();;
}

const char *spTextBox::GetText()
{
    return(ft.GetText());
}

void spTextBox::UpdateGeometry()
{
	UseAttributes();
	
	spShape::UpdateGeometry();
    
	spRect r = gRect();
	spPoint pt;
	if (hMode == spJustifyHLeft) pt = spPoint(r.x+5,r.y+r.height/2);
	else if (hMode == spJustifyHMiddle) pt = spPoint(r.x+r.width/2,r.y+r.height/2);
    
    ft.SetAnchorPoint(pt);
    ft.Format(DC);
}

void spTextBox::Draw(spRect &rect)
{
	if (!flagFrame) UsePen(*wxTRANSPARENT_PEN);
	spShape::Draw(rect);
	if (!flagFrame) UsePen(pen);
	
    DrawFormattedText(ft);
	
	//	DrawCircle(textPt.x,textPt.y,3);
	//	DrawCircle(box.anchor.x,box.anchor.y,5);
}


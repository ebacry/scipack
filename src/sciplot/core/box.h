/*
 *  box.h
 *  SciPlot
 *
 *  Created by bacry on 12/07/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef _SP_BOX_
#define _SP_BOX
#include "scipackdefs.h"


//
// Classes used below
//
class spContainer;
class spObject;

/////////////////////////////
//
// Anchor class
//
// The spAnchor class is used to code the anchor of the box of an object.
// Basically it corresponds to an (x,y) point in the father's container coordinate
// There are several constructors (with different class)
//
//    spAnchor : used to specify (x,y) in father's container coordinate
//    spAnchorNone : an empty anchor
//    spAnchorGrid : used to specify (x,y) in father's container GRID coordinate
//    spAnchorFunc : used to specify (x,y) using a function
//
/////////////////////////////


// The class for a pointer to a function
class spAnchor;
class SPEXPORT spAnchorFuncPtr
{
	public :
        
    // Eventual translation of anchor (used when object is moved)
	spRealPoint translation;
    bool flagIsPixelTranslation;
    void SetPixelTranslation() {flagIsPixelTranslation = true;}
    
    // The C function
    // either : spAnchor (*getAnchorFunc)(spObject *,double xmin,double xmax);
    // or     : spAnchor (*getAnchorFunc)(spObject *);
    void *getFunc;
    bool flagGetFuncXMinXMax;
    void SetGetFunc(spAnchor (*ptr)(spObject *,double xmin,double xmax)) {getFunc = (void *) ptr;flagGetFuncXMinXMax = true;}
    void SetGetFunc(spAnchor (*ptr)(spObject *)) {getFunc = (void *) ptr;flagGetFuncXMinXMax = false;}
    
    // Call the function (either script or C)
	spAnchor Call(spObject *object, double xmin, double xmax);
    
    // Is it set ?
	inline bool IsSet() {return (getFunc != NULL);}
    
    // Constructor
    spAnchorFuncPtr();
	spAnchorFuncPtr(spAnchor (*getAnchorFunc1)(spObject *,double xmin,double xmax));
	spAnchorFuncPtr(spAnchor (*getAnchorFunc1)(spObject *));
    
    // Destructor
    virtual ~spAnchorFuncPtr() {};
    
#ifdef SP_SCRIPT
    // The script function if any
	void *scriptFunc;
    void SetScriptFunc(void *ptr) {scriptFunc = ptr;}
#endif
    
        
};


// The main class
class SPEXPORT spAnchor
{
    protected :
    
	bool flagMoveable;    // True if anchor is moveable
	bool flagNone;        // True if no anchor
	bool flagGrid;        // Indicates whether grid coordinates are used
    
	spRealPoint anchor;   // The anchor point in father's container coordinate
    
    public :
	spAnchorFuncPtr anchorFuncPtr; // The eventual pointer to a function
	
	// Constructor
	spAnchor(double x, double y) {
		anchor = spRealPoint(x,y);
		flagGrid = false;
		flagMoveable = true;
		flagNone = false;
	}
	spAnchor() {
		anchor = spRealPoint(0,0);
		flagGrid = false;
		flagMoveable = true;
		flagNone = false;
	}
	spAnchor(spRealPoint pt) {
		anchor = pt;
		flagGrid = false;
		flagMoveable = true;
		flagNone = false;
	}

	// String representation
	std::string ToStr(bool flagShort) {
		char str[200];
        if (IsNone()) sprintf(str,"spAnchorNone()");
        else if (IsFunc()) sprintf(str,"spAnchorFunc(...)");
        else if (IsGrid()) sprintf(str,"spAnchorGrid(%g,%g)",anchor.x,anchor.y);
        else sprintf(str,"spAnchor(%g,%g)",anchor.x,anchor.y);
		return (std::string) str;
	}
    
    // Setting the anchor
	void Set(spObject *object, spRealPoint anchor1);
	inline void Set(spObject *object, double x, double y) {Set(object,spRealPoint(x,y));}
	inline void SetPixelTranslation() {return anchorFuncPtr.SetPixelTranslation();}
    
    // Move the anchor
	void Move(spObject *object,double dx, double dy);
    
    // Getting the anchor
	spRealPoint Get(spObject *object,bool flagUpdate = true,double xMin=1,double xMax=0);

	// Some tests
    inline 	bool IsFunc() {return anchorFuncPtr.IsSet();}
	inline 	bool IsNone() {return flagNone;}
    
    // Update the anchor if computed with a function
	void Update(spObject *object,double xmin=1,double xmax=-1);
    
    // Grid flag
    inline void SetGrid(bool flag=true) {flagGrid = flag; if (flag) flagMoveable = false;}
	inline bool IsGrid() {return flagGrid;}
    
    // Moveable flag
    inline bool IsMoveable() {return flagMoveable & !flagGrid;}
	inline void SetMoveable(bool flag=true) {if (!flagGrid) flagMoveable=flag;}
};


// The class to build an empty Anchor
class SPEXPORT spAnchorNone : public spAnchor
{
	public :
	
	spAnchorNone() : spAnchor() {
		flagNone = true;
	}
};

// The class to build a grid anchor
class SPEXPORT spAnchorGrid : public spAnchor
{
	public :
    
	spAnchorGrid(double x=0, double y=0) : spAnchor(x, y) {
		flagGrid = true;
		flagMoveable = false;
	}
};

// The class to build an anchor using a function
class SPEXPORT spAnchorFunc : public spAnchor
{
	public :
	
	spAnchorFunc() : spAnchor() {}
	spAnchorFunc(spAnchorFuncPtr anchorFuncPtr1, bool flagMoveable1=false) : spAnchor() {
		anchorFuncPtr = anchorFuncPtr1;
		flagMoveable = flagMoveable1;
	}
};


/////////////////////////////
//
//
// margin class
//
// This class is used to code the margins of the bounding box of an object
//
//
/////////////////////////////

class spMargin;

class SPEXPORT spMarginFuncPtr
{
	public :
    
    // The C Function
    void *getFunc;
    bool flagGetFuncXMinXMax;
    void SetGetFunc(spMargin (*ptr)(spObject *,double xmin,double xmax)) {getFunc = (void *) ptr;flagGetFuncXMinXMax = true;}
    void SetGetFunc(spMargin (*ptr)(spObject *)) {getFunc = (void *) ptr;flagGetFuncXMinXMax = false;}
    
    // Call the function (either script or C)
	spMargin Call(spObject *object, double xmin, double xmax);

    // Is it set ?
	inline bool IsSet() {return (getFunc != NULL);}
    
    // Constructor    
	spMarginFuncPtr(spMargin (*getMarginFunc)(spObject *));
	spMarginFuncPtr(spMargin (*getMarginFunc)(spObject *,double xmin,double xmax));
	spMarginFuncPtr();

    // Destructor
    virtual ~spMarginFuncPtr() {};
    
#ifdef SP_SCRIPT
    // The script function if any
	void *scriptFunc;
    void SetScriptFunc(void *ptr) {scriptFunc = ptr;}
#endif
};


class SPEXPORT spMargin
{
    protected :
    
	double yMargin,myMargin;    // The margins
	double xMargin,mxMargin;
    
	int pixelMargin;  // An additionnal margin expressed in pixels
    
	bool flagPixel;    // Are the marins expressed in pixels ?
	bool flagPixelStraight;    // Are the marins expressed in pixels ?
	bool flagInfinite; // is the margin infinite ?
	bool flagGrid;     // Are the margins expressed in grid coordinates
    bool flagCentered; // Margin for which the anchor is centered
	public :
	
	spMarginFuncPtr marginFuncPtr; // Used when the margin is computed using a function
    
    void Init() {
        xMargin = yMargin = mxMargin = myMargin = 0;
        flagGrid = false;
		flagPixel = false;
		flagPixelStraight = false;
		pixelMargin = 0;
		flagInfinite = false;
        flagCentered = false;
    }
        
    // "Stupid" Constructor
	spMargin() {Init();}

    // Constructor using a single point (coding the distance to the anchor)
	spMargin(double xMargin1,double yMargin1) {
        Init();
        spRealPoint pt(xMargin1,yMargin1);
        if (pt.x >=0) xMargin = pt.x;
        else mxMargin = -pt.x;
        if (pt.y >=0) yMargin = pt.y;
        else myMargin = -pt.y;
    }
    
    // Same as above with a point
    spMargin(spRealPoint pt) {
        Init();
        if (pt.x >=0) xMargin = pt.x;
        else mxMargin = -pt.x;
        if (pt.y >=0) yMargin = pt.y;
        else myMargin = -pt.y;
	};

    
    // Full Constructor
	spMargin(double xMargin1,double yMargin1, double mxMargin1, double myMargin1) {
        Init();
        xMargin = xMargin1;
		yMargin = yMargin1;
		mxMargin = mxMargin1;
		myMargin = myMargin1;
	};
    
    
	// String representation
	std::string ToStr(bool flagShort) {
		char str[200];
        if (IsInfinite()) sprintf(str,"spMarginInfinite()");
        else if (IsFunc()) sprintf(str,"spMarginFunc(...)");
        else if (IsGrid()) sprintf(str,"spMarginGrid(%g,%g,%g,%g)",xMargin,yMargin,mxMargin,myMargin);
        else if (IsPixel()) sprintf(str,"spMarginPixel(%d,%d,%d,%d)",(int) xMargin,(int) yMargin,(int) mxMargin,(int) myMargin);
        else sprintf(str,"spMargin(%g,%g,%g,%g)",xMargin,yMargin,mxMargin,myMargin);
		return (std::string) str;
	}
    
    // Set the margins
    inline void Set(double xMargin1, double yMargin1, double mxMargin1=0, double myMargin1=0) {
		if (marginFuncPtr.IsSet()) return;
		xMargin = xMargin1;
		mxMargin = mxMargin1;
		yMargin = yMargin1;
		myMargin = myMargin1;
	};
    
    // Get the margins in pixel coordinates (if flagPixel) or in container coordinates
    // (if flagGrid then the margins are converted)
	spRealRect Get(spObject *object,bool flagUpdate = true,double xmin=1,double xmax=-1);
    void Get(double *xMargin1,double *yMargin1,double *mxMargin1,double *myMargin1) {
        *xMargin1 = xMargin;
        *yMargin1 = yMargin;
        *mxMargin1 = mxMargin;
        *myMargin1 = myMargin;
    }
    
    inline bool IsDefinedBy2Points() {
        return((mxMargin == 0 && myMargin == 0) || (mxMargin == 0 && yMargin == 0) || (xMargin == 0 && myMargin == 0) || (xMargin == 0 && yMargin == 0));
    }
    spRealPoint GetOriginOposite(spObject *object,spRealPoint origin);
    void SetOriginOposite(spObject *object,spRealPoint pt);
    void SetSize(double dx, double dy);
    void SetSize(spRealPoint dv) {SetSize(dv.x,dv.y);}
    spRealPoint GetSize();

    
    // Some Tests
    inline bool IsFunc() {return marginFuncPtr.IsSet();}
	inline bool IsInfinite() {return flagInfinite;}
	inline bool IsEmpty() {return !IsInfinite() && xMargin==0 && mxMargin == 0 && yMargin==0 && myMargin == 0;};
    
    // Update the margin if computed with a function
	void Update(spObject *object,double xmin=1,double xmax=-1);
    
    // Get the container the margin is expressed in (can return NULL)
    spContainer * GetContainer(spObject *object);
	
    // Additionnal margin expressed in pixels
	inline int GetPixelMargin() {return pixelMargin;}
	inline void SetPixelMargin(int pm) {pixelMargin = pm;}
    
    // Managing the grid flag
	inline void SetGrid(bool flag=true) {flagGrid = flag;}
	inline bool IsGrid() {return flagGrid;}
    
    // Managing the pixel flag
	inline bool IsPixel() {return flagPixel;}
	inline bool IsPixelStraight() {return flagPixelStraight;}
	inline bool SetPixel(bool flag=true) {flagPixel=flag; return flag;}
	inline bool SetPixelStraight(bool flag=true) {flagPixelStraight=flag; return flag;}
    
    inline bool IsResizeable() {return (!(marginFuncPtr.IsSet()) && !IsGrid());}
};


class SPEXPORT spMarginGrid : public spMargin
{
	public :
	
	spMarginGrid() : spMargin() {
        flagGrid = true;
    }

    spMarginGrid(double xMargin1,double yMargin1) : spMargin(xMargin1,yMargin1) {
		flagGrid = true;
	}
    
    spMarginGrid(spRealPoint pt) : spMargin(pt) {
		flagGrid = true;
	};
};

class SPEXPORT spMarginPixel : public spMargin
{
	public :

    spMarginPixel() : spMargin() {
		flagPixel = true;
    }
    
    spMarginPixel(double xMargin1,double yMargin1) : spMargin(xMargin1,yMargin1) {
		flagPixel = true;
	}
    spMarginPixel(spRealPoint pt) : spMargin(pt) {
		flagPixel = true;
	}

	spMarginPixel(double xMargin1,double yMargin1, double mxMargin1, double myMargin1) : spMargin(xMargin1,yMargin1,mxMargin1,myMargin1) {
		flagPixel = true;
	}    
};




class SPEXPORT spMarginCenteredPixel : public spMargin
{
	public :
    
    spMarginCenteredPixel() : spMargin() {
        flagCentered = true;
		flagPixel = true;
    }
    
    spMarginCenteredPixel(double xMargin1,double yMargin1) : spMargin(xMargin1,yMargin1,xMargin1,yMargin1) {
        flagCentered = true;
		flagPixel = true;
	}
    spMarginCenteredPixel(spRealPoint pt) : spMargin(pt) {
        flagCentered = true;
		flagPixel = true;
	}    
};


class SPEXPORT spMarginCentered : public spMargin
{
	public :
    
    spMarginCentered() : spMargin() {
        flagCentered = true;
    };
    
    spMarginCentered(double xMargin1,double yMargin1) : spMargin(xMargin1,yMargin1,xMargin1,yMargin1) {
        flagCentered = true;
	}
    spMarginCentered(spRealPoint pt) : spMargin(pt.x,pt.y,pt.x,pt.y) {
        flagCentered = true;
	}    
};


class SPEXPORT spMarginFunc : public spMargin
{
	public :
	
	spMarginFunc() : spMargin() {}
	spMarginFunc(spMarginFuncPtr marginFuncPtr1) : spMargin() {
		marginFuncPtr = marginFuncPtr1;
	}
};

class SPEXPORT spMarginInfinite : public spMargin
{
	public :
	
	spMarginInfinite() : spMargin()
	{
		flagInfinite=true;
	}
};



/////////////////////////////
//
//
// Box class
//
// This class is used to code the bounding box of an object
//
//
/////////////////////////////

class SPEXPORT spBox
{
	private :
    
	spAnchor anchor;  // The corresponding anchor
	spMargin margin;  // The corresponding margin
	
    
	public :
	
    // Conversion to a string
	std::string ToStr(bool flagShort);
	
    // Constructor
	spBox(spAnchor anchor1=spAnchor(),spMargin margin1=spMargin());
	
    // Managing the grid flag
    inline void SetGrid(bool flag) {margin.SetGrid(flag);anchor.SetGrid(flag);}
	inline bool IsGrid() {return margin.IsGrid() && anchor.IsGrid();}
    
    // Is empty ?
    inline bool IsEmpty() {return margin.IsEmpty();}
    
	//
	// Managing the associated Anchor
	//
	inline spAnchorFuncPtr & GetAnchorFuncPtr() {return anchor.anchorFuncPtr;}
	inline spRealPoint GetAnchor(spObject *object,bool flagUpdate=true,double xmin=1,double xmax=-1) {return anchor.Get(object,flagUpdate,xmin,xmax);}
	inline void UpdateAnchor(spObject *object,double xmin=1,double xmax=-1) {anchor.Update(object, xmin, xmax);}
	inline void SetAnchor(spObject *object,spRealPoint anchor1) {anchor.Set(object,anchor1);}
	inline void SetPixelTranslation() {return anchor.SetPixelTranslation();}
	
	//
	// Managing the associated Margin
	//
	inline spMarginFuncPtr & GetMarginFuncPtr() {return margin.marginFuncPtr;}
	inline bool IsPixelMargin() {return margin.IsPixel();}
	inline bool IsPixelStraightMargin() {return margin.IsPixelStraight();}
	inline void SetPixelMargin(int pm) {margin.SetPixelMargin(pm);}
	inline void UpdateMargin(spObject *object,double xmin=1,double xmax=-1) {margin.Update(object,xmin,xmax);}
    inline bool IsFuncMargin() {return margin.IsFunc();}
    inline spContainer *GetContainerMargin(spObject *obj) {return margin.GetContainer(obj);}
	

	inline bool IsMoveable() {return anchor.IsMoveable();}
	inline void SetMoveable(bool flag=true) {anchor.SetMoveable(flag);}
	inline bool IsInfinite() {return margin.IsInfinite();}
	inline bool IsNoAnchor() {return anchor.IsNone();}
    
	inline bool IsResizeable() {return margin.IsResizeable();}

    inline bool IsDefinedBy2Points() {return margin.IsDefinedBy2Points();}
    inline spRealPoint GetAnchorOposite(spObject *obj) {return margin.GetOriginOposite(obj,anchor.Get(obj,false));}
    inline void SetAnchorOposite(spObject *obj,spRealPoint pt) {margin.SetOriginOposite(obj,pt-anchor.Get(obj,false));}
    inline void SetSize(double dx, double dy) {margin.SetSize(dx,dy);}
    inline spRealPoint GetSize() {return margin.GetSize();}
    inline void SetMargin(double xMargin1, double yMargin1, double mxMargin1=0, double myMargin1=0) {return margin.Set(xMargin1,yMargin1,mxMargin1,myMargin1);}
	inline void SetAnchor(spObject *object, double x,double y) {anchor.Set(object,x,y);}
	inline void Move(spObject *object, double dx,double dy) {anchor.Move(object, dx,dy);}
	inline spRealRect GetMargin(spObject *object,bool flagUpdate=true) {return margin.Get(object,flagUpdate);}
	inline void GetMargin(double *xMargin, double *yMargin, double *mxMargin, double *myMargin) {margin.Get(xMargin,yMargin,mxMargin,myMargin);}
	
    
	//
    // Two rectangles are associated with the box :
    // a- The boundingRect rectangle whioch takes into account the pixelMargin AND the object->penSize
    // b- The rect which does not
    //
	spRect gBoundingRect(spObject *obj);
	spRect gRect(spObject *obj);
	spRealRect lRect(spObject *obj);
    
	//
	// Dealing with getting xMin,xMax,yMin,yMax of a view
	//
	// When getting xMin,xMax,yMin,yMax of a view, we loop on all the objects and call the UnionFloatRect method (with eventual xMin,xMax).
	// It returns a rectangle in local coordinates. This rectangle contains all the local coordinates points of all the spBoxes associated to the objects.
	//
	// Then, in a second pass we call the ExpandX and ExpandY methids in order to expand the so obtain rect in order to include global coordinate points.
	//
	
	// Given a rect (eventually empty if flagRectEmpty == true), expand it in order to include the local coordinates part of the spBox
	// (returns both rect and flagRectEmpty)
	void UnionFloatRect(spRealRect &rect,spObject *object,bool &flagRectEmpty);
	
	// Same as above except that it is restricted to xmin,xmax
	void UnionFloatRect(spRealRect &rect,spObject *object, bool &flagRectEmpty,double xmin, double xmax);
	
	// ExpandX --> expands the xMin and xMax in order to include the object box.
	// ExpandY --> expands the yMin and yMax in order to include the object box. If xmin,xmax are specified then restrict the x to [xmin,xmax]
	// Expand_ --> a static methods ExpandX and ExpandY are based on
	static void Expand_(double &m, double &M, bool flagReverse, double marginView2FatherView, double length, double point, double r, double l);
	//	static void Expand_(double &m, double &M, spMap1d &fatherView2Canvas,double length, double point, double plus, double minus);
	void ExpandX(spObject *object, double &xMin, double &xMax);
	void ExpandY(spObject *object, double &yMin, double &yMax,double xmin=1,double xmax=0);
	
};



#endif

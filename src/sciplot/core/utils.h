/*
 *  sputils.h
 *  SciPlot
 *
 *  Created by bacry on 22/06/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef _SP_UTILS_H_
#define _SP_UTILS_H_



#define ROUND(x) ((int) (x >= 0 ? floor(x+0.5) : ceil(x-0.5))) 
#define MAX(x,y)  ((x) > (y) ? (x) : (y))
#define MIN(x,y)  ((x) < (y) ? (x) : (y))



/////////////////////////////
//
//
// Point class 
//
// This class is used to store a point with integer coordinates
//
//
/////////////////////////////


class spRealPoint;

class SPEXPORT spPoint 
{
	public : 
	
	// Fields
	int x,y;
	
	// String representation
	std::string ToStr(bool flagShort) {
		char str[200];
		sprintf(str,"spPoint(%d,%d)",x,y);
		return (std::string) str;		
	}
	
	// Constructors
	spPoint() : x(0),y(0){}
	spPoint(int x1,int y1) : x(x1),y(y1){}
	spPoint(wxPoint pt) : x(pt.x),y(pt.y) {}
	
	// Destructor
	~spPoint() {}
	
	// spPoint --> wxPoint
	operator const wxPoint() {
		return wxPoint(x,y);
	}

	// Some useful operators
	spPoint & operator += (spPoint pt) {
		x+=pt.x;
		y+=pt.y;
		return *this;
	} 
	spPoint & operator -= (spPoint pt) {
		x-=pt.x;
		y-=pt.y;
		return *this;
	} 
	bool operator == (spPoint pt2) {
		return (x==pt2.x && y==pt2.y);
	} 
	bool operator != (spPoint pt2) {
		return (x!=pt2.x || y!=pt2.y);
	}
	spPoint operator + (spPoint pt2) {
		spPoint res(x+pt2.x,y+pt2.y);
		return res;
	}
	spPoint operator - (spPoint pt2) {
		spPoint res(x-pt2.x,y-pt2.y);
		return res;
	}
	spPoint operator * (spPoint pt2) {
		spPoint res(x*pt2.x,y*pt2.y);
		return res;
	}
	spRealPoint operator / (spRealPoint pt2);
};



/////////////////////////////
//
//
// RealPoint class 
//
// This class is used to store a point with double coordinates
//
//
/////////////////////////////


class SPEXPORT spRealPoint 
{
	public : 
	
	// Fields
	double x,y;
	
	// String representation
	std::string ToStr(bool flagShort) {
		char str[200];
		sprintf(str,"spRealPoint(%g,%g)",x,y);
		return (std::string) str;		
	}
	
	// Constructors
	spRealPoint() : x(0),y(0) {}
	spRealPoint(double x1,double y1) : x(x1),y(y1){}
	spRealPoint(wxPoint pt) : x(pt.x),y(pt.y) {}
	spRealPoint(spPoint pt) : x(pt.x),y(pt.y) {}
	
	// Destructor
	virtual ~spRealPoint() {};
	
	// spRealPoint --> wxRealPoint
	operator const wxRealPoint() {
		return wxRealPoint(x,y);
	}

    spPoint GetPointInt() {return spPoint((int) x, (int) y);}
    spPoint GetRoundPoint() {return spPoint((int) ROUND(x), (int) ROUND(y));}

    void Round() {x = ROUND(x); y = ROUND(y);}

	// Some useful operators
	bool operator == (spRealPoint pt2) {
		return (x==pt2.x && y==pt2.y);
	} 
	bool operator != (spRealPoint pt2) {
		return (x!=pt2.x || y!=pt2.y);
	}

	spRealPoint & operator += (spRealPoint pt) {
		x+=pt.x;
		y+=pt.y;
		return *this;
	} 
	spRealPoint & operator -= (spRealPoint pt) {
		x-=pt.x;
		y-=pt.y;
		return *this;
	} 	
	spRealPoint & operator /= (spRealPoint pt) {
		x/=pt.x;
		y/=pt.y;
		return *this;
	}
	spRealPoint & operator *= (spRealPoint pt) {
		x*=pt.x;
		y*=pt.y;
		return *this;
	}
	spRealPoint operator + (spRealPoint pt2) {
		spRealPoint res(x+pt2.x,y+pt2.y);
		return res;
	}
	spRealPoint operator - (spRealPoint pt2) {
		spRealPoint res(x-pt2.x,y-pt2.y);
		return res;
	}
    spRealPoint operator / (spRealPoint pt2) {
        spRealPoint res(x/pt2.x,y/pt2.y);
        return res;
    }
    spRealPoint operator * (spRealPoint pt2) {
        spRealPoint res(x*pt2.x,y*pt2.y);
        return res;
    }
    spRealPoint Rotate (double angle) {
        angle *= M_PI/180;
        return spRealPoint(x*cos(angle)-y*sin(angle),x*sin(angle)+y*cos(angle));
    }
    spRealPoint operator / (double val) {
        return spRealPoint(x/val,y/val);
    }
    spRealPoint operator + (double val) {
        return spRealPoint(x+val,y+val);
    }
    spRealPoint operator * (double val) {
        return spRealPoint(x*val,y*val);
    }
    spRealPoint operator - (double val) {
        return spRealPoint(x-val,y-val);
    }

};



/////////////////////////////
//
//
// Rect class 
//
// This class is used to store a rectangle with integer coordinates
//
//
/////////////////////////////


class SPEXPORT spRect
{
public:

	// Fields
	int x, y, width, height;
	
	// String representation
	std::string ToStr(bool flagShort) {
		char str[200];
		sprintf(str,"spRect(%d,%d,%d,%d)",x,y,width,height);
		return (std::string) str;		
	}
	
	// Constructors
	spRect(const spPoint& pt1, const spPoint& pt2) {
		wxRect r(wxPoint(pt1.x,pt1.y),wxPoint(pt2.x,pt2.y));
		x = r.x;
		y=r.y;
		width=r.width;
		height = r.height;
	};
	spRect(wxRect &r) : x(r.x),y(r.y),width(r.width),height(r.height) {}
	spRect() : x(0), y(0), width(0), height(0) {}
	spRect(int xx, int yy, int ww, int hh) : x(xx), y(yy), width(ww), height(hh) {}
    

	// Destructor
	~spRect() {};

	// spRect --> wxRect
	operator const wxRect() {
		return wxRect(x,y,width,height);
	}
	
	// Get/Set corner (top left)
	spPoint GetPosition() const {return spPoint(x, y);}
	void SetPosition( const spPoint &p ) { x = p.x; y = p.y; }

	// Get corner (bottom right)
  spPoint GetPosition1() const {return spPoint(x+width, y+height);}
	
	bool IsEmpty() const { return (width <= 0) || (height <= 0); }
	
	// operations with rect
	spRect& Inflate(int dx, int dy);
	spRect& Inflate(int d) { return Inflate(d, d); }
	spRect Inflate(int dx, int dy) const
	{
		spRect r = *this;
		r.Inflate(dx, dy);
		return r;
	}
	
	spRect& Deflate(int dx, int dy) { return Inflate(-dx, -dy); }
	spRect& Deflate(int d) { return Inflate(-d); }
	spRect Deflate(int dx, int dy) const
	{
		spRect r = *this;
		r.Deflate(dx, dy);
		return r;
	}
		
	spRect& Intersect(const spRect& rect);
	spRect Intersect(const spRect& rect) const
	{
		spRect r = *this;
		r.Intersect(rect);
		return r;
	}
	bool Intersects(const spRect& rect) const;
	
	spRect& Union(const spRect& rect);
	spRect Union(const spRect& rect) const
	{
		spRect r = *this;
		r.Union(rect);
		return r;
	}
	spRect Union(const spPoint& pt)
	{
		spRect r(pt.x,pt.y,1,1);
		return Union(r);
	}
	
	// return true if the point is (not strictly) inside the rect
	bool Contains(double x, double y) const;
	bool Contains(int x, int y) const {return Contains((double) x, (double) y);}
	bool Contains(const spPoint& pt) const { return Contains(pt.x, pt.y); }
	bool Contains(const spRealPoint& pt) const { return Contains(pt.x, pt.y); }
	
	
	// compare rectangles
	bool operator==(const spRect& rect) const {
		return rect.x==x && rect.y==y && rect.width==width && rect.height==height;
	}
	bool operator!=(const spRect& rect) const { return !(*this == rect); }

};



/////////////////////////////
//
//
// RealRect class 
//
// This class is used to store a rectangle with double coordinates
//
//
/////////////////////////////


class SPEXPORT spRealRect 
{
public:
	
	// Fields
	double x, y, width, height;

	std::string ToStr(bool flagShort) {
		char str[200];
		sprintf(str,"spRealRect(%g,%g,%g,%g)",x,y,width,height);
		return (std::string) str;		
	}

	// Constructor
	spRealRect() : x(0), y(0), width(0), height(0) { };
	spRealRect(double xx, double yy, double ww, double hh) : x(xx), y(yy), width(ww), height(hh) { };
	spRealRect(const wxRect& rect) : x(rect.x),y(rect.y),width(rect.width),height(rect.height) {};
	spRealRect(const spRect rect) : x(rect.x),y(rect.y),width(rect.width),height(rect.height) {};
	spRealRect(const spRealPoint& topLeft, const spRealPoint& bottomRight) {
		x = topLeft.x;
		y = topLeft.y;
		width = bottomRight.x-x;
		height = bottomRight.y-y;
		Standardize();
	};
	~spRealRect() {};

	// spRealRect --> spRect
	spRect Round() {		
		int xx1,yy1,xx2,yy2;
		xx1 = ROUND(x);
		yy1 = ROUND(y);
		xx2 = ROUND(x+width);
		yy2 = ROUND(y+height);
		int width1 = xx2-xx1;
		int height1 = yy2-yy1;
		if (width1 < 0) {
			xx1+=width1;
			width1 = -width1;
		}
		if (height1 < 0) {
			yy1+=height1;
			height1 = -height1;
		}		
	  return spRect(xx1,yy1,width1,height1);
	}
	
	// Standardize : make the width and height fields positive
	void Standardize() {
		if (width < 0) {
			x+=width;
			width = -width;
		}
		if (height < 0) {
			y+=height;
			height = -height;
		}		
	};
	
	// Set/Get corner methods
	spRealPoint GetPosition() const { return spRealPoint(x, y); }
	void SetPosition( const spRealPoint &p ) { x = p.x; y = p.y; }
	spRealPoint GetPosition1() const {return spRealPoint(x+width, y+height);}
	
	bool IsEmpty() const { return (width <= 0) || (height <= 0); }

	// operations with rect
	spRealRect& Inflate(double dx, double dy);
	spRealRect& Inflate(double d) { return Inflate(d, d); }
	spRealRect Inflate(double dx, double dy) const
	{
		spRealRect r = *this;
		r.Inflate(dx, dy);
		return r;
	}

	spRealRect& Deflate(double dx, double dy) { return Inflate(-dx, -dy); }
	spRealRect& Deflate(int d) { return Inflate(-d); }
	spRealRect Deflate(double dx, double dy) const
	{
		spRealRect r = *this;
		r.Deflate(dx, dy);
		return r;
	}
	
	// Intersection with another rectangle
	spRealRect& Intersect(const spRealRect& rect);
	spRealRect Intersect(const spRealRect& rect) const
	{
		spRealRect r = *this;
		r.Intersect(rect);
		return r;
	}
	bool Intersects(const spRealRect& rect) const;
	
	// Union
	spRealRect& Union(const spRealRect& rect);
	spRealRect Union(const spRealRect& rect) const
	{
		spRealRect r = *this;
		r.Union(rect);
		return r;
	}
	spRealRect Union(const spRealPoint& pt)
	{
		spRealRect r(pt.x,pt.y,0,0);
		return Union(r);
	}
	
	// return true if the point is (not strcitly) inside the rect
	bool Contains(double x1, double y1) const {return x1>=x && x1<x+width && y1>=y && y1<y+height;};
	bool Contains(const spRealPoint& pt) const { return Contains(pt.x, pt.y); }

	// compare rectangles
	bool operator==(const spRealRect& rect) const {
		return rect.x==x && rect.y==y && rect.width==width && rect.height==height;
	}
	bool operator!=(const spRealRect& rect) const { return !(*this == rect); }
			
    spRealPoint GetClosestCorner(spRealPoint pt,bool flagIncludeMidPoints = false);

};

/////////////////////////////	
//
// class spMap1d 
//
// This class implements the local 2 canvas transform for either x or y coordinates
//
// It has two possible forms
//
//    i)  linear form : ax+b
//    ii) log form : a*ln(c*x+d)/ln(base)+b
//
/////////////////////////////

class SPEXPORT spMap1d 
{
  private :
	
	double a,b,c,d,base;
	bool flagLn;
	
  public :
	
	char xORy;
	std::string ToStr(bool flagShort) {
		char str[200];
		if (!flagLn) sprintf(str,"spMap1d(%g*%c+%g)>",a,xORy,b);
		else sprintf(str,"spMap1d(%g*ln(%g*%c+%g)+%g,lnbase = %g)", a,c,xORy,d,b,base);
		return (std::string) str;		
	}

	inline double GetBase() const {return base;}
	inline double GetFactor() const {return a;}
	inline bool GetFlagLn() const {return flagLn;}
	
	//
	// Constructors of form i) 
	//
	spMap1d() : a(1),b(0),c(1),d(0),flagLn(false),xORy('x') {};                               // x 
	spMap1d(double a1,double b1) : a(a1),b(b1),c(1),d(0),flagLn(false),xORy('x') {};          // a*x+b
	spMap1d(double x1,double y1, double x2, double y2, bool flagLn1=false,double base1=10) : c(1),d(0),base(base1),flagLn(flagLn1),xORy('x') {    // This is the linear/log form such that x1->y1 and x2->y2
		if (flagLn) {
			if (x1 <= 0) {
				if (x2 <= 0) {
					if (x1 < x2) {
					  x1 = DBL_MIN*100;
					  x2 = DBL_MIN/10000;
					}
					else {
					  x2 = DBL_MIN*100;
					  x1 = DBL_MIN/10000;
					}
				}
				x1 = DBL_MIN*100;
			}
			else if (x2 <= 0) {
				x2 = DBL_MIN*100;
			}
			x1 = log(x1)/log(base);
			x2 = log(x2)/log(base);
		}
		a = (y2-y1)/(x2-x1);
		b = y1-x1*a;
	};
	
	~spMap1d() {};
	
	//
	// Form ii), one should first build form i) and then call teh following method
	//
	void SetLnScale(double base1=10,double c1=1,double d1=0) {
		c = c1;
		d = d1;
		base = base1;
		flagLn = true;
	}
		
    double GetSign() {return (a > 0 ? 1. : (a < 0 ? -1 : 1));}
    
	//
	// Apply and IApply (for inverse) methods
    
	//
	inline double Apply(double x) {  
		return (flagLn ? a*log(c*x+d)/log(base)+b : a*x+b);
	}
	inline double ApplyDer(double x) {
		return (flagLn ? a/((c*x+d)*log(base)) : a);
	}
	inline double IApply(double x) {
		return (flagLn ? (exp((x-b)*log(base)/a)-d)/c : (x-b)/a);
	}

	//
	// Composition methods
	//
	spMap1d ComposeRight(const spMap1d &tr);  // this o tr
	spMap1d ComposeLeft(const spMap1d &tr);	  // tr o this
	spMap1d operator+(const double x);

	friend ostream& operator << (ostream& o, spMap1d &t);	
};

/////////////////////////////	
//
// class spMap2d 
//
// This class implements the local 2 canvas transform for both x and y coordinates
// It mainly includes two spMap1d objects
//
/////////////////////////////	

class SPEXPORT spMap2d 
{
  public :
	
	spMap1d trX,trY;
	
	std::string ToStr(bool flagShort) {
		char str[200];
		sprintf(str,"spMap2d(%s,%s)",trX.ToStr(flagShort).c_str(),trY.ToStr(flagShort).c_str());
		return (std::string) str;		
	}

	spMap2d(const spMap1d &trx1,const spMap1d &try1) : trX(trx1),trY(try1) {trY.xORy='y';};
	spMap2d() {trY.xORy='y';};

	~spMap2d() {};

    spRealPoint GetSign() {return spRealPoint(trX.GetSign(),trY.GetSign());}

	inline spRealPoint Apply(const spRealPoint p) {return spRealPoint(trX.Apply(p.x),trY.Apply(p.y));};
	inline spRealPoint IApply(const spRealPoint p) {return spRealPoint(trX.IApply(p.x),trY.IApply(p.y));};
	inline spRealPoint IApply(const spPoint p) {return spRealPoint(trX.IApply(p.x),trY.IApply(p.y));};

	inline spRealPoint ApplyDer(const spRealPoint p) {return spRealPoint(trX.ApplyDer(p.x),trY.ApplyDer(p.y));};

	inline spRealRect Apply(const spRealRect &r) {
		spRealPoint pt1 = Apply(r.GetPosition1());
		spRealPoint pt = Apply(r.GetPosition());
		return spRealRect(pt,pt1);
	}
	inline spRealRect IApply(const spRealRect &r) {
		spRealPoint pt1 = IApply(r.GetPosition1());
		spRealPoint pt = IApply(r.GetPosition());
		return spRealRect(pt,pt1);
	}
	spRect ApplyInt(const spRealRect &r);
	spPoint ApplyInt(const spRealPoint &pt);
	
	inline spMap2d ComposeLeft(const spMap2d &tr) {
		return spMap2d(trX.ComposeLeft(tr.trX),trY.ComposeLeft(tr.trY));
	}
	inline spMap2d ComposeRight(const spMap2d &tr) {
		return spMap2d(trX.ComposeRight(tr.trX),trY.ComposeRight(tr.trY));
	}
	spMap2d operator+(const spRealPoint &pt) {
		return spMap2d(trX+pt.x,trY+pt.y);
	}
 
	
};


// extern ostream& operator << (ostream& o, wxPoint &p);
// extern ostream& operator << (ostream& o, spRealPoint &p);
extern ostream& operator << (ostream& o, spRealRect &r);
extern ostream& operator << (ostream& o, spRect &r);
extern ostream& operator << (ostream& o, spMap1d &t);
extern ostream& operator << (ostream& o, spMap2d &t);


#define SP_PEN_FIELD(command1,command2) \
wxPen pen; \
void SetPen(const wxColour &colour, int width = 1, int style = wxSOLID) { \
	wxPen *penPtr = wxThePenList->FindOrCreatePen(colour,width,style); \
    command1; \
	pen = *penPtr; \
    command2; \
}; \
void SetPen(const wxString& colourName, int width = 1, int style = wxSOLID) { \
	wxPen *penPtr = wxThePenList->FindOrCreatePen(colourName,width,style); \
command1; \
	pen = *penPtr; \
  command2; \
}; \
void SetPen(const wxBitmap& stipple, int width = 1) { \
command1; \
	pen = wxPen(stipple,width); \
  command2; \
}; \
void SetPen(const wxPen pen1) { \
command1; \
	pen = pen1; \
  command2; \
}; \
wxPen &GetDefaultPen() { \
	return pen; \
}

#define SP_BRUSH_FIELD(nothing) \
wxBrush brush; \
void SetBrush(const wxColour &colour, int style = wxSOLID) { \
	wxBrush *brushPtr = wxTheBrushList->FindOrCreateBrush(colour,style); \
	brush = *brushPtr; \
}; \
void SetBrush(const wxString& colourName, int style = wxSOLID) { \
	wxBrush *brushPtr = wxTheBrushList->FindOrCreateBrush(colourName,style); \
	brush = *brushPtr; \
}; \
void SetBrush(const wxBitmap& stipple) { \
	brush = wxBrush(stipple); \
}; \
void SetBrush(const wxBrush brush1) { \
	brush = brush1; \
}; \
wxBrush &GetDefaultBrush() { \
	return brush; \
}


#define SP_FONT_FIELD(command) \
wxFont font; \
void SetDefaultFont(int pointSize, wxFontFamily family=wxFONTFAMILY_DEFAULT, int style=wxFONTSTYLE_NORMAL, wxFontWeight weight=wxFONTWEIGHT_NORMAL, const bool underline = false, const wxString& faceName = "", wxFontEncoding encoding = wxFONTENCODING_DEFAULT) { \
  wxFont *fontPtr = wxTheFontList->FindOrCreateFont(pointSize,family,style,weight,underline,faceName,encoding); \
font = *fontPtr; \
  command; \
} \
void SetDefaultFont(const wxFont font1) { \
	font = font1; \
} \
wxFont &GetDefaultFont() { \
return font; \
}


#endif

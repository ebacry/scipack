

#include "sp.h"


bool spInteractor::CheckObjectIsValid(spObject *o)
{
  if (!OnlyOnViews() && !OnlyOnContainers()) return(true);
	if (OnlyOnViews() && o->IsView()) return(true);
	if (OnlyOnContainers() && o->IsContainer()) return(true);
    spWarningf("spInteractor::CheckObjectIsValid","Interactor of type '%s' cannot be added to object of type '%s'",GetClassName(),(*o).GetClassName());
    return false;
}


spInteractor::spInteractor(spObject *o,const char *name) : spRef(false,name)
{		
	flagOn = true;
	if (o) o->AddInteractor(this);	
}

spInteractorCallback& spInteractorCallback::operator =(const spInteractorCallback& other)
{
	getFunc = other.getFunc;
#ifdef SP_SCRIPT
	if (other.scriptFunc) SCRIPT_INCREF(other.scriptFunc);
	if (scriptFunc) SCRIPT_DECREF(scriptFunc);
	scriptFunc = other.scriptFunc;
#endif	
    return *this;
}

spInteractorCallback::spInteractorCallback(void (*callback1)(spInteractor *,spEvent &))
{
	getFunc = callback1;
#ifdef SP_SCRIPT
	scriptFunc = NULL;
#endif
}

void spInteractorCallback::Call(spInteractor *i, spEvent &e) 
{
	if (!IsSet()) return;
	
#ifdef SP_SCRIPT
	if (scriptFunc != NULL) {
		void (*func)(spInteractorCallback *, spInteractor *,spEvent &) = (void (*)(spInteractorCallback *, spInteractor *,spEvent &)) getFunc;
        func(this,i,e);
		return;
	}
#endif
	
	getFunc(i,e);
}

spInteractorCallback::~spInteractorCallback()
{
#ifdef SP_SCRIPT
	if (scriptFunc != NULL) {
		SCRIPT_DECREF(scriptFunc);
		scriptFunc = NULL;
	}
#endif
}

spInteractorCallback::spInteractorCallback(const spInteractorCallback & icb)
{
	getFunc = icb.getFunc;
#ifdef SP_SCRIPT
	scriptFunc = icb.scriptFunc;
	if (scriptFunc != NULL) {
		SCRIPT_INCREF(scriptFunc);
	}		
#endif
}



spInteractor1::~spInteractor1()
{
}

spInteractor1::spInteractor1(spInteractorCallback cb,spEventType eventType, spObject *o, bool flagPropagate1,const char *name) : spInteractor(o,name)
{
	flagPropagate = flagPropagate1;
	type = eventType.type;
	callback = cb;
	modifiers = eventType.modifiers;
}

spInteractor1::spInteractor1(void (*cb)(spInteractor *,spEvent &),spObject *o,int e, unsigned long modifiers1,bool flagPropagate1,const char *name) : spInteractor(o,name)
{		
	flagPropagate = flagPropagate1;
	type = e;
	callback = spInteractorCallback(cb);
	modifiers = modifiers1;
}


spInteractor1::spInteractor1(void (*cb)(spInteractor *,spEvent &),int e, unsigned long modifiers1,bool flagPropagate1,const char *name) : spInteractor(NULL,name)
{
	flagPropagate = flagPropagate1;
	type = e;
	callback = spInteractorCallback(cb);
	modifiers = modifiers1;	
}

spInteractor1::spInteractor1(int e, unsigned long modifiers1,bool flagPropagate1,const char *name) : spInteractor(NULL,name)
{
	flagPropagate = flagPropagate1;
	type = e;
	modifiers = modifiers1;		
}

void spInteractor1::SetCallback(spInteractorCallback cb)
{
	callback = cb;
}


bool spInteractor1::Process(spInteractor *interactor, spEvent &event) {
	if (!IsOn()) return false;
    if (event.type == spOnMsge) {
        if (type != spOnMsge || modifiers != event.modifiers) return false;
        ExecuteCallback(interactor,event);
        return true;
    }
	if (!(type == spOnAnyMotion && (event.type == spOnDragLeft || event.type == spOnDragRight || event.type == spOnDragMiddle || event.type == spOnMotionLeft || event.type == spOnMotionRight || event.type == spOnMotionMiddle || event.type == spOnMotion))) {		
	  if (event.type != type) return false;
		if (event.type == spOnKeyDown) {
			if (event.keycode != modifiers && modifiers != 0) return false;
		}
	  else if (event.modifiers != modifiers) return false;
	}
	spEvent::Propagate();
	ExecuteCallback(interactor,event);
	return true;
};


spInteractorN::spInteractorN(spObject *o,const char *name) : spInteractor(o,name)
{
}

void spInteractorN::Add(spInteractor *cb1)
{
	interactorList.Add(cb1);
	cb1->Ref();
}

bool spInteractorN::Process(spInteractor *interactor, spEvent &event) 
{
	if (!IsOn()) return false;
	
	bool res = false;
	for (int i=0;i<interactorList.Size();i++)
	{
		if (!interactorList[i]->IsOn()) continue;
		res |= interactorList[i]->Process(this,event);
	}
	return res;
}

spInteractorN::~spInteractorN()
{
	while (interactorList.Size()!= 0) {
		spInteractor *i = interactorList[0];
	  interactorList.Remove(i);
		i->Unref();
	}
}

//
// Debug CallBack
//
static void CBDebug(spInteractor *cb,spEvent &event) 
{
  cout << "CBDebug :  Object(" << event.object << ") " << event << " " << endl;	
}

spInteractorDebug::spInteractorDebug(spEventType e, bool flagPropagate1,const char *name) : spInteractor1(CBDebug,e.type,e.modifiers,flagPropagate1,name)
{
}




/*
 *  debug.cpp
 *  sp
 *
 *  Created by bacry on 17/11/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


#include "sp.h"
#include <stdarg.h>

unsigned long spDebug = spDebugNothing;

static char tempStr[10000];

void spSetDebug(unsigned long debugFlag)
{
	spDebug = debugFlag;
}


void spDebugPrint(char *str)
{
    cout << str << endl << std::flush;
}

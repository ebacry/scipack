

#include "sp.h"


//**********************************************************
//
// A 3-button Zoom interactor to be attached to a view
//
//**********************************************************

//
// Left Button CallBack : take out the part which is on the left of the mouse
//
static void CBZoomLeft(spInteractor *cb,spEvent &event) 
{
	spView *v =(spView *) (event.object);
	spRealRect r = v->GetXYBound();
  double xmin,xmax;
	
	if (v->IsReversedX()) {
		xmax = (v->l2top.IApply(event.mouse)).x;		
		xmin = r.x;
	}
	else {		
	  xmin = (v->l2top.IApply(event.mouse)).x;	
		xmax = r.x+r.width;
	}
	r = v->ComputeMinXYBound(xmin,xmax);
	v->SetXYBound(r);		
}

//
// Right Button CallBack : take out the part which is on the right of the mouse
//
static void CBZoomRight(spInteractor *cb,spEvent &event) 
{
	spView *v =(spView *) (event.object);
	spRealRect r = v->GetXYBound();
  double xmin,xmax;
	
	if (!v->IsReversedX()) {
		xmax = (v->l2top.IApply(event.mouse)).x;		
		xmin = r.x;
	}
	else {		
	  xmin = (v->l2top.IApply(event.mouse)).x;	
		xmax = r.x+r.width;
	}
	r = v->ComputeMinXYBound(xmin,xmax);
	v->SetXYBound(r);		
}


//
// Unzoom
//
static void CBUnZoom(spInteractor *cb,spEvent &event) 
{
	spView *v =(spView *) (event.object);
	spRealRect r = v->ComputeMinXYBound();
	v->SetXYBound(r);
}


//
// The 3 button interactor
//
spInteractorZoom3View::spInteractorZoom3View(const char *name) : spInteractorN(NULL,name)
{
    if (name == NULL) SetName(_CCHAR("Zoom Left/right"));
    
	Add(left=new spInteractor1(CBZoomLeft,spOnDownLeft));
	Add(right=new spInteractor1(CBZoomRight,spOnDownRight));
	Add(unzoom=new spInteractor1(CBUnZoom,spOnKeyDown,' '));
}



//**********************************************************
//
// A Wheel mouse Zoom interactor to be attached to a view
//
//**********************************************************


void CBZoomMouseX(spInteractor *cb, spEvent &event)
{
	if (!event.object->IsView()) return;	
	spView *v = (spView *) event.object;

	v->ZoomX(event.mouse,event.wheel);
	SPUpdate();
}

void CBZoomMouseY(spInteractor *cb, spEvent &event)
{
	if (!event.object->IsView()) return;	
	spView *v = (spView *) event.object;
	
	v->ZoomY(event.mouse,event.wheel); 
	SPUpdate();
}

void CBZoomMouseXY(spInteractor *cb, spEvent &event)
{
	if (!event.object->IsView()) return;	
	spView *v = (spView *) event.object;
	
	v->Zoom(event.mouse,event.wheel); 
	SPUpdate();
}

spInteractorZoomWheelView::spInteractorZoomWheelView(const char *name) : spInteractorN(NULL,name)
{
    if (name == NULL) SetName(_CCHAR("Zoom Wheel"));

	Add(mousex=new spInteractor1(CBZoomMouseX,spOnMouseWheel,SP_MOD_SHIFT));
	Add(mousey=new spInteractor1(CBZoomMouseY,spOnMouseWheel,SP_MOD_ALT));
	Add(mousexy=new spInteractor1(CBZoomMouseXY,spOnMouseWheel,SP_MOD_SHIFT | SP_MOD_ALT));
}



//**********************************************************
//
// Zoom using selection with a rectangle
//
//**********************************************************


void CBZoomSelectUpView(spInteractor *cb, spEvent &event)
{
	spInteractorSelectView *i = (spInteractorSelectView *) cb;
	spView *view = ((spView *) (event.object));
	
	if (i->flagYConstrained) {
		double xMin = i->rectSelection.x;
		double xMax = xMin+i->rectSelection.width;
		spRealRect r = view->ComputeMinXYBound(xMin, xMax);
		view->SetXYBound(r);
	}
	else view->SetXYBound(i->rectSelection);
}

spInteractorZoomSelectView::spInteractorZoomSelectView(char constrained, bool flagPropagate,const char *name) : spInteractorSelectView(constrained,flagPropagate,CBZoomSelectUpView,name)
{
    if (name == NULL)
        SetName(_CCHAR(flagYConstrained ? _CCHAR("Zoom Select-Y") : (flagXConstrained ? _CCHAR("Zoom Select-X") :  _CCHAR("Zoom Select-XY"))));

	Add(unzoom=new spInteractor1(CBUnZoom,spOnKeyDown,' '));
}





static void CBSynchroZoom (spInteractor *i,spEvent &e)
{
	spView *me = (spView *) e.object;
	
	spRealRect r = me->GetXYBound();
	spInteractorSynchroZoom *inter = (spInteractorSynchroZoom *) i;
	switch (inter->mode) {
			
		case spSynchroZoomXY : 
			for (int i= 0; i< inter->set.Size();i++) {
				if (me == inter->set[i]) continue;
				((spView *) inter->set[i])->SetXYBound(r);	 	
			}
			break;

		case spSynchroZoomX :
			for (int i= 0; i< inter->set.Size();i++) {
				if (me == inter->set[i]) continue;
				spRealRect r1 = ((spView *) inter->set[i])->ComputeMinXYBound(r.GetPosition().x,r.GetPosition1().x);	 	
				((spView *) inter->set[i])->SetXYBound(r1);	 	
			}
			break;

		default:;
			
	}
	SPUpdate();	
}

static void CBSynchroComputeBound (spInteractor *i,spEvent &e)
{
	spView *me = (spView *) e.object;
	spInteractorSynchroZoom *inter = (spInteractorSynchroZoom *) i;
	
	
	spRealRect r(spRealPoint(e.xMin,e.yMin),spRealPoint(e.xMax,e.yMax));
	
	for (int i= 0; i< inter->set.Size();i++) {
		if (me == inter->set[i]) continue;
		spRealRect r1;
		if (e.flagXFixed) r1 =  ((spView *) inter->set[i])->ComputeMinXYBound(e.xMin,e.xMax);	
		else r1 =  ((spView *) inter->set[i])->ComputeMinXYBound();	
		r.Union(r1);
	}
	e.xMin = r.GetPosition().x;
	e.xMax = r.GetPosition1().x;
	if (inter->mode == spSynchroZoomX) return;
	e.yMin = r.GetPosition().y;
	e.yMax = r.GetPosition1().y;	
}


spInteractorSynchroZoom::spInteractorSynchroZoom(spSynchroZoomMode mode1, spView *view,const char *name) : spInteractorN(NULL,name)
{
	mode = mode1;
	AddView(view);
	Add(new spInteractor1(CBSynchroZoom, spOnChangeBound));
	Add(new spInteractor1(CBSynchroComputeBound, spOnComputeBound));
}

spInteractorSynchroZoom::~spInteractorSynchroZoom()
{
	for (int i= 0; i< set.Size();i++) {
		set[i]->RemoveInteractor(this);
	}
}

void spInteractorSynchroZoom::AddView(spView *view) 
{
	if (view) {
	  set.AddObject(view);
	  view->AddInteractor(this);
	}
}




"""This module allows to solve Fredholm integral equations of the form ::

    F_k(x) = \int_xmin^xmax K_kl(x,s)F_l(s)ds + G_k(x)
"""

__author__ = "Emmanuel Bacry"
__copyright__ = "Copyright (C) 2014 Emmanuel Bacry"
__version__ = "0.131"

import scipack.lab.sig as sig
from scipy.linalg import solve
import numpy as np


class FredholmSolver(object):
    """
    The base class for solving Fredholm integral equations of the form ::
        
        F_k(x) = \int_xmin^xmax K_kl(x,s)F_l(s)ds + G_k(x)
    
    where ``F_k`` are the unknown functions
    
    The arguments of the constructor are :
    
    - ``dimension`` : the number of unknwown functions ``F_k``
    - ``G``: a callable coding the functions ``G_k`` in the above equations.
    It should have the form ``G(k,t)`` where k is in between ``0`` and ``dimension-1``
    - ``K``: a callable coding the functions ``K_kl`` in the above equations.
    It should have the form ``K(k,t,l,s)`` where k and l are in between ``0`` and ``dimension-1``
    
    To solve it, use the ``compute`` method.
    """

    def __init__(self,dimension,G,K):
        
        object.__init__(self)
        self._dimension = dimension
        self._G = G
        self._K = K

        self._n_quad = 0
        self._xmin = 0
        self._xmax = 1

    def compute_norm(self,xmin=-1,xmax=-1):
        """Computes the L1 norm of the solution.
            Before calling this method, one should call the ``compute`` method.
            
            - ``xmin``, ``xmax`` : the L1 norm is computed supposing the solution is compact support between ``[xmin,xmax]''
            """
        
        if (xmin != -1):
            imin = sig.where(self.quad_x>=xmin)
            imin = imin[0]
        else:
            imin = 0
        if (xmax != -1):
            imax = sig.where(self.quad_x<=xmax)
            imax = imax[-1]+1
        else:
            imax = self.quad_x.size
        
        norm = sig.Signal(self._dimension)
        for i in range(0,self._dimension):
            norm[i] = sum(self.solution_on_quad_points[i][imin:imax]*self.quad_w[imin:imax])

        return norm

    def compute(self,n_quad,xmin=0,xmax=40,dx = -1,x=None):
        """Computes solution of Fredholm system.
            
            - ``n_quad`` : the number of quadrature points between ``[xmin,xmax]`` used for solving the system
            - ``xmin`` and ``xmax`` : the solution is estimated on ``[xmin,xmax]``
            - ``dx`` : if <0 then the solution is only estimated on the quadrature points.
            if >0 then it is estimated on a uniform grid of size ``dx`` on ``[xmin,xmax]``
            - ``x`` : if specified it should be a ``Signal``, then the estimation is performed on its abscissa
            
            Returns the solution in a list of ``Signal``'s form.
            After running this function, you can access the following fields :
            
            - ``self.solution`` : the solutions (list of ``Signal``'s)
            - ``self.norm`` : the L1 norm of the solutions (a ``Signal`` object)
            - ``self.quad_x`` : the abscissa used for quadrature (i.e., the abscissa at which the solutions have been estimated)
            - ``self.quad_w`` : the quadrature weights (same size as ``self.quad_x``)
            """
        
        # Should we need to recompute the estimation on new quadrature points ?
        if (self._n_quad != n_quad or self._xmax != xmax or self._xmin != xmin):
            
            self._n_quad = n_quad
            self._xmin = xmin
            self._xmax = xmax


            quad_x,quad_w = sig.GQuadrature(self._xmin,self._xmax,self._n_quad)
            self.quad_x = quad_x
            self.quad_w = quad_w
        
            # We need to solve a dimension*n_samples system
            # It is ordered in the following way v_i(j) --> v(i*n+j)
            M = sig.mat0(self._dimension*self._n_quad)
            v = np.zeros(shape=(self._n_quad*self._dimension,1))
    
            # We must build the system.
            for k in range(0,self._dimension):
                for i in range(0,self._n_quad):  # corresponds to t
                    index1 = k*self._n_quad+i    # corresponds to the couple (k,t)
                    v[index1] = - self._G(k,quad_x[i])
                    for l in range(0,self._dimension):
                        for j in range(0,self._n_quad): # corresponds to s
                            index2 = l*self._n_quad+j   # corresponds to the couple (l,s)
                            M[index1,index2] = quad_w[j]*self._K(k,quad_x[i],l,quad_x[j])-(index1==index2)
    
            # Then we solve it
            res = solve(M,v)
        
            # We rearrange the vector
            res1 = []
            for k in range(0,self._dimension):
                y = res[k*self._n_quad:(k+1)*self._n_quad][:,0]
                res1.append(sig.XY[quad_x,y])

            self.solution_on_quad_points = res1
            self.solution = res1
    
            # Recompute L1 norm
            self.norm = self.compute_norm()
                
        # If dx is specified or x is specified the kernel must be interpolated on the given abscissa
        if (dx > 0 or x is not None):
            raise ValueError,"Not implemented yet"
            if (dx > 0): x = sig.Y[xmin:xmax:dx]
            for i in range(0,self._dimension):
                self.solution[i] = sig.Signal(x.size)
                for j in range(0,x.size):
                    xx = x[j]
                    self.solution[i][j] = self._G(i,xx)
                    self.solution[i][j] += self._G(i,xx)
            

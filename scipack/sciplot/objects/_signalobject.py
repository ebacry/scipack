__author__ = "Emmanuel Bacry"
__copyright__ = "Copyright (C) 2014 Emmanuel Bacry"
__version__ = "0.131"

import scipack.sciplot.sp as sp

#
# Routine to add dynamically a method to a class
#
def _funcToMethod(func,klass,method_name=None):
    import new
    method = new.instancemethod(func,None,klass)
    if not method_name: method_name=func.__name__
    setattr(klass,method_name,method)

def _sigcall(self,**kwargs):
    """Used in the scipack.sciplot.disp module for specifying attributes in the disp command"""
    kwargs['_signal'] = self
    return kwargs

def _sigobj(self,*args,**kwargs):
    """Used in the scipack.sciplot.objects.spsig package to act on displayed Signal"""
    sigobjs = sp.oget(klass=SignalObject)
    for obj in sigobjs:
        if (obj.IsSignal(self)):
            return obj(*args,**kwargs)

_funcToMethod(_sigcall,scipack.lab.sig.Signal,"__call__")
_funcToMethod(_sigobj,scipack.lab.sig.Signal,"obj")


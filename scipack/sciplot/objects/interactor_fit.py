__author__ = "Emmanuel Bacry"
__copyright__ = "Copyright (C) 2014 Emmanuel Bacry"
__version__ = "0.131"


import numpy as np
import scipack.sciplot.sp as sp
import spsig
from scipack.lab.sig import Y,XY,log10
import sys

#
# A scroll interactor with a key
#
class IFitKey(sp.Interactor1):
    """An interactor for performing linear fit of a signal displayed in a view. Only works with sp.View."""
    def __init__(self,name = "fit",key= 'f'):
        sp.Interactor1.__init__(self,cb = IFitKey._CBFit,name=name,eventType=sp.EventType(sp.OnKeyDown,ord(key)))
    
    def CheckObjectIsValid(self,object):
        if (object.IsView()): return True
        sys.stderr.write("Non valid spObject %s for interactor of class %s" %(object,self))
        return False
    
    # The key callback
    def _CBFit(self,event):
        view = event.object
        
        f=view.oget1(':_fit')
        if (f is not None):
            f.Hide()
            view.RemoveObject(f)
            return

        obj = view.FindObjectMouse(event.mouse)

        if (obj is None): return
        if (not isinstance(obj,spsig.SignalObject)): return
        
        x0,x1,y0,y1 = view.bound

        sig = obj.GetSignal()
        sig = sig['x',x0:x1]
        
        if (view.logx or view.logy):
            x = sig.Xarray()
            y = sig.Yarray()
            if (view.logx): x = log10(x)
            if (view.logy): y = log10(y)
            sig = XY[x,y]
        
        r=sig.lstsq()
        a,b=r[0]
        print "%g*X+%g" % (a,b)
        sys.stdout.flush()
    
        N = 10000
        x = slice(x0,x1,N*1j)
        x = Y[x]
        if (view.logx): y = a*log10(x)+b
        else: y = a*x+b
        if (view.logy): y = pow(10,y)
        fit = XY[x,y]
        
        spsig.SignalObject(view,fit,name="_fit").config(pen='blue')
    #        sp.Shape(view,sp.SHAPE_LINE,anchor=sp.Anchor(x0,y0),margin=sp.Margin(x1-x0,y1-y0),name="_fit").config(pen='blue')
        sp.Update()

__author__ = "Emmanuel Bacry"
__copyright__ = "Copyright (C) 2014 Emmanuel Bacry"
__version__ = "0.131"


import scipack.sciplot.sp as sp
from selector import IInteractorSelector



# A zoom selector class
class ICursorSelector(IInteractorSelector):
    """An interactor for managing the cursor. Only work with sp.View."""
    
    def __init__(self,eventType = 'c',name="cursor_selector"):
        IInteractorSelector.__init__(self,eventType = eventType,name=name)
        sp.ICursorView(cursor=sp.CursorCrossHair(),name="Sticky Cursor").config(followMouse=False).AddTo(self)
        sp.ICursorView(cursor=sp.CursorCrossHair(),name="Regular Cursor").config(followMouse=True).AddTo(self)
        sp.ICursorView(cursor=None,name="No Cursor").config(followMouse=True).AddTo(self)
    
    def CheckObjectIsValid(self,object):
        if (object.IsView()): return True
        sys.stderr.write("Non valid spObject %s for interactor of class %s" %(object,self))
        return False

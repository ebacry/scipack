__author__ = "Emmanuel Bacry"
__copyright__ = "Copyright (C) 2014 Emmanuel Bacry"
__version__ = "0.131"



import wx
import scipack.sciplot.sp as sp

#
# An interactor for displaying an overlay object for a certain period of time
#
class IDisplayTempObject(sp.InteractorN):
    """An interactor for displaying an overlay object for a certain period of time.
        When triggered a callback is called. This interactor must be attached to a sp.Container."""
    def __init__(self, object, name = "DisplayTempObject",eventType = 'z',callback=None):
        sp.InteractorN.__init__(self,name=name)
        self._idisplay = sp.Interactor1(cb=IDisplayTempObject._CBDisplay,eventType=eventType,name="IDisplayTempObject:display").AddTo(self)
        self._ierase = sp.Interactor1(cb=IDisplayTempObject._CBErase,eventType=sp.OnTime,name="IDisplayTempObject:erase").AddTo(self)
        self.delay = 600
        self.callback = callback
        self.position = "mouse" # might be "mouse" or "unchanged"
        self.mouseOffset = sp.Point(10,10)
        self._object = object
        self._timer = sp.Timer()
    
        if (not object.IsOverlay()): raise TypeError, "The object must be an overlay object. Use sp.FLAG_OBJECT_OVERLAY in its constructor."

    #
    # The eventType property
    #
    def _GetEventType(self): return self._idisplay.eventType
    def _SetEventType(self,eventType):
        """self.eventType = spEventType(type,key | modifier)"""
        self._idisplay.eventType = eventType
    eventType=sp.attribute(_GetEventType, _SetEventType)

    #
    # The delay property
    #
    def _GetDelay(self): return self._delay
    def _SetDelay(self,delay):
        """self.delay = delay before erasing the object (in ms)"""
        self._delay = delay
    delay=sp.attribute(_GetDelay, _SetDelay)

    #
    # The position property
    #
    def _GetPosition(self): return self._position
    def _SetPosition(self,position):
        """self.position = either 'mouse' or 'center'
            
            The position of the object is either the mouse position
            or the center of the interactor's object"""
        if (position != "mouse" and position != "center"): raise TypeError, "Attribute position must be one of 'mouse' or 'center'"
        self._position = position
    position=sp.attribute(_GetPosition, _SetPosition)

    #
    # The callback property
    #
    def _GetCallback(self): return self._callback
    def _SetCallback(self,callback):
        """self.callback = a simple function with no argument
            
            The callback that is called when interactor is triggered"""
        self._callback = callback
    callback=sp.attribute(_GetCallback, _SetCallback)

    #
    # The mouseOffset property
    #
    def _GetMouseOffset(self): return self._mouseOffset
    def _SetMouseOffset(self,offset):
        """self.mouseOffset = spPoint(xOffset,yOffset)
            
            If self.position=='mouse', then it corresponds to an offset"""
        self._mouseOffset = offset
    mouseOffset=sp.attribute(_GetMouseOffset, _SetMouseOffset)

    
    # The callback for showing the object
    def _CBDisplay(self,event):
        
        # Stop former timer if needed
        self._timer.Stop()

        # We need to erase the object if needed
        self._object.container = None
        
        # Then we make the top container to be the father container of the object
        self._object.container = event.object.container.topcontainer
        
        # We enable the display of the object as an overlay object
        self._object.ActivateOverlay()
            
        # We set the position of the object
        if (self.position == "mouse"):
            self._object.pos = event.mouse+self.mouseOffset
        if (self.position == "center"):
            grect = event.object.gRect()
            self._object.pos = sp.RealPoint(grect.x+grect.width/2,grect.y+grect.height/2)
        
        # Run the timer
        self._timer.Start(event.object,self.delay,True)
    
        # And call the cbAction
        if (self.callback is not None): self.callback()

    # The callback to erase the object
    def _CBErase(self,event):

        if (not event.IsTimerEvent(self._timer)): return
        self._object.container = None
        self._timer.Stop()

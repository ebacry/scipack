__author__ = "Emmanuel Bacry"
__copyright__ = "Copyright (C) 2014 Emmanuel Bacry"
__version__ = "0.131"


import wx

import scipack.sciplot.sp as sp


#
# A scroll interactor
#
class IScrollViewWheel(sp.Interactor1):
    """An interactor for scrolling a view using the mousewheel"""
    def __init__(self,name = "scroll_wheel",direction = 'x'):
        sp.Interactor1.__init__(self,cb = IScrollViewWheel._CBScroll,name=name,eventType=sp.OnMouseWheel)
        self.direction = direction
        self.fixedYScale = False
    
    def CheckObjectIsValid(self,object):
        if (object.IsView()): return True
        sys.stderr.write("Non valid spObject %s for interactor of class %s" %(object,self))
        return False

    # direction attribute
    def _GetDirection(self): return self._direction
    def _SetDirection(self,val):
        """self.direction = 'x' or 'y'
            
            Direction of the scroll is either the 'x' axis or the 'y' axis."""
        if (val != 'x' and val != 'y'):
            raise(TypeError("Direction is either 'x' or 'y'"))
        self._direction = val
    direction=sp.attribute(_GetDirection,_SetDirection)

    # adapt y scale attribute
    def _GetFixedYScale(self): return self._fixedYScale
    def _SetFixedYScale(self,val):
        """self.fixedYScale = True or False (default is True)
            
            When self.direction is 'x' then if this attribute is set the y-scale does not change when scrolling."""
        if (val != True and val != False):
            raise(TypeError("fixedYScale is True or False"))
        self._fixedYScale = val
    fixedYScale=sp.attribute(_GetFixedYScale,_SetFixedYScale)

    
    # The MouseWheel callback
    def _CBScroll(self,event):
        
        if (self._direction == 'y'):
            event.object.ScrollY(event.wheel)
        else:
            event.object.ScrollX(event.wheel,not self.fixedYScale)


sp.attribute_hide(IScrollViewWheel,'eventType')



#
# A scroll interactor with a key
#
class IScrollViewKey(sp.Interactor1):
    """An interactor for scrolling a view in one direction using a key"""
    def __init__(self,name = "scroll",direction = 'x+',key= wx.WXK_RIGHT, speed = 5):
        sp.Interactor1.__init__(self,cb = IScrollViewKey._CBScroll,name=name,eventType=sp.EventType(sp.OnKeyDown,key))
        self.direction = direction
        self.speed = speed
    
    def CheckObjectIsValid(self,object):
        if (object.IsView()): return True
        sys.stderr.write("Non valid spObject %s for interactor of class %s" %(object,self))
        return False

    # Direction attribute
    def _GetDirection(self): return self._direction
    def _SetDirection(self,val):
        """self.direction = 'x+' or 'x-' or 'y+' or 'y-'
            
            Direction of the scroll
            """
        if (val != 'x+' and val != 'y+' and val != 'x-' and val != 'y-'):
            print "Direction is either 'x+', 'x-', 'y+' or 'y'"
            raise(TypeError)
        self._direction = val
    direction=sp.attribute(_GetDirection,_SetDirection)
    
    # speed attribute
    def _GetSpeed(self): return self._speed
    def _SetSpeed(self,val):
        """self.speed = integer
            
            Speed of the scroll (5 is a 'good value')
            """
        self._speed = val
    speed=sp.attribute(_GetSpeed,_SetSpeed)
    
            
    # The MouseWheel callback
    def _CBScroll(self,event):
            
        if (self._direction == 'y+'):
            event.object.ScrollY(self.speed)
        elif (self._direction == 'y-'):
            event.object.ScrollY(-self.speed)
        elif (self._direction == 'x-'):
            event.object.ScrollX(-self.speed,True)
        else:
            event.object.ScrollX(self.speed,True)

                

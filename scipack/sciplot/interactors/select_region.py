__author__ = "Emmanuel Bacry"
__copyright__ = "Copyright (C) 2014 Emmanuel Bacry"
__version__ = "0.131"


import wx
import math
import scipack.sciplot.sp as sp

#
# An interactor for selecting a region in a containe
#
class ISelectRegion(sp.InteractorN):
    """An interactor for selecting a rectangle in a view and trigger a callback."""
    def __init__(self, name = "SelectRegion",constrained='y',button=sp.OnDownLeft,flagPropagate=False):
        sp.InteractorN.__init__(self,name=name)
        self._imouseDown = sp.Interactor1(cb=ISelectRegion._CBDown,eventType=sp.OnDownLeft,name="ISelectRegion:down").AddTo(self)
        self._imouseUp = sp.Interactor1(cb=ISelectRegion._CBUp,eventType=sp.OnUpLeft,name="ISelectRegion:up").AddTo(self)
        self._imouseDrag = sp.Interactor1(cb=ISelectRegion._CBMotion,eventType=sp.OnDragLeft,name="ISelectRegion:drag").AddTo(self)
        self._itimer = sp.Interactor1(cb=ISelectRegion._CBTime,eventType=sp.OnTime,name="ISelectRegion:timer").AddTo(self)
        
        self.constrained = constrained
        self.brush = None
        self.pen = None
        self.callback = None
        self._timer = sp.Timer()

    
    #
    # The button property
    #
    def _GetButton(self):
        return self._imouseDown.eventType
    def _SetButton(self,buttonEventType):
        """self.button = spOnDownLeft | spOnDownRight | spOnDownMiddle
            
            The button to use for selecting the rectangle"""
        eventType = sp.EventType(buttonEventType)
        if (not eventType.IsButtonDown()):
            print "I am expecting a Button Down eventType"
            raise(TypeError)
        self._imouseDown.eventType = eventType
        self._imouseUp.eventType = eventType.DownToUp()
        self._imouseDrag.eventType = eventType.DownToDrag()
    button = sp.attribute(_GetButton,_SetButton)
    
    #
    # The constrained property
    #
    def _GetConstrained(self): return self._constrained
    def _SetConstrained(self,constrainType):
        """self.constrained = either 'x', 'y' or ''
            
            if 'x' then the whole abscissa range is always selected
            if 'y' then the whole ordinate range is always selected
            if '' then there is no constraint
        """
        if (constrainType != 'x' and constrainType != 'y' and constrainType != ''):
            print "I am expecting either 'x', 'y' or ''"
            raise(TypeError)
        self._constrained = constrainType
    constrained = sp.attribute(_GetConstrained,_SetConstrained)
    
    #
    # The brush property
    #
    def _GetBrush(self): return self._brush
    def _SetBrush(self,brush):
        """self.brush = brush
            
            The brush to use to draw the rectangle"""
        self._brush = brush
    brush = sp.attribute(_GetBrush,_SetBrush)
    
    #
    # The pen property
    #
    def _GetPen(self): return self._pen
    def _SetPen(self,pen):
        """self.pen = pen
            
            The pen to use to draw the rectangle"""
        self._pen = pen
    pen = sp.attribute(_GetPen,_SetPen)

    #
    # The callback property
    #
    def _GetCallback(self): return self._callback
    def _SetCallback(self,callback):
        """self.callback = a callable
            
            either None or a callable with two arguments :  f(view,selectedRectangle)
            This will be called when mouse button is released
        """
        self._callback = callback
    callback = sp.attribute(_GetCallback,_SetCallback)

    
    #
    # Valid only for views only
    #
    def CheckObjectIsValid(self,object):
        if (object.IsView()): return True
        sys.stderr.write("Non valid spObject %s for interactor of class %s" %(object,self))
        return False
    
    # The MouseDown callback
    def _CBDown(self,event):
        
        # The view and the corresponding rect
        self._view = event.object
        self._bview = self._view.gRect()

        # The final selection (in local coordinates)
        self.rectSelection = sp.RealRect()
        
        # Get the mouse (constrained point)
        pt = event.mouse
        if (self._constrained == 'y'): pt.y = self._bview.y
        elif (self._constrained == 'x'): pt.x = self._bview.x
        self.refPoint = self._view.Top2L(pt)
        
        # We create the overlay object for selection
        self._selectObject = sp.Shape(self._view,shape=sp.SHAPE_RECT,anchor = sp.Anchor(self.refPoint),margin = sp.Margin(0,0),flags = sp.FLAG_OBJECT_OVERLAY_BUFFER)
        if (self.brush): self._selectObject.brush = self.brush
        if (self.pen): self._selectObject.pen = self.pen
        self._selectObject.ActivateOverlay()
    
    
    # The MouseUp callback
    def _CBUp(self, event):
        self._selectObject.DeactivateOverlay()
        del self._selectObject
        del self._bview
        if (self.callback): self.callback(self._view,self.rectSelection)
        del self._view
        self._timer.Stop()
    
    
    # The Timer callback
    def _CBTime(self,event):
        if (not event.IsTimerEvent(self._timer)): return
        self._CBMotion(event)
    
    # The MouseDrag callback
    def _CBMotion(self,event):
                
        # Get the mouse constrained point
        pt = event.mouse;
        if (self._constrained == 'y'): pt.y = self._bview.y+self._bview.height-1
        elif (self._constrained == 'x'): pt.x = self._bview.x+self._bview.width-1;
        
        # Case of an event generated by a motion of the mouse outside of the view
        # --> start the timer
        if (event.eventType.type != sp.OnTime and not self._bview.Contains(pt)):
            if (not self._timer.IsRunning()): self._timer.Start(event.object,10)
            return
        
        # Case the event is generated by the timer but the mouse is inside the view
        # --> stop the timer
        if (event.eventType.type == sp.OnTime and self._bview.Contains(pt)):
            self._timer.Stop()
            return
            
        # Case the event is generated by the timer
        if (event.eventType.type == sp.OnTime):
            p = self._view.Top2L(pt)
            r = self._view.boundRect
            r.Union(p)
            self._view.boundRect = r
            sp.Update()
    
        # In any case
        m = event.object.Top2L(pt)
        self._selectObject.pos1 = m
        self._UpdateSelection()
        
    def _UpdateSelection(self):
        self.rectSelection = self._selectObject.lRect()
        #        self.rectSelection.Standardize()
        if (self._constrained == 'y'):
            bound = self._view.boundRect
            self.rectSelection.y = bound.y
            self.rectSelection.height = bound.height
        elif (self._constrained == 'x'):
            bound = self._view.boundRect
            self.rectSelection.x = bound.x
            self.rectSelection.width = bound.width
            




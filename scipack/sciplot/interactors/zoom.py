__author__ = "Emmanuel Bacry"
__copyright__ = "Copyright (C) 2014 Emmanuel Bacry"
__version__ = "0.131"



import scipack.sciplot.sp as sp
from selector import IInteractorSelector
from select_region import ISelectRegion



#
# A 3-button zoom
#
class IZoom3View(sp.InteractorN):
    """An interactor for zooming/unzooming specifying only x-abscissa. Must be attached to a sp.View."""    
    def __init__(self,name = "Zoom3"):
        sp.InteractorN.__init__(self,name=name)
        self._iLeft = sp.Interactor1(cb=IZoom3View._CBLeft,eventType=sp.OnDownLeft,name="iZoom3:left").AddTo(self)
        self._iRight = sp.Interactor1(cb=IZoom3View._CBRight,eventType=sp.OnDownRight,name="iZoom3:right").AddTo(self)
        self._iUnzoom = sp.Interactor1(cb=IZoom3View._CBUnzoom,eventType=' ',name="iZoom3:midle").AddTo(self)
    
    
    def CheckObjectIsValid(self,object):
        if (object.IsView()): return True
        sys.stderr.write("Non valid spObject %s for interactor of class %s" %(object,self))
        return False
    
    # The left property to set the left event
    def _GetLeft(self): return self._iLeft.eventType
    def _SetLeft(self,eventType):
        """self.left = eventType
            
            Type of event for dropping left handside of the mouse
        """
        eventType = sp.EventType(eventType)
        self._iLeft.eventType = eventType
    left = sp.attribute(_GetLeft,_SetLeft)
    
    # The right property to set the right event
    def _GetRight(self): return self._iRight.eventType
    def _SetRight(self,eventType):
        """self.right = eventType
            
            Type of event for dropping right handside of the mouse
            """
        eventType = sp.EventType(eventType)
        self._iRight.eventType = eventType
    right = sp.attribute(_GetRight,_SetRight)
    
    # The unzoom property
    def _GetUnzoom(self): return self._iUnzoom.eventType
    def _SetUnzoom(self,eventType):
        """self.unzoom = eventType
            
            Type of event for unzooming
            """
        eventType = sp.EventType(eventType)
        self._iUnzoom.eventType = eventType
    unzoom = sp.attribute(_GetUnzoom,_SetUnzoom)


    # The Left callback
    def  _CBLeft(self,event):
        view = event.object
        [xmin,xmax,ymin,ymax] = view.bound
        signx = view.L2TopSign().x
        if (signx<0):
            xmax = view.Top2L(event.mouse).x;
        else:
            xmin = view.Top2L(event.mouse).x;
        view.bound = view.GetMinBound(xmin,xmax)
    
    # The Right callback
    def  _CBRight(self,event):
        view = event.object
        [xmin,xmax,ymin,ymax] = view.bound
        signx = view.L2TopSign().x
        if (signx>0):
            xmax = view.Top2L(event.mouse).x;
        else:
            xmin = view.Top2L(event.mouse).x;
        view.bound = view.GetMinBound(xmin,xmax)
    
    # The Unzoom callback
    def  _CBUnzoom(self,event):
        view = event.object
        view.bound = view.GetMinBound()



class IZoomSelect(ISelectRegion):
    """An 'classic' interactor for zooming/unzooming using rectangles. Must be attached to a sp.View."""
    def __init__(self, name = "ZoomSelect",constrained='y',button=sp.OnDownLeft,flagPropagate=False):
        ISelectRegion.__init__(self,name=name,constrained=constrained,button=button,flagPropagate=flagPropagate)
        self.callback = IZoomSelect._callback
        self._iUnzoom = sp.Interactor1(cb=IZoomSelect._CBUnzoom,eventType=' ',name="IZoomSelect:Unzoom").AddTo(self)

    def CheckObjectIsValid(self,object):
        if (object.IsView()): return True
        sys.stderr.write("Non valid spObject %s for interactor of class %s" %(object,self))
        return False
    
    # The unzoom property
    def _GetUnzoom(self): return self._iUnzoom.eventType
    def _SetUnzoom(self,eventType):
        """self.unzoom = eventType
            
            Type of event for unzooming
            """
        eventType = sp.EventType(eventType)
        self._iUnzoom.eventType = eventType
    unzoom = sp.attribute(_GetUnzoom,_SetUnzoom)

    @staticmethod
    def _callback(view,rect):
        view.boundRect = rect

    def _CBUnzoom(self,event):
        view = event.object
        view.bound = view.GetMinBound()

sp.attribute_hide(IZoomSelect,'callback')


# A zoom selector class
class IZoomSelector(IInteractorSelector):
    """An interactor that allows to select with a key between different zoom interactors. Must be attached to a sp.View."""
    def __init__(self,eventType = 'z',name="zoom_selector"):
        IInteractorSelector.__init__(self,eventType = eventType,name=name)
        self.interactor_zoom3 = IZoom3View(name="Zoom Left/Right").config().AddTo(self)
        self.interactor_selecty  = IZoomSelect(name="Zoom Select-Y").config(constrained='y').AddTo(self)
        self.interactor_selectxy  = IZoomSelect(name="Zoom Select-XY").config(constrained='').AddTo(self)

    def CheckObjectIsValid(self,object):
        if (object.IsView()): return True
        sys.stderr.write("Non valid spObject %s for interactor of class %s" %(object,self))
        return False
                            

#
# A scroll zoom
#
class IZoomWheel(sp.Interactor1):
    """An interactor for zooming around the mouse using the mouse wheel. Must be attached to a sp.View."""
    def __init__(self,name = "zoom_wheel",):
        sp.Interactor1.__init__(self,cb = IZoomWheel._CBWheel,name=name,eventType=(sp.OnMouseWheel,sp.ModShift))
        self._factor = 1.1
    
    def CheckObjectIsValid(self,object):
        if (object.IsView()): return True
        sys.stderr.write("Non valid spObject %s for interactor of class %s" %(object,self))
        return False
    
    # The factor property
    def _GetFactor(self): return self._factor
    def _SetFactor(self,eventType):
        """self.factor = float
            
            The factor used for zooming (good value is 1.1)
            """
        self._factor = factor
    factor = sp.attribute(_GetFactor,_SetFactor)

    # The Left callback
    def  _CBWheel(self,event):
        view = event.object
        wheel = event.wheel
        mouse = sp.RealPoint(event.mouse)
        view.ZoomX(mouse,wheel)
        [xmin,xmax,ymin,ymax] = view.bound
        view.SetMinBound(xmin,xmax)


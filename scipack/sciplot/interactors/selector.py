__author__ = "Emmanuel Bacry"
__copyright__ = "Copyright (C) 2014 Emmanuel Bacry"
__version__ = "0.131"


import wx
import scipack.sciplot.sp as sp
from temp_object import IDisplayTempObject


#
# An interactor for selecting between different interactors using temp object displayed
#
class IInteractorSelector(IDisplayTempObject):
    """An interactor which allows to select between different interactors
        using a key to switch.
        A text is displayed to indicate which interactor is active."""
    def __init__(self, name = "InteractorSelector",eventType = 'z'):

        obj = sp.Text(anchor=sp.Anchor(),hmode=sp.JUSTIFY_HMIDDLE,vmode=sp.JUSTIFY_VMIDDLE,flags=sp.FLAG_OBJECT_OVERLAY_BUFFER).config(fontSize=12,fontWeight=wx.FONTWEIGHT_BOLD,frameMargin=(5,4),brush=wx.Colour(130,130,130))

        IDisplayTempObject.__init__(self,object = obj,eventType=eventType,callback=self._Change,name=name)
        
        self._index = -1
        self._interactorList = []
    
    def Add(self,inter):
        """
        Adds the interactor inter to the selector.
        """
        IDisplayTempObject.Add(self,inter)
        if (not hasattr(self, '_interactorList')): return
        self._interactorList += [inter]
        if (len(self._interactorList) == 1):
            self._index = 0
            inter.On()
        else: inter.Off()

    def _Change(self):
        if (self._index == -1): return
        self._interactorList[self._index].Off()
        self._index += 1
        self._index = self._index % len(self._interactorList)
        self._interactorList[self._index].On()
        self._object.text = self._interactorList[self._index].name


sp.attribute_hide(IInteractorSelector,'callback')
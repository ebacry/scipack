import re
import wx
from os.path import expanduser

class Config(object):
    """The class that allows to deal with reading and executing the .spconfig file. You should not create any instance of this class. The only way you should use this class is calling the static method loadfile() in order to reload the ``.spconfig`` file if you changed it.
        This file should be placed in your home directory.
        If you don't know what you home directory is, just type (under python) :
            
            >>> from os.path import expanduser
            >>> expanduser("~")
        """
    
    _all_objects = {}
    _all_interactors = {}
    _isinit = False
    _all_code = []
    
    @staticmethod
    def loadfile():
        """reload the .spconfilg file"""
        Config._all_objects = {}

        s = '([^:.]*)(:*)'
        mypattern = '([^:.]+)(:*)'+8*s+"\.(?P<att>[^=]*)(?P<equal>=)? *(?P<val>.*)"
        Config._regexp = re.compile(mypattern)

        home = expanduser("~")
        filename = home+'/.spconfig'
        try :
            f = open(filename,'r')
        except :
            print "*** Warning : No .spconfig file found"
            Config._isinit = True
            return
                
        Config._all_objects = {}
        Config._all_interactors = {}
        Config._all_code = []

        lines  = f.readlines()
        f.close()
        for line in lines:
            line = line.rstrip()
            line = line.lstrip()
            m = re.match("[^#]*",line)
            line = m.group(0)
            if (line == ''): continue
            Config(line)

        Config._isinit = True

        Config._local_env  = {}
        for code in Config._all_code:
            exec(code,None,Config._local_env)
    
    
    def __init__(self,line):
                
        # Is the line a python code line
        if (line[0] != '@' and line[0] != '&'):
            Config._all_code += [line]
            return
        
        if (line[0] == '@'): flagObject = True
        else: flagObject = False
        line = line[1:]
                
        # Otherwise it is either an object or an interactor line
        m=Config._regexp.match(line)
        
        if (not m):
            print "Bad Line %s" % line
            raise(TypeError)
        
        l = []
        for g in m.groups():
            if (g == ''): break
            if (g == ':'):
                l[-1]['::'] = False
                continue
            if (g == '::'):
                l[-1]['::'] = True
                continue
            l+=[{'klass' : g}]
        
        self.list = l
        self.att = m.group('att').rstrip()
        if (m.group('val') != ''): self.val = eval(m.group('val'))
        else: self.val = None
        if (m.group("equal") == '='): self.method = False
        else: self.method = True
                
        self.line = line
                
        for l in self.list:
            m = re.match('([^\(]*)(\([^\)]*\))?',l['klass'])
            if (not m):
                print "Bad expression %s" % l['klass']
                raise(TypeError)
            l['klass'] = m.group(1)
            if (l['klass'] == ''): l['klass'] = None
            l['name'] = m.group(2)
            if (l['name']): l['name'] = l['name'][1:-1]

        self.list.reverse()
        klass = self.list[0]['klass']
        if (not flagObject):
            self.list[0]['klass'] = klass
            if (not Config._all_interactors.has_key(klass)): Config._all_interactors[klass] = []
            Config._all_interactors[klass] += [self]
        else:
            if (not Config._all_objects.has_key(klass)): Config._all_objects[klass] = []
            Config._all_objects[klass] += [self]

  
  
    # Tries to match the object with a single pattern
    def _is_object_matched1(self,spobject,pattern):
        
        object_klass = spobject.__class__
        object_name = spobject.name
        npatt = pattern['name']
        kpatt = pattern['klass']
        
        if (npatt and npatt != object_name): return False
        else:
            if (not kpatt): return True

        success = False
        
        while(not success):
            if (kpatt == object_klass.__name__): success = True
            else :
                object_klass = object_klass.__base__
                if (object_klass is object): break
        
        return success
            


    # Tries to match the object starting with self.list[i]
    def _is_object_matched_list(self,spobject,i):
        # First we try as is (independantly of an eventual ::)
        success = True
        res = self._is_object_matched1(spobject,self.list[i])
        if (not res): success = False
        else:
            if (len(self.list)==i+1): return True
            if (not spobject.container or spobject.IsTopContainer()): return False
            success = self._is_object_matched_list(spobject.container,i+1)


        if (success): return True
        if (not self.list[i]['::']): return False

        if (not spobject.container or spobject.IsTopContainer()): return False

        return self._is_object_matched_list(spobject.container,i)



    # Is the object matched by a particular line ?
    def _is_object_matched(self,object):

        # We already know that the class is good.
        # We should check for the name if needed
        if (self.list[0]['name'] and self.list[0]['name'] != object.name): return False

        if (len(self.list) == 1): return True
        
        # Now we have to loop
        if (object.IsTopContainer() or not object.container): return False

        return self._is_object_matched_list(object.container,1)


    # Tries to match the object with all the lines 
    @staticmethod
    def config(spobject):

        if (not Config._isinit): Config.loadfile()

        # We get the class hierarchy
        hierarchy = []
        object_klass = spobject.__class__
        while(object_klass is not object):
            hierarchy += [object_klass.__name__]
            object_klass = object_klass.__base__
        hierarchy.reverse()
    
        # We loop on the hierarchy
        hierarchy += [None]
        if (spobject._IsInteractor()):
            all  = Config._all_interactors
        else:
            all  = Config._all_objects
        for klass_name in hierarchy:
            #            print "  --> config %s" % klass_name
            if (all.has_key(klass_name)):
                for cline in all[klass_name]:
                    if (cline._is_object_matched(spobject)):
                        if (cline.method):
                            #                            print "spobject."+cline.att
                            #print spobject
                            Config._local_env['__spobject'] = spobject
                            eval("__spobject."+cline.att,None,Config._local_env)
                        #del Config._local_env['__spobject']
                        else:
                            #                            print "    --> config attribute %s" % cline.att
                            d = {cline.att:cline.val}
                            spobject(update=False,**d)











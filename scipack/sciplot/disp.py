__author__ = "Emmanuel Bacry"
__copyright__ = "Copyright (C) 2014 Emmanuel Bacry"
__version__ = "0.131"

import sys
import wx
import scipack.sciplot.sp as sp
import scipack.lab.sig as sig
from scipack.sciplot.objects.spsig import SignalObject

from scipack.sciplot.interactors.zoom_synchro import IZoomSynchro



#
#
# The class of SignalObject to be used by the disp command
#
#

class SignalObjectDisp(SignalObject):
    """This is a derived class of the class :class:`~scipack.sciplot.objects.spsig.SignalObject` that is exclusively used in the ``disp`` command
        in order to add more functionalities.
        For now it deals with what the cursor is printing.
        If the corresponding signal has a field 
            - ``label`` (with as many entries as the signal itself), it prints it.
            - ``cursor``, it must correspond to a callable of the form
                ``f(obj,sig,index,mouse) --> (string,x,y)``
            where
                - ``obj``    : is the ``sp.SignalObjectDisp``
                - ``sig``    : the corresponding signal
                - ``mouse``  : the mouse coordinate in the signal coordinate
                - ``index``  : the index of the signal which is the closest to the mouse
            It must return a triplet
                - ``string`` : the string to be printed
                - ``x`` and ``y`` : the (signal) coordinate of the cursor
            A basic example is
                >>> def f(obj,sig,index,mouse) 
                ...     return ("x = %s [index = %d] y = %g" % (sig.label[index],sig[index]),sig['X',index],sig[index])
        """

    # Cursor which follows the signal and print labels
    @staticmethod
    def __cursor(obj,sig,index,mouse):
        return ("%s x = %s [index = %d] y = %g" % (sig.name, sig.label[index],index,sig['Y',index]),sig['X',index],sig['Y',index])
    
    # The cursor callback method sent to the sp.Object
    @staticmethod
    def __cursor_cb(obj,flag_regular,mouse):
        sig = obj.signal
        index = sig.X2Index(mouse.x)
        if (flag_regular): return obj._cursor(obj,sig,index,mouse)

    # The constructor
    def __init__(self,container,signal,flags=0,name=None):

        SignalObject.__init__(self,container,signal,flags,name)
        
        self.signal = signal # Remember the signal
        self._cursor = None  # The callback for the full mode cursor
        if (not hasattr(signal,"cursor") and not hasattr(signal,"label")): return

        self.SetCursorCallback(self.__cursor_cb)
        
        if (hasattr(signal,"label")):
            self._cursor = SignalObjectDisp.__cursor
        else: self._cursor = signal.cursor

    # The signal field must be deleted
    def __del__(self):
        del self.signal


SignalObjectClass = SignalObjectDisp




#sp.SetDebug(sp.DEBUG_UPDATE_PAINT + sp.DEBUG_GEOMETRY_UPDATE)

config = dict(
              fontsize = 12,
              #  fontfamily => Wx::FONTFAMILY_TELETYPE,
              fontcolor = "black",
              size  = (600,300),
              pos  = (50,800),
              )


def _ppcm(a,b):
    p=a*b
    while(a!=b):
        if (a<b): b-=a
        else: a-=b
    return p/a


class DispTopContainer(sp.TopContainer):
    """This is the TopContainer class that the disp function adresses. It is included in a DispWindow wxWidget window (it fills up the whole area of the window)."""
    @staticmethod
    def GetFromName(name):
        """Returns a DispTopContainer from its name"""
        for tc in sp.oget('.*'):
            if (tc.name==name): return tc
        return None


    @staticmethod
    def GetCur():
        """Returns the current DispTopContainer"""
        return sp.TopContainer.GetCurTopContainer()


    def __init__(self,name=None):
        sp.TopContainer.__init__(self,canvas=None,name=name)
        self.synchro_interactor = IZoomSynchro()

    def SetBound(self,xbound=None,ybound=None,flag_unzoom=False):
        
        views = self.oget(klass=sp.View)
        
        #
        # Dealing with bounds
        #
        if (xbound or ybound or flag_unzoom):
            for v in views:
                if (xbound and ybound): v.bound = [xbound[0],xbound[1],ybound[0],ybound[1]]
                elif (xbound): v.bound = v.GetMinBound(xbound[0],xbound[1])
                elif (ybound):
                    b = v.GetMinBound()
                    v.bound = [b[0],b[1],ybound[0],ybound[1]]
                else:
                    v.bound=v.GetMinBound()
    
    def SetSynchro(self,synchro=None):
        
        #
        # Dealing with synchro
        #
        if (synchro is not None):
            
            self.synchro_interactor.Clear()
            
            if (synchro ==''): return
            if (synchro !='x' and synchro !='xy'): return
            
            
            views = self.oget(klass=sp.View)
            
            for v in views:
                self.synchro_interactor.AddView(v)
            self.synchro_interactor.mode = synchro
    

    #
    # Set the list of signals
    #
    def SetSignals(self,signals):
        
        for o in self.objects:
            self.RemoveObject(o)
        
        arglist = signals
        
        ##### Compute N and M
        M = 1
        N = len(arglist)
        for arg in arglist:
            if (isinstance(arg,list) and len(arg)!=0):
                M = _ppcm(len(arg),M)

        self.grid = [N,M]

        #####  Loop on signals
        
        x = 0
        y = 0
        fvn=1
        sign=1

        for arg in arglist:
            x = 0
            # Case argument is a list --> we are dealing with a row
            if (isinstance(arg,list)):
                # Width of each element on this row
                if (len(arg) ==0):
                    width = M
                    fv = sp.FramedView(self,sp.AnchorGrid(x,y),sp.MarginGrid(width,1),name='fv%s' % fvn).config()
                    fv.axisObject.Hide()
                    fv.viewObject.Hide()
                    fvn+=1
                    x+=width
                    continue
                else: width = M/len(arg)
                
                # Loop on each element of the row
                for arg1 in arg:
                    fv = sp.FramedView(self,sp.AnchorGrid(x,y),sp.MarginGrid(width,1),name='fv%s' % fvn).config()
                    v = fv.viewObject
                    fvn+=1
                    
                    # If element is a list --> superpose graph
                    if (isinstance(arg1,list)):
                        
                        if (len(arg1)==0):
                            fv.axisObject.Hide()
                            fv.viewObject.Hide()
                            x+=width
                            continue
                        
                        # Loop on superposed graph
                        for arg2 in arg1:
                            att = dict()
                            if (isinstance(arg2,dict)):
                                att = arg2
                                arg2 = arg2['_signal']
                                del att['_signal']
                            if (not isinstance(arg2,sig.Signal)): arg2 = sig.Signal(arg2)
                            o=SignalObjectClass(v,arg2,name='%d'%sign).config(**att)
                            sign+=1
                            if (arg2.IsComplex()):
                                o=SignalObjectClass(v,arg2,name='%d'%sign).config(real=False,**att)
                                col = o('penColour')
                                col = wx.Colour(col.red,col.green,col.blue,col.alpha/4)
                                o(penColour = col)
                                sign+=1
                    else :
                        if (arg1 is None):
                            fv.axisObject.Hide()
                            fv.viewObject.Hide()
                            x+=width
                            continue
                        att = dict()
                        if (isinstance(arg1,dict)):
                            att = arg1
                            arg1 = arg1['_signal']
                            del att['_signal']
                        if (not isinstance(arg1,sig.Signal)): arg1 = sig.Signal(arg1)
                        o=SignalObjectClass(v,arg1,name='%d'%sign).config(**att)
                        sign+=1
                        if (arg1.IsComplex()):
                            o=SignalObjectClass(v,arg1,name='%d'%sign).config(real=False,**att)
                            col = o('penColour')
                            col = wx.Colour(col.red,col.green,col.blue,col.alpha/4)
                            o(penColour = col)
                            sign+=1
            
                    v.SetMinBound()
                    x+=width
            
            # We are not dealing with a row
            else :

                fv = sp.FramedView(self,sp.AnchorGrid(x,y),sp.MarginGrid(1,1),name='fv%s' %fvn).config()

                v = fv.viewObject
                fvn+=1
                if (arg is None):
                    fv.axisObject.Hide()
                    fv.viewObject.Hide()
                    continue
                
                att = dict()
                if (isinstance(arg,dict)):
                    att = arg
                    arg = arg['_signal']
                    del att['_signal']
                if (not isinstance(arg,sig.Signal)): arg = sig.Signal(arg)
                o=SignalObjectDisp(v,arg,name='%d'%sign).config(**att)
                sign+=1
                if (arg.IsComplex()):
                    o=SignalObjectClass(v,arg,name='%d'%sign).config(real=False,**att)
                    col = o('penColour')
                    col = wx.Colour(col.red,col.green,col.blue,col.alpha/4)
                    o(penColour = col)
                    sign+=1
            
                v.SetMinBound()
            
            x = 0
            y +=1




#
# A DispWindow Window that encapsulates a single DispCanvas
#
from scipack.sciplot.sp import Config
class DispWindow(wx.Frame):
    """
    This is the wxWdigets window on which the disp command operates. It contains a single wxWidget which is 
    an instance of sp.wxTopContainer which fills up the whole window. 
    This sp.wxTopContainer is associated to a DispTopContainer which is entirely managed by SciPlot.
    """

    def __init__(self, name="SP",title=None):

        self.pos = config['pos']
        self.size = config['size']
        Config.config(self)
        
        if (title is None): title=name
        wx.Frame.__init__(self, None, -1, title,pos=self.pos, size=self.size)
        self._wxtc = sp.wxTopContainer(self,wx.Point(0,0),wx.Size(1,1),tc=DispTopContainer(name=name))

    # The following 3 routines are here for the Config system to work with this class
    def __call__(self,update,**kwargs):
        if (kwargs.has_key('pos')): self.pos = kwargs['pos']
        if (kwargs.has_key('size')): self.size = kwargs['size']
    def _IsInteractor(self):
        return False
    def AddInteractor(self,*args):
        pass


def disp(window=None,*signals,**kwargs):
    """
    disp(window=None,*signals,x=[xmin,xmax] or None,y=[ymin,ymax] or None,synchro='x' or 'xy' or '',pos=(x,y),size=(w,h),unzoom=False,update=True,att=None)
    
    The parameters are : 
        
            - ``window``  : a string which corresponds to the window's name, If the window already exists it is used,
                if not it is created. The window is a wxWidgets window which is an instance of the class DispWindow.
                It points to a ``sp.wxTopContainer`` (a wxWidget) which itself corresponds to a spTopContainer
            - ``pos`` and ``size`` : optional arguments that allow to specify the position and the size of 
                the window the display takes place in.
            - ``signals`` :
                * In its simplest form this should be a list of instances of the class scipack.lab.sig.Signal. 
                In that case, the signals are displayed one on top of the other.
                * If an element of this list is itself a list of Signal's, then this list is displayed horizontally.
                * If an element of this last list is again a list of Signal's, then this list is displayed on the same plot
            For each signal sig displayed a scipack.sciplot.objects.spsig.SignalObject is built for displaying it. If the signal
            is complex valued, two are created, the second one with a more transparent color. Thus both real and imaginary part
            will be displayed.
            You can change the attributes of this object within the disp command by using the syntax
                sig(att1=val1,...,attn=valn)
            When a Signal has been displayed, if a reference to this signal still exists in your current environment 
            (let's say it corresponds to the variable sig), then you can address the corresponding SignalObject using
            the method sig.obj(*args,**kwargs). This method gets the corresponding SignalObject and passes its arguments to
            its __call__ method. Thus you can set its attributes using the syntax
                sig.obj(att1=val1,...,attn=valn)
            - ``unzoom``: if ``True`` then unzoom is performed
            - ``x`` : optionnal argument that enables to fix the x-bounds. If None then automatic x-bounds are used.
            - ``y`` : optionnal argument that enables to fix the y-bounds. If None then automatic y-bounds are used.
            - ``synchro`` : optional argument that controls bounds synchronization between the plots.
                * ``synchro='x'`` : only the x-bounds are synchronized (default value)
                * ``synchro='xy'`` : both x and y-bounds are synchronized
                * ``synchro=''`` : no synchronization
            For more sophisticated synchronization you should use the two functions ``synchro`` and ``desynchro`` in the
            module  :mod:`scipack.sciplot.interactors.zoom_syncro`
            - ``update`` : if False then all the objects and attributes are set but no drawing happens.
                In order to perform refresh you need to call ``sp.Update()``
            - ``att = [dict1,...,dictN]`` : a list of dictionnaries that are sent to the function :func:`~scipack.sciplot.sp.att`
                using ``sp.att(*dict,update=False)``. This is used for setting attributes
        

    A LOT OF INTERACTIONS are available with these graphs. Please refer to the tutorial documentation on ``disp``
        
    **Examples**

    Manipulating fields of displayed signals
        
    >>> from scipack.lab.sig import *
    >>> from scipack.sciplot.disp import disp
    >>> w = rnorm(2000)
    >>> x = rnorm(1500)
    >>> y = rnorm(1700)
    >>> z = rnorm(1800)
    >>> disp("myWindow",[w,x(pen='blue',symbol='o')],[[y(pen='green'),z]])
    >>> y.obj(penWith=2)

    Displaying a signal setting the background of the view to red in a single command
    
    >>> disp (rnorm(100),att=[dict(pattern='::view',brush='red')])
        
    Doing the same thing in several lines

    >>> import scipack.sciplot.sp as sp
    >>> disp (rnorm(100),update = False)
    >>> sp.att('::view',brush='red')
        
    Displaying a comment
        
    >>> sp.CommentAdd("::view",(30,0),"This is my comment")
            
    """

    flag_unzoom = False
    

    if (window is not None and not isinstance(window,basestring)):
        signals = (window,)+signals
        window = None
        flag_unzoom = True
    
    w = None
    if (window is None):
        w = DispTopContainer.GetCur()
        if (w is None): window = "SP"
    if (w is None):
        w = DispTopContainer.GetFromName(window)
    if (w is None):
        w = DispWindow(window)
        w1 = w
        w.Show(True)
        w=DispTopContainer.GetFromName(window)

    if (kwargs.has_key('pos')):
        wx.FindWindowById(w.canvas.wxWindowId).TopLevelParent.Position = kwargs['pos']
    if (kwargs.has_key('size')):
        wx.FindWindowById(w.canvas.wxWindowId).TopLevelParent.Size = kwargs['size']

    if (kwargs.has_key('unzoom')): flag_unzoom = True
    if (kwargs.has_key('x')): xbound = kwargs['x']
    else: xbound = None
    if (kwargs.has_key('y')): ybound = kwargs['y']
    else: ybound = None

    if (kwargs.has_key('synchro')):
        synchro = kwargs['synchro']
        flag_unzoom = True
    else: synchro = None

    if (len(signals)!= 0):
        w.SetSignals(signals)
        if (synchro is None): w.SetSynchro('x')
        else: w.SetSynchro(synchro)
        v = w.oget1("::view")
        if (v): v.SetMinBound()

    if (kwargs.has_key('att')):
        for d in kwargs['att']:
            sp.att(**d)

    w.SetSynchro(synchro)
    w.SetBound(xbound,ybound,flag_unzoom)

    if (kwargs.has_key('update')):
        update = kwargs['update']
    else:
        update = True
    if (update): sp.Update()

#    return w1





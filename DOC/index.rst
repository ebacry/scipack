.. scipack documentation master file, created by
   sphinx-quickstart on Sun Mar 24 23:05:44 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to scipack's documentation!
===================================


.. toctree::
   :maxdepth: 3

   introduction
   installation
   run
   tutorial_scipack
   scipack.sciplot
   scipack.lab


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


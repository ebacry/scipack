Various Objects
=============

Comment Object
--------------

.. autoclass:: scipack.sciplot.sp.Comment
    :members: anchor,dist,shapeObject,textObject
    :undoc-members:
    :show-inheritance:
    :exclude-members: Draw


.. autofunction:: scipack.sciplot.sp.CommentAdd


CursorCrossHair Object
---------------------

.. autoclass:: scipack.sciplot.sp.CursorCrossHair
    :members:
    :undoc-members:
    :show-inheritance:
    :exclude-members: Draw

Text Object
----------

.. autoclass:: scipack.sciplot.sp.Text
    :members:
    :undoc-members:
    :show-inheritance:
    :exclude-members: Draw



TextBox Object
-------------

.. autoclass:: scipack.sciplot.sp.TextBox
    :members:
    :undoc-members:
    :show-inheritance:
    :exclude-members: Draw


Shape Object
-------------

.. autoclass:: scipack.sciplot.sp.Shape
    :members:
    :undoc-members:
    :show-inheritance:
    :exclude-members: Draw

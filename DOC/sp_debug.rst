Debugging functions
==================

.. autofunction:: scipack.sciplot.sp.SetDebug

.. autofunction:: scipack.sciplot.sp.InfoAllocClass

.. autofunction:: scipack.sciplot.sp.InfoAllocObjects

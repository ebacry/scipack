Link with wxWidget/wxPython : the class wxTopContainer
======================================================

.. autoclass:: scipack.sciplot.sp.wxTopContainer
    :members:
    :undoc-members:
    :show-inheritance:

The sp Module
=============

This module corresponds to the core of the :mod:`scipack.sciplot` package.


.. toctree::
    :maxdepth: 2

    sp_points_others
    sp_anchors_margins
    sp_object
    sp_various_objects
    sp_basic_containers
    sp_interactors
    sp_events
    sp_wxwidget
    sp_att
    sp_names
    sp_others
    sp_debug






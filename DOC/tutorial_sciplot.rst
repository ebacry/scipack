Sciplot Tutorial
===============

Introduction to sciplot : link with wxWidget
--------------------------------------------

Sciplot is a library for displaying and interacting with graphic objects. It is based on the wxWidgets library and is able to work both with C++ wxWidgets implementation and wxPython implementation.
The wxWidgets library allows to create a window (a wxFrame) which contains a whole hierarchy of graphich objects (whose base-class is wxWindow). Thus a wxFrame may contain several wxWindow. Each of these wxWindow can contain
several wxWindows and so on recursively.  

The core of the sciplot package is in the :mod:`scipack.sciplot.sp` module.
It defines its own subclass of wxWindow, the :class:`~scipack.sciplot.sp.wxTopContainer` class, that it manages fully : both content AND interaction. 

In the same philosophy as in wxWidget, the wxTopContainer may contain several objects (instances of :class:`~scipack.sciplot.sp.Object`) that can, in turn, be actual instances of :class:`~scipack.sciplot.sp.Container`
(which is a subclass of :class:`~scipack.sciplot.sp.Object`) which may contain several objects and so on... 
Thus each Object (instance of :class:`~scipack.sciplot.sp.Object`)  belongs to a unique Container (instance of :class:`~scipack.sciplot.sp.Container`). 
The topmost Container is an instance of  :class:`~scipack.sciplot.sp.TopContainer` which is associated to a unique wxTopContainer (instance of :class:`~scipack.sciplot.sp.wxTopContainer`). 


.. warning::
    Before starting this tutorial, it is important that you installed the default ``.spconfig`` file in your user directory

.. note ::
    In the next section you will learn the basics of SciPlot. If you want to jump directly to advanced features you should skip directly to the :doc:`tutorial_disp`.
    

Hello World !
-------------

As we said, sciplot is based on wxWidget. Moreover, the core of the sciplot package is in the :mod:`scipack.sciplot.sp` module. Thus, if you intend to use sciplot you should start your session with

>>> import wx
>>> from scipack.sciplot.sp import *

Then since sciplot essentially works within a window created by wxWidget, we should open one

>>> f=wx.Frame(None,size=(200,200))

We should attach a :class:`~scipack.sciplot.sp.wxTopContainer` to his window. Let us recall that a :class:`~scipack.sciplot.sp.wxTopContainer`  is a widget that is managed by sciplot through its associated
:class:`~scipack.sciplot.sp.TopContainer`.

>>> wxtc = wxTopContainer(f)
>>> tc = wxtc.tc
>>> f.Show()
True

So tc is the object SciPlot is going to deal with. It is an instance of the :class:`~scipack.sciplot.sp.TopContainer` class.

.. note::
    You might find that this is pretty complex for just opening a window. The reason is that here we teach you the basics of sciplot.  Later on, we will introduce you to
    high level functions that will manage opening windows for you. You should skip to the section of the tutorial that talks about the disp function if you want to deal with higher level functions.


We can add some simple sciplot objects in it. Let's add a simple text object in the middle of the window

>>> size = f.GetClientSize()
>>> hello = Text(container=tc,anchor=Anchor(size.x/2,size.y/2),hmode=JUSTIFY_HMIDDLE,vmode=JUSTIFY_VMIDDLE,name="mytext").config(text="Hello World!")
>>> Update()

You should see the text ``Hello World!`` appearing in the middle of the window. Let us explain the second line we just typed in.
The class :class:`~scipack.sciplot.sp.Text` corresponds to a graphic object which displays a text. The arguments are the following

.. function:: Text(container,anchor,hmode,vmode,name)

    :param container: The container object the object is in. In our case it is the topcontainer tc
    :param anchor: Every objects has an anchor point (who is an instance of :class:`~scipack.sciplot.sp.Anchor`). It is the reference point of the object. When the anchor is moved, the object is moved. In our case we chose the middle point of the window.
    :param hmode: Defines the horizontal justication of the text with respect to the anchor. In our case, it is horizontally centered on the anchor point.
    :param vmode: Defines the vertical justication of the text with respect to the anchor. In our case, it is vertically centered on the anchor point.
    :param name: The name of the object. Whenever you create an object you can specify its name. If you don't, the default name will be the name of the class where the first letter has been downcased.

Every object has some attributes (e.g., a pen attribute, a brush attribute). When an object is created some attributes can be configured automatically using the :class:`~scipack.sciplot.sp.Config` system of configuration.
It uses a file (``.spconfig`` in your user directory) to setup automatically some attributes of objects of certain type or with a certain name. For instance, if in your ``.spconfig`` file there is a line::

    @Text.fontColour = 'blue'

then all the Text objects, by default will draw their text in blue. When you create an object you need to call its :func:`~scipack.sciplot.sp.Object.config()` method in order to configure it.
Moreover, this method let's you set any attribute you want (after configuration has been performed). This is what we just did in order to set the text to be displayed.

Finally, the :func:`~scipack.sciplot.sp.Update` function is called to update the drawing of the window.

Dealing with SciPlot objects using their names
----------------------------------------------

The fullname of a SciPlot object indicates the hole hierarchy of the objects it is in:

>>> hello.fullname()
'SciPlot:mytext'

which indicates that the name of the object hello is``mytex`` and that it is in a container called ``SciPlo`` (which is by default the name of the TopContainer). If the object hello was in a container named ``containe`` which itself was in the topcontainer, then the fullname of hello would be ``SciPlot:container:mytext``

The string representation of a SciPlot object indicates both its class and its "fullname" :

>>> hello
spText(SciPlot:mytext)

Of course, you can change the name of any object : 

>>> hello.name = "newname"
>>> hello.fullname()
'SciPlot:newname'
>>> hello.name = "mytext"


In order to get an object using its name you can use the :func:`~scipack.sciplot.sp.oget` function.
The :func:`~scipack.sciplot.sp.oget` function returns all the objects whose fullname matches a string pattern. The string pattern is basically a regular regexp Python pattern. 
To understand how it works, let's add another text object in a container. First, we add a rectangular :class:`~scipack.sciplot.sp.Container` in the topcontainer (a rectangle at position x=10,y=10 and of width=height=50)

>>> ct = Container(tc,anchor=Anchor(10,10),margin=Margin(50,50),name="mycontainer").config()
>>> Update()

Since by default a container has a white background, you should see a white rectangle in the window. Now let's add a Text object in this container

>>> hello1 = Text(container=ct,anchor=Anchor(1,30),hmode=JUSTIFY_HLEFT,name="mytext1").config(text="A")
>>> Update()

To get all the topcontainers 

>>> oget('.*')
[spTopContainer(SciPlot)]

or

>>> oget(klass=sp.TopContainer)
[spTopContainer(SciPlot)]

To get the current topcontainer

>>> TopContainer.GetCurTopContainer()
spTopContainer(SciPlot)

To get all the objects directly in a topcontainer

>>> oget('.*:.*')
[spText(SciPlot:mytext), spContainer(SciPlot:container)]

To get all the objects directly in the ``SciPlot`` topcontainer

>>> oget('SciPlot:.*')
[spText(SciPlot:mytext), spContainer(SciPlot:container)]

To get any object whose topcontainer is ``SciPlot``, you can use the ``::`` string to indicate any hierarchy level

>>> oget('SciPlot::.*')
[spText(SciPlot:mytext), spContainer(SciPlot:container), spText(SciPlot:container:mytext1)]

Actually, to obtain all the objects

>>> oget()
[spTopContainer(SciPlot), spText(SciPlot:mytext), spContainer(SciPlot:container), spText(SciPlot:container:mytext1)]

Any pattern which starts with ``:`` refers implicitely to the 'current' topcontainer, i.e., the last topcontainer visited by the mouse or you drawn in.

>>> oget('::mycontainer')
[spContainer(SciPlot:mycontainer)]

or 

>>> oget('::mycontainer:.*')
[spText(SciPlot:container:mytext1)]

Finally, oget lets you specify the class of objects you want. Thus to get all the Text objects 

>>> oget(klass=Text)
[spText(SciPlot:mytext), spText(SciPlot:container:mytext1)]

.. note::
    If you are looking for a particular object, the :func:`~scipack.sciplot.sp.oget1` function works exactly the same way as :func:`~scipack.sciplot.sp.oget` except that it returns the first found element.


Playing around with object attributes
-------------------------------------

There are several ways to set/get the attributes of an object or to get help on them. 

The first one should be used if you have a variable pointing to the object (as in our last example with the variable hello).

Case you have a direct access to the object
+++++++++++++++++++++++++++++++++++++++++++++

To print the list of all available attributes

>>> hello()
Attributes of spText(SciPlot:mytext)
['angle', 'brush', 'brushColour', 'brushStyle', 'colour', 'font', 'fontColour', 'fontFaceName', 'fontFamily', 
'fontSize', 'fontStyle', 'fontWeight', 'frameMargin', 'hMode', 'pen', 'penColour', 'penStyle', 'penWidth', 
'pos', 'text', 'vMode']

To get this list just use the :class:`~scipack.sciplot.sp.object.alist` method :

>>> hello.alist()
['angle', 'brush', 'brushColour', 'brushStyle', 'colour', 'font', 'fontColour', 'fontFaceName', 'fontFamily', 
'fontSize', 'fontStyle', 'fontWeight', 'frameMargin', 'hMode', 'pen', 'penColour', 'penStyle', 'penWidth', 
'pos', 'text', 'vMode']

To get some help on a particular attribute, use the :class:`~scipack.sciplot.sp.object.ahelp` method :

>>> hello.ahelp('fontColour')
self.fontColour = wx.Colour
.
The colour of the font, either a string (e.g., 'blue')
or a wx.Colour (e.g., wx.BLACK or wx.Colour(r,g,b,alpha)

To get the value of an attribute you can use either

>>> hello('text')
'Hello World!'

or 

>>> hello.fontColour
wx.Colour(0, 0, 0, 255)

And finally to set an attribute, you can write

>>> hello.text='I changed the text'

In order to see the change, you must update the display

>>> Update()

You can use also the syntax 

>>> hello(text='I changed the text again')

which updates the display automatically unless you set the argument 'update' to False :

>>> hello(update=False,text='I changed the text once more')

This last syntax allows to change different attributes at once

>>> hello(text='This is blue',fontColour=wx.BLUE)


Using the name of the object
++++++++++++++++++++++++++++

There are functions corresponding to each methods above. These functions work with name patterns. 
To get the list of attributes of the container

>>> alist('::mycontainer')
['brush', 'brushColour', 'brushStyle', 'colour', 'font', 'fontColour', 'fontFaceName', 'fontFamily', 'fontSize', 'fontStyle', 
'fontWeight', 'margin', 'pen', 'penColour', 'penStyle', 'penWidth', 'pos', 'pos1', 'size']

Let us note that if several objects matches the pattern only the first one is used.
To get some help on an attribute

>>> ahelp('::mycontainer','fontSize')
spContainer(mycontainer)
self.fontSize = size
.
    size : an integer representing the size of the font

To get an attribute

>>> att('::mycontainer','brushColour')
wx.Colour(255, 255, 0, 255)

To change an attribute (and update the display)

>>> att('::mycontainer',brush='yellow')

Let us note that this last function applies to **all** matching objects and not just to the first one found.
This function actually can be used to replace the :func:`~scipack:sciplot.sp.alist` function, if you don't specify any attribute

>>> att('::mycontainer')
['brush', 'brushColour', 'brushStyle', 'colour', 'font', 'fontColour', 'fontFaceName', 'fontFamily', 'fontSize', 'fontStyle', 
'fontWeight', 'margin', 'pen', 'penColour', 'penStyle', 'penWidth', 'pos', 'pos1', 'size']

Using default popup menus
+++++++++++++++++++++++++

You haven't learned about interactors in sciplot. They are objects that let you define, very easily, ways to interact with objects.
The class :class:`~scipack.sciplot.interactors.popup.IPopupMenu` defines a generic interactor for managing popup menus. The class
:class:`~scipack.sciplot.interactors.popup.IHelpAttributesPopupMenu` derives from it and is an interactor that displays a popup menu that
lets you navigate through the hierarchy of objects, and get/set the attributes of any object.
In the default ``.spconfig`` file the line::

    &IHelpAttributesPopupMenu.eventType=EventType(OnKeyDown,wx.WXK_ESCAPE)
    
defines the escape key as the default event for the popup menu to popup. If you want to learn more about the configuration possibilities look at the class :class:`scipack.sciplot.sp.Config`.

.. note::
    Interactors have attributes too and eventType is an attribute of a lot of them. You'll learn more about it in the next section
    
Let's play with it. Go with the mouse pointer on top of grey topcontainer and press the escape key. You should see a popup menu with two entries : 

    - ``spTopContainer[SciPlot]`` : this entry gives you access to all the attributes of the topcontainer
    - ``Objects`` : this entry gives you access to all the objects contained in the topcontainer. For each object there is a similar popup menu.
    
If you select the entry ``Objects->SciPlot:mytext[Text]->text``, you can change the text displayed !

Let us point out that, if this popup interactor is active on all the objects it is because in the default ``.spconfig`` file, there are the lines ::
    
    iHelpAttributesPopupMenu = IHelpAttributesPopupMenu().config()
    @Object.AddInteractor(iHelpAttributesPopupMenu)


An introduction to interactors
-------------------------------

Interactors are objects that let you interact with the SciPlot graphic objects. They can be 

    - "simple" actions that just reacts to a particular event (see :class:`~scipack.sciplot.sp.EventType` for the different event types). This is the case for instance of the :class:`~scipack.sciplot.interactors.popup.IHelpAttributesPopupMenu` interactor we just described in the previous section. These interactors are instances of the class :class:`~scipack.sciplot.sp.Interactor1`
    
    - or "complex" actions, i.e., a set of "simple" interactors that share some information. This is the case for instance for a drag and drop kind of interaction. The drag and drop consists of 3 simple interactors (click on an object, move the mouse and then unclick) that work together. This is the case of the :class:`~scipack.sciplot.interactors.move.IMove` interactor that we shall see soon. These interactors are instances of the class :class:`~scipack.sciplot.sp.InteractorN`

The :class:`~scipack.sciplot.interactors.move.IMove` interactor allows to move objects which are in a container. Let's start again from scratch :

>>> import wx
>>> from scipack.sciplot.sp import *
>>> f=wx.Frame(None,size=(400,400))
>>> tc=wxTopContainer(f).tc
>>> f.Show()
True
>>> hello = Text(container=tc,anchor=Anchor(200,200),hmode=JUSTIFY_HMIDDLE,vmode=JUSTIFY_VMIDDLE,name="mytext").config(text="Hello World!")
>>> Update()

Now let's create an instance of the :class:`~scipack.sciplot.interactors.move.IMove` interactor and add it to the topcontainer

>>> from  scipack.sciplot.interactors import IMove
>>> i = IMove()
>>> tc.AddInteractor(i)

Just try now to drag and drop the text using the left button !
Interactors have attributes. They use the same syntax as graphic objects.

>>> i()
['button', 'scrollingSpeed']
>>> i.button 
spEventType(spOnDownLeft,spModNone)
>>> i.ahelp('button')
self.button = spOnDownLeft | spOnDownRight | spOnDownMiddle
.
Mouse button for moving the objects

And in the same way you can set/get the attributes of the available interactors using a popup menu interactor : the :class:`scipack.sciplot.interactors.popup.IHelpInteractorsPopupMenu`.
By default it is associated to the ``tab`` key. Just go in the window with the mouse and hit the ``tab`` key, each interactor has an entry in the popup menu. A ``x`` sign indicates that the interactor is 
not activated. A complex interactor entry displays an other popup menu with all its interactors and so on, recursively.

Again, let us point out that, if this popup interactor is active on all the objects it is because in the default ``.spconfig`` file, there are the lines ::

    iHelpInteractorsPopupMenu = IHelpInteractorsPopupMenu().config()
    @Object.AddInteractor(iHelpInteractorsPopupMenu)


The configuration file
-----------------------

The configuration file is named ``.spconfig`` and should be placed in the User directory. You should use the default file. At startup, it is read. Each line is read separately following the rules

    - A line starting by a ``&`` is followed by the name of an interactor class. It indicates a default attribute value for instances of this class. It uses the following syntax  ``&klass.attribute_name = value``
    - A line starting by a ``@`` has either the following syntax ``@klass1(name1):klass2(name2)...:klassN(nameN).attribute_name = value`` or ``@klass1(name1):klass2(name2)...:klassN(nameN).method(args)``

    where the klass's are sciplot object classes and the name's are object names. Both are optionals. 
    ``@klass1(name1):klass2(name2)...:klassN(nameN)`` refers to an object of class ``klassN`` and name ``nameN`` who is contained in an object of class ``klassN-1`` and name ``nameN-1`` which is contained in .... and so on.
    For all the matching objects, the first syntax allows to set a default value for an attribute and the second synatx allows to call a method.

For instance ::

    @FramedView:Text(title).fontSize = 15

means that every instance of class ``Text`` and whose name is ``title`` and which is included in a container which is an instance of ``FramedView`` should have a font size default value of 15.

Every line which does not start by a ``&`` or a ``@`` will be executed once (at startup) in a closed environment. That allows to define some variables (generally interactors) that are used for configuration. Thus for instance, if you want to have a container to move objects around in a ``FramedView``, you would have::

    from scipack.sciplot.interactors import IMove
    iMoveContainer = IMove().config()
    @FramedView.AddInteractor(iMoveContainer)



A shape object - Introduction to anchors and margins
-----------------------------------------------------

We have seen :class:`~scipack.sciplot.sp.Text` objects. This class is used for displaying text by specifying an anchor for the text itself. There are sevral other objects you should know about.
Let us lok at the :class:`~scipack.sciplot.sp.Shape` class. It let's you define an object which can be either a (finite or infinite) line, a rectangle, an ellipsis or a (single/double) arrow.
Let's draw a yellow circle with red background. Starting from scratch

>>> import wx
>>> from scipack.sciplot.sp import *
>>> f=wx.Frame(None,size=(400,400))
>>> tc=wxTopContainer(f).tc
>>> f.Show()
True
>>> shape = Shape(tc,shape=SHAPE_ELLIPSE,anchor=Anchor(200,200),margin=Margin(30,30),name="myshape").config(pen="yellow",brush="red")
>>> Update()

A circle is drawn, the top left point of the "frame rectangle" is (200,200) and the diameter is 30. The :class:`~scipack.sciplot.sp.Margin` object is to be understood as the margin around the anchor point.
They are two 30's in the margin definition because it is a circle, the first 30 corresponds to the margin at the right of the anchor and the second one to the bottom of the anchor (the y abscissa is increasing from top to bottom).
Actually, the margin lets you specify margins in all directions starting from right and going counter clock wise. Thus you could add a circle which has a diameter of 30 and which is centered at (300,300) by doing

>>> shape = Shape(tc,shape=SHAPE_ELLIPSE,anchor=Anchor(300,300),margin=Margin(15,15,15,15),name="myshape").config(pen="yellow",brush="red")
>>> Update()


sciplot let's you do that directly using the :class:`~scipack.sciplot.sp.MarginCentered` class

>>> shape = Shape(tc,shape=SHAPE_ELLIPSE,anchor=Anchor(300,300),margin=MarginCentered(15,15),name="myshape").config(pen="yellow",brush="red")

Now what if you want a circle to be centered in the middle of the topcontainer and stay at the center even when the window is resized (when the window is resized the by default in wxWidget the wxTopContainer is resized).
For that purpose we need to define an anchor which is a function. This is possible with the :class:`~scipack.sciplot.sp.AnchorFunc` class::

    def center(obj):
        width = obj.container.margin[0]
        height = obj.container.margin[1]
        return Anchor(width/2,height/2)

    Shape(tc,shape=SHAPE_ELLIPSE,anchor=AnchorFunc(center),margin=MarginCentered(15,15),name="myshape").config(pen="yellow",brush="red")

Try to resize the window, you'll see the circle stay at the center. 

.. note:: 
    a :class:`~scipack.sciplot.sp.MarginFunc` class lets you define the margin of an object using a function.

Very often when you plot some graphs, you want the display to be divided into some subplots all of them being resized when the window is resized. 
This could be done using class:`~scipack.sciplot.sp.MarginFunc` and :class:`~scipack.sciplot.sp.AnchorFunc`, however ther is a container devoted to this task :
the :class:`~scipack.sciplot.sp.Grid` container class (which of course derives from the class :class:`~scipack.sciplot.sp.Container`).
It lets you define a uniform table (of arbitrary size) (with optional margins) and you can specify coordinates of any objects within this container using ``Grid`` coordinate.
Let's give a simple example. We first erase the topcontainer : 

>>> tc.RemoveAllObjects()

Then

>>> tc.grid = [2,2]  # The nxm size of the grid
>>> Shape(tc,shape=SHAPE_ELLIPSE,anchor=AnchorGrid(0,0),margin=MarginGrid(1,1),name="myshape").config(pen="yellow",brush="red")
>>> Shape(tc,shape=SHAPE_ELLIPSE,anchor=AnchorGrid(1,0),margin=MarginGrid(1,2),name="myshape1").config(pen="yellow",brush="blue")
>>> Update()

Try to resize the window


Views and FramedViews
----------------------

First steps
+++++++++++

Till now we have just worked with containers whose coordinates are integers and correspond to pixels. The class:`~scipack.sciplot.sp.View` class is a container with float coordinates 
(by default the y abscissa axis is reversed : it increases from bottom to top). The view has a map (an instance of class:`~scipack.sciplot.sp.Map2d`) which says how to map a point in local coordinate to the screen.
For each axis, for now, it can be linear or log. You can ask a view to zoom (by specifying its x and y bounds) or to unzoom in order to fit all the  objects it contains.
Let's play with it


>>> tc.RemoveAllObjects()
>>> tc.grid = [1,1]
>>> view = View(tc,anchor=AnchorGrid(0,0),margin=MarginGrid(1,1)).config()
>>> shape = Shape(view,shape=SHAPE_ELLIPSE, anchor=Anchor(0.5,0.5),margin=MarginCentered(.1,.2)).config(brush="yellow")
>>> shape1 = Shape(view,shape=SHAPE_ELLIPSE, anchor=Anchor(1,0),margin=MarginCentered(.1,.1)).config(brush="blue")
>>> view.SetMinBound()

The ``SetMinBound()`` method performed the unzoom. You could force the unzoom within ``[xmin,xmax]`` (letting the y bounds to adapt) by

>>> view.SetMinBound(0,1)

If you want to set all the bounds manually :

>>> view.bound = [-1,1,-1,1]

bound is an attribute of the view and the format is ``[xmin,xmax,ymin,ymax]``. In these last examples the objects within the view have been specified using local coordinate.
This is always the case of the anchor (that has to be in its container coordinate) however you could specify the margin in pixels instead.
Thus adding a circle of radius of 50 pixels

>>> shape2 = Shape(view,shape=SHAPE_ELLIPSE, anchor=Anchor(0.8,0.25),margin=MarginPixel(50,50)).config(brush="red")

Try to change the bounds and you'll understand what's going on

>>> view.bound = [-10,10,-10,10]

the radius of ``shape2`` does not change since it has been specified using pixel size and not local size. You'll notice that if you resize the window the view will be resized but the bounds will change so that the
objects do not move.
If you want the bounds of a view not to change when the view is resized, you should add a flag when the view is created : 

>>> tc.RemoveAllObjects()
>>> view = View(tc,anchor=AnchorGrid(0,0),margin=MarginGrid(1,1),flags=FLAG_VIEW_KEEPBOUND_WHEN_RESIZED).config(
>>> shape = Shape(view,shape=SHAPE_ELLIPSE, anchor=Anchor(0.5,0.5),margin=MarginCentered(.1,.2)).config(brush="yellow")
>>> shape1 = Shape(view,shape=SHAPE_ELLIPSE, anchor=Anchor(1,0),margin=MarginCentered(.1,.1)).config(brush="blue")
>>> shape2 = Shape(view,shape=SHAPE_ELLIPSE, anchor=Anchor(0.8,0.25),margin=MarginPixel(50,50)).config(brush="red")

Try to resize the window now.

You want to add a nice frame to the view ? With axis, title, labels and so on ? This is what the class :class:`scipack.sciplot.sp.FramedView` is for. It derives from 
:class:`scipack.sciplot.sp.Grid` , has a size of 1 by 1 (but with large margins to leave spaces for the axis and other objects). It automatically creates the view in it, an associated axis object 
(instance of :class:`scipack.sciplot.sp.AxisView`), a title, an x-label and a y-label (instances of :class:`scipack.sciplot.sp.Text) and a text box at the bottom for displaying info :

>>> tc.RemoveObject(view)
>>> fv = FramedView(tc,anchor=AnchorGrid(0,0),margin=MarginGrid(1,1)).config()
>>> view = fv.viewObject
>>> shape = Shape(view,shape=SHAPE_ELLIPSE, anchor=Anchor(0.5,0.5),margin=MarginCentered(.1,.2)).config(brush="yellow")
>>> shape1 = Shape(view,shape=SHAPE_ELLIPSE, anchor=Anchor(1,0),margin=MarginCentered(.1,.1)).config(brush="blue")
>>> view.SetMinBound()
>>> Update()


Playing around with many default interactors
+++++++++++++++++++++++++++++++++++++++++++++

If you are using the default ``.spconfig`` file. You should be able to use a lot of interactors in FramedView's and in View's with FramedViews. Here is an exhaustive list (you should run a disp example or the last example just above) :

    - move the title and the labels using drag and drop with the left button (while holding the shift key pressed)
    - right/left click in the view to zoom (drop part which is on the right/left x-side of the mouse)
    - hit the space key in the view to unzoom
    - hit the 'z' key on the view to switch between 3 zoom modes :
        * selecting a y-constrained rectangle
        * selecting a no-constrained rectangle
        * using the right/left click as described abpve
    - if you are dispalying signals the 'c' key should change the cursor mode
    - scrolling using the mouse wheel
    - zooming around one point using the mouse wheel + shift key


Adding a comment
+++++++++++++++++

The sp.Comment object let's you add a "comment" box with an arrow within a container. Let's rerun the previous example

>>> fv = FramedView(tc,anchor=AnchorGrid(0,0),margin=MarginGrid(1,1)).config()
>>> view = fv.viewObject
>>> shape = Shape(view,shape=SHAPE_ELLIPSE, anchor=Anchor(0.5,0.5),margin=MarginCentered(.1,.2)).config(brush="yellow")
>>> shape1 = Shape(view,shape=SHAPE_ELLIPSE, anchor=Anchor(1,0),margin=MarginCentered(.1,.1)).config(brush="blue")
>>> view.SetMinBound()
>>> Update()

you can then type

>>> Comment(view,pt=(0.5,0.5),str="This is the center\n of the yellow circle").config()

Using the left mouse button along with the shift key lets you move the comment box .....

The Comment object is an example of a Group container. A Group is a special container that simply groups object. There is no margin associated to it (it is as if it is infinite).
The Comment object is made of two objects : a Text object and a Shape object (the arrow). The Group class allows to control the relative geometry between the different objects.

Printing or creating Postscript/SVG files
------------------------------------------

You can print or create a postscript or svg file from the content of a
:class:`~scipack.sciplot.sp.TopContainer` instance. For that purpose you can use one of the methods
:func:`~scipack.sciplot.sp.TopContainer.Print`, :func:`~scipack.sciplot.sp.TopContainer.ToPS` or :func:`~scipack.sciplot.sp.TopContainer.ToSVG`.

Let us point out that each object has a field ``whiteBrushPrinting`` which (when set) allows to force the background (i.e., the brush) to be white when printing operations are performed.

An example of a new Object class
---------------------------------

in construction

An example of a new Interactor class
-------------------------------------

in construction




















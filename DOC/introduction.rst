Introduction
============

.. note::
    The :doc:`tutorial_disp` gives you a good overview of scipack capabilities.

Scipack is a Python package that mainly puts together two functionalities :

High level wxWidget-based object oriented display
-------------------------------------------------

The :mod:`scipack.sciplot` package is a wxWidget-based advanced package for displaying graphic objects and interacting with them.
The displays are handled by a (C++ based) component (SciPlot).
It is fast AND high level. 
It is object oriented and new graphic objects can be defined either directly in C++ or in Python.
Since it uses wxWidget, it is wxPython compatible.
SciPlot creates a wxWidget that is entirely managed (drawings + interactions) by itself.
SciPlot has high level interactors, that can be defined either directly in C++ or in Python, that let's you handle
any events using very short codes.
Attributes of both interactors and objects can be changed very easily (either using the command line or using popup menu's).

The :mod:`scipack.sciplot.sp` module defines the core methods the :mod:`scipack.sciplot` package. It includes many objects such as
spViews, which allow to display objects using real coordinates (x,y axis, ....), comment boxes (a text box with an arrow pointing on some point), and many more !


The :mod:`scipack.sciplot.objects` package defines external graphic objects. For now there is basically just one module (the :mod:`scipack.sciplot.objects.spsig` module) which allows to display signals (cf below)

The :mod:`scipack.sciplot.interactors` package defines external interactors. It includes
zooming, scrolling of views, moving objects, changing attributes of any object (both aspects of the object
and ways of interacting with it) using popup menus, synchronization of views so that several views share the same axis, and many more !


A 1d signal processing compatible with numpy
--------------------------------------------

The aim of the :mod:`scipack.lab` package is to implement various signal processing algorithms. For now there is very little.
There is basically a single module :mod:`scipack.lab.sig`
which is an implementation of a signal class which allows to manipulate either uniformely or non uniformely 1d sampled signals. It is fully compatible with numpy.

In the near future, the :mod:`scipack.lab` package should include other modules that work with signals.



How to run
==========


For running the package using the plotting capabilities, you should do::

    ipython qtconsole  --pylab=wx
    
Then I recommend starting the session with

    >>> import scipack.sciplot as sp
    >>> from scipack.lab.sig import *
    >>> from scipack.sciplot.disp import disp

Actually, in my default ipython startup file, I have the following lines (importing common functions I am using)

    >>> import wx
    >>> import scipack.sciplot.sp as sp
    >>> from scipack.lab.sig import *
    >>> from scipack.sciplot.disp import disp
    >>> from scipack.sciplot.sp  import Update, ahelp, alist, att, oget, oget1
    >>> from scipack.sciplot.interactors  import synchro, desynchro


You should read the tutorial to learn where to go from here. If you are impatient just skip directly to the :doc:`tutorial_disp`.



For running the package without the plotting capabilities, just start the python interpreter (I recomment ipython) and then ::

    >>> from scipack.lab.sig import *
    


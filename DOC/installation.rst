
Download
=========================

Download scipack
++++++++++++++++

You can download the whole package from the link

`<https://bitbucket.org/ebacry/scipack>`_

On the right panel, in front the Size item there is a download button. Just click on it !


Installation
=========================


Several installation are available depending on what you want to install.

    - Installation of scipack.lab alone (just to be able to work with Signal's)
    - Installation of scipack entirely (scipack.lab + scipack.sciplot which includes all the plotting capabilities)

It is basically the same process except that you need much less libraries for just scipack.lab.

In any case you should first install python > 2.7 installed (I use 2.7.2) along with numpy,scipy.

For plotting capabilities you should also have ipython installed (I use ipython 0.13) to be downloaded `here <http://ipython.org/install.html>`_

I recommend strongly to use the anaconda distribution (that you can install locally on your account):

    https://store.continuum.io/cshop/anaconda/
    
It is free and it includes : python, numpy, scipy, ipython and matplotlib.


Installation on MacOS X 64 bits (>10.8)
---------------------------------------


Installation of the full scipack
+++++++++++++++++++++++++++++++++

Installation requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^


1- You should have wxPython/wxWidget installed (you need wxWidget > 2.9.2 so that corresponds to wxPython > 2.9.2).
You should download the sources in (link at the bottom of the page : wxPython-src) from `here <http://wxpython.org/download.php>`_.

For installation basically you need to go inside the wxPython-src/wxPython directory::

    cd wxPython-src-2.9.4/wxPython
    python build-wxpython.py --install --osx_cocoa
    sudo make install    
    
.. warning:: 
    Be careful to use here the python you intend to use with scipack.


2- You have to check that the command wx-config is available in the shell (just type wx-config and return)

3- You can test that ipython works with wx. Try::

    ipython qtconsole --pylab=wx

A terminal should open (and in the background a wx loop is running !)

You can configure ipython so that the qtconsole will always open with --pylab=wx ::

    emacs ~/.ipython/profile_default/ipython_qtconsole_config.py
    
Look for ``c.IPKernelApp.pylab`` and change the line into ::

    c.IPKernelApp.pylab = 'wx'

5- Install swig >2.0 from `here <http://www.swig.org>`_


Installation of the package itself
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1- Download the whole scipack github directory. Then ::

    cd scipack
    python setup.py build_ext --inplace

.. warning ::
    The package is installed in the scipack directory. So you should set it in your PYTHONPATH.



2- You should have a configuration file for sciplot. There is one in the package : the file  ``.spconfig``
Just copy it to your home directory


Installation of the scipack.lab only (no plotting)
++++++++++++++++++++++++++++++++++++++++++++++++++

1- Install swig >2.0 from `here <http://www.swig.org>`_

2- Download the whole scipack github directory. Then ::

    cd scipack
    python setup.py lab build_ext --inplace

.. warning ::
    The package is installed in the scipack directory. So you should set it in your PYTHONPATH.





Installation on Windows 7
--------------------------


Installation of the full scipack
+++++++++++++++++++++++++++++++++

Installation requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^

I recommend you install the pythonxy distribution which includes a lot of the things you need.

1- You should have wxPython/wxWidget installed (you need wxWidget > 2.9.2 so that corresponds to wxPython > 2.9.2).
The binaries are available

???2- You have to check that the command wx-config is available in the shell
(it should tell you that wxWidget 2.9.2 is installed)

3- You can test that ipython works with wx. Try::

    ipython qtconsole --pylab=wx

A terminal should open (and in the background a wx loop is running !)

You can configure ipython so that the qtconsole will always open with --pylab=wx. ::

    emacs ~/.ipython/profile_default/ipython_qtconsole_config.py
    
Look for ``c.IPKernelApp.pylab`` and change the line into ::

    c.IPKernelApp.pylab = 'wx'

5- Install swig 2.0 from `here <http://www.swig.org>`_


Installation of the package itself
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1- Download the whole scipack github directory. Then ::

    cd scipack
    python setup.py build_ext --inplace --compiler=mingw32

.. warning ::
    The package is installed in the scipack directory. So you should set it in your PYTHONPATH (which is a global environment variable).



2- You should have a configuration file for sciplot. There is one in the package : the file  ``.spconfig``
Just copy it to your home directory.
If you don't know what you home directory is, just type (under python) :
            
    >>> from os.path import expanduser
    >>> expanduser("~")



Installation of the scipack.lab only (no plotting)
++++++++++++++++++++++++++++++++++++++++++++++++++

1- Install swig 2.0 from `here <http://www.swig.org>`_

2- Download the whole scipack github directory. Then ::

    cd scipack
    python setup.py lab build_ext --inplace  --compiler=mingw32

.. warning ::
    The package is installed in the scipack directory. So you should set it in your PYTHONPATH (which is a global environment variable).





The main Object class
====================


.. autoclass:: scipack.sciplot.sp.Object
    :members:
    :undoc-members:
    :show-inheritance:

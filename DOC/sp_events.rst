Event related classes
=====================


.. autoclass:: scipack.sciplot.sp.EventType
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: scipack.sciplot.sp.Event
    :members:
    :undoc-members:
    :show-inheritance:
    
.. autoclass:: scipack.sciplot.sp.Timer
    :members:
    :undoc-members:
    :show-inheritance:


Points, Rects and Transforms
========================

These are various point and rectangle classes used by sciplot

Points and Rects
--------------

.. autoclass:: scipack.sciplot.sp.Point
    :members: x,y,Get
    :undoc-members:
    :show-inheritance:

.. autoclass::  scipack.sciplot.sp.RealPoint
    :members: x,y,Get,Round,Rotate
    :undoc-members:
    :show-inheritance:

.. autoclass::  scipack.sciplot.sp.Rect
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass::  scipack.sciplot.sp.RealRect
    :members:
    :undoc-members:
    :show-inheritance:


Transforms
---------

.. autoclass::  scipack.sciplot.sp.Map1d
    :members:
    :undoc-members:
    :show-inheritance:


.. autoclass::  scipack.sciplot.sp.Map2d
    :members:
    :undoc-members:
    :show-inheritance:


Various other classes
=====================

.. autoclass:: scipack.sciplot.sp.WeakRefObject
    :members:
    :undoc-members:
    :show-inheritance:


.. autoclass:: scipack.sciplot.sp.Config
    :members: loadfile
    :undoc-members:
    :show-inheritance:

The disp Module
===================


The DispTopContainer
---------------------

.. autoclass:: scipack.sciplot.disp.DispTopContainer
    :members: GetFromName,GetCur
    :undoc-members:
    :show-inheritance:


The DispWindow
-----------------------

.. autoclass:: scipack.sciplot.disp.DispWindow
    :members: 
    :undoc-members:
    :show-inheritance:


The disp function
-----------------

.. autofunction:: scipack.sciplot.disp.disp


The SignalObjectDisp class
--------------------------

.. autoclass:: scipack.sciplot.disp.SignalObjectDisp

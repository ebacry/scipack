The basic interactor classes
============================


.. autoclass:: scipack.sciplot.sp.Interactor
    :members:
    :undoc-members:
    :show-inheritance:


.. autoclass:: scipack.sciplot.sp.Interactor1
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: scipack.sciplot.sp.InteractorN
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: scipack.sciplot.sp.ICursorView
    :members:
    :undoc-members:
    :show-inheritance:
